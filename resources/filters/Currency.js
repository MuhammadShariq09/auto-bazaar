import moment from 'moment';

export default {
    "currency": value => {
        value = Number(value);
        if (value >= 0) {
            return "$" + value.toFixed(2).toLocaleString();
        }
        return null;
    },
    "date": (value) => {
        if(!value)
            return null;
        return moment(value).format('DD-MM-YYYY');
    },
    "datetime": (value) => {
        if(!value)
            return null;
        return moment(value).format('DD-MM-YYYY hh:i');
    }
};
