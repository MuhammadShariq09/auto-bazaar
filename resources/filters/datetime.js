const Filters = {
    date: (value) => {
        const date = Filters.dateTime(value);
        return date.substr(0, 10);
    },
    time: (value) => {
        const date = Filters.dateTime(value);
        return date.substr(11);
    },
    dateTime: (value) => {
        if (!value) return '';
        const date = new Date(value);
        return date.toLocaleString('en-US');
        // return date.toLocaleString('en-US', { hour: 'numeric', hour12: true })
    }
};
export default Filters;
