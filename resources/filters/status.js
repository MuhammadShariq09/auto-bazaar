const Filters = {
    status: (value) => {
        switch (value) {
            case 0:
                return "Pending";
            case 1:
                return "APPROVED";
            case 2:
                return "ACTIVE";
            case 3:
                return "REJECTED";
            case 4:
                return "PENDING_PAYMENT";
            case 5:
                return "SOLD";
            case 6:
                return "DISPUTE";
            case "DEFEATED":
                return "DEFEATED";
            case "WINNER":
                return "WINNER";
            default:
                return "Invalid Status"
        }
    }
};
export default Filters;
