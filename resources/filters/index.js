import status from './status';
import dateTime from './datetime'
import currency from './Currency'
import isFeatured from './IsFeatured'

export default {
    ...status,
    ...currency,
    ...dateTime,
    ...isFeatured
};
