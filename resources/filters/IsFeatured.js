const Filters = {
    IsFeatured: (value) => {
        if(!value)
            return false;
        let expireAt = new Date(value);
        return new Date() <= expireAt;
    }
};
export default Filters;
