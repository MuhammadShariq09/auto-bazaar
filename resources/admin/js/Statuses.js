export default {
    PENDING: 0,
    APPROVED: 1,
    ACTIVE: 2,
    REJECTED: 3,
    PENDING_PAYMENT: 4,
    SOLD: 5,
}
