import VueRouter from "vue-router";

const DashboardComponent = () => import(/* webpackChunkName: "dashboard" */ './components/DashboardComponent');
const ChangePassword = () => import(/* webpackChunkName: "change-password" */ './components/profile/ChangePassword');
const UsersIndex = () => import(/* webpackChunkName: "users-listing" */ './components/users/Index');
const AddUser = () => import(/* webpackChunkName: "users-listing" */'./components/users/Add');
const UsersShow = () => import(/* webpackChunkName: "users-show" */ './components/users/Show');
const DealersIndex = () => import(/* webpackChunkName: "dealers-listing" */ './components/dealers/Index');
const DealersShow = () => import(/* webpackChunkName: "dealers-show" */ './components/dealers/Show');
const NotFound = () => import(/* webpackChunkName: "notfound" */ './components/NotFound');
const PackagesIndex = () => import(/* webpackChunkName: "packages-index" */ './components/packages/Index');
const AdsIndex = () => import(/* webpackChunkName: "ads-listing" */ './components/ads/Index');
const AdsShow = () => import(/* webpackChunkName: "ads-listing" */ './components/ads/Show');
const AdsEdit = () => import(/* webpackChunkName: "ads-edit" */ './components/ads/Edit');
const FeedbackIndex = () => import(/* webpackChunkName: "feedback-listing" */ './components/feedback/Index');
const FeedbackShow = () => import(/* webpackChunkName: "feedback-show" */ './components/feedback/Show');
const PaymentsIndex = () => import(/* webpackChunkName: "payments-index" */ './components/payments/Index');
const BannersIndex = () => import(/* webpackChunkName: "banners-listing" */ './components/banners/Index');
const AddBanner = () => import(/* webpackChunkName: "banners-create" */ './components/banners/Add');
const BannerShow = () => import(/* webpackChunkName: "banners-create" */ './components/banners/Show');
const PageIndex = () => import(/* webpackChunkName: "pages-listing" */ './components/pages/Index');
const PageShow = () => import(/* webpackChunkName: "pages-show" */ './components/pages/Show');


const router = new VueRouter({
    mode: 'history',
    base: window.base_url_with_admin.replace((window.location.protocol + '//' + window.location.host), ''),
    routes: [
        {
            path: '/',
            redirect: '/dashboard'
        },
        {
            path: '/dashboard',
            name: 'home',
            component: DashboardComponent,
            meta: {
                title: "Dashboard",
                description: ""
            }
        },
        {
            path: '/feedback',
            name: 'feedback.index',
            component: FeedbackIndex,
            meta: {
                title: "Feedback",
                description: ""
            }
        },
        
        {
            path: '/feedback/:id',
            name: 'feedback.show',
            component: FeedbackShow,
            meta: {
                title: "Feedback Details",
                description: ""
            }
        },
        {
            path: '/banners',
            name: 'banners.index',
            component: BannersIndex,
            meta: {
                title: "Banners",
                description: ""
            }
        },
        {
            path: '/banners/create',
            name: 'banners.create',
            component: AddBanner,
            meta: {
                title: "Add Banner",
                description: ""
            }
        },
        {
                path: '/banners/:id/edit',
                name: 'banners.edit',
                component: BannerShow,
                meta: {
                  title: "Banner Update",
                  description: "",
                  breadcrumb: [{
                    name: "Dashboard",
                    link: "home",
                    isActive: false
                  }, {
                    name: "Banners",
                    link: "banners.index",
                    isActive: false
                  }, {
                    name: "Banner Update",
                    link: "banners.edit",
                    isActive: true
                  }]
                }
              }, {
                path: '/banners/:id',
                name: 'banners.show',
                component: BannerShow,
                meta: {
                  title: "Banner View",
                  description: "",
                  breadcrumb: [{
                    name: "Dashboard",
                    link: "home",
                    isActive: false
                  }, {
                    name: "Banners",
                    link: "banners.index",
                    isActive: false
                  }, {
                    name: "View",
                    link: "",
                    isActive: true
                  }]
                }
              }, 
              {
                path: '/pages',
                name: 'pages.index',
                component: PageIndex,
                meta: {
                    title: "pages",
                    description: ""
                }
            },
            
            {
                    path: '/pages/:id/edit',
                    name: 'pages.edit',
                    component: PageShow,
                    meta: {
                      title: "Page Update",
                      description: "",
                      breadcrumb: [{
                        name: "Dashboard",
                        link: "home",
                        isActive: false
                      }, {
                        name: "Page",
                        link: "pages.index",
                        isActive: false
                      }, {
                        name: "Page Update",
                        link: "pages.edit",
                        isActive: true
                      }]
                    }
                  }, {
                    path: '/pages/:id',
                    name: 'pages.show',
                    component: PageShow,
                    meta: {
                      title: "Page View",
                      description: "",
                      breadcrumb: [{
                        name: "Dashboard",
                        link: "home",
                        isActive: false
                      }, {
                        name: "Page",
                        link: "pages.index",
                        isActive: false
                      }, {
                        name: "View",
                        link: "",
                        isActive: true
                      }]
                    }
                  }, 
        {
            path: '/password/change',
            name: 'password.change',
            component: ChangePassword,
            meta: {
                title: "Change Password",
                description: ""
            }
        },
        {
            path: '/users/dealers',
            name: 'dealers.index',
            component: DealersIndex,
            meta: {
                title: "Dealers listing",
                description: ""
            }
        },
        {
            path: '/users/dealers/:id',
            name: 'dealers.show',
            component: DealersShow,
            meta: {
                title: "Dealer Profile",
                description: "",
                breadcrumb: [
                    {
                        name: "Dashboard",
                        link: "home",
                        isActive: false,
                    },
                    {
                        name: "Users",
                        link: "users.index",
                        isActive: false,
                    },
                    {
                        name: "Dealers",
                        link: "dealers.index",
                        isActive: false,
                    },
                    {
                        name: "Dealer Profile",
                        link: "",
                        isActive: true,
                    },
                ]
            }
        },
        {
            path: '/users',
            name: 'users.index',
            component: UsersIndex,
            meta: {
                title: "Users listing",
                description: ""
            }
        },
        {
            path: '/users/create',
            name: 'users.create',
            component: AddUser,
            meta: {
                title: "Add User",
                description: ""
            }
        },

        {
            path: '/users/:id',
            name: 'users.show',
            component: UsersShow,
            meta: {
                title: "User Profile",
                description: "",
                breadcrumb: [
                    {
                        name: "Dashboard",
                        link: "home",
                        isActive: false,
                    },
                    {
                        name: "Users",
                        link: "users.index",
                        isActive: false,
                    },
                    {
                        name: "User Profile",
                        link: "",
                        isActive: true,
                    },
                ]
            }
        },
        {
            path: '/ads',
            name: 'ads.index',
            component: AdsIndex,
            meta: {
                title: "Ad listing",
                description: "",
                status: '1,2,4,5',
                breadcrumb: [
                    {
                        name: "Dashboard",
                        link: "home",
                        isActive: false,
                    },
                    {
                        name: "Ads",
                        link: "ads.index",
                        isActive: true,
                    },
                ]
            }
        },
        {
            path: '/pending/ad',
            name: 'ads.pending.index',
            component: AdsIndex,
            meta: {
                title: "PENDING APPROVALS",
                description: "",
                status: '0',
                breadcrumb: [
                    {
                        name: "Dashboard",
                        link: "home",
                        isActive: false,
                    },
                    {
                        name: "Pending Ads",
                        link: "pending.ads.index",
                        isActive: true,
                    },
                ]
            }
        },
        {
            path: '/ads/:id',
            name: 'ads.show',
            component: AdsShow,
            meta: {
                title: "Ad Detail",
                description: "",
                breadcrumb: [
                    {
                        name: "Dashboard",
                        link: "home",
                        isActive: false,
                    },
                    {
                        name: "Ads",
                        link: "ads.index",
                        isActive: false,
                    },
                    {
                        name: "Ad Details",
                        link: "ads.show",
                        isActive: true,
                    },
                ]
            }
        },
        {
            path: '/ads/:id/edit',
            name: 'ads.edit',
            component: AdsEdit,
            meta: {
                title: "Ad Update",
                description: "",
                breadcrumb: [
                    {
                        name: "Dashboard",
                        link: "home",
                        isActive: false,
                    },
                    {
                        name: "Ads",
                        link: "ads.index",
                        isActive: false,
                    },
                    {
                        name: "Ad Update",
                        link: "ads.edit",
                        isActive: true,
                    },
                ]
            }
        },
        {
            path: '/manage-packages',
            name: 'packages.index',
            component: PackagesIndex,
            meta: {
                title: "Manage Packages",
                description: ""
            }
        },
        {
            path: '/payments',
            name: 'payments.index',
            component: PaymentsIndex,
            meta: {
                title: "Payments listing",
                description: ""
            }
        },
        {
            path: '*',
            name: 'notfound',
            component: NotFound,
            meta: {
                title: "Not found",
                description: ""
            }
        },
    ],
});

router.beforeEach((to, from, next) => {
    // This goes through the matched routes from last to first, finding the closest route with a title.
    // eg. if we have /some/deep/nested/route and /some, /deep, and /nested have titles, nested's will be chosen.
    const nearestWithTitle = to.matched.slice().reverse().find(r => r.meta && r.meta.title);

    // Find the nearest route element with meta tags.
    const nearestWithMeta = to.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);
    const previousNearestWithMeta = from.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);

    // If a route with a title was found, set the document (page) title to that value.
    if(nearestWithTitle) document.title = "Auto Bazaar - " + nearestWithTitle.meta.title;

    // Remove any stale meta tags from the document using the key attribute we set below.
    Array.from(document.querySelectorAll('[data-vue-router-controlled]')).map(el => el.parentNode.removeChild(el));

    // Skip rendering meta tags if there are none.
    if(!nearestWithMeta) return next();

    // Turn the meta tag definitions into actual elements in the head.
    nearestWithMeta.meta.metaTags.map(tagDef => {
        const tag = document.createElement('meta');

        Object.keys(tagDef).forEach(key => {
            tag.setAttribute(key, tagDef[key]);
        });

        // We use this to track which meta tags we create, so we don't interfere with other ones.
        tag.setAttribute('data-vue-router-controlled', '');

        return tag;
    })
    // Add the meta tags to the document head.
        .forEach(tag => document.head.appendChild(tag));

    next();
});

export default router;
