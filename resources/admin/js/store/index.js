import Vuex from 'vuex';
import Vue from 'vue';
import packages from './modules/packages';

// Load Vuex
Vue.use(Vuex);

// Create store
export default new Vuex.Store({
    modules: {
        packages
    }
});
