const state = {
    packages: [],
};
const getters = {
    all: state => state.packages,
};
const actions = {
    //create
    /*async update({ commit }, pkg) {
        await axios.put(`packages/${pkg.id}`, pkg)
            .then(res => commit('update', res.data))
            .catch(err => err);
    },*/
    async getAll({ commit }) {
        /*if(state.packages.length > 0)
            return;*/
        await axios.get(`packages`)
            .then(({data}) => commit('get', data))
            .catch(err => err);
    }
};
const mutations = {
    update: (state, todo) => (state.todos.unshift(todo)),
    get: (state, packages) => (state.packages = packages)
};

export default {
    state,
    getters,
    actions,
    mutations
}
