
require('./bootstrap');

import Vue from 'vue';
import VueRouter from "vue-router";
import VeeValidate from 'vee-validate';

import VueToastr2 from 'vue-toastr-2'
import 'vue-toastr-2/dist/vue-toastr-2.min.css'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import VuejsDialog from 'vuejs-dialog';
import 'vuejs-dialog/dist/vuejs-dialog.min.css';
import VueCroppie from 'vue-croppie';
import 'croppie/croppie.css' // import the croppie css manually

Vue.use(VueCroppie);

    /*import { required } from "vee-validate/dist/rules";

    import { ValidationObserver, ValidationProvider, extend } from 'vee-validate';

    // Add a rule.
    extend('secret', {
        validate: value => value === 'example',
        message: 'This is not the magic word'
    });

    // Register it globally
    Vue.component('ValidationProvider', ValidationProvider);
    Vue.component('ValidationObserver', ValidationObserver);*/

window.toastr = require('toastr');

Vue.use(VueRouter);
Vue.use(VeeValidate);
Vue.use(VueToastr2);
Vue.use(BootstrapVue);
Vue.use(VuejsDialog);
import index from './store/index';

import VueTimeago from 'vue-timeago'

Vue.use(VueTimeago, {
    locale: 'en', // Default locale
});

Vue.use(VuejsDialog, {
    html: true,
    loader: true,
    okText: 'Proceed',
    cancelText: 'Cancel',
    // animation: 'bounce'
});

import Statuses from "./Statuses";


import Filters from '../../filters/index.js';
for (let filter in Filters) Vue.filter(filter, Filters[filter]);

Vue.mixin({
    data(){
        return {
            statuses: Statuses
        }
    }
});

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('top-header', require('./components/HeaderComponent.vue').default);
Vue.component('side-navbar', require('./components/SidebarComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import router from "./router.js";

import App from './components/AppComponent.vue';

const app = new Vue({
    el: '#app',
    router,
    components: { App },
    store: index,
    mounted(){

    },
    beforeRouteUpdate(to, from, next) {

    }
});

$(document).on('keypress', 'input[type=number]', function(evt){
    if (evt.which !== 8 && evt.which !== 0 && evt.which < 48 || evt.which > 57)
        evt.preventDefault();
});
