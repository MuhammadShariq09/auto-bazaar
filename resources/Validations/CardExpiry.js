export default {
    getMessage: field => 'The ' + field + ' must be valid.',
    validate: value => {
        let x;
        // if(value.indexOf('/') >= 0) {
            x = value.split('/');
        // }else{
        //     if(value.trim().length < 6){
        //         return false
        //     } else {
        //         x = [value.substr(0, 2), value.substr(2, 4)];
        //     }
        // }

        if (x.length < 2) return false;
        const today = new Date();
        const selectedDate = new Date(x[1], x[0], today.getDate());
        return selectedDate >= today;
    }
}
