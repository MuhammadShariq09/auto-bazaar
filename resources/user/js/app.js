import Vue from "vue";

require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router'
import VeeValidate from 'vee-validate';
import router from "./router.js";
import App from './components/AppComponent.vue';
import VueToastr2 from 'vue-toastr-2';
import 'vue-toastr-2/dist/vue-toastr-2.min.css'
import VuejsDialog from "vuejs-dialog";
import 'vuejs-dialog/dist/vuejs-dialog.min.css';
import Statuses from '../../admin/js/Statuses'
import Filters from '../../filters/Currency';
import vWow from 'v-wow';
import VueCroppie from 'vue-croppie';
import 'croppie/croppie.css' // import the croppie css manually

Vue.use(VueCroppie);
Vue.prototype.$baseUrl = window.base_url;
import VueWow from 'vue-wow';
window.toastr = require('toastr');
window.Vue.prototype.$baseUrl = window.base_url;

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Object.keys(Filters).forEach(filter => Vue.filter(filter, Filters[filter]) );

Vue.component('user-header', require('./components/HeaderComponent.vue').default);
Vue.component('user-footer', require('./components/FooterComponent.vue').default);
Vue.component('register-body', require('./views/RegistrationComponent.vue').default);
Vue.use(VueRouter);
Vue.use(VeeValidate);
Vue.use(VueToastr2);
Vue.use(VuejsDialog);
import VueTimeago from 'vue-timeago'

Vue.use(VueTimeago, {
    locale: 'en', // Default locale
});

import * as Wow from 'wow.js';

Vue.use(vWow);
Vue.use(VuejsDialog, {
    html: true,
    loader: true,
    okText: 'Proceed',
    cancelText: 'Cancel',
    // animation: 'bounce'
});


Vue.mixin({
    data(){
        return {
            makes: [],
            models: [],
            status: Statuses
        }
    },
    methods: {
        loadMakes(){
            axios.get(`/makes`)
                .then(({data}) => {
                    this.makes = data;
                });
        },
        loadModals(make_id){
            axios.get(`/modals?make_id=${make_id}`)
                .then(({data}) => {
                    this.models = data;
                });
        },
        numberToStatus(status){
            switch (status) {
                case 0:
                    return 'Pending';
                case 1:
                    return 'Approved';
                case 2:
                    return 'Active';
                case 3:
                    return 'Rejected';
                case 4:
                    return 'Pending Payment';
                case 5:
                    return 'Sold';
                default:
                    return 'invalid status'
            }
        }
    },
    computed: {
        allYears(){
            const year = (new Date()).getFullYear();
            const years = [];
            for(let i = year - 40; i <= year; i ++)
            {
                years.push(i);
            }
            return years;
        }
    }
});

import RegistrationBody from "./views/RegistrationComponent";
import ForgotPassword from "./views/ForgotPasswordComponent";

if(document.getElementById('registerApp'))
{
    new Vue({
        el: '#registerApp',
        components: { RegistrationBody, ForgotPassword },
    });
}else{
    const app = new Vue({
        el: '#app',
        router,
        components: { App },
        mounted(){},
        beforeRouteUpdate(to, from, next) {}
    });
}

$(document).on('keypress', 'input[type=number]', function(evt){
    if (evt.which !== 8 && evt.which !== 0 && evt.which < 48 || evt.which > 57)
        evt.preventDefault();
});
