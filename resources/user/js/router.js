import VueRouter from "vue-router";
import DealerChatComponent from "./views/DealerChatComponent";

const HomeComponent = () => import(/* webpackChunkName: "user-dashboard" */ './components/HomeComponent');
const AboutComponent = () => import(/* webpackChunkName: "user-dashboard" */ './views/AboutComponent');
const ServiceComponent = () => import(/* webpackChunkName: "user-service" */ './views/ServiceComponent');
const BlogComponent = () => import(/* webpackChunkName: "user-blog" */ './views/BlogComponent');
const PrepareCarForSaleComponent = () => import('./views/PrepareCarForSaleComponent');
const ContactComponent = () => import(/* webpackChunkName: "user-contact" */ './views/ContactComponent');
const CarForSaleComponent = () => import(/* webpackChunkName: "user-car-for-sale" */ './views/CarForSaleComponent');
const AdDetailsComponent = () => import(/* webpackChunkName: "user-ad-details" */ './views/AdDetailsComponent');
const ImageCropper = () => import(/* webpackChunkName: "user-ad-details" */ '../../js/components/ImageCropper');
const ProfileComponent = () => import(/* webpackChunkName: "user-profile" */ './views/ProfileComponent');
const SaleYourCarComponent = () => import(/* webpackChunkName: "user-sale-car" */ './views/SaleYourCarComponent');
const PackagesComponent = () => import(/* webpackChunkName: "user-ad-packages" */ './views/PackagesComponent');
const MyListingComponent = () => import(/* webpackChunkName: "user-ad-packages" */ './views/MyListingComponent');
const WishListComponent = () => import(/* webpackChunkName: "user-wishlists" */ './views/WishListComponent');
const ContactAdminComponent = () => import(/* webpackChunkName: "user-wishlists" */ './views/ContactAdminComponent');
const AdPaymentComponent = () => import(/* webpackChunkName: "user-ad-payment" */ './views/AdPaymentComponent');
const ChatComponent = () => import(/* webpackChunkName: "user-chat" */ './views/ChatComponent');

const router = new VueRouter({
    mode: 'history',
    base: window.base_url.replace((window.location.protocol + '//' + window.location.host), ''),
// base: `${(process.env.NODE_ENV === 'adminProduction' || process.env.NODE_ENV === 'userProduction') ? process.env.MIX_PRODUCTION_VUE_ROUTER_BASE_URL: process.env.MIX_VUE_ROUTER_BASE_URL}/`,
    linkActiveClass: 'active',
    routes: [
        {
            path: '/home',
            name: 'home',
            component: HomeComponent,
            meta: {
                title: "Home",
                shouldShowTitle: false,
                description: ""
            }
        },
        {
            path: '/home',
            name: 'advertisers',
            component: HomeComponent,
            meta: {
                title: "Advertisers",
                shouldShowTitle: false,
                description: ""
            }
        },
        {
            path: '/',
            redirect: { name: 'home'},
        },
        {
            path: '/about-us',
            name: 'about',
            component: AboutComponent,
            meta: {
                title: "About us",
                shouldShowTitle: true,
                description: ""
            }
        },
        {
            path: '/services',
            name: 'services',
            component: ServiceComponent,
            meta: {
                title: "Service",
                shouldShowTitle: true,
                description: ""
            }
        },
        {
            path: '/blog',
            name: 'blog',
            component: BlogComponent,
            meta: {
                title: "Blog",
                shouldShowTitle: true,
                description: ""
            }
        },
        {
            path: '/prepare-car-for-sale',
            name: 'prepare.car.for.sale',
            component: PrepareCarForSaleComponent,
            meta: {
                title: "Prepare Car For Sale",
                shouldShowTitle: true,
                description: ""
            }
        },
        {
            path: '/contact',
            name: 'contact',
            component: ContactComponent,
            meta: {
                title: "Contact",
                shouldShowTitle: true,
                description: ""
            }
        },
        {
            path: '/cars-for-sale',
            name: 'car.for.sale',
            component: CarForSaleComponent,
            meta: {
                title: "Cars for Sale",
                shouldShowTitle: true,
                description: ""
            }
        },
        /*{
            path: '/image-cropper',
            name: 'image-cropper',
            component: ImageCropper,
            meta: {
                title: "Image Cropper",
                shouldShowTitle: true,
                description: ""
            }
        },*/
        {
            path: '/car-details/:id',
            name: 'ad.details',
            component: AdDetailsComponent,
            meta: {
                title: "Car Details",
                shouldShowTitle: true,
                description: ""
            }
        },
        {
            path: '/ads/:id/approved-ad-details',
            name: 'ad.payment',
            component: AdPaymentComponent,
            meta: {
                title: "My Listings",
                shouldShowTitle: true,
                description: ""
            }
        },
        {
            path: '/account-details',
            name: 'user.profile',
            component: ProfileComponent,
            meta: {
                title: "Account Details",
                shouldShowTitle: true,
                description: ""
            }
        },
        {
            path: '/sell-your-car',
            name: 'sale.car',
            component: SaleYourCarComponent,
            meta: {
                title: "Sale Your Car",
                shouldShowTitle: true,
                description: ""
            }
        },
        {
            path: '/my-listings',
            name: 'user.mylisting',
            component: MyListingComponent,
            meta: {
                title: "My Listings",
                shouldShowTitle: true,
                description: ""
            }
        },
        {
            path: '/chat/:toId',
            name: 'chat',
            component: ChatComponent,
            meta: {
                title: "Chat",
                shouldShowTitle: true,
                description: ""
            }
        },
        {
            path: '/chat',
            name: 'dealer_chat',
            component: DealerChatComponent,
            meta: {
                title: "Chat",
                shouldShowTitle: true,
                description: ""
            }
        },
        {
            path: '/my-wishlist',
            name: 'user.wishlists',
            component: WishListComponent,
            meta: {
                title: "My Wishlist",
                shouldShowTitle: true,
                description: ""
            }
        },
        {
            path: '/contact-admin',
            name: 'user.admin.contact',
            component: ContactAdminComponent,
            meta: {
                title: "Contact Admin",
                shouldShowTitle: true,
                description: ""
            }
        },
        {
            path: '/find-dealers',
            name: 'user.find.dealer',
            component: () => import(/* webpackChunkName: "user-find-dealer" */ './views/FindDealerComponent'),
            meta: {
                title: "Find Dealers",
                shouldShowTitle: true,
                description: ""
            }
        },
        {
            path: '/dealer-profile/:id',
            name: 'dealers.detail',
            component: () => import(/* webpackChunkName: "user-dealer-details" */ './views/DealerDetailComponent'),
            meta: {
                title: "Dealer Profile",
                shouldShowTitle: true,
                description: ""
            }
        },
        {
            path: '/create-new-password',
            name: 'dealers.password.change',
            component: () => import(/* webpackChunkName: "user-dealer-details" */ './views/ChangePassword'),
            meta: {
                title: "Create New Password",
                shouldShowTitle: true,
                description: ""
            }
        },
        {
            path: '/edit-profile',
            name: 'dealer.profile.edit',
            component: () => import(/* webpackChunkName: "dealer-edit-profile" */ './views/DealerEditProfileComponent'),
            meta: {
                title: "Edit Profile",
                shouldShowTitle: true,
                description: ""
            }
        },
        {
            path: '/account-information',
            name: 'dealer.account.edit',
            component: () => import(/* webpackChunkName: "dealer-edit-account" */ './views/DealerEditAccountComponent'),
            meta: {
                title: "Account Information",
                shouldShowTitle: true,
                description: ""
            }
        },
        {
            path: '/terms-and-conditions',
            name: 'terms.and.conditions',
            component: () => import(/* webpackChunkName: "terms-and-conditions" */ './views/TermsAndConditionsComponent'),
            meta: {
                title: "Terms and Conditions",
                shouldShowTitle: true,
                description: ""
            }
        },
    ],
});

router.beforeEach((to, from, next) => {
    // This goes through the matched routes from last to first, finding the closest route with a title.
    // eg. if we have /some/deep/nested/route and /some, /deep, and /nested have titles, nested's will be chosen.
    const nearestWithTitle = to.matched.slice().reverse().find(r => r.meta && r.meta.title);

    // Find the nearest route element with meta tags.
    const nearestWithMeta = to.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);
    const previousNearestWithMeta = from.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);

    // If a route with a title was found, set the document (page) title to that value.
    if(nearestWithTitle) document.title = "Auto Bazaar - " + nearestWithTitle.meta.title;

    // Remove any stale meta tags from the document using the key attribute we set below.
    Array.from(document.querySelectorAll('[data-vue-router-controlled]')).map(el => el.parentNode.removeChild(el));

    // Skip rendering meta tags if there are none.
    if(!nearestWithMeta) return next();

    // Turn the meta tag definitions into actual elements in the head.
    nearestWithMeta.meta.metaTags.map(tagDef => {
        const tag = document.createElement('meta');

        Object.keys(tagDef).forEach(key => {
            tag.setAttribute(key, tagDef[key]);
        });

        // We use this to track which meta tags we create, so we don't interfere with other ones.
        tag.setAttribute('data-vue-router-controlled', '');

        return tag;
    })
    // Add the meta tags to the document head.
        .forEach(tag => document.head.appendChild(tag));

    next();
});

export default router;
