! function(e, t) {
    "use strict";
    "object" == typeof module && "object" == typeof module.exports ? module.exports = e.document ? t(e, !0) : function(e) {
        if (!e.document) throw new Error("jQuery requires a window with a document");
        return t(e)
    } : t(e)
}("undefined" != typeof window ? window : this, function(e, t) {
    "use strict";

    function n(e, t, n) {
        var r, i = (t = t || ae).createElement("script");
        if (i.text = e, n)
            for (r in be) n[r] && (i[r] = n[r]);
        t.head.appendChild(i).parentNode.removeChild(i)
    }

    function r(e) {
        return null == e ? e + "" : "object" == typeof e || "function" == typeof e ? pe[de.call(e)] || "object" : typeof e
    }

    function i(e) {
        var t = !!e && "length" in e && e.length,
            n = r(e);
        return !me(e) && !xe(e) && ("array" === n || 0 === t || "number" == typeof t && t > 0 && t - 1 in e)
    }

    function o(e, t) {
        return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase()
    }

    function a(e, t, n) {
        return me(t) ? we.grep(e, function(e, r) {
            return !!t.call(e, r, e) !== n
        }) : t.nodeType ? we.grep(e, function(e) {
            return e === t !== n
        }) : "string" != typeof t ? we.grep(e, function(e) {
            return fe.call(t, e) > -1 !== n
        }) : we.filter(t, e, n)
    }

    function s(e, t) {
        for (;
            (e = e[t]) && 1 !== e.nodeType;);
        return e
    }

    function u(e) {
        var t = {};
        return we.each(e.match(Le) || [], function(e, n) {
            t[n] = !0
        }), t
    }

    function l(e) {
        return e
    }

    function c(e) {
        throw e
    }

    function f(e, t, n, r) {
        var i;
        try {
            e && me(i = e.promise) ? i.call(e).done(t).fail(n) : e && me(i = e.then) ? i.call(e, t, n) : t.apply(void 0, [e].slice(r))
        } catch (e) {
            n.apply(void 0, [e])
        }
    }

    function p() {
        ae.removeEventListener("DOMContentLoaded", p), e.removeEventListener("load", p), we.ready()
    }

    function d(e, t) {
        return t.toUpperCase()
    }

    function h(e) {
        return e.replace(Me, "ms-").replace(Re, d)
    }

    function g() {
        this.expando = we.expando + g.uid++
    }

    function y(e) {
        return "true" === e || "false" !== e && ("null" === e ? null : e === +e + "" ? +e : Be.test(e) ? JSON.parse(e) : e)
    }

    function v(e, t, n) {
        var r;
        if (void 0 === n && 1 === e.nodeType)
            if (r = "data-" + t.replace(Fe, "-$&").toLowerCase(), "string" == typeof(n = e.getAttribute(r))) {
                try {
                    n = y(n)
                } catch (e) {}
                $e.set(e, t, n)
            } else n = void 0;
        return n
    }

    function m(e, t, n, r) {
        var i, o, a = 20,
            s = r ? function() {
                return r.cur()
            } : function() {
                return we.css(e, t, "")
            },
            u = s(),
            l = n && n[3] || (we.cssNumber[t] ? "" : "px"),
            c = (we.cssNumber[t] || "px" !== l && +u) && ze.exec(we.css(e, t));
        if (c && c[3] !== l) {
            for (u /= 2, l = l || c[3], c = +u || 1; a--;) we.style(e, t, c + l), (1 - o) * (1 - (o = s() / u || .5)) <= 0 && (a = 0), c /= o;
            c *= 2, we.style(e, t, c + l), n = n || []
        }
        return n && (c = +c || +u || 0, i = n[1] ? c + (n[1] + 1) * n[2] : +n[2], r && (r.unit = l, r.start = c, r.end = i)), i
    }

    function x(e) {
        var t, n = e.ownerDocument,
            r = e.nodeName,
            i = Ge[r];
        return i || (t = n.body.appendChild(n.createElement(r)), i = we.css(t, "display"), t.parentNode.removeChild(t), "none" === i && (i = "block"), Ge[r] = i, i)
    }

    function b(e, t) {
        for (var n, r, i = [], o = 0, a = e.length; o < a; o++)(r = e[o]).style && (n = r.style.display, t ? ("none" === n && (i[o] = We.get(r, "display") || null, i[o] || (r.style.display = "")), "" === r.style.display && Ue(r) && (i[o] = x(r))) : "none" !== n && (i[o] = "none", We.set(r, "display", n)));
        for (o = 0; o < a; o++) null != i[o] && (e[o].style.display = i[o]);
        return e
    }

    function w(e, t) {
        var n;
        return n = "undefined" != typeof e.getElementsByTagName ? e.getElementsByTagName(t || "*") : "undefined" != typeof e.querySelectorAll ? e.querySelectorAll(t || "*") : [], void 0 === t || t && o(e, t) ? we.merge([e], n) : n
    }

    function T(e, t) {
        for (var n = 0, r = e.length; n < r; n++) We.set(e[n], "globalEval", !t || We.get(t[n], "globalEval"))
    }

    function C(e, t, n, i, o) {
        for (var a, s, u, l, c, f, p = t.createDocumentFragment(), d = [], h = 0, g = e.length; h < g; h++)
            if ((a = e[h]) || 0 === a)
                if ("object" === r(a)) we.merge(d, a.nodeType ? [a] : a);
                else if (Ze.test(a)) {
            for (s = s || p.appendChild(t.createElement("div")), u = (Qe.exec(a) || ["", ""])[1].toLowerCase(), l = Ke[u] || Ke._default, s.innerHTML = l[1] + we.htmlPrefilter(a) + l[2], f = l[0]; f--;) s = s.lastChild;
            we.merge(d, s.childNodes), (s = p.firstChild).textContent = ""
        } else d.push(t.createTextNode(a));
        for (p.textContent = "", h = 0; a = d[h++];)
            if (i && we.inArray(a, i) > -1) o && o.push(a);
            else if (c = we.contains(a.ownerDocument, a), s = w(p.appendChild(a), "script"), c && T(s), n)
            for (f = 0; a = s[f++];) Je.test(a.type || "") && n.push(a);
        return p
    }

    function E() {
        return !0
    }

    function k() {
        return !1
    }

    function S() {
        try {
            return ae.activeElement
        } catch (e) {}
    }

    function D(e, t, n, r, i, o) {
        var a, s;
        if ("object" == typeof t) {
            "string" != typeof n && (r = r || n, n = void 0);
            for (s in t) D(e, s, n, r, t[s], o);
            return e
        }
        if (null == r && null == i ? (i = n, r = n = void 0) : null == i && ("string" == typeof n ? (i = r, r = void 0) : (i = r, r = n, n = void 0)), !1 === i) i = k;
        else if (!i) return e;
        return 1 === o && (a = i, (i = function(e) {
            return we().off(e), a.apply(this, arguments)
        }).guid = a.guid || (a.guid = we.guid++)), e.each(function() {
            we.event.add(this, t, i, r, n)
        })
    }

    function N(e, t) {
        return o(e, "table") && o(11 !== t.nodeType ? t : t.firstChild, "tr") ? we(e).children("tbody")[0] || e : e
    }

    function A(e) {
        return e.type = (null !== e.getAttribute("type")) + "/" + e.type, e
    }

    function j(e) {
        return "true/" === (e.type || "").slice(0, 5) ? e.type = e.type.slice(5) : e.removeAttribute("type"), e
    }

    function q(e, t) {
        var n, r, i, o, a, s, u, l;
        if (1 === t.nodeType) {
            if (We.hasData(e) && (o = We.access(e), a = We.set(t, o), l = o.events)) {
                delete a.handle, a.events = {};
                for (i in l)
                    for (n = 0, r = l[i].length; n < r; n++) we.event.add(t, i, l[i][n])
            }
            $e.hasData(e) && (s = $e.access(e), u = we.extend({}, s), $e.set(t, u))
        }
    }

    function L(e, t) {
        var n = t.nodeName.toLowerCase();
        "input" === n && Ye.test(e.type) ? t.checked = e.checked : "input" !== n && "textarea" !== n || (t.defaultValue = e.defaultValue)
    }

    function H(e, t, r, i) {
        t = le.apply([], t);
        var o, a, s, u, l, c, f = 0,
            p = e.length,
            d = p - 1,
            h = t[0],
            g = me(h);
        if (g || p > 1 && "string" == typeof h && !ve.checkClone && at.test(h)) return e.each(function(n) {
            var o = e.eq(n);
            g && (t[0] = h.call(this, n, o.html())), H(o, t, r, i)
        });
        if (p && (o = C(t, e[0].ownerDocument, !1, e, i), a = o.firstChild, 1 === o.childNodes.length && (o = a), a || i)) {
            for (u = (s = we.map(w(o, "script"), A)).length; f < p; f++) l = o, f !== d && (l = we.clone(l, !0, !0), u && we.merge(s, w(l, "script"))), r.call(e[f], l, f);
            if (u)
                for (c = s[s.length - 1].ownerDocument, we.map(s, j), f = 0; f < u; f++) l = s[f], Je.test(l.type || "") && !We.access(l, "globalEval") && we.contains(c, l) && (l.src && "module" !== (l.type || "").toLowerCase() ? we._evalUrl && we._evalUrl(l.src) : n(l.textContent.replace(st, ""), c, l))
        }
        return e
    }

    function O(e, t, n) {
        for (var r, i = t ? we.filter(t, e) : e, o = 0; null != (r = i[o]); o++) n || 1 !== r.nodeType || we.cleanData(w(r)), r.parentNode && (n && we.contains(r.ownerDocument, r) && T(w(r, "script")), r.parentNode.removeChild(r));
        return e
    }

    function P(e, t, n) {
        var r, i, o, a, s = e.style;
        return (n = n || lt(e)) && ("" !== (a = n.getPropertyValue(t) || n[t]) || we.contains(e.ownerDocument, e) || (a = we.style(e, t)), !ve.pixelBoxStyles() && ut.test(a) && ct.test(t) && (r = s.width, i = s.minWidth, o = s.maxWidth, s.minWidth = s.maxWidth = s.width = a, a = n.width, s.width = r, s.minWidth = i, s.maxWidth = o)), void 0 !== a ? a + "" : a
    }

    function M(e, t) {
        return {
            get: function() {
                return e() ? void delete this.get : (this.get = t).apply(this, arguments)
            }
        }
    }

    function R(e) {
        if (e in yt) return e;
        for (var t = e[0].toUpperCase() + e.slice(1), n = gt.length; n--;)
            if ((e = gt[n] + t) in yt) return e
    }

    function I(e) {
        var t = we.cssProps[e];
        return t || (t = we.cssProps[e] = R(e) || e), t
    }

    function W(e, t, n) {
        var r = ze.exec(t);
        return r ? Math.max(0, r[2] - (n || 0)) + (r[3] || "px") : t
    }

    function $(e, t, n, r, i, o) {
        var a = "width" === t ? 1 : 0,
            s = 0,
            u = 0;
        if (n === (r ? "border" : "content")) return 0;
        for (; a < 4; a += 2) "margin" === n && (u += we.css(e, n + Xe[a], !0, i)), r ? ("content" === n && (u -= we.css(e, "padding" + Xe[a], !0, i)), "margin" !== n && (u -= we.css(e, "border" + Xe[a] + "Width", !0, i))) : (u += we.css(e, "padding" + Xe[a], !0, i), "padding" !== n ? u += we.css(e, "border" + Xe[a] + "Width", !0, i) : s += we.css(e, "border" + Xe[a] + "Width", !0, i));
        return !r && o >= 0 && (u += Math.max(0, Math.ceil(e["offset" + t[0].toUpperCase() + t.slice(1)] - o - u - s - .5))), u
    }

    function B(e, t, n) {
        var r = lt(e),
            i = P(e, t, r),
            o = "border-box" === we.css(e, "boxSizing", !1, r),
            a = o;
        if (ut.test(i)) {
            if (!n) return i;
            i = "auto"
        }
        return a = a && (ve.boxSizingReliable() || i === e.style[t]), ("auto" === i || !parseFloat(i) && "inline" === we.css(e, "display", !1, r)) && (i = e["offset" + t[0].toUpperCase() + t.slice(1)], a = !0), (i = parseFloat(i) || 0) + $(e, t, n || (o ? "border" : "content"), a, r, i) + "px"
    }

    function F(e, t, n, r, i) {
        return new F.prototype.init(e, t, n, r, i)
    }

    function _() {
        mt && (!1 === ae.hidden && e.requestAnimationFrame ? e.requestAnimationFrame(_) : e.setTimeout(_, we.fx.interval), we.fx.tick())
    }

    function z() {
        return e.setTimeout(function() {
            vt = void 0
        }), vt = Date.now()
    }

    function X(e, t) {
        var n, r = 0,
            i = {
                height: e
            };
        for (t = t ? 1 : 0; r < 4; r += 2 - t) i["margin" + (n = Xe[r])] = i["padding" + n] = e;
        return t && (i.opacity = i.width = e), i
    }

    function U(e, t, n) {
        for (var r, i = (Y.tweeners[t] || []).concat(Y.tweeners["*"]), o = 0, a = i.length; o < a; o++)
            if (r = i[o].call(n, t, e)) return r
    }

    function V(e, t, n) {
        var r, i, o, a, s, u, l, c, f = "width" in t || "height" in t,
            p = this,
            d = {},
            h = e.style,
            g = e.nodeType && Ue(e),
            y = We.get(e, "fxshow");
        n.queue || (null == (a = we._queueHooks(e, "fx")).unqueued && (a.unqueued = 0, s = a.empty.fire, a.empty.fire = function() {
            a.unqueued || s()
        }), a.unqueued++, p.always(function() {
            p.always(function() {
                a.unqueued--, we.queue(e, "fx").length || a.empty.fire()
            })
        }));
        for (r in t)
            if (i = t[r], xt.test(i)) {
                if (delete t[r], o = o || "toggle" === i, i === (g ? "hide" : "show")) {
                    if ("show" !== i || !y || void 0 === y[r]) continue;
                    g = !0
                }
                d[r] = y && y[r] || we.style(e, r)
            }
        if ((u = !we.isEmptyObject(t)) || !we.isEmptyObject(d)) {
            f && 1 === e.nodeType && (n.overflow = [h.overflow, h.overflowX, h.overflowY], null == (l = y && y.display) && (l = We.get(e, "display")), "none" === (c = we.css(e, "display")) && (l ? c = l : (b([e], !0), l = e.style.display || l, c = we.css(e, "display"), b([e]))), ("inline" === c || "inline-block" === c && null != l) && "none" === we.css(e, "float") && (u || (p.done(function() {
                h.display = l
            }), null == l && (c = h.display, l = "none" === c ? "" : c)), h.display = "inline-block")), n.overflow && (h.overflow = "hidden", p.always(function() {
                h.overflow = n.overflow[0], h.overflowX = n.overflow[1], h.overflowY = n.overflow[2]
            })), u = !1;
            for (r in d) u || (y ? "hidden" in y && (g = y.hidden) : y = We.access(e, "fxshow", {
                display: l
            }), o && (y.hidden = !g), g && b([e], !0), p.done(function() {
                g || b([e]), We.remove(e, "fxshow");
                for (r in d) we.style(e, r, d[r])
            })), u = U(g ? y[r] : 0, r, p), r in y || (y[r] = u.start, g && (u.end = u.start, u.start = 0))
        }
    }

    function G(e, t) {
        var n, r, i, o, a;
        for (n in e)
            if (r = h(n), i = t[r], o = e[n], Array.isArray(o) && (i = o[1], o = e[n] = o[0]), n !== r && (e[r] = o, delete e[n]), (a = we.cssHooks[r]) && "expand" in a) {
                o = a.expand(o), delete e[r];
                for (n in o) n in e || (e[n] = o[n], t[n] = i)
            } else t[r] = i
    }

    function Y(e, t, n) {
        var r, i, o = 0,
            a = Y.prefilters.length,
            s = we.Deferred().always(function() {
                delete u.elem
            }),
            u = function() {
                if (i) return !1;
                for (var t = vt || z(), n = Math.max(0, l.startTime + l.duration - t), r = 1 - (n / l.duration || 0), o = 0, a = l.tweens.length; o < a; o++) l.tweens[o].run(r);
                return s.notifyWith(e, [l, r, n]), r < 1 && a ? n : (a || s.notifyWith(e, [l, 1, 0]), s.resolveWith(e, [l]), !1)
            },
            l = s.promise({
                elem: e,
                props: we.extend({}, t),
                opts: we.extend(!0, {
                    specialEasing: {},
                    easing: we.easing._default
                }, n),
                originalProperties: t,
                originalOptions: n,
                startTime: vt || z(),
                duration: n.duration,
                tweens: [],
                createTween: function(t, n) {
                    var r = we.Tween(e, l.opts, t, n, l.opts.specialEasing[t] || l.opts.easing);
                    return l.tweens.push(r), r
                },
                stop: function(t) {
                    var n = 0,
                        r = t ? l.tweens.length : 0;
                    if (i) return this;
                    for (i = !0; n < r; n++) l.tweens[n].run(1);
                    return t ? (s.notifyWith(e, [l, 1, 0]), s.resolveWith(e, [l, t])) : s.rejectWith(e, [l, t]), this
                }
            }),
            c = l.props;
        for (G(c, l.opts.specialEasing); o < a; o++)
            if (r = Y.prefilters[o].call(l, e, c, l.opts)) return me(r.stop) && (we._queueHooks(l.elem, l.opts.queue).stop = r.stop.bind(r)), r;
        return we.map(c, U, l), me(l.opts.start) && l.opts.start.call(e, l), l.progress(l.opts.progress).done(l.opts.done, l.opts.complete).fail(l.opts.fail).always(l.opts.always), we.fx.timer(we.extend(u, {
            elem: e,
            anim: l,
            queue: l.opts.queue
        })), l
    }

    function Q(e) {
        return (e.match(Le) || []).join(" ")
    }

    function J(e) {
        return e.getAttribute && e.getAttribute("class") || ""
    }

    function K(e) {
        return Array.isArray(e) ? e : "string" == typeof e ? e.match(Le) || [] : []
    }

    function Z(e, t, n, i) {
        var o;
        if (Array.isArray(t)) we.each(t, function(t, r) {
            n || qt.test(e) ? i(e, r) : Z(e + "[" + ("object" == typeof r && null != r ? t : "") + "]", r, n, i)
        });
        else if (n || "object" !== r(t)) i(e, t);
        else
            for (o in t) Z(e + "[" + o + "]", t[o], n, i)
    }

    function ee(e) {
        return function(t, n) {
            "string" != typeof t && (n = t, t = "*");
            var r, i = 0,
                o = t.toLowerCase().match(Le) || [];
            if (me(n))
                for (; r = o[i++];) "+" === r[0] ? (r = r.slice(1) || "*", (e[r] = e[r] || []).unshift(n)) : (e[r] = e[r] || []).push(n)
        }
    }

    function te(e, t, n, r) {
        function i(s) {
            var u;
            return o[s] = !0, we.each(e[s] || [], function(e, s) {
                var l = s(t, n, r);
                return "string" != typeof l || a || o[l] ? a ? !(u = l) : void 0 : (t.dataTypes.unshift(l), i(l), !1)
            }), u
        }
        var o = {},
            a = e === _t;
        return i(t.dataTypes[0]) || !o["*"] && i("*")
    }

    function ne(e, t) {
        var n, r, i = we.ajaxSettings.flatOptions || {};
        for (n in t) void 0 !== t[n] && ((i[n] ? e : r || (r = {}))[n] = t[n]);
        return r && we.extend(!0, e, r), e
    }

    function re(e, t, n) {
        for (var r, i, o, a, s = e.contents, u = e.dataTypes;
            "*" === u[0];) u.shift(), void 0 === r && (r = e.mimeType || t.getResponseHeader("Content-Type"));
        if (r)
            for (i in s)
                if (s[i] && s[i].test(r)) {
                    u.unshift(i);
                    break
                }
        if (u[0] in n) o = u[0];
        else {
            for (i in n) {
                if (!u[0] || e.converters[i + " " + u[0]]) {
                    o = i;
                    break
                }
                a || (a = i)
            }
            o = o || a
        }
        if (o) return o !== u[0] && u.unshift(o), n[o]
    }

    function ie(e, t, n, r) {
        var i, o, a, s, u, l = {},
            c = e.dataTypes.slice();
        if (c[1])
            for (a in e.converters) l[a.toLowerCase()] = e.converters[a];
        for (o = c.shift(); o;)
            if (e.responseFields[o] && (n[e.responseFields[o]] = t), !u && r && e.dataFilter && (t = e.dataFilter(t, e.dataType)), u = o, o = c.shift())
                if ("*" === o) o = u;
                else if ("*" !== u && u !== o) {
            if (!(a = l[u + " " + o] || l["* " + o]))
                for (i in l)
                    if ((s = i.split(" "))[1] === o && (a = l[u + " " + s[0]] || l["* " + s[0]])) {
                        !0 === a ? a = l[i] : !0 !== l[i] && (o = s[0], c.unshift(s[1]));
                        break
                    }
            if (!0 !== a)
                if (a && e["throws"]) t = a(t);
                else try {
                    t = a(t)
                } catch (e) {
                    return {
                        state: "parsererror",
                        error: a ? e : "No conversion from " + u + " to " + o
                    }
                }
        }
        return {
            state: "success",
            data: t
        }
    }
    var oe = [],
        ae = e.document,
        se = Object.getPrototypeOf,
        ue = oe.slice,
        le = oe.concat,
        ce = oe.push,
        fe = oe.indexOf,
        pe = {},
        de = pe.toString,
        he = pe.hasOwnProperty,
        ge = he.toString,
        ye = ge.call(Object),
        ve = {},
        me = function(e) {
            return "function" == typeof e && "number" != typeof e.nodeType
        },
        xe = function(e) {
            return null != e && e === e.window
        },
        be = {
            type: !0,
            src: !0,
            noModule: !0
        },
        we = function(e, t) {
            return new we.fn.init(e, t)
        },
        Te = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
    we.fn = we.prototype = {
        jquery: "3.3.1",
        constructor: we,
        length: 0,
        toArray: function() {
            return ue.call(this)
        },
        get: function(e) {
            return null == e ? ue.call(this) : e < 0 ? this[e + this.length] : this[e]
        },
        pushStack: function(e) {
            var t = we.merge(this.constructor(), e);
            return t.prevObject = this, t
        },
        each: function(e) {
            return we.each(this, e)
        },
        map: function(e) {
            return this.pushStack(we.map(this, function(t, n) {
                return e.call(t, n, t)
            }))
        },
        slice: function() {
            return this.pushStack(ue.apply(this, arguments))
        },
        first: function() {
            return this.eq(0)
        },
        last: function() {
            return this.eq(-1)
        },
        eq: function(e) {
            var t = this.length,
                n = +e + (e < 0 ? t : 0);
            return this.pushStack(n >= 0 && n < t ? [this[n]] : [])
        },
        end: function() {
            return this.prevObject || this.constructor()
        },
        push: ce,
        sort: oe.sort,
        splice: oe.splice
    }, we.extend = we.fn.extend = function() {
        var e, t, n, r, i, o, a = arguments[0] || {},
            s = 1,
            u = arguments.length,
            l = !1;
        for ("boolean" == typeof a && (l = a, a = arguments[s] || {}, s++), "object" == typeof a || me(a) || (a = {}), s === u && (a = this, s--); s < u; s++)
            if (null != (e = arguments[s]))
                for (t in e) n = a[t], a !== (r = e[t]) && (l && r && (we.isPlainObject(r) || (i = Array.isArray(r))) ? (i ? (i = !1, o = n && Array.isArray(n) ? n : []) : o = n && we.isPlainObject(n) ? n : {}, a[t] = we.extend(l, o, r)) : void 0 !== r && (a[t] = r));
        return a
    }, we.extend({
        expando: "jQuery" + ("3.3.1" + Math.random()).replace(/\D/g, ""),
        isReady: !0,
        error: function(e) {
            throw new Error(e)
        },
        noop: function() {},
        isPlainObject: function(e) {
            var t, n;
            return !(!e || "[object Object]" !== de.call(e) || (t = se(e)) && ("function" != typeof(n = he.call(t, "constructor") && t.constructor) || ge.call(n) !== ye))
        },
        isEmptyObject: function(e) {
            var t;
            for (t in e) return !1;
            return !0
        },
        globalEval: function(e) {
            n(e)
        },
        each: function(e, t) {
            var n, r = 0;
            if (i(e))
                for (n = e.length; r < n && !1 !== t.call(e[r], r, e[r]); r++);
            else
                for (r in e)
                    if (!1 === t.call(e[r], r, e[r])) break;
            return e
        },
        trim: function(e) {
            return null == e ? "" : (e + "").replace(Te, "")
        },
        makeArray: function(e, t) {
            var n = t || [];
            return null != e && (i(Object(e)) ? we.merge(n, "string" == typeof e ? [e] : e) : ce.call(n, e)), n
        },
        inArray: function(e, t, n) {
            return null == t ? -1 : fe.call(t, e, n)
        },
        merge: function(e, t) {
            for (var n = +t.length, r = 0, i = e.length; r < n; r++) e[i++] = t[r];
            return e.length = i, e
        },
        grep: function(e, t, n) {
            for (var r, i = [], o = 0, a = e.length, s = !n; o < a; o++)(r = !t(e[o], o)) !== s && i.push(e[o]);
            return i
        },
        map: function(e, t, n) {
            var r, o, a = 0,
                s = [];
            if (i(e))
                for (r = e.length; a < r; a++) null != (o = t(e[a], a, n)) && s.push(o);
            else
                for (a in e) null != (o = t(e[a], a, n)) && s.push(o);
            return le.apply([], s)
        },
        guid: 1,
        support: ve
    }), "function" == typeof Symbol && (we.fn[Symbol.iterator] = oe[Symbol.iterator]), we.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function(e, t) {
        pe["[object " + t + "]"] = t.toLowerCase()
    });
    var Ce = function(e) {
        function t(e, t, n, r) {
            var i, o, a, s, u, l, c, p = t && t.ownerDocument,
                h = t ? t.nodeType : 9;
            if (n = n || [], "string" != typeof e || !e || 1 !== h && 9 !== h && 11 !== h) return n;
            if (!r && ((t ? t.ownerDocument || t : B) !== H && L(t), t = t || H, P)) {
                if (11 !== h && (u = ve.exec(e)))
                    if (i = u[1]) {
                        if (9 === h) {
                            if (!(a = t.getElementById(i))) return n;
                            if (a.id === i) return n.push(a), n
                        } else if (p && (a = p.getElementById(i)) && W(t, a) && a.id === i) return n.push(a), n
                    } else {
                        if (u[2]) return K.apply(n, t.getElementsByTagName(e)), n;
                        if ((i = u[3]) && T.getElementsByClassName && t.getElementsByClassName) return K.apply(n, t.getElementsByClassName(i)), n
                    }
                if (T.qsa && !U[e + " "] && (!M || !M.test(e))) {
                    if (1 !== h) p = t, c = e;
                    else if ("object" !== t.nodeName.toLowerCase()) {
                        for ((s = t.getAttribute("id")) ? s = s.replace(we, Te) : t.setAttribute("id", s = $), o = (l = S(e)).length; o--;) l[o] = "#" + s + " " + d(l[o]);
                        c = l.join(","), p = me.test(e) && f(t.parentNode) || t
                    }
                    if (c) try {
                        return K.apply(n, p.querySelectorAll(c)), n
                    } catch (e) {} finally {
                        s === $ && t.removeAttribute("id")
                    }
                }
            }
            return N(e.replace(se, "$1"), t, n, r)
        }

        function n() {
            function e(n, r) {
                return t.push(n + " ") > C.cacheLength && delete e[t.shift()], e[n + " "] = r
            }
            var t = [];
            return e
        }

        function r(e) {
            return e[$] = !0, e
        }

        function i(e) {
            var t = H.createElement("fieldset");
            try {
                return !!e(t)
            } catch (e) {
                return !1
            } finally {
                t.parentNode && t.parentNode.removeChild(t), t = null
            }
        }

        function o(e, t) {
            for (var n = e.split("|"), r = n.length; r--;) C.attrHandle[n[r]] = t
        }

        function a(e, t) {
            var n = t && e,
                r = n && 1 === e.nodeType && 1 === t.nodeType && e.sourceIndex - t.sourceIndex;
            if (r) return r;
            if (n)
                for (; n = n.nextSibling;)
                    if (n === t) return -1;
            return e ? 1 : -1
        }

        function s(e) {
            return function(t) {
                return "input" === t.nodeName.toLowerCase() && t.type === e
            }
        }

        function u(e) {
            return function(t) {
                var n = t.nodeName.toLowerCase();
                return ("input" === n || "button" === n) && t.type === e
            }
        }

        function l(e) {
            return function(t) {
                return "form" in t ? t.parentNode && !1 === t.disabled ? "label" in t ? "label" in t.parentNode ? t.parentNode.disabled === e : t.disabled === e : t.isDisabled === e || t.isDisabled !== !e && Ee(t) === e : t.disabled === e : "label" in t && t.disabled === e
            }
        }

        function c(e) {
            return r(function(t) {
                return t = +t, r(function(n, r) {
                    for (var i, o = e([], n.length, t), a = o.length; a--;) n[i = o[a]] && (n[i] = !(r[i] = n[i]))
                })
            })
        }

        function f(e) {
            return e && "undefined" != typeof e.getElementsByTagName && e
        }

        function p() {}

        function d(e) {
            for (var t = 0, n = e.length, r = ""; t < n; t++) r += e[t].value;
            return r
        }

        function h(e, t, n) {
            var r = t.dir,
                i = t.next,
                o = i || r,
                a = n && "parentNode" === o,
                s = _++;
            return t.first ? function(t, n, i) {
                for (; t = t[r];)
                    if (1 === t.nodeType || a) return e(t, n, i);
                return !1
            } : function(t, n, u) {
                var l, c, f, p = [F, s];
                if (u) {
                    for (; t = t[r];)
                        if ((1 === t.nodeType || a) && e(t, n, u)) return !0
                } else
                    for (; t = t[r];)
                        if (1 === t.nodeType || a)
                            if (f = t[$] || (t[$] = {}), c = f[t.uniqueID] || (f[t.uniqueID] = {}), i && i === t.nodeName.toLowerCase()) t = t[r] || t;
                            else {
                                if ((l = c[o]) && l[0] === F && l[1] === s) return p[2] = l[2];
                                if (c[o] = p, p[2] = e(t, n, u)) return !0
                            } return !1
            }
        }

        function g(e) {
            return e.length > 1 ? function(t, n, r) {
                for (var i = e.length; i--;)
                    if (!e[i](t, n, r)) return !1;
                return !0
            } : e[0]
        }

        function y(e, n, r) {
            for (var i = 0, o = n.length; i < o; i++) t(e, n[i], r);
            return r
        }

        function v(e, t, n, r, i) {
            for (var o, a = [], s = 0, u = e.length, l = null != t; s < u; s++)(o = e[s]) && (n && !n(o, r, i) || (a.push(o), l && t.push(s)));
            return a
        }

        function m(e, t, n, i, o, a) {
            return i && !i[$] && (i = m(i)), o && !o[$] && (o = m(o, a)), r(function(r, a, s, u) {
                var l, c, f, p = [],
                    d = [],
                    h = a.length,
                    g = r || y(t || "*", s.nodeType ? [s] : s, []),
                    m = !e || !r && t ? g : v(g, p, e, s, u),
                    x = n ? o || (r ? e : h || i) ? [] : a : m;
                if (n && n(m, x, s, u), i)
                    for (l = v(x, d), i(l, [], s, u), c = l.length; c--;)(f = l[c]) && (x[d[c]] = !(m[d[c]] = f));
                if (r) {
                    if (o || e) {
                        if (o) {
                            for (l = [], c = x.length; c--;)(f = x[c]) && l.push(m[c] = f);
                            o(null, x = [], l, u)
                        }
                        for (c = x.length; c--;)(f = x[c]) && (l = o ? ee(r, f) : p[c]) > -1 && (r[l] = !(a[l] = f))
                    }
                } else x = v(x === a ? x.splice(h, x.length) : x), o ? o(null, a, x, u) : K.apply(a, x)
            })
        }

        function x(e) {
            for (var t, n, r, i = e.length, o = C.relative[e[0].type], a = o || C.relative[" "], s = o ? 1 : 0, u = h(function(e) {
                    return e === t
                }, a, !0), l = h(function(e) {
                    return ee(t, e) > -1
                }, a, !0), c = [function(e, n, r) {
                    var i = !o && (r || n !== A) || ((t = n).nodeType ? u(e, n, r) : l(e, n, r));
                    return t = null, i
                }]; s < i; s++)
                if (n = C.relative[e[s].type]) c = [h(g(c), n)];
                else {
                    if ((n = C.filter[e[s].type].apply(null, e[s].matches))[$]) {
                        for (r = ++s; r < i && !C.relative[e[r].type]; r++);
                        return m(s > 1 && g(c), s > 1 && d(e.slice(0, s - 1).concat({
                            value: " " === e[s - 2].type ? "*" : ""
                        })).replace(se, "$1"), n, s < r && x(e.slice(s, r)), r < i && x(e = e.slice(r)), r < i && d(e))
                    }
                    c.push(n)
                }
            return g(c)
        }

        function b(e, n) {
            var i = n.length > 0,
                o = e.length > 0,
                a = function(r, a, s, u, l) {
                    var c, f, p, d = 0,
                        h = "0",
                        g = r && [],
                        y = [],
                        m = A,
                        x = r || o && C.find.TAG("*", l),
                        b = F += null == m ? 1 : Math.random() || .1,
                        w = x.length;
                    for (l && (A = a === H || a || l); h !== w && null != (c = x[h]); h++) {
                        if (o && c) {
                            for (f = 0, a || c.ownerDocument === H || (L(c), s = !P); p = e[f++];)
                                if (p(c, a || H, s)) {
                                    u.push(c);
                                    break
                                }
                            l && (F = b)
                        }
                        i && ((c = !p && c) && d--, r && g.push(c))
                    }
                    if (d += h, i && h !== d) {
                        for (f = 0; p = n[f++];) p(g, y, a, s);
                        if (r) {
                            if (d > 0)
                                for (; h--;) g[h] || y[h] || (y[h] = Q.call(u));
                            y = v(y)
                        }
                        K.apply(u, y), l && !r && y.length > 0 && d + n.length > 1 && t.uniqueSort(u)
                    }
                    return l && (F = b, A = m), g
                };
            return i ? r(a) : a
        }
        var w, T, C, E, k, S, D, N, A, j, q, L, H, O, P, M, R, I, W, $ = "sizzle" + 1 * new Date,
            B = e.document,
            F = 0,
            _ = 0,
            z = n(),
            X = n(),
            U = n(),
            V = function(e, t) {
                return e === t && (q = !0), 0
            },
            G = {}.hasOwnProperty,
            Y = [],
            Q = Y.pop,
            J = Y.push,
            K = Y.push,
            Z = Y.slice,
            ee = function(e, t) {
                for (var n = 0, r = e.length; n < r; n++)
                    if (e[n] === t) return n;
                return -1
            },
            te = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
            ne = "[\\x20\\t\\r\\n\\f]",
            re = "(?:\\\\.|[\\w-]|[^\0-\\xa0])+",
            ie = "\\[" + ne + "*(" + re + ")(?:" + ne + "*([*^$|!~]?=)" + ne + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + re + "))|)" + ne + "*\\]",
            oe = ":(" + re + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + ie + ")*)|.*)\\)|)",
            ae = new RegExp(ne + "+", "g"),
            se = new RegExp("^" + ne + "+|((?:^|[^\\\\])(?:\\\\.)*)" + ne + "+$", "g"),
            ue = new RegExp("^" + ne + "*," + ne + "*"),
            le = new RegExp("^" + ne + "*([>+~]|" + ne + ")" + ne + "*"),
            ce = new RegExp("=" + ne + "*([^\\]'\"]*?)" + ne + "*\\]", "g"),
            fe = new RegExp(oe),
            pe = new RegExp("^" + re + "$"),
            de = {
                ID: new RegExp("^#(" + re + ")"),
                CLASS: new RegExp("^\\.(" + re + ")"),
                TAG: new RegExp("^(" + re + "|[*])"),
                ATTR: new RegExp("^" + ie),
                PSEUDO: new RegExp("^" + oe),
                CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + ne + "*(even|odd|(([+-]|)(\\d*)n|)" + ne + "*(?:([+-]|)" + ne + "*(\\d+)|))" + ne + "*\\)|)", "i"),
                bool: new RegExp("^(?:" + te + ")$", "i"),
                needsContext: new RegExp("^" + ne + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + ne + "*((?:-\\d)?\\d*)" + ne + "*\\)|)(?=[^-]|$)", "i")
            },
            he = /^(?:input|select|textarea|button)$/i,
            ge = /^h\d$/i,
            ye = /^[^{]+\{\s*\[native \w/,
            ve = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
            me = /[+~]/,
            xe = new RegExp("\\\\([\\da-f]{1,6}" + ne + "?|(" + ne + ")|.)", "ig"),
            be = function(e, t, n) {
                var r = "0x" + t - 65536;
                return r !== r || n ? t : r < 0 ? String.fromCharCode(r + 65536) : String.fromCharCode(r >> 10 | 55296, 1023 & r | 56320)
            },
            we = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
            Te = function(e, t) {
                return t ? "\0" === e ? "�" : e.slice(0, -1) + "\\" + e.charCodeAt(e.length - 1).toString(16) + " " : "\\" + e
            },
            Ce = function() {
                L()
            },
            Ee = h(function(e) {
                return !0 === e.disabled && ("form" in e || "label" in e)
            }, {
                dir: "parentNode",
                next: "legend"
            });
        try {
            K.apply(Y = Z.call(B.childNodes), B.childNodes), Y[B.childNodes.length].nodeType
        } catch (e) {
            K = {
                apply: Y.length ? function(e, t) {
                    J.apply(e, Z.call(t))
                } : function(e, t) {
                    for (var n = e.length, r = 0; e[n++] = t[r++];);
                    e.length = n - 1
                }
            }
        }
        T = t.support = {}, k = t.isXML = function(e) {
            var t = e && (e.ownerDocument || e).documentElement;
            return !!t && "HTML" !== t.nodeName
        }, L = t.setDocument = function(e) {
            var t, n, r = e ? e.ownerDocument || e : B;
            return r !== H && 9 === r.nodeType && r.documentElement ? (H = r, O = H.documentElement, P = !k(H), B !== H && (n = H.defaultView) && n.top !== n && (n.addEventListener ? n.addEventListener("unload", Ce, !1) : n.attachEvent && n.attachEvent("onunload", Ce)), T.attributes = i(function(e) {
                return e.className = "i", !e.getAttribute("className")
            }), T.getElementsByTagName = i(function(e) {
                return e.appendChild(H.createComment("")), !e.getElementsByTagName("*").length
            }), T.getElementsByClassName = ye.test(H.getElementsByClassName), T.getById = i(function(e) {
                return O.appendChild(e).id = $, !H.getElementsByName || !H.getElementsByName($).length
            }), T.getById ? (C.filter.ID = function(e) {
                var t = e.replace(xe, be);
                return function(e) {
                    return e.getAttribute("id") === t
                }
            }, C.find.ID = function(e, t) {
                if ("undefined" != typeof t.getElementById && P) {
                    var n = t.getElementById(e);
                    return n ? [n] : []
                }
            }) : (C.filter.ID = function(e) {
                var t = e.replace(xe, be);
                return function(e) {
                    var n = "undefined" != typeof e.getAttributeNode && e.getAttributeNode("id");
                    return n && n.value === t
                }
            }, C.find.ID = function(e, t) {
                if ("undefined" != typeof t.getElementById && P) {
                    var n, r, i, o = t.getElementById(e);
                    if (o) {
                        if ((n = o.getAttributeNode("id")) && n.value === e) return [o];
                        for (i = t.getElementsByName(e), r = 0; o = i[r++];)
                            if ((n = o.getAttributeNode("id")) && n.value === e) return [o]
                    }
                    return []
                }
            }), C.find.TAG = T.getElementsByTagName ? function(e, t) {
                return "undefined" != typeof t.getElementsByTagName ? t.getElementsByTagName(e) : T.qsa ? t.querySelectorAll(e) : void 0
            } : function(e, t) {
                var n, r = [],
                    i = 0,
                    o = t.getElementsByTagName(e);
                if ("*" === e) {
                    for (; n = o[i++];) 1 === n.nodeType && r.push(n);
                    return r
                }
                return o
            }, C.find.CLASS = T.getElementsByClassName && function(e, t) {
                if ("undefined" != typeof t.getElementsByClassName && P) return t.getElementsByClassName(e)
            }, R = [], M = [], (T.qsa = ye.test(H.querySelectorAll)) && (i(function(e) {
                O.appendChild(e).innerHTML = "<a id='" + $ + "'></a><select id='" + $ + "-\r\\' msallowcapture=''><option selected=''></option></select>", e.querySelectorAll("[msallowcapture^='']").length && M.push("[*^$]=" + ne + "*(?:''|\"\")"), e.querySelectorAll("[selected]").length || M.push("\\[" + ne + "*(?:value|" + te + ")"), e.querySelectorAll("[id~=" + $ + "-]").length || M.push("~="), e.querySelectorAll(":checked").length || M.push(":checked"), e.querySelectorAll("a#" + $ + "+*").length || M.push(".#.+[+~]")
            }), i(function(e) {
                e.innerHTML = "<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";
                var t = H.createElement("input");
                t.setAttribute("type", "hidden"), e.appendChild(t).setAttribute("name", "D"), e.querySelectorAll("[name=d]").length && M.push("name" + ne + "*[*^$|!~]?="), 2 !== e.querySelectorAll(":enabled").length && M.push(":enabled", ":disabled"), O.appendChild(e).disabled = !0, 2 !== e.querySelectorAll(":disabled").length && M.push(":enabled", ":disabled"), e.querySelectorAll("*,:x"), M.push(",.*:")
            })), (T.matchesSelector = ye.test(I = O.matches || O.webkitMatchesSelector || O.mozMatchesSelector || O.oMatchesSelector || O.msMatchesSelector)) && i(function(e) {
                T.disconnectedMatch = I.call(e, "*"), I.call(e, "[s!='']:x"), R.push("!=", oe)
            }), M = M.length && new RegExp(M.join("|")), R = R.length && new RegExp(R.join("|")), t = ye.test(O.compareDocumentPosition), W = t || ye.test(O.contains) ? function(e, t) {
                var n = 9 === e.nodeType ? e.documentElement : e,
                    r = t && t.parentNode;
                return e === r || !(!r || 1 !== r.nodeType || !(n.contains ? n.contains(r) : e.compareDocumentPosition && 16 & e.compareDocumentPosition(r)))
            } : function(e, t) {
                if (t)
                    for (; t = t.parentNode;)
                        if (t === e) return !0;
                return !1
            }, V = t ? function(e, t) {
                if (e === t) return q = !0, 0;
                var n = !e.compareDocumentPosition - !t.compareDocumentPosition;
                return n || (1 & (n = (e.ownerDocument || e) === (t.ownerDocument || t) ? e.compareDocumentPosition(t) : 1) || !T.sortDetached && t.compareDocumentPosition(e) === n ? e === H || e.ownerDocument === B && W(B, e) ? -1 : t === H || t.ownerDocument === B && W(B, t) ? 1 : j ? ee(j, e) - ee(j, t) : 0 : 4 & n ? -1 : 1)
            } : function(e, t) {
                if (e === t) return q = !0, 0;
                var n, r = 0,
                    i = e.parentNode,
                    o = t.parentNode,
                    s = [e],
                    u = [t];
                if (!i || !o) return e === H ? -1 : t === H ? 1 : i ? -1 : o ? 1 : j ? ee(j, e) - ee(j, t) : 0;
                if (i === o) return a(e, t);
                for (n = e; n = n.parentNode;) s.unshift(n);
                for (n = t; n = n.parentNode;) u.unshift(n);
                for (; s[r] === u[r];) r++;
                return r ? a(s[r], u[r]) : s[r] === B ? -1 : u[r] === B ? 1 : 0
            }, H) : H
        }, t.matches = function(e, n) {
            return t(e, null, null, n)
        }, t.matchesSelector = function(e, n) {
            if ((e.ownerDocument || e) !== H && L(e), n = n.replace(ce, "='$1']"), T.matchesSelector && P && !U[n + " "] && (!R || !R.test(n)) && (!M || !M.test(n))) try {
                var r = I.call(e, n);
                if (r || T.disconnectedMatch || e.document && 11 !== e.document.nodeType) return r
            } catch (e) {}
            return t(n, H, null, [e]).length > 0
        }, t.contains = function(e, t) {
            return (e.ownerDocument || e) !== H && L(e), W(e, t)
        }, t.attr = function(e, t) {
            (e.ownerDocument || e) !== H && L(e);
            var n = C.attrHandle[t.toLowerCase()],
                r = n && G.call(C.attrHandle, t.toLowerCase()) ? n(e, t, !P) : void 0;
            return void 0 !== r ? r : T.attributes || !P ? e.getAttribute(t) : (r = e.getAttributeNode(t)) && r.specified ? r.value : null
        }, t.escape = function(e) {
            return (e + "").replace(we, Te)
        }, t.error = function(e) {
            throw new Error("Syntax error, unrecognized expression: " + e)
        }, t.uniqueSort = function(e) {
            var t, n = [],
                r = 0,
                i = 0;
            if (q = !T.detectDuplicates, j = !T.sortStable && e.slice(0), e.sort(V), q) {
                for (; t = e[i++];) t === e[i] && (r = n.push(i));
                for (; r--;) e.splice(n[r], 1)
            }
            return j = null, e
        }, E = t.getText = function(e) {
            var t, n = "",
                r = 0,
                i = e.nodeType;
            if (i) {
                if (1 === i || 9 === i || 11 === i) {
                    if ("string" == typeof e.textContent) return e.textContent;
                    for (e = e.firstChild; e; e = e.nextSibling) n += E(e)
                } else if (3 === i || 4 === i) return e.nodeValue
            } else
                for (; t = e[r++];) n += E(t);
            return n
        }, (C = t.selectors = {
            cacheLength: 50,
            createPseudo: r,
            match: de,
            attrHandle: {},
            find: {},
            relative: {
                ">": {
                    dir: "parentNode",
                    first: !0
                },
                " ": {
                    dir: "parentNode"
                },
                "+": {
                    dir: "previousSibling",
                    first: !0
                },
                "~": {
                    dir: "previousSibling"
                }
            },
            preFilter: {
                ATTR: function(e) {
                    return e[1] = e[1].replace(xe, be), e[3] = (e[3] || e[4] || e[5] || "").replace(xe, be), "~=" === e[2] && (e[3] = " " + e[3] + " "), e.slice(0, 4)
                },
                CHILD: function(e) {
                    return e[1] = e[1].toLowerCase(), "nth" === e[1].slice(0, 3) ? (e[3] || t.error(e[0]), e[4] = +(e[4] ? e[5] + (e[6] || 1) : 2 * ("even" === e[3] || "odd" === e[3])), e[5] = +(e[7] + e[8] || "odd" === e[3])) : e[3] && t.error(e[0]), e
                },
                PSEUDO: function(e) {
                    var t, n = !e[6] && e[2];
                    return de.CHILD.test(e[0]) ? null : (e[3] ? e[2] = e[4] || e[5] || "" : n && fe.test(n) && (t = S(n, !0)) && (t = n.indexOf(")", n.length - t) - n.length) && (e[0] = e[0].slice(0, t), e[2] = n.slice(0, t)), e.slice(0, 3))
                }
            },
            filter: {
                TAG: function(e) {
                    var t = e.replace(xe, be).toLowerCase();
                    return "*" === e ? function() {
                        return !0
                    } : function(e) {
                        return e.nodeName && e.nodeName.toLowerCase() === t
                    }
                },
                CLASS: function(e) {
                    var t = z[e + " "];
                    return t || (t = new RegExp("(^|" + ne + ")" + e + "(" + ne + "|$)")) && z(e, function(e) {
                        return t.test("string" == typeof e.className && e.className || "undefined" != typeof e.getAttribute && e.getAttribute("class") || "")
                    })
                },
                ATTR: function(e, n, r) {
                    return function(i) {
                        var o = t.attr(i, e);
                        return null == o ? "!=" === n : !n || (o += "", "=" === n ? o === r : "!=" === n ? o !== r : "^=" === n ? r && 0 === o.indexOf(r) : "*=" === n ? r && o.indexOf(r) > -1 : "$=" === n ? r && o.slice(-r.length) === r : "~=" === n ? (" " + o.replace(ae, " ") + " ").indexOf(r) > -1 : "|=" === n && (o === r || o.slice(0, r.length + 1) === r + "-"))
                    }
                },
                CHILD: function(e, t, n, r, i) {
                    var o = "nth" !== e.slice(0, 3),
                        a = "last" !== e.slice(-4),
                        s = "of-type" === t;
                    return 1 === r && 0 === i ? function(e) {
                        return !!e.parentNode
                    } : function(t, n, u) {
                        var l, c, f, p, d, h, g = o !== a ? "nextSibling" : "previousSibling",
                            y = t.parentNode,
                            v = s && t.nodeName.toLowerCase(),
                            m = !u && !s,
                            x = !1;
                        if (y) {
                            if (o) {
                                for (; g;) {
                                    for (p = t; p = p[g];)
                                        if (s ? p.nodeName.toLowerCase() === v : 1 === p.nodeType) return !1;
                                    h = g = "only" === e && !h && "nextSibling"
                                }
                                return !0
                            }
                            if (h = [a ? y.firstChild : y.lastChild], a && m) {
                                for (x = (d = (l = (c = (f = (p = y)[$] || (p[$] = {}))[p.uniqueID] || (f[p.uniqueID] = {}))[e] || [])[0] === F && l[1]) && l[2], p = d && y.childNodes[d]; p = ++d && p && p[g] || (x = d = 0) || h.pop();)
                                    if (1 === p.nodeType && ++x && p === t) {
                                        c[e] = [F, d, x];
                                        break
                                    }
                            } else if (m && (x = d = (l = (c = (f = (p = t)[$] || (p[$] = {}))[p.uniqueID] || (f[p.uniqueID] = {}))[e] || [])[0] === F && l[1]), !1 === x)
                                for (;
                                    (p = ++d && p && p[g] || (x = d = 0) || h.pop()) && ((s ? p.nodeName.toLowerCase() !== v : 1 !== p.nodeType) || !++x || (m && ((c = (f = p[$] || (p[$] = {}))[p.uniqueID] || (f[p.uniqueID] = {}))[e] = [F, x]), p !== t)););
                            return (x -= i) === r || x % r == 0 && x / r >= 0
                        }
                    }
                },
                PSEUDO: function(e, n) {
                    var i, o = C.pseudos[e] || C.setFilters[e.toLowerCase()] || t.error("unsupported pseudo: " + e);
                    return o[$] ? o(n) : o.length > 1 ? (i = [e, e, "", n], C.setFilters.hasOwnProperty(e.toLowerCase()) ? r(function(e, t) {
                        for (var r, i = o(e, n), a = i.length; a--;) e[r = ee(e, i[a])] = !(t[r] = i[a])
                    }) : function(e) {
                        return o(e, 0, i)
                    }) : o
                }
            },
            pseudos: {
                not: r(function(e) {
                    var t = [],
                        n = [],
                        i = D(e.replace(se, "$1"));
                    return i[$] ? r(function(e, t, n, r) {
                        for (var o, a = i(e, null, r, []), s = e.length; s--;)(o = a[s]) && (e[s] = !(t[s] = o))
                    }) : function(e, r, o) {
                        return t[0] = e, i(t, null, o, n), t[0] = null, !n.pop()
                    }
                }),
                has: r(function(e) {
                    return function(n) {
                        return t(e, n).length > 0
                    }
                }),
                contains: r(function(e) {
                    return e = e.replace(xe, be),
                        function(t) {
                            return (t.textContent || t.innerText || E(t)).indexOf(e) > -1
                        }
                }),
                lang: r(function(e) {
                    return pe.test(e || "") || t.error("unsupported lang: " + e), e = e.replace(xe, be).toLowerCase(),
                        function(t) {
                            var n;
                            do
                                if (n = P ? t.lang : t.getAttribute("xml:lang") || t.getAttribute("lang")) return (n = n.toLowerCase()) === e || 0 === n.indexOf(e + "-"); while ((t = t.parentNode) && 1 === t.nodeType);
                            return !1
                        }
                }),
                target: function(t) {
                    var n = e.location && e.location.hash;
                    return n && n.slice(1) === t.id
                },
                root: function(e) {
                    return e === O
                },
                focus: function(e) {
                    return e === H.activeElement && (!H.hasFocus || H.hasFocus()) && !!(e.type || e.href || ~e.tabIndex);
                },
                enabled: l(!1),
                disabled: l(!0),
                checked: function(e) {
                    var t = e.nodeName.toLowerCase();
                    return "input" === t && !!e.checked || "option" === t && !!e.selected
                },
                selected: function(e) {
                    return e.parentNode && e.parentNode.selectedIndex, !0 === e.selected
                },
                empty: function(e) {
                    for (e = e.firstChild; e; e = e.nextSibling)
                        if (e.nodeType < 6) return !1;
                    return !0
                },
                parent: function(e) {
                    return !C.pseudos.empty(e)
                },
                header: function(e) {
                    return ge.test(e.nodeName)
                },
                input: function(e) {
                    return he.test(e.nodeName)
                },
                button: function(e) {
                    var t = e.nodeName.toLowerCase();
                    return "input" === t && "button" === e.type || "button" === t
                },
                text: function(e) {
                    var t;
                    return "input" === e.nodeName.toLowerCase() && "text" === e.type && (null == (t = e.getAttribute("type")) || "text" === t.toLowerCase())
                },
                first: c(function() {
                    return [0]
                }),
                last: c(function(e, t) {
                    return [t - 1]
                }),
                eq: c(function(e, t, n) {
                    return [n < 0 ? n + t : n]
                }),
                even: c(function(e, t) {
                    for (var n = 0; n < t; n += 2) e.push(n);
                    return e
                }),
                odd: c(function(e, t) {
                    for (var n = 1; n < t; n += 2) e.push(n);
                    return e
                }),
                lt: c(function(e, t, n) {
                    for (var r = n < 0 ? n + t : n; --r >= 0;) e.push(r);
                    return e
                }),
                gt: c(function(e, t, n) {
                    for (var r = n < 0 ? n + t : n; ++r < t;) e.push(r);
                    return e
                })
            }
        }).pseudos.nth = C.pseudos.eq;
        for (w in {
                radio: !0,
                checkbox: !0,
                file: !0,
                password: !0,
                image: !0
            }) C.pseudos[w] = s(w);
        for (w in {
                submit: !0,
                reset: !0
            }) C.pseudos[w] = u(w);
        return p.prototype = C.filters = C.pseudos, C.setFilters = new p, S = t.tokenize = function(e, n) {
            var r, i, o, a, s, u, l, c = X[e + " "];
            if (c) return n ? 0 : c.slice(0);
            for (s = e, u = [], l = C.preFilter; s;) {
                r && !(i = ue.exec(s)) || (i && (s = s.slice(i[0].length) || s), u.push(o = [])), r = !1, (i = le.exec(s)) && (r = i.shift(), o.push({
                    value: r,
                    type: i[0].replace(se, " ")
                }), s = s.slice(r.length));
                for (a in C.filter) !(i = de[a].exec(s)) || l[a] && !(i = l[a](i)) || (r = i.shift(), o.push({
                    value: r,
                    type: a,
                    matches: i
                }), s = s.slice(r.length));
                if (!r) break
            }
            return n ? s.length : s ? t.error(e) : X(e, u).slice(0)
        }, D = t.compile = function(e, t) {
            var n, r = [],
                i = [],
                o = U[e + " "];
            if (!o) {
                for (t || (t = S(e)), n = t.length; n--;)(o = x(t[n]))[$] ? r.push(o) : i.push(o);
                (o = U(e, b(i, r))).selector = e
            }
            return o
        }, N = t.select = function(e, t, n, r) {
            var i, o, a, s, u, l = "function" == typeof e && e,
                c = !r && S(e = l.selector || e);
            if (n = n || [], 1 === c.length) {
                if ((o = c[0] = c[0].slice(0)).length > 2 && "ID" === (a = o[0]).type && 9 === t.nodeType && P && C.relative[o[1].type]) {
                    if (!(t = (C.find.ID(a.matches[0].replace(xe, be), t) || [])[0])) return n;
                    l && (t = t.parentNode), e = e.slice(o.shift().value.length)
                }
                for (i = de.needsContext.test(e) ? 0 : o.length; i-- && (a = o[i], !C.relative[s = a.type]);)
                    if ((u = C.find[s]) && (r = u(a.matches[0].replace(xe, be), me.test(o[0].type) && f(t.parentNode) || t))) {
                        if (o.splice(i, 1), !(e = r.length && d(o))) return K.apply(n, r), n;
                        break
                    }
            }
            return (l || D(e, c))(r, t, !P, n, !t || me.test(e) && f(t.parentNode) || t), n
        }, T.sortStable = $.split("").sort(V).join("") === $, T.detectDuplicates = !!q, L(), T.sortDetached = i(function(e) {
            return 1 & e.compareDocumentPosition(H.createElement("fieldset"))
        }), i(function(e) {
            return e.innerHTML = "<a href='#'></a>", "#" === e.firstChild.getAttribute("href")
        }) || o("type|href|height|width", function(e, t, n) {
            if (!n) return e.getAttribute(t, "type" === t.toLowerCase() ? 1 : 2)
        }), T.attributes && i(function(e) {
            return e.innerHTML = "<input/>", e.firstChild.setAttribute("value", ""), "" === e.firstChild.getAttribute("value")
        }) || o("value", function(e, t, n) {
            if (!n && "input" === e.nodeName.toLowerCase()) return e.defaultValue
        }), i(function(e) {
            return null == e.getAttribute("disabled")
        }) || o(te, function(e, t, n) {
            var r;
            if (!n) return !0 === e[t] ? t.toLowerCase() : (r = e.getAttributeNode(t)) && r.specified ? r.value : null
        }), t
    }(e);
    we.find = Ce, we.expr = Ce.selectors, we.expr[":"] = we.expr.pseudos, we.uniqueSort = we.unique = Ce.uniqueSort, we.text = Ce.getText, we.isXMLDoc = Ce.isXML, we.contains = Ce.contains, we.escapeSelector = Ce.escape;
    var Ee = function(e, t, n) {
            for (var r = [], i = void 0 !== n;
                (e = e[t]) && 9 !== e.nodeType;)
                if (1 === e.nodeType) {
                    if (i && we(e).is(n)) break;
                    r.push(e)
                }
            return r
        },
        ke = function(e, t) {
            for (var n = []; e; e = e.nextSibling) 1 === e.nodeType && e !== t && n.push(e);
            return n
        },
        Se = we.expr.match.needsContext,
        De = /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i;
    we.filter = function(e, t, n) {
        var r = t[0];
        return n && (e = ":not(" + e + ")"), 1 === t.length && 1 === r.nodeType ? we.find.matchesSelector(r, e) ? [r] : [] : we.find.matches(e, we.grep(t, function(e) {
            return 1 === e.nodeType
        }))
    }, we.fn.extend({
        find: function(e) {
            var t, n, r = this.length,
                i = this;
            if ("string" != typeof e) return this.pushStack(we(e).filter(function() {
                for (t = 0; t < r; t++)
                    if (we.contains(i[t], this)) return !0
            }));
            for (n = this.pushStack([]), t = 0; t < r; t++) we.find(e, i[t], n);
            return r > 1 ? we.uniqueSort(n) : n
        },
        filter: function(e) {
            return this.pushStack(a(this, e || [], !1))
        },
        not: function(e) {
            return this.pushStack(a(this, e || [], !0))
        },
        is: function(e) {
            return !!a(this, "string" == typeof e && Se.test(e) ? we(e) : e || [], !1).length
        }
    });
    var Ne, Ae = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/;
    (we.fn.init = function(e, t, n) {
        var r, i;
        if (!e) return this;
        if (n = n || Ne, "string" == typeof e) {
            if (!(r = "<" === e[0] && ">" === e[e.length - 1] && e.length >= 3 ? [null, e, null] : Ae.exec(e)) || !r[1] && t) return !t || t.jquery ? (t || n).find(e) : this.constructor(t).find(e);
            if (r[1]) {
                if (t = t instanceof we ? t[0] : t, we.merge(this, we.parseHTML(r[1], t && t.nodeType ? t.ownerDocument || t : ae, !0)), De.test(r[1]) && we.isPlainObject(t))
                    for (r in t) me(this[r]) ? this[r](t[r]) : this.attr(r, t[r]);
                return this
            }
            return (i = ae.getElementById(r[2])) && (this[0] = i, this.length = 1), this
        }
        return e.nodeType ? (this[0] = e, this.length = 1, this) : me(e) ? void 0 !== n.ready ? n.ready(e) : e(we) : we.makeArray(e, this)
    }).prototype = we.fn, Ne = we(ae);
    var je = /^(?:parents|prev(?:Until|All))/,
        qe = {
            children: !0,
            contents: !0,
            next: !0,
            prev: !0
        };
    we.fn.extend({
        has: function(e) {
            var t = we(e, this),
                n = t.length;
            return this.filter(function() {
                for (var e = 0; e < n; e++)
                    if (we.contains(this, t[e])) return !0
            })
        },
        closest: function(e, t) {
            var n, r = 0,
                i = this.length,
                o = [],
                a = "string" != typeof e && we(e);
            if (!Se.test(e))
                for (; r < i; r++)
                    for (n = this[r]; n && n !== t; n = n.parentNode)
                        if (n.nodeType < 11 && (a ? a.index(n) > -1 : 1 === n.nodeType && we.find.matchesSelector(n, e))) {
                            o.push(n);
                            break
                        }
            return this.pushStack(o.length > 1 ? we.uniqueSort(o) : o)
        },
        index: function(e) {
            return e ? "string" == typeof e ? fe.call(we(e), this[0]) : fe.call(this, e.jquery ? e[0] : e) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
        },
        add: function(e, t) {
            return this.pushStack(we.uniqueSort(we.merge(this.get(), we(e, t))))
        },
        addBack: function(e) {
            return this.add(null == e ? this.prevObject : this.prevObject.filter(e))
        }
    }), we.each({
        parent: function(e) {
            var t = e.parentNode;
            return t && 11 !== t.nodeType ? t : null
        },
        parents: function(e) {
            return Ee(e, "parentNode")
        },
        parentsUntil: function(e, t, n) {
            return Ee(e, "parentNode", n)
        },
        next: function(e) {
            return s(e, "nextSibling")
        },
        prev: function(e) {
            return s(e, "previousSibling")
        },
        nextAll: function(e) {
            return Ee(e, "nextSibling")
        },
        prevAll: function(e) {
            return Ee(e, "previousSibling")
        },
        nextUntil: function(e, t, n) {
            return Ee(e, "nextSibling", n)
        },
        prevUntil: function(e, t, n) {
            return Ee(e, "previousSibling", n)
        },
        siblings: function(e) {
            return ke((e.parentNode || {}).firstChild, e)
        },
        children: function(e) {
            return ke(e.firstChild)
        },
        contents: function(e) {
            return o(e, "iframe") ? e.contentDocument : (o(e, "template") && (e = e.content || e), we.merge([], e.childNodes))
        }
    }, function(e, t) {
        we.fn[e] = function(n, r) {
            var i = we.map(this, t, n);
            return "Until" !== e.slice(-5) && (r = n), r && "string" == typeof r && (i = we.filter(r, i)), this.length > 1 && (qe[e] || we.uniqueSort(i), je.test(e) && i.reverse()), this.pushStack(i)
        }
    });
    var Le = /[^\x20\t\r\n\f]+/g;
    we.Callbacks = function(e) {
        e = "string" == typeof e ? u(e) : we.extend({}, e);
        var t, n, i, o, a = [],
            s = [],
            l = -1,
            c = function() {
                for (o = o || e.once, i = t = !0; s.length; l = -1)
                    for (n = s.shift(); ++l < a.length;) !1 === a[l].apply(n[0], n[1]) && e.stopOnFalse && (l = a.length, n = !1);
                e.memory || (n = !1), t = !1, o && (a = n ? [] : "")
            },
            f = {
                add: function() {
                    return a && (n && !t && (l = a.length - 1, s.push(n)), function i(t) {
                        we.each(t, function(t, n) {
                            me(n) ? e.unique && f.has(n) || a.push(n) : n && n.length && "string" !== r(n) && i(n)
                        })
                    }(arguments), n && !t && c()), this
                },
                remove: function() {
                    return we.each(arguments, function(e, t) {
                        for (var n;
                            (n = we.inArray(t, a, n)) > -1;) a.splice(n, 1), n <= l && l--
                    }), this
                },
                has: function(e) {
                    return e ? we.inArray(e, a) > -1 : a.length > 0
                },
                empty: function() {
                    return a && (a = []), this
                },
                disable: function() {
                    return o = s = [], a = n = "", this
                },
                disabled: function() {
                    return !a
                },
                lock: function() {
                    return o = s = [], n || t || (a = n = ""), this
                },
                locked: function() {
                    return !!o
                },
                fireWith: function(e, n) {
                    return o || (n = [e, (n = n || []).slice ? n.slice() : n], s.push(n), t || c()), this
                },
                fire: function() {
                    return f.fireWith(this, arguments), this
                },
                fired: function() {
                    return !!i
                }
            };
        return f
    }, we.extend({
        Deferred: function(t) {
            var n = [
                    ["notify", "progress", we.Callbacks("memory"), we.Callbacks("memory"), 2],
                    ["resolve", "done", we.Callbacks("once memory"), we.Callbacks("once memory"), 0, "resolved"],
                    ["reject", "fail", we.Callbacks("once memory"), we.Callbacks("once memory"), 1, "rejected"]
                ],
                r = "pending",
                i = {
                    state: function() {
                        return r
                    },
                    always: function() {
                        return o.done(arguments).fail(arguments), this
                    },
                    "catch": function(e) {
                        return i.then(null, e)
                    },
                    pipe: function() {
                        var e = arguments;
                        return we.Deferred(function(t) {
                            we.each(n, function(n, r) {
                                var i = me(e[r[4]]) && e[r[4]];
                                o[r[1]](function() {
                                    var e = i && i.apply(this, arguments);
                                    e && me(e.promise) ? e.promise().progress(t.notify).done(t.resolve).fail(t.reject) : t[r[0] + "With"](this, i ? [e] : arguments)
                                })
                            }), e = null
                        }).promise()
                    },
                    then: function(t, r, i) {
                        function o(t, n, r, i) {
                            return function() {
                                var s = this,
                                    u = arguments,
                                    f = function() {
                                        var e, f;
                                        if (!(t < a)) {
                                            if ((e = r.apply(s, u)) === n.promise()) throw new TypeError("Thenable self-resolution");
                                            f = e && ("object" == typeof e || "function" == typeof e) && e.then, me(f) ? i ? f.call(e, o(a, n, l, i), o(a, n, c, i)) : (a++, f.call(e, o(a, n, l, i), o(a, n, c, i), o(a, n, l, n.notifyWith))) : (r !== l && (s = void 0, u = [e]), (i || n.resolveWith)(s, u))
                                        }
                                    },
                                    p = i ? f : function() {
                                        try {
                                            f()
                                        } catch (e) {
                                            we.Deferred.exceptionHook && we.Deferred.exceptionHook(e, p.stackTrace), t + 1 >= a && (r !== c && (s = void 0, u = [e]), n.rejectWith(s, u))
                                        }
                                    };
                                t ? p() : (we.Deferred.getStackHook && (p.stackTrace = we.Deferred.getStackHook()), e.setTimeout(p))
                            }
                        }
                        var a = 0;
                        return we.Deferred(function(e) {
                            n[0][3].add(o(0, e, me(i) ? i : l, e.notifyWith)), n[1][3].add(o(0, e, me(t) ? t : l)), n[2][3].add(o(0, e, me(r) ? r : c))
                        }).promise()
                    },
                    promise: function(e) {
                        return null != e ? we.extend(e, i) : i
                    }
                },
                o = {};
            return we.each(n, function(e, t) {
                var a = t[2],
                    s = t[5];
                i[t[1]] = a.add, s && a.add(function() {
                    r = s
                }, n[3 - e][2].disable, n[3 - e][3].disable, n[0][2].lock, n[0][3].lock), a.add(t[3].fire), o[t[0]] = function() {
                    return o[t[0] + "With"](this === o ? void 0 : this, arguments), this
                }, o[t[0] + "With"] = a.fireWith
            }), i.promise(o), t && t.call(o, o), o
        },
        when: function(e) {
            var t = arguments.length,
                n = t,
                r = Array(n),
                i = ue.call(arguments),
                o = we.Deferred(),
                a = function(e) {
                    return function(n) {
                        r[e] = this, i[e] = arguments.length > 1 ? ue.call(arguments) : n, --t || o.resolveWith(r, i)
                    }
                };
            if (t <= 1 && (f(e, o.done(a(n)).resolve, o.reject, !t), "pending" === o.state() || me(i[n] && i[n].then))) return o.then();
            for (; n--;) f(i[n], a(n), o.reject);
            return o.promise()
        }
    });
    var He = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
    we.Deferred.exceptionHook = function(t, n) {
        e.console && e.console.warn && t && He.test(t.name) && e.console.warn("jQuery.Deferred exception: " + t.message, t.stack, n)
    }, we.readyException = function(t) {
        e.setTimeout(function() {
            throw t
        })
    };
    var Oe = we.Deferred();
    we.fn.ready = function(e) {
        return Oe.then(e)["catch"](function(e) {
            we.readyException(e)
        }), this
    }, we.extend({
        isReady: !1,
        readyWait: 1,
        ready: function(e) {
            (!0 === e ? --we.readyWait : we.isReady) || (we.isReady = !0, !0 !== e && --we.readyWait > 0 || Oe.resolveWith(ae, [we]))
        }
    }), we.ready.then = Oe.then, "complete" === ae.readyState || "loading" !== ae.readyState && !ae.documentElement.doScroll ? e.setTimeout(we.ready) : (ae.addEventListener("DOMContentLoaded", p), e.addEventListener("load", p));
    var Pe = function(e, t, n, i, o, a, s) {
            var u = 0,
                l = e.length,
                c = null == n;
            if ("object" === r(n)) {
                o = !0;
                for (u in n) Pe(e, t, u, n[u], !0, a, s)
            } else if (void 0 !== i && (o = !0, me(i) || (s = !0), c && (s ? (t.call(e, i), t = null) : (c = t, t = function(e, t, n) {
                    return c.call(we(e), n)
                })), t))
                for (; u < l; u++) t(e[u], n, s ? i : i.call(e[u], u, t(e[u], n)));
            return o ? e : c ? t.call(e) : l ? t(e[0], n) : a
        },
        Me = /^-ms-/,
        Re = /-([a-z])/g,
        Ie = function(e) {
            return 1 === e.nodeType || 9 === e.nodeType || !+e.nodeType
        };
    g.uid = 1, g.prototype = {
        cache: function(e) {
            var t = e[this.expando];
            return t || (t = {}, Ie(e) && (e.nodeType ? e[this.expando] = t : Object.defineProperty(e, this.expando, {
                value: t,
                configurable: !0
            }))), t
        },
        set: function(e, t, n) {
            var r, i = this.cache(e);
            if ("string" == typeof t) i[h(t)] = n;
            else
                for (r in t) i[h(r)] = t[r];
            return i
        },
        get: function(e, t) {
            return void 0 === t ? this.cache(e) : e[this.expando] && e[this.expando][h(t)]
        },
        access: function(e, t, n) {
            return void 0 === t || t && "string" == typeof t && void 0 === n ? this.get(e, t) : (this.set(e, t, n), void 0 !== n ? n : t)
        },
        remove: function(e, t) {
            var n, r = e[this.expando];
            if (void 0 !== r) {
                if (void 0 !== t) {
                    n = (t = Array.isArray(t) ? t.map(h) : (t = h(t)) in r ? [t] : t.match(Le) || []).length;
                    for (; n--;) delete r[t[n]]
                }(void 0 === t || we.isEmptyObject(r)) && (e.nodeType ? e[this.expando] = void 0 : delete e[this.expando])
            }
        },
        hasData: function(e) {
            var t = e[this.expando];
            return void 0 !== t && !we.isEmptyObject(t)
        }
    };
    var We = new g,
        $e = new g,
        Be = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
        Fe = /[A-Z]/g;
    we.extend({
        hasData: function(e) {
            return $e.hasData(e) || We.hasData(e)
        },
        data: function(e, t, n) {
            return $e.access(e, t, n)
        },
        removeData: function(e, t) {
            $e.remove(e, t)
        },
        _data: function(e, t, n) {
            return We.access(e, t, n)
        },
        _removeData: function(e, t) {
            We.remove(e, t)
        }
    }), we.fn.extend({
        data: function(e, t) {
            var n, r, i, o = this[0],
                a = o && o.attributes;
            if (void 0 === e) {
                if (this.length && (i = $e.get(o), 1 === o.nodeType && !We.get(o, "hasDataAttrs"))) {
                    for (n = a.length; n--;) a[n] && 0 === (r = a[n].name).indexOf("data-") && (r = h(r.slice(5)), v(o, r, i[r]));
                    We.set(o, "hasDataAttrs", !0)
                }
                return i
            }
            return "object" == typeof e ? this.each(function() {
                $e.set(this, e)
            }) : Pe(this, function(t) {
                var n;
                if (o && void 0 === t) {
                    if (void 0 !== (n = $e.get(o, e))) return n;
                    if (void 0 !== (n = v(o, e))) return n
                } else this.each(function() {
                    $e.set(this, e, t)
                })
            }, null, t, arguments.length > 1, null, !0)
        },
        removeData: function(e) {
            return this.each(function() {
                $e.remove(this, e)
            })
        }
    }), we.extend({
        queue: function(e, t, n) {
            var r;
            if (e) return t = (t || "fx") + "queue", r = We.get(e, t), n && (!r || Array.isArray(n) ? r = We.access(e, t, we.makeArray(n)) : r.push(n)), r || []
        },
        dequeue: function(e, t) {
            t = t || "fx";
            var n = we.queue(e, t),
                r = n.length,
                i = n.shift(),
                o = we._queueHooks(e, t),
                a = function() {
                    we.dequeue(e, t)
                };
            "inprogress" === i && (i = n.shift(), r--), i && ("fx" === t && n.unshift("inprogress"), delete o.stop, i.call(e, a, o)), !r && o && o.empty.fire()
        },
        _queueHooks: function(e, t) {
            var n = t + "queueHooks";
            return We.get(e, n) || We.access(e, n, {
                empty: we.Callbacks("once memory").add(function() {
                    We.remove(e, [t + "queue", n])
                })
            })
        }
    }), we.fn.extend({
        queue: function(e, t) {
            var n = 2;
            return "string" != typeof e && (t = e, e = "fx", n--), arguments.length < n ? we.queue(this[0], e) : void 0 === t ? this : this.each(function() {
                var n = we.queue(this, e, t);
                we._queueHooks(this, e), "fx" === e && "inprogress" !== n[0] && we.dequeue(this, e)
            })
        },
        dequeue: function(e) {
            return this.each(function() {
                we.dequeue(this, e)
            })
        },
        clearQueue: function(e) {
            return this.queue(e || "fx", [])
        },
        promise: function(e, t) {
            var n, r = 1,
                i = we.Deferred(),
                o = this,
                a = this.length,
                s = function() {
                    --r || i.resolveWith(o, [o])
                };
            for ("string" != typeof e && (t = e, e = void 0), e = e || "fx"; a--;)(n = We.get(o[a], e + "queueHooks")) && n.empty && (r++, n.empty.add(s));
            return s(), i.promise(t)
        }
    });
    var _e = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
        ze = new RegExp("^(?:([+-])=|)(" + _e + ")([a-z%]*)$", "i"),
        Xe = ["Top", "Right", "Bottom", "Left"],
        Ue = function(e, t) {
            return "none" === (e = t || e).style.display || "" === e.style.display && we.contains(e.ownerDocument, e) && "none" === we.css(e, "display")
        },
        Ve = function(e, t, n, r) {
            var i, o, a = {};
            for (o in t) a[o] = e.style[o], e.style[o] = t[o];
            i = n.apply(e, r || []);
            for (o in t) e.style[o] = a[o];
            return i
        },
        Ge = {};
    we.fn.extend({
        show: function() {
            return b(this, !0)
        },
        hide: function() {
            return b(this)
        },
        toggle: function(e) {
            return "boolean" == typeof e ? e ? this.show() : this.hide() : this.each(function() {
                Ue(this) ? we(this).show() : we(this).hide()
            })
        }
    });
    var Ye = /^(?:checkbox|radio)$/i,
        Qe = /<([a-z][^\/\0>\x20\t\r\n\f]+)/i,
        Je = /^$|^module$|\/(?:java|ecma)script/i,
        Ke = {
            option: [1, "<select multiple='multiple'>", "</select>"],
            thead: [1, "<table>", "</table>"],
            col: [2, "<table><colgroup>", "</colgroup></table>"],
            tr: [2, "<table><tbody>", "</tbody></table>"],
            td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
            _default: [0, "", ""]
        };
    Ke.optgroup = Ke.option, Ke.tbody = Ke.tfoot = Ke.colgroup = Ke.caption = Ke.thead, Ke.th = Ke.td;
    var Ze = /<|&#?\w+;/;
    ! function() {
        var e = ae.createDocumentFragment().appendChild(ae.createElement("div")),
            t = ae.createElement("input");
        t.setAttribute("type", "radio"), t.setAttribute("checked", "checked"), t.setAttribute("name", "t"), e.appendChild(t), ve.checkClone = e.cloneNode(!0).cloneNode(!0).lastChild.checked, e.innerHTML = "<textarea>x</textarea>", ve.noCloneChecked = !!e.cloneNode(!0).lastChild.defaultValue
    }();
    var et = ae.documentElement,
        tt = /^key/,
        nt = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
        rt = /^([^.]*)(?:\.(.+)|)/;
    we.event = {
        global: {},
        add: function(e, t, n, r, i) {
            var o, a, s, u, l, c, f, p, d, h, g, y = We.get(e);
            if (y)
                for (n.handler && (n = (o = n).handler, i = o.selector), i && we.find.matchesSelector(et, i), n.guid || (n.guid = we.guid++), (u = y.events) || (u = y.events = {}), (a = y.handle) || (a = y.handle = function(t) {
                        return "undefined" != typeof we && we.event.triggered !== t.type ? we.event.dispatch.apply(e, arguments) : void 0
                    }), l = (t = (t || "").match(Le) || [""]).length; l--;) d = g = (s = rt.exec(t[l]) || [])[1], h = (s[2] || "").split(".").sort(), d && (f = we.event.special[d] || {}, d = (i ? f.delegateType : f.bindType) || d, f = we.event.special[d] || {}, c = we.extend({
                    type: d,
                    origType: g,
                    data: r,
                    handler: n,
                    guid: n.guid,
                    selector: i,
                    needsContext: i && we.expr.match.needsContext.test(i),
                    namespace: h.join(".")
                }, o), (p = u[d]) || ((p = u[d] = []).delegateCount = 0, f.setup && !1 !== f.setup.call(e, r, h, a) || e.addEventListener && e.addEventListener(d, a)), f.add && (f.add.call(e, c), c.handler.guid || (c.handler.guid = n.guid)), i ? p.splice(p.delegateCount++, 0, c) : p.push(c), we.event.global[d] = !0)
        },
        remove: function(e, t, n, r, i) {
            var o, a, s, u, l, c, f, p, d, h, g, y = We.hasData(e) && We.get(e);
            if (y && (u = y.events)) {
                for (l = (t = (t || "").match(Le) || [""]).length; l--;)
                    if (s = rt.exec(t[l]) || [], d = g = s[1], h = (s[2] || "").split(".").sort(), d) {

                        for (f = we.event.special[d] || {}, p = u[d = (r ? f.delegateType : f.bindType) || d] || [], s = s[2] && new RegExp("(^|\\.)" + h.join("\\.(?:.*\\.|)") + "(\\.|$)"), a = o = p.length; o--;) c = p[o], !i && g !== c.origType || n && n.guid !== c.guid || s && !s.test(c.namespace) || r && r !== c.selector && ("**" !== r || !c.selector) || (p.splice(o, 1), c.selector && p.delegateCount--, f.remove && f.remove.call(e, c));
                        a && !p.length && (f.teardown && !1 !== f.teardown.call(e, h, y.handle) || we.removeEvent(e, d, y.handle), delete u[d])
                    } else
                        for (d in u) we.event.remove(e, d + t[l], n, r, !0);
                we.isEmptyObject(u) && We.remove(e, "handle events")
            }
        },
        dispatch: function(e) {
            var t, n, r, i, o, a, s = we.event.fix(e),
                u = new Array(arguments.length),
                l = (We.get(this, "events") || {})[s.type] || [],
                c = we.event.special[s.type] || {};
            for (u[0] = s, t = 1; t < arguments.length; t++) u[t] = arguments[t];
            if (s.delegateTarget = this, !c.preDispatch || !1 !== c.preDispatch.call(this, s)) {
                for (a = we.event.handlers.call(this, s, l), t = 0;
                    (i = a[t++]) && !s.isPropagationStopped();)
                    for (s.currentTarget = i.elem, n = 0;
                        (o = i.handlers[n++]) && !s.isImmediatePropagationStopped();) s.rnamespace && !s.rnamespace.test(o.namespace) || (s.handleObj = o, s.data = o.data, void 0 !== (r = ((we.event.special[o.origType] || {}).handle || o.handler).apply(i.elem, u)) && !1 === (s.result = r) && (s.preventDefault(), s.stopPropagation()));
                return c.postDispatch && c.postDispatch.call(this, s), s.result
            }
        },
        handlers: function(e, t) {
            var n, r, i, o, a, s = [],
                u = t.delegateCount,
                l = e.target;
            if (u && l.nodeType && !("click" === e.type && e.button >= 1))
                for (; l !== this; l = l.parentNode || this)
                    if (1 === l.nodeType && ("click" !== e.type || !0 !== l.disabled)) {
                        for (o = [], a = {}, n = 0; n < u; n++) void 0 === a[i = (r = t[n]).selector + " "] && (a[i] = r.needsContext ? we(i, this).index(l) > -1 : we.find(i, this, null, [l]).length), a[i] && o.push(r);
                        o.length && s.push({
                            elem: l,
                            handlers: o
                        })
                    }
            return l = this, u < t.length && s.push({
                elem: l,
                handlers: t.slice(u)
            }), s
        },
        addProp: function(e, t) {
            Object.defineProperty(we.Event.prototype, e, {
                enumerable: !0,
                configurable: !0,
                get: me(t) ? function() {
                    if (this.originalEvent) return t(this.originalEvent)
                } : function() {
                    if (this.originalEvent) return this.originalEvent[e]
                },
                set: function(t) {
                    Object.defineProperty(this, e, {
                        enumerable: !0,
                        configurable: !0,
                        writable: !0,
                        value: t
                    })
                }
            })
        },
        fix: function(e) {
            return e[we.expando] ? e : new we.Event(e)
        },
        special: {
            load: {
                noBubble: !0
            },
            focus: {
                trigger: function() {
                    if (this !== S() && this.focus) return this.focus(), !1
                },
                delegateType: "focusin"
            },
            blur: {
                trigger: function() {
                    if (this === S() && this.blur) return this.blur(), !1
                },
                delegateType: "focusout"
            },
            click: {
                trigger: function() {
                    if ("checkbox" === this.type && this.click && o(this, "input")) return this.click(), !1
                },
                _default: function(e) {
                    return o(e.target, "a")
                }
            },
            beforeunload: {
                postDispatch: function(e) {
                    void 0 !== e.result && e.originalEvent && (e.originalEvent.returnValue = e.result)
                }
            }
        }
    }, we.removeEvent = function(e, t, n) {
        e.removeEventListener && e.removeEventListener(t, n)
    }, we.Event = function(e, t) {
        return this instanceof we.Event ? (e && e.type ? (this.originalEvent = e, this.type = e.type, this.isDefaultPrevented = e.defaultPrevented || void 0 === e.defaultPrevented && !1 === e.returnValue ? E : k, this.target = e.target && 3 === e.target.nodeType ? e.target.parentNode : e.target, this.currentTarget = e.currentTarget, this.relatedTarget = e.relatedTarget) : this.type = e, t && we.extend(this, t), this.timeStamp = e && e.timeStamp || Date.now(), this[we.expando] = !0, void 0) : new we.Event(e, t)
    }, we.Event.prototype = {
        constructor: we.Event,
        isDefaultPrevented: k,
        isPropagationStopped: k,
        isImmediatePropagationStopped: k,
        isSimulated: !1,
        preventDefault: function() {
            var e = this.originalEvent;
            this.isDefaultPrevented = E, e && !this.isSimulated && e.preventDefault()
        },
        stopPropagation: function() {
            var e = this.originalEvent;
            this.isPropagationStopped = E, e && !this.isSimulated && e.stopPropagation()
        },
        stopImmediatePropagation: function() {
            var e = this.originalEvent;
            this.isImmediatePropagationStopped = E, e && !this.isSimulated && e.stopImmediatePropagation(), this.stopPropagation()
        }
    }, we.each({
        altKey: !0,
        bubbles: !0,
        cancelable: !0,
        changedTouches: !0,
        ctrlKey: !0,
        detail: !0,
        eventPhase: !0,
        metaKey: !0,
        pageX: !0,
        pageY: !0,
        shiftKey: !0,
        view: !0,
        "char": !0,
        charCode: !0,
        key: !0,
        keyCode: !0,
        button: !0,
        buttons: !0,
        clientX: !0,
        clientY: !0,
        offsetX: !0,
        offsetY: !0,
        pointerId: !0,
        pointerType: !0,
        screenX: !0,
        screenY: !0,
        targetTouches: !0,
        toElement: !0,
        touches: !0,
        which: function(e) {
            var t = e.button;
            return null == e.which && tt.test(e.type) ? null != e.charCode ? e.charCode : e.keyCode : !e.which && void 0 !== t && nt.test(e.type) ? 1 & t ? 1 : 2 & t ? 3 : 4 & t ? 2 : 0 : e.which
        }
    }, we.event.addProp), we.each({
        mouseenter: "mouseover",
        mouseleave: "mouseout",
        pointerenter: "pointerover",
        pointerleave: "pointerout"
    }, function(e, t) {
        we.event.special[e] = {
            delegateType: t,
            bindType: t,
            handle: function(e) {
                var n, r = this,
                    i = e.relatedTarget,
                    o = e.handleObj;
                return i && (i === r || we.contains(r, i)) || (e.type = o.origType, n = o.handler.apply(this, arguments), e.type = t), n
            }
        }
    }), we.fn.extend({
        on: function(e, t, n, r) {
            return D(this, e, t, n, r)
        },
        one: function(e, t, n, r) {
            return D(this, e, t, n, r, 1)
        },
        off: function(e, t, n) {
            var r, i;
            if (e && e.preventDefault && e.handleObj) return r = e.handleObj, we(e.delegateTarget).off(r.namespace ? r.origType + "." + r.namespace : r.origType, r.selector, r.handler), this;
            if ("object" == typeof e) {
                for (i in e) this.off(i, t, e[i]);
                return this
            }
            return !1 !== t && "function" != typeof t || (n = t, t = void 0), !1 === n && (n = k), this.each(function() {
                we.event.remove(this, e, n, t)
            })
        }
    });
    var it = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,
        ot = /<script|<style|<link/i,
        at = /checked\s*(?:[^=]|=\s*.checked.)/i,
        st = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;
    we.extend({
        htmlPrefilter: function(e) {
            return e.replace(it, "<$1></$2>")
        },
        clone: function(e, t, n) {
            var r, i, o, a, s = e.cloneNode(!0),
                u = we.contains(e.ownerDocument, e);
            if (!(ve.noCloneChecked || 1 !== e.nodeType && 11 !== e.nodeType || we.isXMLDoc(e)))
                for (a = w(s), r = 0, i = (o = w(e)).length; r < i; r++) L(o[r], a[r]);
            if (t)
                if (n)
                    for (o = o || w(e), a = a || w(s), r = 0, i = o.length; r < i; r++) q(o[r], a[r]);
                else q(e, s);
            return (a = w(s, "script")).length > 0 && T(a, !u && w(e, "script")), s
        },
        cleanData: function(e) {
            for (var t, n, r, i = we.event.special, o = 0; void 0 !== (n = e[o]); o++)
                if (Ie(n)) {
                    if (t = n[We.expando]) {
                        if (t.events)
                            for (r in t.events) i[r] ? we.event.remove(n, r) : we.removeEvent(n, r, t.handle);
                        n[We.expando] = void 0
                    }
                    n[$e.expando] && (n[$e.expando] = void 0)
                }
        }
    }), we.fn.extend({
        detach: function(e) {
            return O(this, e, !0)
        },
        remove: function(e) {
            return O(this, e)
        },
        text: function(e) {
            return Pe(this, function(e) {
                return void 0 === e ? we.text(this) : this.empty().each(function() {
                    1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || (this.textContent = e)
                })
            }, null, e, arguments.length)
        },
        append: function() {
            return H(this, arguments, function(e) {
                1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || N(this, e).appendChild(e)
            })
        },
        prepend: function() {
            return H(this, arguments, function(e) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var t = N(this, e);
                    t.insertBefore(e, t.firstChild)
                }
            })
        },
        before: function() {
            return H(this, arguments, function(e) {
                this.parentNode && this.parentNode.insertBefore(e, this)
            })
        },
        after: function() {
            return H(this, arguments, function(e) {
                this.parentNode && this.parentNode.insertBefore(e, this.nextSibling)
            })
        },
        empty: function() {
            for (var e, t = 0; null != (e = this[t]); t++) 1 === e.nodeType && (we.cleanData(w(e, !1)), e.textContent = "");
            return this
        },
        clone: function(e, t) {
            return e = null != e && e, t = null == t ? e : t, this.map(function() {
                return we.clone(this, e, t)
            })
        },
        html: function(e) {
            return Pe(this, function(e) {
                var t = this[0] || {},
                    n = 0,
                    r = this.length;
                if (void 0 === e && 1 === t.nodeType) return t.innerHTML;
                if ("string" == typeof e && !ot.test(e) && !Ke[(Qe.exec(e) || ["", ""])[1].toLowerCase()]) {
                    e = we.htmlPrefilter(e);
                    try {
                        for (; n < r; n++) 1 === (t = this[n] || {}).nodeType && (we.cleanData(w(t, !1)), t.innerHTML = e);
                        t = 0
                    } catch (e) {}
                }
                t && this.empty().append(e)
            }, null, e, arguments.length)
        },
        replaceWith: function() {
            var e = [];
            return H(this, arguments, function(t) {
                var n = this.parentNode;
                we.inArray(this, e) < 0 && (we.cleanData(w(this)), n && n.replaceChild(t, this))
            }, e)
        }
    }), we.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function(e, t) {
        we.fn[e] = function(e) {
            for (var n, r = [], i = we(e), o = i.length - 1, a = 0; a <= o; a++) n = a === o ? this : this.clone(!0), we(i[a])[t](n), ce.apply(r, n.get());
            return this.pushStack(r)
        }
    });
    var ut = new RegExp("^(" + _e + ")(?!px)[a-z%]+$", "i"),
        lt = function(t) {
            var n = t.ownerDocument.defaultView;
            return n && n.opener || (n = e), n.getComputedStyle(t)
        },
        ct = new RegExp(Xe.join("|"), "i");
    ! function() {
        function t() {
            if (l) {
                u.style.cssText = "position:absolute;left:-11111px;width:60px;margin-top:1px;padding:0;border:0", l.style.cssText = "position:relative;display:block;box-sizing:border-box;overflow:scroll;margin:auto;border:1px;padding:1px;width:60%;top:1%", et.appendChild(u).appendChild(l);
                var t = e.getComputedStyle(l);
                r = "1%" !== t.top, s = 12 === n(t.marginLeft), l.style.right = "60%", a = 36 === n(t.right), i = 36 === n(t.width), l.style.position = "absolute", o = 36 === l.offsetWidth || "absolute", et.removeChild(u), l = null
            }
        }

        function n(e) {
            return Math.round(parseFloat(e))
        }
        var r, i, o, a, s, u = ae.createElement("div"),
            l = ae.createElement("div");
        l.style && (l.style.backgroundClip = "content-box", l.cloneNode(!0).style.backgroundClip = "", ve.clearCloneStyle = "content-box" === l.style.backgroundClip, we.extend(ve, {
            boxSizingReliable: function() {
                return t(), i
            },
            pixelBoxStyles: function() {
                return t(), a
            },
            pixelPosition: function() {
                return t(), r
            },
            reliableMarginLeft: function() {
                return t(), s
            },
            scrollboxSize: function() {
                return t(), o
            }
        }))
    }();
    var ft = /^(none|table(?!-c[ea]).+)/,
        pt = /^--/,
        dt = {
            position: "absolute",
            visibility: "hidden",
            display: "block"
        },
        ht = {
            letterSpacing: "0",
            fontWeight: "400"
        },
        gt = ["Webkit", "Moz", "ms"],
        yt = ae.createElement("div").style;
    we.extend({
        cssHooks: {
            opacity: {
                get: function(e, t) {
                    if (t) {
                        var n = P(e, "opacity");
                        return "" === n ? "1" : n
                    }
                }
            }
        },
        cssNumber: {
            animationIterationCount: !0,
            columnCount: !0,
            fillOpacity: !0,
            flexGrow: !0,
            flexShrink: !0,
            fontWeight: !0,
            lineHeight: !0,
            opacity: !0,
            order: !0,
            orphans: !0,
            widows: !0,
            zIndex: !0,
            zoom: !0
        },
        cssProps: {},
        style: function(e, t, n, r) {
            if (e && 3 !== e.nodeType && 8 !== e.nodeType && e.style) {
                var i, o, a, s = h(t),
                    u = pt.test(t),
                    l = e.style;
                if (u || (t = I(s)), a = we.cssHooks[t] || we.cssHooks[s], void 0 === n) return a && "get" in a && void 0 !== (i = a.get(e, !1, r)) ? i : l[t];
                "string" == (o = typeof n) && (i = ze.exec(n)) && i[1] && (n = m(e, t, i), o = "number"), null != n && n === n && ("number" === o && (n += i && i[3] || (we.cssNumber[s] ? "" : "px")), ve.clearCloneStyle || "" !== n || 0 !== t.indexOf("background") || (l[t] = "inherit"), a && "set" in a && void 0 === (n = a.set(e, n, r)) || (u ? l.setProperty(t, n) : l[t] = n))
            }
        },
        css: function(e, t, n, r) {
            var i, o, a, s = h(t);
            return pt.test(t) || (t = I(s)), (a = we.cssHooks[t] || we.cssHooks[s]) && "get" in a && (i = a.get(e, !0, n)), void 0 === i && (i = P(e, t, r)), "normal" === i && t in ht && (i = ht[t]), "" === n || n ? (o = parseFloat(i), !0 === n || isFinite(o) ? o || 0 : i) : i
        }
    }), we.each(["height", "width"], function(e, t) {
        we.cssHooks[t] = {
            get: function(e, n, r) {
                if (n) return !ft.test(we.css(e, "display")) || e.getClientRects().length && e.getBoundingClientRect().width ? B(e, t, r) : Ve(e, dt, function() {
                    return B(e, t, r)
                })
            },
            set: function(e, n, r) {
                var i, o = lt(e),
                    a = "border-box" === we.css(e, "boxSizing", !1, o),
                    s = r && $(e, t, r, a, o);
                return a && ve.scrollboxSize() === o.position && (s -= Math.ceil(e["offset" + t[0].toUpperCase() + t.slice(1)] - parseFloat(o[t]) - $(e, t, "border", !1, o) - .5)), s && (i = ze.exec(n)) && "px" !== (i[3] || "px") && (e.style[t] = n, n = we.css(e, t)), W(e, n, s)
            }
        }
    }), we.cssHooks.marginLeft = M(ve.reliableMarginLeft, function(e, t) {
        if (t) return (parseFloat(P(e, "marginLeft")) || e.getBoundingClientRect().left - Ve(e, {
            marginLeft: 0
        }, function() {
            return e.getBoundingClientRect().left
        })) + "px"
    }), we.each({
        margin: "",
        padding: "",
        border: "Width"
    }, function(e, t) {
        we.cssHooks[e + t] = {
            expand: function(n) {
                for (var r = 0, i = {}, o = "string" == typeof n ? n.split(" ") : [n]; r < 4; r++) i[e + Xe[r] + t] = o[r] || o[r - 2] || o[0];
                return i
            }
        }, "margin" !== e && (we.cssHooks[e + t].set = W)
    }), we.fn.extend({
        css: function(e, t) {
            return Pe(this, function(e, t, n) {
                var r, i, o = {},
                    a = 0;
                if (Array.isArray(t)) {
                    for (r = lt(e), i = t.length; a < i; a++) o[t[a]] = we.css(e, t[a], !1, r);
                    return o
                }
                return void 0 !== n ? we.style(e, t, n) : we.css(e, t)
            }, e, t, arguments.length > 1)
        }
    }), we.Tween = F, F.prototype = {
        constructor: F,
        init: function(e, t, n, r, i, o) {
            this.elem = e, this.prop = n, this.easing = i || we.easing._default, this.options = t, this.start = this.now = this.cur(), this.end = r, this.unit = o || (we.cssNumber[n] ? "" : "px")
        },
        cur: function() {
            var e = F.propHooks[this.prop];
            return e && e.get ? e.get(this) : F.propHooks._default.get(this)
        },
        run: function(e) {
            var t, n = F.propHooks[this.prop];
            return this.options.duration ? this.pos = t = we.easing[this.easing](e, this.options.duration * e, 0, 1, this.options.duration) : this.pos = t = e, this.now = (this.end - this.start) * t + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), n && n.set ? n.set(this) : F.propHooks._default.set(this), this
        }
    }, F.prototype.init.prototype = F.prototype, F.propHooks = {
        _default: {
            get: function(e) {
                var t;
                return 1 !== e.elem.nodeType || null != e.elem[e.prop] && null == e.elem.style[e.prop] ? e.elem[e.prop] : (t = we.css(e.elem, e.prop, "")) && "auto" !== t ? t : 0
            },
            set: function(e) {
                we.fx.step[e.prop] ? we.fx.step[e.prop](e) : 1 !== e.elem.nodeType || null == e.elem.style[we.cssProps[e.prop]] && !we.cssHooks[e.prop] ? e.elem[e.prop] = e.now : we.style(e.elem, e.prop, e.now + e.unit)
            }
        }
    }, F.propHooks.scrollTop = F.propHooks.scrollLeft = {
        set: function(e) {
            e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now)
        }
    }, we.easing = {
        linear: function(e) {
            return e
        },
        swing: function(e) {
            return .5 - Math.cos(e * Math.PI) / 2
        },
        _default: "swing"
    }, we.fx = F.prototype.init, we.fx.step = {};
    var vt, mt, xt = /^(?:toggle|show|hide)$/,
        bt = /queueHooks$/;
    we.Animation = we.extend(Y, {
            tweeners: {
                "*": [function(e, t) {
                    var n = this.createTween(e, t);
                    return m(n.elem, e, ze.exec(t), n), n
                }]
            },
            tweener: function(e, t) {
                me(e) ? (t = e, e = ["*"]) : e = e.match(Le);
                for (var n, r = 0, i = e.length; r < i; r++) n = e[r], Y.tweeners[n] = Y.tweeners[n] || [], Y.tweeners[n].unshift(t)
            },
            prefilters: [V],
            prefilter: function(e, t) {
                t ? Y.prefilters.unshift(e) : Y.prefilters.push(e)
            }
        }), we.speed = function(e, t, n) {
            var r = e && "object" == typeof e ? we.extend({}, e) : {
                complete: n || !n && t || me(e) && e,
                duration: e,
                easing: n && t || t && !me(t) && t
            };
            return we.fx.off ? r.duration = 0 : "number" != typeof r.duration && (r.duration in we.fx.speeds ? r.duration = we.fx.speeds[r.duration] : r.duration = we.fx.speeds._default), null != r.queue && !0 !== r.queue || (r.queue = "fx"), r.old = r.complete, r.complete = function() {
                me(r.old) && r.old.call(this), r.queue && we.dequeue(this, r.queue)
            }, r
        }, we.fn.extend({
            fadeTo: function(e, t, n, r) {
                return this.filter(Ue).css("opacity", 0).show().end().animate({
                    opacity: t
                }, e, n, r)
            },
            animate: function(e, t, n, r) {
                var i = we.isEmptyObject(e),
                    o = we.speed(t, n, r),
                    a = function() {
                        var t = Y(this, we.extend({}, e), o);
                        (i || We.get(this, "finish")) && t.stop(!0)
                    };
                return a.finish = a, i || !1 === o.queue ? this.each(a) : this.queue(o.queue, a)
            },
            stop: function(e, t, n) {
                var r = function(e) {
                    var t = e.stop;
                    delete e.stop, t(n)
                };
                return "string" != typeof e && (n = t, t = e, e = void 0), t && !1 !== e && this.queue(e || "fx", []), this.each(function() {
                    var t = !0,
                        i = null != e && e + "queueHooks",
                        o = we.timers,
                        a = We.get(this);
                    if (i) a[i] && a[i].stop && r(a[i]);
                    else
                        for (i in a) a[i] && a[i].stop && bt.test(i) && r(a[i]);
                    for (i = o.length; i--;) o[i].elem !== this || null != e && o[i].queue !== e || (o[i].anim.stop(n), t = !1, o.splice(i, 1));
                    !t && n || we.dequeue(this, e)
                })
            },
            finish: function(e) {
                return !1 !== e && (e = e || "fx"),
                    this.each(function() {
                        var t, n = We.get(this),
                            r = n[e + "queue"],
                            i = n[e + "queueHooks"],
                            o = we.timers,
                            a = r ? r.length : 0;
                        for (n.finish = !0, we.queue(this, e, []), i && i.stop && i.stop.call(this, !0), t = o.length; t--;) o[t].elem === this && o[t].queue === e && (o[t].anim.stop(!0), o.splice(t, 1));
                        for (t = 0; t < a; t++) r[t] && r[t].finish && r[t].finish.call(this);
                        delete n.finish
                    })
            }
        }), we.each(["toggle", "show", "hide"], function(e, t) {
            var n = we.fn[t];
            we.fn[t] = function(e, r, i) {
                return null == e || "boolean" == typeof e ? n.apply(this, arguments) : this.animate(X(t, !0), e, r, i)
            }
        }), we.each({
            slideDown: X("show"),
            slideUp: X("hide"),
            slideToggle: X("toggle"),
            fadeIn: {
                opacity: "show"
            },
            fadeOut: {
                opacity: "hide"
            },
            fadeToggle: {
                opacity: "toggle"
            }
        }, function(e, t) {
            we.fn[e] = function(e, n, r) {
                return this.animate(t, e, n, r)
            }
        }), we.timers = [], we.fx.tick = function() {
            var e, t = 0,
                n = we.timers;
            for (vt = Date.now(); t < n.length; t++)(e = n[t])() || n[t] !== e || n.splice(t--, 1);
            n.length || we.fx.stop(), vt = void 0
        }, we.fx.timer = function(e) {
            we.timers.push(e), we.fx.start()
        }, we.fx.interval = 13, we.fx.start = function() {
            mt || (mt = !0, _())
        }, we.fx.stop = function() {
            mt = null
        }, we.fx.speeds = {
            slow: 600,
            fast: 200,
            _default: 400
        }, we.fn.delay = function(t, n) {
            return t = we.fx ? we.fx.speeds[t] || t : t, n = n || "fx", this.queue(n, function(n, r) {
                var i = e.setTimeout(n, t);
                r.stop = function() {
                    e.clearTimeout(i)
                }
            })
        },
        function() {
            var e = ae.createElement("input"),
                t = ae.createElement("select").appendChild(ae.createElement("option"));
            e.type = "checkbox", ve.checkOn = "" !== e.value, ve.optSelected = t.selected, (e = ae.createElement("input")).value = "t", e.type = "radio", ve.radioValue = "t" === e.value
        }();
    var wt, Tt = we.expr.attrHandle;
    we.fn.extend({
        attr: function(e, t) {
            return Pe(this, we.attr, e, t, arguments.length > 1)
        },
        removeAttr: function(e) {
            return this.each(function() {
                we.removeAttr(this, e)
            })
        }
    }), we.extend({
        attr: function(e, t, n) {
            var r, i, o = e.nodeType;
            if (3 !== o && 8 !== o && 2 !== o) return "undefined" == typeof e.getAttribute ? we.prop(e, t, n) : (1 === o && we.isXMLDoc(e) || (i = we.attrHooks[t.toLowerCase()] || (we.expr.match.bool.test(t) ? wt : void 0)), void 0 !== n ? null === n ? void we.removeAttr(e, t) : i && "set" in i && void 0 !== (r = i.set(e, n, t)) ? r : (e.setAttribute(t, n + ""), n) : i && "get" in i && null !== (r = i.get(e, t)) ? r : null == (r = we.find.attr(e, t)) ? void 0 : r)
        },
        attrHooks: {
            type: {
                set: function(e, t) {
                    if (!ve.radioValue && "radio" === t && o(e, "input")) {
                        var n = e.value;
                        return e.setAttribute("type", t), n && (e.value = n), t
                    }
                }
            }
        },
        removeAttr: function(e, t) {
            var n, r = 0,
                i = t && t.match(Le);
            if (i && 1 === e.nodeType)
                for (; n = i[r++];) e.removeAttribute(n)
        }
    }), wt = {
        set: function(e, t, n) {
            return !1 === t ? we.removeAttr(e, n) : e.setAttribute(n, n), n
        }
    }, we.each(we.expr.match.bool.source.match(/\w+/g), function(e, t) {
        var n = Tt[t] || we.find.attr;
        Tt[t] = function(e, t, r) {
            var i, o, a = t.toLowerCase();
            return r || (o = Tt[a], Tt[a] = i, i = null != n(e, t, r) ? a : null, Tt[a] = o), i
        }
    });
    var Ct = /^(?:input|select|textarea|button)$/i,
        Et = /^(?:a|area)$/i;
    we.fn.extend({
        prop: function(e, t) {
            return Pe(this, we.prop, e, t, arguments.length > 1)
        },
        removeProp: function(e) {
            return this.each(function() {
                delete this[we.propFix[e] || e]
            })
        }
    }), we.extend({
        prop: function(e, t, n) {
            var r, i, o = e.nodeType;
            if (3 !== o && 8 !== o && 2 !== o) return 1 === o && we.isXMLDoc(e) || (t = we.propFix[t] || t, i = we.propHooks[t]), void 0 !== n ? i && "set" in i && void 0 !== (r = i.set(e, n, t)) ? r : e[t] = n : i && "get" in i && null !== (r = i.get(e, t)) ? r : e[t]
        },
        propHooks: {
            tabIndex: {
                get: function(e) {
                    var t = we.find.attr(e, "tabindex");
                    return t ? parseInt(t, 10) : Ct.test(e.nodeName) || Et.test(e.nodeName) && e.href ? 0 : -1
                }
            }
        },
        propFix: {
            "for": "htmlFor",
            "class": "className"
        }
    }), ve.optSelected || (we.propHooks.selected = {
        get: function(e) {
            var t = e.parentNode;
            return t && t.parentNode && t.parentNode.selectedIndex, null
        },
        set: function(e) {
            var t = e.parentNode;
            t && (t.selectedIndex, t.parentNode && t.parentNode.selectedIndex)
        }
    }), we.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function() {
        we.propFix[this.toLowerCase()] = this
    }), we.fn.extend({
        addClass: function(e) {
            var t, n, r, i, o, a, s, u = 0;
            if (me(e)) return this.each(function(t) {
                we(this).addClass(e.call(this, t, J(this)))
            });
            if ((t = K(e)).length)
                for (; n = this[u++];)
                    if (i = J(n), r = 1 === n.nodeType && " " + Q(i) + " ") {
                        for (a = 0; o = t[a++];) r.indexOf(" " + o + " ") < 0 && (r += o + " ");
                        i !== (s = Q(r)) && n.setAttribute("class", s)
                    }
            return this
        },
        removeClass: function(e) {
            var t, n, r, i, o, a, s, u = 0;
            if (me(e)) return this.each(function(t) {
                we(this).removeClass(e.call(this, t, J(this)))
            });
            if (!arguments.length) return this.attr("class", "");
            if ((t = K(e)).length)
                for (; n = this[u++];)
                    if (i = J(n), r = 1 === n.nodeType && " " + Q(i) + " ") {
                        for (a = 0; o = t[a++];)
                            for (; r.indexOf(" " + o + " ") > -1;) r = r.replace(" " + o + " ", " ");
                        i !== (s = Q(r)) && n.setAttribute("class", s)
                    }
            return this
        },
        toggleClass: function(e, t) {
            var n = typeof e,
                r = "string" === n || Array.isArray(e);
            return "boolean" == typeof t && r ? t ? this.addClass(e) : this.removeClass(e) : me(e) ? this.each(function(n) {
                we(this).toggleClass(e.call(this, n, J(this), t), t)
            }) : this.each(function() {
                var t, i, o, a;
                if (r)
                    for (i = 0, o = we(this), a = K(e); t = a[i++];) o.hasClass(t) ? o.removeClass(t) : o.addClass(t);
                else void 0 !== e && "boolean" !== n || ((t = J(this)) && We.set(this, "__className__", t), this.setAttribute && this.setAttribute("class", t || !1 === e ? "" : We.get(this, "__className__") || ""))
            })
        },
        hasClass: function(e) {
            var t, n, r = 0;
            for (t = " " + e + " "; n = this[r++];)
                if (1 === n.nodeType && (" " + Q(J(n)) + " ").indexOf(t) > -1) return !0;
            return !1
        }
    });
    var kt = /\r/g;
    we.fn.extend({
        val: function(e) {
            var t, n, r, i = this[0];
            return arguments.length ? (r = me(e), this.each(function(n) {
                var i;
                1 === this.nodeType && (null == (i = r ? e.call(this, n, we(this).val()) : e) ? i = "" : "number" == typeof i ? i += "" : Array.isArray(i) && (i = we.map(i, function(e) {
                    return null == e ? "" : e + ""
                })), (t = we.valHooks[this.type] || we.valHooks[this.nodeName.toLowerCase()]) && "set" in t && void 0 !== t.set(this, i, "value") || (this.value = i))
            })) : i ? (t = we.valHooks[i.type] || we.valHooks[i.nodeName.toLowerCase()]) && "get" in t && void 0 !== (n = t.get(i, "value")) ? n : "string" == typeof(n = i.value) ? n.replace(kt, "") : null == n ? "" : n : void 0
        }
    }), we.extend({
        valHooks: {
            option: {
                get: function(e) {
                    var t = we.find.attr(e, "value");
                    return null != t ? t : Q(we.text(e))
                }
            },
            select: {
                get: function(e) {
                    var t, n, r, i = e.options,
                        a = e.selectedIndex,
                        s = "select-one" === e.type,
                        u = s ? null : [],
                        l = s ? a + 1 : i.length;
                    for (r = a < 0 ? l : s ? a : 0; r < l; r++)
                        if (((n = i[r]).selected || r === a) && !n.disabled && (!n.parentNode.disabled || !o(n.parentNode, "optgroup"))) {
                            if (t = we(n).val(), s) return t;
                            u.push(t)
                        }
                    return u
                },
                set: function(e, t) {
                    for (var n, r, i = e.options, o = we.makeArray(t), a = i.length; a--;)((r = i[a]).selected = we.inArray(we.valHooks.option.get(r), o) > -1) && (n = !0);
                    return n || (e.selectedIndex = -1), o
                }
            }
        }
    }), we.each(["radio", "checkbox"], function() {
        we.valHooks[this] = {
            set: function(e, t) {
                if (Array.isArray(t)) return e.checked = we.inArray(we(e).val(), t) > -1
            }
        }, ve.checkOn || (we.valHooks[this].get = function(e) {
            return null === e.getAttribute("value") ? "on" : e.value
        })
    }), ve.focusin = "onfocusin" in e;
    var St = /^(?:focusinfocus|focusoutblur)$/,
        Dt = function(e) {
            e.stopPropagation()
        };
    we.extend(we.event, {
        trigger: function(t, n, r, i) {
            var o, a, s, u, l, c, f, p, d = [r || ae],
                h = he.call(t, "type") ? t.type : t,
                g = he.call(t, "namespace") ? t.namespace.split(".") : [];
            if (a = p = s = r = r || ae, 3 !== r.nodeType && 8 !== r.nodeType && !St.test(h + we.event.triggered) && (h.indexOf(".") > -1 && (h = (g = h.split(".")).shift(), g.sort()), l = h.indexOf(":") < 0 && "on" + h, t = t[we.expando] ? t : new we.Event(h, "object" == typeof t && t), t.isTrigger = i ? 2 : 3, t.namespace = g.join("."), t.rnamespace = t.namespace ? new RegExp("(^|\\.)" + g.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, t.result = void 0, t.target || (t.target = r), n = null == n ? [t] : we.makeArray(n, [t]), f = we.event.special[h] || {}, i || !f.trigger || !1 !== f.trigger.apply(r, n))) {
                if (!i && !f.noBubble && !xe(r)) {
                    for (u = f.delegateType || h, St.test(u + h) || (a = a.parentNode); a; a = a.parentNode) d.push(a), s = a;
                    s === (r.ownerDocument || ae) && d.push(s.defaultView || s.parentWindow || e)
                }
                for (o = 0;
                    (a = d[o++]) && !t.isPropagationStopped();) p = a, t.type = o > 1 ? u : f.bindType || h, (c = (We.get(a, "events") || {})[t.type] && We.get(a, "handle")) && c.apply(a, n), (c = l && a[l]) && c.apply && Ie(a) && (t.result = c.apply(a, n), !1 === t.result && t.preventDefault());
                return t.type = h, i || t.isDefaultPrevented() || f._default && !1 !== f._default.apply(d.pop(), n) || !Ie(r) || l && me(r[h]) && !xe(r) && ((s = r[l]) && (r[l] = null), we.event.triggered = h, t.isPropagationStopped() && p.addEventListener(h, Dt), r[h](), t.isPropagationStopped() && p.removeEventListener(h, Dt), we.event.triggered = void 0, s && (r[l] = s)), t.result
            }
        },
        simulate: function(e, t, n) {
            var r = we.extend(new we.Event, n, {
                type: e,
                isSimulated: !0
            });
            we.event.trigger(r, null, t)
        }
    }), we.fn.extend({
        trigger: function(e, t) {
            return this.each(function() {
                we.event.trigger(e, t, this)
            })
        },
        triggerHandler: function(e, t) {
            var n = this[0];
            if (n) return we.event.trigger(e, t, n, !0)
        }
    }), ve.focusin || we.each({
        focus: "focusin",
        blur: "focusout"
    }, function(e, t) {
        var n = function(e) {
            we.event.simulate(t, e.target, we.event.fix(e))
        };
        we.event.special[t] = {
            setup: function() {
                var r = this.ownerDocument || this,
                    i = We.access(r, t);
                i || r.addEventListener(e, n, !0), We.access(r, t, (i || 0) + 1)
            },
            teardown: function() {
                var r = this.ownerDocument || this,
                    i = We.access(r, t) - 1;
                i ? We.access(r, t, i) : (r.removeEventListener(e, n, !0), We.remove(r, t))
            }
        }
    });
    var Nt = e.location,
        At = Date.now(),
        jt = /\?/;
    we.parseXML = function(e) {
        var t;
        if (!e || "string" != typeof e) return null;
        try {
            t = (new n.DOMParser).parseFromString(e, "text/xml")
        } catch (n) {
            t = void 0
        }
        return t && !t.getElementsByTagName("parsererror").length || we.error("Invalid XML: " + e), t
    };
    var qt = /\[\]$/,
        Lt = /\r?\n/g,
        Ht = /^(?:submit|button|image|reset|file)$/i,
        Ot = /^(?:input|select|textarea|keygen)/i;
    we.param = function(e, t) {
        var n, r = [],
            i = function(e, t) {
                var n = me(t) ? t() : t;
                r[r.length] = encodeURIComponent(e) + "=" + encodeURIComponent(null == n ? "" : n)
            };
        if (Array.isArray(e) || e.jquery && !we.isPlainObject(e)) we.each(e, function() {
            i(this.name, this.value)
        });
        else
            for (n in e) Z(n, e[n], t, i);
        return r.join("&")
    }, we.fn.extend({
        serialize: function() {
            return we.param(this.serializeArray())
        },
        serializeArray: function() {
            return this.map(function() {
                var e = we.prop(this, "elements");
                return e ? we.makeArray(e) : this
            }).filter(function() {
                var e = this.type;
                return this.name && !we(this).is(":disabled") && Ot.test(this.nodeName) && !Ht.test(e) && (this.checked || !Ye.test(e))
            }).map(function(e, t) {
                var n = we(this).val();
                return null == n ? null : Array.isArray(n) ? we.map(n, function(e) {
                    return {
                        name: t.name,
                        value: e.replace(Lt, "\r\n")
                    }
                }) : {
                    name: t.name,
                    value: n.replace(Lt, "\r\n")
                }
            }).get()
        }
    });
    var Pt = /%20/g,
        Mt = /#.*$/,
        Rt = /([?&])_=[^&]*/,
        It = /^(.*?):[ \t]*([^\r\n]*)$/gm,
        Wt = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
        $t = /^(?:GET|HEAD)$/,
        Bt = /^\/\//,
        Ft = {},
        _t = {},
        zt = "*/".concat("*"),
        Xt = ae.createElement("a");
    Xt.href = Nt.href, we.extend({
        active: 0,
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: Nt.href,
            type: "GET",
            isLocal: Wt.test(Nt.protocol),
            global: !0,
            processData: !0,
            async: !0,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            accepts: {
                "*": zt,
                text: "text/plain",
                html: "text/html",
                xml: "application/xml, text/xml",
                json: "application/json, text/javascript"
            },
            contents: {
                xml: /\bxml\b/,
                html: /\bhtml/,
                json: /\bjson\b/
            },
            responseFields: {
                xml: "responseXML",
                text: "responseText",
                json: "responseJSON"
            },
            converters: {
                "* text": String,
                "text html": !0,
                "text json": JSON.parse,
                "text xml": we.parseXML
            },
            flatOptions: {
                url: !0,
                context: !0
            }
        },
        ajaxSetup: function(e, t) {
            return t ? ne(ne(e, we.ajaxSettings), t) : ne(we.ajaxSettings, e)
        },
        ajaxPrefilter: ee(Ft),
        ajaxTransport: ee(_t),
        ajax: function(e, t) {},
        getJSON: function(e, t, n) {
            return we.get(e, t, n, "json")
        },
        getScript: function(e, t) {
            return we.get(e, void 0, t, "script")
        }
    }), we.each(["get", "post"], function(e, t) {
        we[t] = function(e, n, r, i) {
            return me(n) && (i = i || r, r = n, n = void 0), we.ajax(we.extend({
                url: e,
                type: t,
                dataType: i,
                data: n,
                success: r
            }, we.isPlainObject(e) && e))
        }
    }), we._evalUrl = function(e) {
        return we.ajax({
            url: e,
            type: "GET",
            dataType: "script",
            cache: !0,
            async: !1,
            global: !1,
            "throws": !0
        })
    }, we.fn.extend({
        wrapAll: function(e) {
            var t;
            return this[0] && (me(e) && (e = e.call(this[0])), t = we(e, this[0].ownerDocument).eq(0).clone(!0), this[0].parentNode && t.insertBefore(this[0]), t.map(function() {
                for (var e = this; e.firstElementChild;) e = e.firstElementChild;
                return e
            }).append(this)), this
        },
        wrapInner: function(e) {
            return me(e) ? this.each(function(t) {
                we(this).wrapInner(e.call(this, t))
            }) : this.each(function() {
                var t = we(this),
                    n = t.contents();
                n.length ? n.wrapAll(e) : t.append(e)
            })
        },
        wrap: function(e) {
            var t = me(e);
            return this.each(function(n) {
                we(this).wrapAll(t ? e.call(this, n) : e)
            })
        },
        unwrap: function(e) {
            return this.parent(e).not("body").each(function() {
                we(this).replaceWith(this.childNodes)
            }), this
        }
    }), we.expr.pseudos.hidden = function(e) {
        return !we.expr.pseudos.visible(e)
    }, we.expr.pseudos.visible = function(e) {
        return !!(e.offsetWidth || e.offsetHeight || e.getClientRects().length)
    }, we.ajaxSettings.xhr = function() {
        try {
            return new e.XMLHttpRequest
        } catch (e) {}
    };
    var Ut = {
            0: 200,
            1223: 204
        },
        Vt = we.ajaxSettings.xhr();
    ve.cors = !!Vt && "withCredentials" in Vt, ve.ajax = Vt = !!Vt, we.ajaxTransport(function(e) {
        var t, n;
        if (ve.cors || Vt && !e.crossDomain) return {
            send: function(r, i) {
                var o, a = e.xhr();
                if (a.open(e.type, e.url, e.async, e.username, e.password), e.xhrFields)
                    for (o in e.xhrFields) a[o] = e.xhrFields[o];
                e.mimeType && a.overrideMimeType && a.overrideMimeType(e.mimeType), e.crossDomain || r["X-Requested-With"] || (r["X-Requested-With"] = "XMLHttpRequest");
                for (o in r) a.setRequestHeader(o, r[o]);
                t = function(e) {
                    return function() {
                        t && (t = n = a.onload = a.onerror = a.onabort = a.ontimeout = a.onreadystatechange = null, "abort" === e ? a.abort() : "error" === e ? "number" != typeof a.status ? i(0, "error") : i(a.status, a.statusText) : i(Ut[a.status] || a.status, a.statusText, "text" !== (a.responseType || "text") || "string" != typeof a.responseText ? {
                            binary: a.response
                        } : {
                            text: a.responseText
                        }, a.getAllResponseHeaders()))
                    }
                }, a.onload = t(), n = a.onerror = a.ontimeout = t("error"), void 0 !== a.onabort ? a.onabort = n : a.onreadystatechange = function() {
                    4 === a.readyState && s.setTimeout(function() {
                        t && n()
                    })
                }, t = t("abort");
                try {
                    a.send(e.hasContent && e.data || null)
                } catch (s) {
                    if (t) throw s
                }
            },
            abort: function() {
                t && t()
            }
        }
    }), we.ajaxPrefilter(function(e) {
        e.crossDomain && (e.contents.script = !1)
    }), we.ajaxSetup({
        accepts: {
            script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
        },
        contents: {
            script: /\b(?:java|ecma)script\b/
        },
        converters: {
            "text script": function(e) {
                return we.globalEval(e), e
            }
        }
    }), we.ajaxPrefilter("script", function(e) {
        void 0 === e.cache && (e.cache = !1), e.crossDomain && (e.type = "GET")
    }), we.ajaxTransport("script", function(e) {
        if (e.crossDomain) {
            var t, n;
            return {
                send: function(r, i) {
                    t = we("<script>").prop({
                        charset: e.scriptCharset,
                        src: e.url
                    }).on("load error", n = function(e) {
                        t.remove(), n = null, e && i("error" === e.type ? 404 : 200, e.type)
                    }), ae.head.appendChild(t[0])
                },
                abort: function() {
                    n && n()
                }
            }
        }
    });
    var Gt = [],
        Yt = /(=)\?(?=&|$)|\?\?/;
    we.ajaxSetup({
        jsonp: "callback",
        jsonpCallback: function() {
            var e = Gt.pop() || we.expando + "_" + At++;
            return this[e] = !0, e
        }
    }), we.ajaxPrefilter("json jsonp", function(t, n, r) {
        var i, o, a, s = !1 !== t.jsonp && (Yt.test(t.url) ? "url" : "string" == typeof t.data && 0 === (t.contentType || "").indexOf("application/x-www-form-urlencoded") && Yt.test(t.data) && "data");
        if (s || "jsonp" === t.dataTypes[0]) return i = t.jsonpCallback = me(t.jsonpCallback) ? t.jsonpCallback() : t.jsonpCallback, s ? t[s] = t[s].replace(Yt, "$1" + i) : !1 !== t.jsonp && (t.url += (jt.test(t.url) ? "&" : "?") + t.jsonp + "=" + i), t.converters["script json"] = function() {
            return a || we.error(i + " was not called"), a[0]
        }, t.dataTypes[0] = "json", o = e[i], e[i] = function() {
            a = arguments
        }, r.always(function() {
            void 0 === o ? we(e).removeProp(i) : e[i] = o, t[i] && (t.jsonpCallback = n.jsonpCallback, Gt.push(i)), a && me(o) && o(a[0]), a = o = void 0
        }), "script"
    }), ve.createHTMLDocument = function() {
        var e = ae.implementation.createHTMLDocument("").body;
        return e.innerHTML = "<form></form><form></form>", 2 === e.childNodes.length
    }(), we.parseHTML = function(e, t, n) {
        if ("string" != typeof e) return [];
        "boolean" == typeof t && (n = t, t = !1);
        var r, i, o;
        return t || (ve.createHTMLDocument ? ((r = (t = ae.implementation.createHTMLDocument("")).createElement("base")).href = ae.location.href, t.head.appendChild(r)) : t = ae), i = De.exec(e), o = !n && [], i ? [t.createElement(i[1])] : (i = C([e], t, o), o && o.length && we(o).remove(), we.merge([], i.childNodes))
    }, we.fn.load = function(e, t, n) {
        var r, i, o, a = this,
            s = e.indexOf(" ");
        return s > -1 && (r = Q(e.slice(s)), e = e.slice(0, s)), me(t) ? (n = t, t = void 0) : t && "object" == typeof t && (i = "POST"), a.length > 0 && we.ajax({
            url: e,
            type: i || "GET",
            dataType: "html",
            data: t
        }).done(function(e) {
            o = arguments, a.html(r ? we("<div>").append(we.parseHTML(e)).find(r) : e)
        }).always(n && function(e, t) {
            a.each(function() {
                n.apply(this, o || [e.responseText, t, e])
            })
        }), this
    }, we.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function(e, t) {
        we.fn[t] = function(e) {
            return this.on(t, e)
        }
    }), we.expr.pseudos.animated = function(e) {
        return we.grep(we.timers, function(t) {
            return e === t.elem
        }).length
    }, we.offset = {
        setOffset: function(e, t, n) {
            var r, i, o, a, s, u, l, c = we.css(e, "position"),
                f = we(e),
                p = {};
            "static" === c && (e.style.position = "relative"), s = f.offset(), o = we.css(e, "top"), u = we.css(e, "left"), (l = ("absolute" === c || "fixed" === c) && (o + u).indexOf("auto") > -1) ? (a = (r = f.position()).top, i = r.left) : (a = parseFloat(o) || 0, i = parseFloat(u) || 0), me(t) && (t = t.call(e, n, we.extend({}, s))), null != t.top && (p.top = t.top - s.top + a), null != t.left && (p.left = t.left - s.left + i), "using" in t ? t.using.call(e, p) : f.css(p)
        }
    }, we.fn.extend({
        offset: function(e) {
            if (arguments.length) return void 0 === e ? this : this.each(function(t) {
                we.offset.setOffset(this, e, t)
            });
            var t, n, r = this[0];
            return r ? r.getClientRects().length ? (t = r.getBoundingClientRect(), n = r.ownerDocument.defaultView, {
                top: t.top + n.pageYOffset,
                left: t.left + n.pageXOffset
            }) : {
                top: 0,
                left: 0
            } : void 0
        },
        position: function() {
            if (this[0]) {
                var e, t, n, r = this[0],
                    i = {
                        top: 0,
                        left: 0
                    };
                if ("fixed" === we.css(r, "position")) t = r.getBoundingClientRect();
                else {
                    for (t = this.offset(), n = r.ownerDocument, e = r.offsetParent || n.documentElement; e && (e === n.body || e === n.documentElement) && "static" === we.css(e, "position");) e = e.parentNode;
                    e && e !== r && 1 === e.nodeType && ((i = we(e).offset()).top += we.css(e, "borderTopWidth", !0), i.left += we.css(e, "borderLeftWidth", !0))
                }
                return {
                    top: t.top - i.top - we.css(r, "marginTop", !0),
                    left: t.left - i.left - we.css(r, "marginLeft", !0)
                }
            }
        },
        offsetParent: function() {
            return this.map(function() {
                for (var e = this.offsetParent; e && "static" === we.css(e, "position");) e = e.offsetParent;
                return e || et
            })
        }
    }), we.each({
        scrollLeft: "pageXOffset",
        scrollTop: "pageYOffset"
    }, function(e, t) {
        var n = "pageYOffset" === t;
        we.fn[e] = function(r) {
            return Pe(this, function(e, r, i) {
                var o;
                return xe(e) ? o = e : 9 === e.nodeType && (o = e.defaultView), void 0 === i ? o ? o[t] : e[r] : void(o ? o.scrollTo(n ? o.pageXOffset : i, n ? i : o.pageYOffset) : e[r] = i)
            }, e, r, arguments.length)
        }
    }), we.each(["top", "left"], function(e, t) {
        we.cssHooks[t] = M(ve.pixelPosition, function(e, n) {
            if (n) return n = P(e, t), ut.test(n) ? we(e).position()[t] + "px" : n
        })
    }), we.each({
        Height: "height",
        Width: "width"
    }, function(e, t) {
        we.each({
            padding: "inner" + e,
            content: t,
            "": "outer" + e
        }, function(n, r) {
            we.fn[r] = function(i, o) {
                var a = arguments.length && (n || "boolean" != typeof i),
                    s = n || (!0 === i || !0 === o ? "margin" : "border");
                return Pe(this, function(t, n, i) {
                    var o;
                    return xe(t) ? 0 === r.indexOf("outer") ? t["inner" + e] : t.document.documentElement["client" + e] : 9 === t.nodeType ? (o = t.documentElement, Math.max(t.body["scroll" + e], o["scroll" + e], t.body["offset" + e], o["offset" + e], o["client" + e])) : void 0 === i ? we.css(t, n, s) : we.style(t, n, i, s)
                }, t, a ? i : void 0, a)
            }
        })
    }), we.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "), function(e, t) {
        we.fn[t] = function(e, n) {
            return arguments.length > 0 ? this.on(t, null, e, n) : this.trigger(t)
        }
    }), we.fn.extend({
        hover: function(e, t) {
            return this.mouseenter(e).mouseleave(t || e)
        }
    }), we.fn.extend({
        bind: function(e, t, n) {
            return this.on(e, null, t, n)
        },
        unbind: function(e, t) {
            return this.off(e, null, t)
        },
        delegate: function(e, t, n, r) {
            return this.on(t, e, n, r)
        },
        undelegate: function(e, t, n) {
            return 1 === arguments.length ? this.off(e, "**") : this.off(t, e || "**", n)
        }
    }), we.proxy = function(e, t) {
        var n, r, i;
        if ("string" == typeof t && (n = e[t], t = e, e = n), me(e)) return r = ue.call(arguments, 2), i = function() {
            return e.apply(t || this, r.concat(ue.call(arguments)))
        }, i.guid = e.guid = e.guid || we.guid++, i
    }, we.holdReady = function(e) {
        e ? we.readyWait++ : we.ready(!0)
    }, we.isArray = Array.isArray, we.parseJSON = JSON.parse, we.nodeName = o, we.isFunction = me, we.isWindow = xe, we.camelCase = h, we.type = r, we.now = Date.now, we.isNumeric = function(e) {
        var t = we.type(e);
        return ("number" === t || "string" === t) && !isNaN(e - parseFloat(e))
    }, "function" == typeof define && define.amd && define("jquery", [], function() {
        return we
    });
    var Qt = e.jQuery,
        Jt = e.$;
    return we.noConflict = function(t) {
        return e.$ === we && (e.$ = Jt), t && e.jQuery === we && (e.jQuery = Qt), we
    }, t || (e.jQuery = e.$ = we), we
});
! function(e, n) {
    "use strict";
    "function" == typeof define && define.amd ? define(["jquery"], function(t) {
        return n(t, e, e.document, e.Math)
    }) : "object" == typeof exports && exports ? module.exports = n(require("jquery"), e, e.document, e.Math) : n(jQuery, e, e.document, e.Math)
}("undefined" != typeof window ? window : this, function(e, n, t, o, i) {
    "use strict";
    var a = "fullpage-wrapper",
        l = "." + a,
        r = "fp-responsive",
        s = "fp-notransition",
        c = "fp-destroyed",
        d = "fp-enabled",
        f = "fp-viewing",
        u = "active",
        h = "." + u,
        p = "fp-completely",
        v = "." + p,
        g = ".section",
        m = "fp-section",
        w = "." + m,
        y = w + h,
        S = w + ":first",
        b = w + ":last",
        x = "fp-tableCell",
        C = "." + x,
        T = "fp-auto-height",
        k = "fp-normal-scroll",
        L = "fp-nav",
        A = "#" + L,
        O = "fp-tooltip",
        I = "." + O,
        E = "fp-show-active",
        M = ".slide",
        B = "fp-slide",
        R = "." + B,
        z = R + h,
        H = "fp-slides",
        D = "." + H,
        P = "fp-slidesContainer",
        q = "." + P,
        F = "fp-table",
        V = "fp-slidesNav",
        j = "." + V,
        Y = j + " a",
        N = "fp-controlArrow",
        X = "." + N,
        U = "fp-prev",
        W = "." + U,
        K = N + " " + U,
        _ = X + W,
        Q = "fp-next",
        G = "." + Q,
        J = N + " " + Q,
        Z = X + G,
        $ = e(n),
        ee = e(t);
    e.fn.fullpage = function(N) {
        function W(n, t) {
            n || Zn(0), ot("autoScrolling", n, t);
            var o = e(y);
            N.autoScrolling && !N.scrollBar ? (lt.css({
                overflow: "hidden",
                height: "100%"
            }), Q(Rt.recordHistory, "internal"), gt.css({
                "-ms-touch-action": "none",
                "touch-action": "none"
            }), o.length && Zn(o.position().top)) : (lt.css({
                overflow: "visible",
                height: "initial"
            }), Q(!1, "internal"), gt.css({
                "-ms-touch-action": "",
                "touch-action": ""
            }), o.length && lt.scrollTop(o.position().top))
        }

        function Q(e, n) {
            ot("recordHistory", e, n)
        }

        function G(e, n) {
            ot("scrollingSpeed", e, n)
        }

        function ne(e, n) {
            ot("fitToSection", e, n)
        }

        function te(e) {
            N.lockAnchors = e
        }

        function oe(e) {
            e ? (Xn(), Un()) : (Nn(), Wn())
        }

        function ie(n, t) {
            "undefined" != typeof t ? (t = t.replace(/ /g, "").split(","), e.each(t, function(e, t) {
                et(n, t, "m")
            })) : (et(n, "all", "m"), n ? (oe(!0), Kn()) : (oe(!1), _n()))
        }

        function ae(n, t) {
            "undefined" != typeof t ? (t = t.replace(/ /g, "").split(","), e.each(t, function(e, t) {
                et(n, t, "k")
            })) : (et(n, "all", "k"), N.keyboardScrolling = n)
        }

        function le() {
            var n = e(y).prev(w);
            n.length || !N.loopTop && !N.continuousVertical || (n = e(w).last()), n.length && Ne(n, null, !0)
        }

        function re() {
            var n = e(y).next(w);
            n.length || !N.loopBottom && !N.continuousVertical || (n = e(w).first()), n.length && Ne(n, null, !1)
        }

        function se(e, n) {
            G(0, "internal"), ce(e, n), G(Rt.scrollingSpeed, "internal")
        }

        function ce(e, n) {
            var t = Rn(e);
            "undefined" != typeof n ? Hn(e, n) : t.length > 0 && Ne(t)
        }

        function de(e) {
            Ve("right", e)
        }

        function fe(e) {
            Ve("left", e)
        }

        function ue(n) {
            if (!gt.hasClass(c)) {
                wt = !0, mt = $.height(), e(w).each(function() {
                    var n = e(this).find(D),
                        t = e(this).find(R);
                    N.verticalCentered && e(this).find(C).css("height", Mn(e(this)) + "px"), e(this).css("height", mt + "px"), t.length > 1 && mn(n, n.find(z))
                }), N.scrollOverflow && Ct.createScrollBarForAll();
                var t = e(y),
                    o = t.index(w);
                o && se(o + 1), wt = !1, e.isFunction(N.afterResize) && n && N.afterResize.call(gt), e.isFunction(N.afterReBuild) && !n && N.afterReBuild.call(gt)
            }
        }

        function he(n) {
            var t = rt.hasClass(r);
            n ? t || (W(!1, "internal"), ne(!1, "internal"), e(A).hide(), rt.addClass(r), e.isFunction(N.afterResponsive) && N.afterResponsive.call(gt, n)) : t && (W(Rt.autoScrolling, "internal"), ne(Rt.autoScrolling, "internal"), e(A).show(), rt.removeClass(r), e.isFunction(N.afterResponsive) && N.afterResponsive.call(gt, n))
        }

        function pe() {
            N.css3 && (N.css3 = Yn()), N.scrollBar = N.scrollBar || N.hybrid, ge(), me(), ie(!0), W(N.autoScrolling, "internal"), xn(), jn(), "complete" === t.readyState && nn(), $.on("load", nn)
        }

        function ve() {
            $.on("scroll", Ie).on("hashchange", tn).blur(fn).resize(bn), ee.keydown(an).keyup(rn).on("click touchstart", A + " a", un).on("click touchstart", Y, hn).on("click", I, ln), e(w).on("click touchstart", X, dn), N.normalScrollElements && (ee.on("mouseenter touchstart", N.normalScrollElements, function() {
                ie(!1)
            }), ee.on("mouseleave touchend", N.normalScrollElements, function() {
                ie(!0)
            }))
        }

        function ge() {
            var n = gt.find(N.sectionSelector);
            N.anchors.length || (N.anchors = n.filter("[data-anchor]").map(function() {
                return e(this).data("anchor").toString()
            }).get()), N.navigationTooltips.length || (N.navigationTooltips = n.filter("[data-tooltip]").map(function() {
                return e(this).data("tooltip").toString()
            }).get())
        }

        function me() {
            gt.css({
                height: "100%",
                position: "relative"
            }), gt.addClass(a), e("html").addClass(d), mt = $.height(), gt.removeClass(c), be(), e(w).each(function(n) {
                var t = e(this),
                    o = t.find(R),
                    i = o.length;
                t.data("fp-styles", t.attr("style")), ye(t, n), Se(t, n), i > 0 ? we(t, o, i) : N.verticalCentered && En(t)
            }), N.fixedElements && N.css3 && e(N.fixedElements).appendTo(rt), N.navigation && Ce(), Te(), N.scrollOverflow ? Ct = N.scrollOverflowHandler.init(N) : Ae()
        }

        function we(n, t, o) {
            var i = 100 * o,
                a = 100 / o;
            t.wrapAll('<div class="' + P + '" />'), t.parent().wrap('<div class="' + H + '" />'), n.find(q).css("width", i + "%"), o > 1 && (N.controlArrows && xe(n), N.slidesNavigation && Pn(n, o)), t.each(function(n) {
                e(this).css("width", a + "%"), N.verticalCentered && En(e(this))
            });
            var l = n.find(z);
            l.length && (0 !== e(y).index(w) || 0 === e(y).index(w) && 0 !== l.index()) ? Jn(l, "internal") : t.eq(0).addClass(u)
        }

        function ye(n, t) {
            t || 0 !== e(y).length || n.addClass(u), ut = e(y), n.css("height", mt + "px"), N.paddingTop && n.css("padding-top", N.paddingTop), N.paddingBottom && n.css("padding-bottom", N.paddingBottom), "undefined" != typeof N.sectionsColor[t] && n.css("background-color", N.sectionsColor[t]), "undefined" != typeof N.anchors[t] && n.attr("data-anchor", N.anchors[t])
        }

        function Se(n, t) {
            "undefined" != typeof N.anchors[t] && n.hasClass(u) && An(N.anchors[t], t), N.menu && N.css3 && e(N.menu).closest(l).length && e(N.menu).appendTo(rt)
        }

        function be() {
            gt.find(N.sectionSelector).addClass(m), gt.find(N.slideSelector).addClass(B)
        }

        function xe(e) {
            e.find(D).after('<div class="' + K + '"></div><div class="' + J + '"></div>'), "#fff" != N.controlArrowColor && (e.find(Z).css("border-color", "transparent transparent transparent " + N.controlArrowColor), e.find(_).css("border-color", "transparent " + N.controlArrowColor + " transparent transparent")), N.loopHorizontal || e.find(_).hide()
        }

        function Ce() {
            rt.append('<div id="' + L + '"><ul></ul></div>');
            var n = e(A);
            n.addClass(function() {
                return N.showActiveTooltip ? E + " " + N.navigationPosition : N.navigationPosition
            });
            for (var t = 0; t < e(w).length; t++) {
                var o = "";
                N.anchors.length && (o = N.anchors[t]);
                var i = '<li><a href="#' + o + '"><span></span></a>',
                    a = N.navigationTooltips[t];
                "undefined" != typeof a && "" !== a && (i += '<div class="' + O + " " + N.navigationPosition + '">' + a + "</div>"), i += "</li>", n.find("ul").append(i)
            }
            e(A).css("margin-top", "-" + e(A).height() / 2 + "px"), e(A).find("li").eq(e(y).index(w)).find("a").addClass(u)
        }

        function Te() {
            gt.find('iframe[src*="youtube.com/embed/"]').each(function() {
                ke(e(this), "enablejsapi=1")
            })
        }

        function ke(e, n) {
            var t = e.attr("src");
            e.attr("src", t + Le(t) + n)
        }

        function Le(e) {
            return /\?/.test(e) ? "&" : "?"
        }

        function Ae() {
            var n = e(y);
            n.addClass(p), Ge(n), Je(n), N.scrollOverflow && N.scrollOverflowHandler.afterLoad(), Oe() && e.isFunction(N.afterLoad) && N.afterLoad.call(n, n.data("anchor"), n.index(w) + 1), e.isFunction(N.afterRender) && N.afterRender.call(gt)
        }

        function Oe() {
            var e = Rn(on().section);
            return !e || e.length && e.index() === ut.index()
        }

        function Ie() {
            var n;
            if (!N.autoScrolling || N.scrollBar) {
                var o = $.scrollTop(),
                    i = Be(o),
                    a = 0,
                    l = o + $.height() / 2,
                    r = rt.height() - $.height() === o,
                    s = t.querySelectorAll(w);
                if (r) a = s.length - 1;
                else if (o)
                    for (var c = 0; c < s.length; ++c) {
                        var d = s[c];
                        d.offsetTop <= l && (a = c)
                    } else a = 0;
                if (Me(i) && (e(y).hasClass(p) || e(y).addClass(p).siblings().removeClass(p)), n = e(s).eq(a), !n.hasClass(u)) {
                    zt = !0;
                    var f, h, v = e(y),
                        g = v.index(w) + 1,
                        m = On(n),
                        S = n.data("anchor"),
                        b = n.index(w) + 1,
                        x = n.find(z);
                    x.length && (h = x.data("anchor"), f = x.index()), St && (n.addClass(u).siblings().removeClass(u), e.isFunction(N.onLeave) && N.onLeave.call(v, g, b, m), e.isFunction(N.afterLoad) && N.afterLoad.call(n, S, b), $e(v), Ge(n), Je(n), An(S, b - 1), N.anchors.length && (ct = S), qn(f, h, S, b)), clearTimeout(At), At = setTimeout(function() {
                        zt = !1
                    }, 100)
                }
                N.fitToSection && (clearTimeout(Ot), Ot = setTimeout(function() {
                    N.fitToSection && e(y).outerHeight() <= mt && Ee()
                }, N.fitToSectionDelay))
            }
        }

        function Ee() {
            St && (wt = !0, Ne(e(y)), wt = !1)
        }

        function Me(n) {
            var t = e(y).position().top,
                o = t + $.height();
            return "up" == n ? o >= $.scrollTop() + $.height() : t <= $.scrollTop()
        }

        function Be(e) {
            var n = e > Ht ? "down" : "up";
            return Ht = e, jt = e, n
        }

        function Re(n) {
            if (xt.m[n]) {
                var t = "down" === n ? re : le;
                if (N.scrollOverflow) {
                    var o = N.scrollOverflowHandler.scrollable(e(y)),
                        i = "down" === n ? "bottom" : "top";
                    if (o.length > 0) {
                        if (!N.scrollOverflowHandler.isScrolled(i, o)) return !0;
                        t()
                    } else t()
                } else t()
            }
        }

        function ze(e) {
            var n = e.originalEvent;
            N.autoScrolling && De(n) && e.preventDefault()
        }

        function He(n) {
            var t = n.originalEvent,
                i = e(t.target).closest(w);
            if (De(t)) {
                N.autoScrolling && n.preventDefault();
                var a = Gn(t);
                qt = a.y, Ft = a.x, i.find(D).length && o.abs(Pt - Ft) > o.abs(Dt - qt) ? !ht && o.abs(Pt - Ft) > $.outerWidth() / 100 * N.touchSensitivity && (Pt > Ft ? xt.m.right && de(i) : xt.m.left && fe(i)) : N.autoScrolling && St && o.abs(Dt - qt) > $.height() / 100 * N.touchSensitivity && (Dt > qt ? Re("down") : qt > Dt && Re("up"))
            }
        }

        function De(e) {
            return "undefined" == typeof e.pointerType || "mouse" != e.pointerType
        }

        function Pe(e) {
            var n = e.originalEvent;
            if (N.fitToSection && lt.stop(), De(n)) {
                var t = Gn(n);
                Dt = t.y, Pt = t.x
            }
        }

        function qe(e, n) {
            for (var t = 0, i = e.slice(o.max(e.length - n, 1)), a = 0; a < i.length; a++) t += i[a];
            return o.ceil(t / n)
        }

        function Fe(t) {
            var i = (new Date).getTime(),
                a = e(v).hasClass(k);
            if (N.autoScrolling && !ft && !a) {
                t = t || n.event;
                var l = t.wheelDelta || -t.deltaY || -t.detail,
                    r = o.max(-1, o.min(1, l)),
                    s = "undefined" != typeof t.wheelDeltaX || "undefined" != typeof t.deltaX,
                    c = o.abs(t.wheelDeltaX) < o.abs(t.wheelDelta) || o.abs(t.deltaX) < o.abs(t.deltaY) || !s;
                bt.length > 149 && bt.shift(), bt.push(o.abs(l)), N.scrollBar && (t.preventDefault ? t.preventDefault() : t.returnValue = !1);
                var d = i - Vt;
                if (Vt = i, d > 200 && (bt = []), St) {
                    var f = qe(bt, 10),
                        u = qe(bt, 70),
                        h = f >= u;
                    h && c && Re(r < 0 ? "down" : "up")
                }
                return !1
            }
            N.fitToSection && lt.stop()
        }

        function Ve(n, t) {
            var o = "undefined" == typeof t ? e(y) : t,
                i = o.find(D),
                a = i.find(R).length;
            if (!(!i.length || ht || a < 2)) {
                var l = i.find(z),
                    r = null;
                if (r = "left" === n ? l.prev(R) : l.next(R), !r.length) {
                    if (!N.loopHorizontal) return;
                    r = "left" === n ? l.siblings(":last") : l.siblings(":first")
                }
                ht = !0, mn(i, r, n)
            }
        }

        function je() {
            e(z).each(function() {
                Jn(e(this), "internal")
            })
        }

        function Ye(e) {
            var n = e.position(),
                t = n.top,
                o = n.top > jt,
                i = t - mt + e.outerHeight(),
                a = N.bigSectionsDestination;
            return e.outerHeight() > mt ? (o || a) && "bottom" !== a || (t = i) : (o || wt && e.is(":last-child")) && (t = i), jt = t, t
        }

        function Ne(n, t, o) {
            if ("undefined" != typeof n) {
                var i, a, l = Ye(n),
                    r = {
                        element: n,
                        callback: t,
                        isMovementUp: o,
                        dtop: l,
                        yMovement: On(n),
                        anchorLink: n.data("anchor"),
                        sectionIndex: n.index(w),
                        activeSlide: n.find(z),
                        activeSection: e(y),
                        leavingSection: e(y).index(w) + 1,
                        localIsResizing: wt
                    };
                if (!(r.activeSection.is(n) && !wt || N.scrollBar && $.scrollTop() === r.dtop && !n.hasClass(T))) {
                    if (r.activeSlide.length && (i = r.activeSlide.data("anchor"), a = r.activeSlide.index()), e.isFunction(N.onLeave) && !r.localIsResizing) {
                        var s = r.yMovement;
                        if ("undefined" != typeof o && (s = o ? "up" : "down"), N.onLeave.call(r.activeSection, r.leavingSection, r.sectionIndex + 1, s) === !1) return
                    }
                    N.autoScrolling && N.continuousVertical && "undefined" != typeof r.isMovementUp && (!r.isMovementUp && "up" == r.yMovement || r.isMovementUp && "down" == r.yMovement) && (r = We(r)), r.localIsResizing || $e(r.activeSection), N.scrollOverflow && N.scrollOverflowHandler.beforeLeave(), n.addClass(u).siblings().removeClass(u), Ge(n), N.scrollOverflow && N.scrollOverflowHandler.onLeave(), St = !1, qn(a, i, r.anchorLink, r.sectionIndex), Xe(r), ct = r.anchorLink, An(r.anchorLink, r.sectionIndex)
                }
            }
        }

        function Xe(n) {
            if (N.css3 && N.autoScrolling && !N.scrollBar) {
                var t = "translate3d(0px, -" + o.round(n.dtop) + "px, 0px)";
                Bn(t, !0), N.scrollingSpeed ? (clearTimeout(kt), kt = setTimeout(function() {
                    _e(n)
                }, N.scrollingSpeed)) : _e(n)
            } else {
                var i = Ue(n);
                e(i.element).animate(i.options, N.scrollingSpeed, N.easing).promise().done(function() {
                    N.scrollBar ? setTimeout(function() {
                        _e(n)
                    }, 30) : _e(n)
                })
            }
        }

        function Ue(e) {
            var n = {};
            return N.autoScrolling && !N.scrollBar ? (n.options = {
                top: -e.dtop
            }, n.element = l) : (n.options = {
                scrollTop: e.dtop
            }, n.element = "html, body"), n
        }

        function We(n) {
            return n.isMovementUp ? e(y).before(n.activeSection.nextAll(w)) : e(y).after(n.activeSection.prevAll(w).get().reverse()), Zn(e(y).position().top), je(), n.wrapAroundElements = n.activeSection, n.dtop = n.element.position().top, n.yMovement = On(n.element), n.leavingSection = n.activeSection.index(w) + 1, n.sectionIndex = n.element.index(w), n
        }

        function Ke(n) {
            n.wrapAroundElements && n.wrapAroundElements.length && (n.isMovementUp ? e(S).before(n.wrapAroundElements) : e(b).after(n.wrapAroundElements), Zn(e(y).position().top), je())
        }

        function _e(n) {
            Ke(n), e.isFunction(N.afterLoad) && !n.localIsResizing && N.afterLoad.call(n.element, n.anchorLink, n.sectionIndex + 1), N.scrollOverflow && N.scrollOverflowHandler.afterLoad(), n.localIsResizing || Je(n.element), n.element.addClass(p).siblings().removeClass(p), St = !0, e.isFunction(n.callback) && n.callback.call(this)
        }

        function Qe(e, n) {
            e.attr(n, e.data(n)).removeAttr("data-" + n)
        }

        function Ge(n) {
            if (N.lazyLoading) {
                var t, o = en(n);
                o.find("img[data-src], img[data-srcset], source[data-src], source[data-srcset], video[data-src], audio[data-src], iframe[data-src]").each(function() {
                    if (t = e(this), e.each(["src", "srcset"], function(e, n) {
                            var o = t.attr("data-" + n);
                            "undefined" != typeof o && o && Qe(t, n)
                        }), t.is("source") && !t.closest("picture").length) {
                        var n = t.closest("video").length ? "video" : "audio";
                        t.closest(n).get(0).load()
                    }
                })
            }
        }

        function Je(n) {
            var t = en(n);
            t.find("video, audio").each(function() {
                var n = e(this).get(0);
                n.hasAttribute("data-autoplay") && "function" == typeof n.play && n.play()
            }), t.find('iframe[src*="youtube.com/embed/"]').each(function() {
                var n = e(this).get(0);
                n.hasAttribute("data-autoplay") && Ze(n), n.onload = function() {
                    n.hasAttribute("data-autoplay") && Ze(n)
                }
            })
        }

        function Ze(e) {
            e.contentWindow.postMessage('{"event":"command","func":"playVideo","args":""}', "*")
        }

        function $e(n) {
            var t = en(n);
            t.find("video, audio").each(function() {
                var n = e(this).get(0);
                n.hasAttribute("data-keepplaying") || "function" != typeof n.pause || n.pause()
            }), t.find('iframe[src*="youtube.com/embed/"]').each(function() {
                var n = e(this).get(0);
                /youtube\.com\/embed\//.test(e(this).attr("src")) && !n.hasAttribute("data-keepplaying") && e(this).get(0).contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', "*")
            })
        }

        function en(n) {
            var t = n.find(z);
            return t.length && (n = e(t)), n
        }

        function nn() {
            var e = on(),
                n = e.section,
                t = e.slide;
            n && (N.animateAnchor ? Hn(n, t) : se(n, t))
        }

        function tn() {
            if (!zt && !N.lockAnchors) {
                var e = on(),
                    n = e.section,
                    t = e.slide,
                    o = "undefined" == typeof ct,
                    i = "undefined" == typeof ct && "undefined" == typeof t && !ht;
                n && n.length && (n && n !== ct && !o || i || !ht && dt != t) && Hn(n, t)
            }
        }

        function on() {
            var e, t, o = n.location.hash;
            if (o.length) {
                var i = o.replace("#", "").split("/"),
                    a = o.indexOf("#/") > -1;
                e = a ? "/" + i[1] : decodeURIComponent(i[0]);
                var l = a ? i[2] : i[1];
                l && l.length && (t = decodeURIComponent(l))
            }
            return {
                section: e,
                slide: t
            }
        }

        function an(n) {
            clearTimeout(It);
            var t = e(":focus"),
                o = n.which;
            if (9 === o) vn(n);
            else if (!t.is("textarea") && !t.is("input") && !t.is("select") && "true" !== t.attr("contentEditable") && "" !== t.attr("contentEditable") && N.keyboardScrolling && N.autoScrolling) {
                var i = [40, 38, 32, 33, 34];
                e.inArray(o, i) > -1 && n.preventDefault(), ft = n.ctrlKey, It = setTimeout(function() {
                    pn(n)
                }, 150)
            }
        }

        function ln() {
            e(this).prev().trigger("click")
        }

        function rn(e) {
            yt && (ft = e.ctrlKey)
        }

        function sn(e) {
            2 == e.which && (Yt = e.pageY, gt.on("mousemove", gn))
        }

        function cn(e) {
            2 == e.which && gt.off("mousemove")
        }

        function dn() {
            var n = e(this).closest(w);
            e(this).hasClass(U) ? xt.m.left && fe(n) : xt.m.right && de(n)
        }

        function fn() {
            yt = !1, ft = !1
        }

        function un(n) {
            n.preventDefault();
            var t = e(this).parent().index();
            Ne(e(w).eq(t))
        }

        function hn(n) {
            n.preventDefault();
            var t = e(this).closest(w).find(D),
                o = t.find(R).eq(e(this).closest("li").index());
            mn(t, o)
        }

        function pn(n) {
            var t = n.shiftKey;
            if (St || !([37, 39].indexOf(n.which) < 0)) switch (n.which) {
                case 38:
                case 33:
                    xt.k.up && le();
                    break;
                case 32:
                    if (t && xt.k.up) {
                        le();
                        break
                    }
                case 40:
                case 34:
                    xt.k.down && re();
                    break;
                case 36:
                    xt.k.up && ce(1);
                    break;
                case 35:
                    xt.k.down && ce(e(w).length);
                    break;
                case 37:
                    xt.k.left && fe();
                    break;
                case 39:
                    xt.k.right && de();
                    break;
                default:
                    return
            }
        }

        function vn(n) {
            function t(e) {
                return e.preventDefault(), s.first().focus()
            }
            var o = n.shiftKey,
                i = e(":focus"),
                a = e(y),
                l = a.find(z),
                r = l.length ? l : a,
                s = r.find(Bt).not('[tabindex="-1"]');
            i.length ? i.closest(y, z).length || (i = t(n)) : t(n), (!o && i.is(s.last()) || o && i.is(s.first())) && n.preventDefault()
        }

        function gn(e) {
            St && (e.pageY < Yt && xt.m.up ? le() : e.pageY > Yt && xt.m.down && re()), Yt = e.pageY
        }

        function mn(n, t, o) {
            var i = n.closest(w),
                a = {
                    slides: n,
                    destiny: t,
                    direction: o,
                    destinyPos: t.position(),
                    slideIndex: t.index(),
                    section: i,
                    sectionIndex: i.index(w),
                    anchorLink: i.data("anchor"),
                    slidesNav: i.find(j),
                    slideAnchor: Vn(t),
                    prevSlide: i.find(z),
                    prevSlideIndex: i.find(z).index(),
                    localIsResizing: wt
                };
            return a.xMovement = In(a.prevSlideIndex, a.slideIndex), a.localIsResizing || (St = !1), N.onSlideLeave && !a.localIsResizing && "none" !== a.xMovement && e.isFunction(N.onSlideLeave) && N.onSlideLeave.call(a.prevSlide, a.anchorLink, a.sectionIndex + 1, a.prevSlideIndex, a.direction, a.slideIndex) === !1 ? void(ht = !1) : (t.addClass(u).siblings().removeClass(u), a.localIsResizing || ($e(a.prevSlide), Ge(t)), !N.loopHorizontal && N.controlArrows && (i.find(_).toggle(0 !== a.slideIndex), i.find(Z).toggle(!t.is(":last-child"))), i.hasClass(u) && !a.localIsResizing && qn(a.slideIndex, a.slideAnchor, a.anchorLink, a.sectionIndex), void yn(n, a, !0))
        }

        function wn(n) {
            Sn(n.slidesNav, n.slideIndex), n.localIsResizing || (e.isFunction(N.afterSlideLoad) && N.afterSlideLoad.call(n.destiny, n.anchorLink, n.sectionIndex + 1, n.slideAnchor, n.slideIndex), St = !0, Je(n.destiny)), ht = !1
        }

        function yn(e, n, t) {
            var i = n.destinyPos;
            if (N.css3) {
                var a = "translate3d(-" + o.round(i.left) + "px, 0px, 0px)";
                Cn(e.find(q)).css($n(a)), Lt = setTimeout(function() {
                    t && wn(n)
                }, N.scrollingSpeed, N.easing)
            } else e.animate({
                scrollLeft: o.round(i.left)
            }, N.scrollingSpeed, N.easing, function() {
                t && wn(n)
            })
        }

        function Sn(e, n) {
            e.find(h).removeClass(u), e.find("li").eq(n).find("a").addClass(u)
        }

        function bn() {
            if (xn(), pt) {
                var n = e(t.activeElement);
                if (!n.is("textarea") && !n.is("input") && !n.is("select")) {
                    var i = $.height();
                    o.abs(i - Nt) > 20 * o.max(Nt, i) / 100 && (ue(!0), Nt = i)
                }
            } else clearTimeout(Tt), Tt = setTimeout(function() {
                ue(!0)
            }, 350)
        }

        function xn() {
            var e = N.responsive || N.responsiveWidth,
                n = N.responsiveHeight,
                t = e && $.outerWidth() < e,
                o = n && $.height() < n;
            e && n ? he(t || o) : e ? he(t) : n && he(o)
        }

        function Cn(e) {
            var n = "all " + N.scrollingSpeed + "ms " + N.easingcss3;
            return e.removeClass(s), e.css({
                "-webkit-transition": n,
                transition: n
            })
        }

        function Tn(e) {
            return e.addClass(s)
        }

        function kn(n, t) {
            N.navigation && (e(A).find(h).removeClass(u), n ? e(A).find('a[href="#' + n + '"]').addClass(u) : e(A).find("li").eq(t).find("a").addClass(u))
        }

        function Ln(n) {
            N.menu && (e(N.menu).find(h).removeClass(u), e(N.menu).find('[data-menuanchor="' + n + '"]').addClass(u))
        }

        function An(e, n) {
            Ln(e), kn(e, n)
        }

        function On(n) {
            var t = e(y).index(w),
                o = n.index(w);
            return t == o ? "none" : t > o ? "up" : "down"
        }

        function In(e, n) {
            return e == n ? "none" : e > n ? "left" : "right"
        }

        function En(n) {
            if (!n.hasClass(F)) {
                var t = e('<div class="' + x + '" />').height(Mn(n));
                n.addClass(F).wrapInner(t)
            }
        }

        function Mn(e) {
            var n = mt;
            if (N.paddingTop || N.paddingBottom) {
                var t = e;
                t.hasClass(m) || (t = e.closest(w));
                var o = parseInt(t.css("padding-top")) + parseInt(t.css("padding-bottom"));
                n = mt - o
            }
            return n
        }

        function Bn(e, n) {
            n ? Cn(gt) : Tn(gt), gt.css($n(e)), setTimeout(function() {
                gt.removeClass(s)
            }, 10)
        }

        function Rn(n) {
            var t = gt.find(w + '[data-anchor="' + n + '"]');
            if (!t.length) {
                var o = "undefined" != typeof n ? n - 1 : 0;
                t = e(w).eq(o)
            }
            return t
        }

        function zn(e, n) {
            var t = n.find(R + '[data-anchor="' + e + '"]');
            return t.length || (e = "undefined" != typeof e ? e : 0, t = n.find(R).eq(e)), t
        }

        function Hn(e, n) {
            var t = Rn(e);
            if (t.length) {
                var o = zn(n, t);
                e === ct || t.hasClass(u) ? Dn(o) : Ne(t, function() {
                    Dn(o)
                })
            }
        }

        function Dn(e) {
            e.length && mn(e.closest(D), e)
        }

        function Pn(e, n) {
            e.append('<div class="' + V + '"><ul></ul></div>');
            var t = e.find(j);
            t.addClass(N.slidesNavPosition);
            for (var o = 0; o < n; o++) t.find("ul").append('<li><a href="#"><span></span></a></li>');
            t.css("margin-left", "-" + t.width() / 2 + "px"), t.find("li").first().find("a").addClass(u)
        }

        function qn(e, n, t, o) {
            var i = "";
            N.anchors.length && !N.lockAnchors && (e ? ("undefined" != typeof t && (i = t), "undefined" == typeof n && (n = e), dt = n, Fn(i + "/" + n)) : "undefined" != typeof e ? (dt = n, Fn(t)) : Fn(t)), jn()
        }

        function Fn(e) {
            if (N.recordHistory) location.hash = e;
            else if (pt || vt) n.history.replaceState(i, i, "#" + e);
            else {
                var t = n.location.href.split("#")[0];
                n.location.replace(t + "#" + e)
            }
        }

        function Vn(e) {
            var n = e.data("anchor"),
                t = e.index();
            return "undefined" == typeof n && (n = t), n
        }

        function jn() {
            var n = e(y),
                t = n.find(z),
                o = Vn(n),
                i = Vn(t),
                a = String(o);
            t.length && (a = a + "-" + i), a = a.replace("/", "-").replace("#", "");
            var l = new RegExp("\\b\\s?" + f + "-[^\\s]+\\b", "g");
            rt[0].className = rt[0].className.replace(l, ""), rt.addClass(f + "-" + a)
        }

        function Yn() {
            var e, o = t.createElement("p"),
                a = {
                    webkitTransform: "-webkit-transform",
                    OTransform: "-o-transform",
                    msTransform: "-ms-transform",
                    MozTransform: "-moz-transform",
                    transform: "transform"
                };
            t.body.insertBefore(o, null);
            for (var l in a) o.style[l] !== i && (o.style[l] = "translate3d(1px,1px,1px)", e = n.getComputedStyle(o).getPropertyValue(a[l]));
            return t.body.removeChild(o), e !== i && e.length > 0 && "none" !== e
        }

        function Nn() {
            t.addEventListener ? (t.removeEventListener("mousewheel", Fe, !1), t.removeEventListener("wheel", Fe, !1), t.removeEventListener("MozMousePixelScroll", Fe, !1)) : t.detachEvent("onmousewheel", Fe)
        }

        function Xn() {
            var e, o = "";
            n.addEventListener ? e = "addEventListener" : (e = "attachEvent", o = "on");
            var a = "onwheel" in t.createElement("div") ? "wheel" : t.onmousewheel !== i ? "mousewheel" : "DOMMouseScroll";
            "DOMMouseScroll" == a ? t[e](o + "MozMousePixelScroll", Fe, !1) : t[e](o + a, Fe, !1)
        }

        function Un() {
            gt.on("mousedown", sn).on("mouseup", cn)
        }

        function Wn() {
            gt.off("mousedown", sn).off("mouseup", cn)
        }

        function Kn() {
            (pt || vt) && (N.autoScrolling && rt.off(Mt.touchmove).on(Mt.touchmove, ze), e(l).off(Mt.touchstart).on(Mt.touchstart, Pe).off(Mt.touchmove).on(Mt.touchmove, He))
        }

        function _n() {
            (pt || vt) && (N.autoScrolling && rt.off(Mt.touchmove), e(l).off(Mt.touchstart).off(Mt.touchmove))
        }

        function Qn() {
            var e;
            return e = n.PointerEvent ? {
                down: "pointerdown",
                move: "pointermove"
            } : {
                down: "MSPointerDown",
                move: "MSPointerMove"
            }
        }

        function Gn(e) {
            var n = [];
            return n.y = "undefined" != typeof e.pageY && (e.pageY || e.pageX) ? e.pageY : e.touches[0].pageY, n.x = "undefined" != typeof e.pageX && (e.pageY || e.pageX) ? e.pageX : e.touches[0].pageX, vt && De(e) && (N.scrollBar || !N.autoScrolling) && (n.y = e.touches[0].pageY, n.x = e.touches[0].pageX), n
        }

        function Jn(e, n) {
            G(0, "internal"), "undefined" != typeof n && (wt = !0), mn(e.closest(D), e), "undefined" != typeof n && (wt = !1), G(Rt.scrollingSpeed, "internal")
        }

        function Zn(e) {
            var n = o.round(e);
            if (N.css3 && N.autoScrolling && !N.scrollBar) {
                var t = "translate3d(0px, -" + n + "px, 0px)";
                Bn(t, !1)
            } else N.autoScrolling && !N.scrollBar ? gt.css("top", -n) : lt.scrollTop(n)
        }

        function $n(e) {
            return {
                "-webkit-transform": e,
                "-moz-transform": e,
                "-ms-transform": e,
                transform: e
            }
        }

        function et(n, t, o) {
            "all" !== t ? xt[o][t] = n : e.each(Object.keys(xt[o]), function(e, t) {
                xt[o][t] = n
            })
        }

        function nt(n) {
            W(!1, "internal"), ie(!1), ae(!1), gt.addClass(c), clearTimeout(Lt), clearTimeout(kt), clearTimeout(Tt), clearTimeout(At), clearTimeout(Ot), $.off("scroll", Ie).off("hashchange", tn).off("resize", bn), ee.off("keydown", an).off("keyup", rn).off("click touchstart", A + " a").off("mouseenter", A + " li").off("mouseleave", A + " li").off("click touchstart", Y).off("mouseover", N.normalScrollElements).off("mouseout", N.normalScrollElements), e(w).off("click touchstart", X), clearTimeout(Lt), clearTimeout(kt), n && tt()
        }

        function tt() {
            Zn(0), gt.find("img[data-src], source[data-src], audio[data-src], iframe[data-src]").each(function() {
                Qe(e(this), "src")
            }), gt.find("img[data-srcset]").each(function() {
                Qe(e(this), "srcset")
            }), e(A + ", " + j + ", " + X).remove(), e(w).css({
                height: "",
                "background-color": "",
                padding: ""
            }), e(R).css({
                width: ""
            }), gt.css({
                height: "",
                position: "",
                "-ms-touch-action": "",
                "touch-action": ""
            }), lt.css({
                overflow: "",
                height: ""
            }), e("html").removeClass(d), rt.removeClass(r), e.each(rt.get(0).className.split(/\s+/), function(e, n) {
                0 === n.indexOf(f) && rt.removeClass(n)
            }), e(w + ", " + R).each(function() {
                N.scrollOverflowHandler && N.scrollOverflowHandler.remove(e(this)), e(this).removeClass(F + " " + u), e(this).attr("style", e(this).data("fp-styles"))
            }), Tn(gt), gt.find(C + ", " + q + ", " + D).each(function() {
                e(this).replaceWith(this.childNodes)
            }), gt.css({
                "-webkit-transition": "none",
                transition: "none"
            }), lt.scrollTop(0);
            var n = [m, B, P];
            e.each(n, function(n, t) {
                e("." + t).removeClass(t)
            })
        }

        function ot(e, n, t) {
            N[e] = n, "internal" !== t && (Rt[e] = n)
        }

        function it() {
            var n = ["fadingEffect", "continuousHorizontal", "scrollHorizontally", "interlockedSlides", "resetSliders", "responsiveSlides", "offsetSections", "dragAndMove", "scrollOverflowReset", "parallax"];
            return e("html").hasClass(d) ? void at("error", "Fullpage.js can only be initialized once and you are doing it multiple times!") : (N.continuousVertical && (N.loopTop || N.loopBottom) && (N.continuousVertical = !1, at("warn", "Option `loopTop/loopBottom` is mutually exclusive with `continuousVertical`; `continuousVertical` disabled")), N.scrollBar && N.scrollOverflow && at("warn", "Option `scrollBar` is mutually exclusive with `scrollOverflow`. Sections with scrollOverflow might not work well in Firefox"), !N.continuousVertical || !N.scrollBar && N.autoScrolling || (N.continuousVertical = !1, at("warn", "Scroll bars (`scrollBar:true` or `autoScrolling:false`) are mutually exclusive with `continuousVertical`; `continuousVertical` disabled")), N.scrollOverflow && !N.scrollOverflowHandler && (N.scrollOverflow = !1, at("error", "The option `scrollOverflow:true` requires the file `scrolloverflow.min.js`. Please include it before fullPage.js.")), e.each(n, function(e, n) {
                N[n] && at("warn", "fullpage.js extensions require jquery.fullpage.extensions.min.js file instead of the usual jquery.fullpage.js. Requested: " + n)
            }), void e.each(N.anchors, function(n, t) {
                var o = ee.find("[name]").filter(function() {
                        return e(this).attr("name") && e(this).attr("name").toLowerCase() == t.toLowerCase()
                    }),
                    i = ee.find("[id]").filter(function() {
                        return e(this).attr("id") && e(this).attr("id").toLowerCase() == t.toLowerCase()
                    });
                (i.length || o.length) && (at("error", "data-anchor tags can not have the same value as any `id` element on the site (or `name` element for IE)."), i.length && at("error", '"' + t + '" is is being used by another element `id` property'), o.length && at("error", '"' + t + '" is is being used by another element `name` property'))
            }))
        }

        function at(e, n) {
            console && console[e] && console[e]("fullPage: " + n)
        }
        if (e("html").hasClass(d)) return void it();
        var lt = e("html, body"),
            rt = e("body"),
            st = e.fn.fullpage;
        N = e.extend({
            menu: !1,
            anchors: [],
            lockAnchors: !1,
            navigation: !1,
            navigationPosition: "right",
            navigationTooltips: [],
            showActiveTooltip: !1,
            slidesNavigation: !1,
            slidesNavPosition: "bottom",
            scrollBar: !1,
            hybrid: !1,
            css3: !0,
            scrollingSpeed: 700,
            autoScrolling: !0,
            fitToSection: !0,
            fitToSectionDelay: 1e3,
            easing: "easeInOutCubic",
            easingcss3: "ease",
            loopBottom: !1,
            loopTop: !1,
            loopHorizontal: !0,
            continuousVertical: !1,
            continuousHorizontal: !1,
            scrollHorizontally: !1,
            interlockedSlides: !1,
            dragAndMove: !1,
            offsetSections: !1,
            resetSliders: !1,
            fadingEffect: !1,
            normalScrollElements: null,
            scrollOverflow: !1,
            scrollOverflowReset: !1,
            scrollOverflowHandler: e.fn.fp_scrolloverflow ? e.fn.fp_scrolloverflow.iscrollHandler : null,
            scrollOverflowOptions: null,
            touchSensitivity: 5,
            normalScrollElementTouchThreshold: 5,
            bigSectionsDestination: null,
            keyboardScrolling: !0,
            animateAnchor: !0,
            recordHistory: !0,
            controlArrows: !0,
            controlArrowColor: "#fff",
            verticalCentered: !0,
            sectionsColor: [],
            paddingTop: 0,
            paddingBottom: 0,
            fixedElements: null,
            responsive: 0,
            responsiveWidth: 0,
            responsiveHeight: 0,
            responsiveSlides: !1,
            parallax: !1,
            parallaxOptions: {
                type: "reveal",
                percentage: 62,
                property: "translate"
            },
            sectionSelector: g,
            slideSelector: M,
            afterLoad: null,
            onLeave: null,
            afterRender: null,
            afterResize: null,
            afterReBuild: null,

            afterSlideLoad: null,
            onSlideLeave: null,
            afterResponsive: null,
            lazyLoading: !0
        }, N);
        var ct, dt, ft, ut, ht = !1,
            pt = navigator.userAgent.match(/(iPhone|iPod|iPad|Android|playbook|silk|BlackBerry|BB10|Windows Phone|Tizen|Bada|webOS|IEMobile|Opera Mini)/),
            vt = "ontouchstart" in n || navigator.msMaxTouchPoints > 0 || navigator.maxTouchPoints,
            gt = e(this),
            mt = $.height(),
            wt = !1,
            yt = !0,
            St = !0,
            bt = [],
            xt = {};
        xt.m = {
            up: !0,
            down: !0,
            left: !0,
            right: !0
        }, xt.k = e.extend(!0, {}, xt.m);
        var Ct, Tt, kt, Lt, At, Ot, It, Et = Qn(),
            Mt = {
                touchmove: "ontouchmove" in n ? "touchmove" : Et.move,
                touchstart: "ontouchstart" in n ? "touchstart" : Et.down
            },
            Bt = 'a[href], area[href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), iframe, object, embed, [tabindex="0"], [contenteditable]',
            Rt = e.extend(!0, {}, N);
        it(), e.extend(e.easing, {
            easeInOutCubic: function(e, n, t, o, i) {
                return (n /= i / 2) < 1 ? o / 2 * n * n * n + t : o / 2 * ((n -= 2) * n * n + 2) + t
            }
        }), e(this).length && (st.version = "2.9.7", st.setAutoScrolling = W, st.setRecordHistory = Q, st.setScrollingSpeed = G, st.setFitToSection = ne, st.setLockAnchors = te, st.setMouseWheelScrolling = oe, st.setAllowScrolling = ie, st.setKeyboardScrolling = ae, st.moveSectionUp = le, st.moveSectionDown = re, st.silentMoveTo = se, st.moveTo = ce, st.moveSlideRight = de, st.moveSlideLeft = fe, st.fitToSection = Ee, st.reBuild = ue, st.setResponsive = he, st.destroy = nt, st.shared = {
            afterRenderActions: Ae
        }, pe(), ve());
        var zt = !1,
            Ht = 0,
            Dt = 0,
            Pt = 0,
            qt = 0,
            Ft = 0,
            Vt = (new Date).getTime(),
            jt = 0,
            Yt = 0,
            Nt = mt
    }
});
! function() {
    "use strict";

    function t(t) {
        l.push(t), 1 == l.length && f()
    }

    function e() {
        for (; l.length;) l[0](), l.shift()
    }

    function n(t) {
        this.a = u, this.b = void 0, this.f = [];
        var e = this;
        try {
            t(function(t) {
                r(e, t)
            }, function(t) {
                a(e, t)
            })
        } catch (n) {
            a(e, n)
        }
    }

    function o(t) {
        return new n(function(e, n) {
            n(t)
        })
    }

    function i(t) {
        return new n(function(e) {
            e(t)
        })
    }

    function r(t, e) {
        if (t.a == u) {
            if (e == t) throw new TypeError;
            var n = !1;
            try {
                var o = e && e.then;
                if (null != e && "object" == typeof e && "function" == typeof o) return void o.call(e, function(e) {
                    n || r(t, e), n = !0
                }, function(e) {
                    n || a(t, e), n = !0
                })
            } catch (i) {
                return void(n || a(t, i))
            }
            t.a = 0, t.b = e, c(t)
        }
    }

    function a(t, e) {
        if (t.a == u) {
            if (e == t) throw new TypeError;
            t.a = 1, t.b = e, c(t)
        }
    }

    function c(e) {
        t(function() {
            if (e.a != u)
                for (; e.f.length;) {
                    var t = e.f.shift(),
                        n = t[0],
                        o = t[1],
                        i = t[2],
                        t = t[3];
                    try {
                        0 == e.a ? i("function" == typeof n ? n.call(void 0, e.b) : e.b) : 1 == e.a && ("function" == typeof o ? i(o.call(void 0, e.b)) : t(e.b))
                    } catch (r) {
                        t(r)
                    }
                }
        })
    }

    function s(t) {
        return new n(function(e, n) {
            function o(n) {
                return function(o) {
                    a[n] = o, r += 1, r == t.length && e(a)
                }
            }
            var r = 0,
                a = [];
            0 == t.length && e(a);
            for (var c = 0; c < t.length; c += 1) i(t[c]).c(o(c), n)
        })
    }

    function d(t) {
        return new n(function(e, n) {
            for (var o = 0; o < t.length; o += 1) i(t[o]).c(e, n)
        })
    }
    var f, l = [];
    f = function() {
        setTimeout(e)
    };
    var u = 2;
    n.prototype.g = function(t) {
        return this.c(void 0, t)
    }, n.prototype.c = function(t, e) {
        var o = this;
        return new n(function(n, i) {
            o.f.push([t, e, n, i]), c(o)
        })
    }, window.Promise || (window.Promise = n, window.Promise.resolve = i, window.Promise.reject = o, window.Promise.race = d, window.Promise.all = s, window.Promise.prototype.then = n.prototype.c, window.Promise.prototype["catch"] = n.prototype.g)
}(),
function() {
    function t(t, e) {
        document.addEventListener ? t.addEventListener("scroll", e, !1) : t.attachEvent("scroll", e)
    }

    function e(t) {
        document.body ? t() : document.addEventListener ? document.addEventListener("DOMContentLoaded", function e() {
            document.removeEventListener("DOMContentLoaded", e), t()
        }) : document.attachEvent("onreadystatechange", function n() {
            "interactive" != document.readyState && "complete" != document.readyState || (document.detachEvent("onreadystatechange", n), t())
        })
    }

    function n(t) {
        this.a = document.createElement("div"), this.a.setAttribute("aria-hidden", "true"), this.a.appendChild(document.createTextNode(t)), this.b = document.createElement("span"), this.c = document.createElement("span"), this.h = document.createElement("span"), this.f = document.createElement("span"), this.g = -1, this.b.style.cssText = "max-width:none;display:inline-block;position:absolute;height:100%;width:100%;overflow:scroll;font-size:16px;", this.c.style.cssText = "max-width:none;display:inline-block;position:absolute;height:100%;width:100%;overflow:scroll;font-size:16px;", this.f.style.cssText = "max-width:none;display:inline-block;position:absolute;height:100%;width:100%;overflow:scroll;font-size:16px;", this.h.style.cssText = "display:inline-block;width:200%;height:200%;font-size:16px;max-width:none;", this.b.appendChild(this.h), this.c.appendChild(this.f), this.a.appendChild(this.b), this.a.appendChild(this.c)
    }

    function o(t, e) {
        t.a.style.cssText = "max-width:none;min-width:20px;min-height:20px;display:inline-block;overflow:hidden;position:absolute;width:auto;margin:0;padding:0;top:-999px;white-space:nowrap;font-synthesis:none;font:" + e + ";"
    }

    function i(t) {
        var e = t.a.offsetWidth,
            n = e + 100;
        return t.f.style.width = n + "px", t.c.scrollLeft = n, t.b.scrollLeft = t.b.scrollWidth + 100, t.g !== e && (t.g = e, !0)
    }

    function r(e, n) {
        function o() {
            var t = r;
            i(t) && t.a.parentNode && n(t.g)
        }
        var r = e;
        t(e.b, o), t(e.c, o), i(e)
    }

    function a(t, e) {
        var n = e || {};
        this.family = t, this.style = n.style || "normal", this.weight = n.weight || "normal", this.stretch = n.stretch || "normal"
    }

    function c() {
        if (null === u)
            if (s() && /Apple/.test(window.navigator.vendor)) {
                var t = /AppleWebKit\/([0-9]+)(?:\.([0-9]+))(?:\.([0-9]+))/.exec(window.navigator.userAgent);
                u = !!t && 603 > parseInt(t[1], 10)
            } else u = !1;
        return u
    }

    function s() {
        return null === p && (p = !!document.fonts), p
    }

    function d() {
        if (null === h) {
            var t = document.createElement("div");
            try {
                t.style.font = "condensed 100px sans-serif"
            } catch (e) {}
            h = "" !== t.style.font
        }
        return h
    }

    function f(t, e) {
        return [t.style, t.weight, d() ? t.stretch : "", "100px", e].join(" ")
    }
    var l = null,
        u = null,
        h = null,
        p = null;
    a.prototype.load = function(t, i) {
        var a = this,
            d = t || "BESbswy",
            u = 0,
            h = i || 3e3,
            p = (new Date).getTime();
        return new Promise(function(t, i) {
            if (s() && !c()) {
                var m = new Promise(function(t, e) {
                        function n() {
                            (new Date).getTime() - p >= h ? e() : document.fonts.load(f(a, '"' + a.family + '"'), d).then(function(e) {
                                1 <= e.length ? t() : setTimeout(n, 25)
                            }, function() {
                                e()
                            })
                        }
                        n()
                    }),
                    w = new Promise(function(t, e) {
                        u = setTimeout(e, h)
                    });
                Promise.race([w, m]).then(function() {
                    clearTimeout(u), t(a)
                }, function() {
                    i(a)
                })
            } else e(function() {
                function e() {
                    var e;
                    (e = -1 != v && -1 != y || -1 != v && -1 != g || -1 != y && -1 != g) && ((e = v != y && v != g && y != g) || (null === l && (e = /AppleWebKit\/([0-9]+)(?:\.([0-9]+))/.exec(window.navigator.userAgent), l = !!e && (536 > parseInt(e[1], 10) || 536 === parseInt(e[1], 10) && 11 >= parseInt(e[2], 10))), e = l && (v == b && y == b && g == b || v == x && y == x && g == x || v == E && y == E && g == E)), e = !e), e && (T.parentNode && T.parentNode.removeChild(T), clearTimeout(u), t(a))
                }

                function c() {
                    if ((new Date).getTime() - p >= h) T.parentNode && T.parentNode.removeChild(T), i(a);
                    else {
                        var t = document.hidden;
                        !0 !== t && void 0 !== t || (v = s.a.offsetWidth, y = m.a.offsetWidth, g = w.a.offsetWidth, e()), u = setTimeout(c, 50)
                    }
                }
                var s = new n(d),
                    m = new n(d),
                    w = new n(d),
                    v = -1,
                    y = -1,
                    g = -1,
                    b = -1,
                    x = -1,
                    E = -1,
                    T = document.createElement("div");
                T.dir = "ltr", o(s, f(a, "sans-serif")), o(m, f(a, "serif")), o(w, f(a, "monospace")), T.appendChild(s.a), T.appendChild(m.a), T.appendChild(w.a), document.body.appendChild(T), b = s.a.offsetWidth, x = m.a.offsetWidth, E = w.a.offsetWidth, c(), r(s, function(t) {
                    v = t, e()
                }), o(s, f(a, '"' + a.family + '",sans-serif')), r(m, function(t) {
                    y = t, e()
                }), o(m, f(a, '"' + a.family + '",serif')), r(w, function(t) {
                    g = t, e()
                }), o(w, f(a, '"' + a.family + '",monospace'))
            })
        })
    }, "object" == typeof module ? module.exports = a : (window.FontFaceObserver = a, window.FontFaceObserver.prototype.load = a.prototype.load)
}();

function hexToRgb(e) {
    var a = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
    e = e.replace(a, function(e, a, t, i) {
        return a + a + t + t + i + i
    });
    var t = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(e);
    return t ? {
        r: parseInt(t[1], 16),
        g: parseInt(t[2], 16),
        b: parseInt(t[3], 16)
    } : null
}

function clamp(e, a, t) {
    return Math.min(Math.max(e, a), t)
}

function isInArray(e, a) {
    return a.indexOf(e) > -1
}
var pJS = function(e, a) {
    var t = document.querySelector("." + e + " > .particles-js-canvas-el");
    this.pJS = {
        canvas: {
            el: t,
            w: t.offsetWidth,
            h: t.offsetHeight
        },
        "particles": {
    "number": {
      "value": 80,
      "density": {
        "enable": true,
        "value_area": 700
      }
    },
    "color": {
      "value": "#fff"
    },
    "shape": {
      "type": "circle",
      "stroke": {
        "width": 0,
        "color": "#fff"
      },
      "polygon": {
        "nb_sides": 5
      },
    },
    "opacity": {
      "value": 0.5,
      "random": false,
      "anim": {
        "enable": false,
        "speed": 1,
        "opacity_min": 0.1,
        "sync": false
      }
    },
    "size": {
      "value": 3,
      "random": true,
      "anim": {
        "enable": false,
        "speed": 40,
        "size_min": 0.1,
        "sync": false
      }
    },
    "line_linked": {
      "enable": true,
      "distance": 150,
      "color": "#fff",
      "opacity": 0.4,
      "width": 1
    },
    "move": {
      "enable": true,
      "speed": 9,
      "direction": "none",
      "random": false,
      "straight": false,
      "out_mode": "out",
      "bounce": false,
      "attract": {
        "enable": false,
        "rotateX": 600,
        "rotateY": 1200
      }
    
            },
            array: []
        },
        interactivity: {
            detect_on: "canvas",
            events: {
                onhover: {
                    enable: !0,
                    mode: "grab"
                },
                onclick: {
                    enable: !0,
                    mode: "push"
                },
                resize: !0
            },
            modes: {
                grab: {
                    distance: 100,
                    line_linked: {
                        opacity: 1
                    }
                },
                bubble: {
                    distance: 200,
                    size: 80,
                    duration: .4
                },
                repulse: {
                    distance: 200,
                    duration: .4
                },
                push: {
                    particles_nb: 4
                },
                remove: {
                    particles_nb: 2
                }
            },
            mouse: {}
        },
        retina_detect: !1,
        fn: {
            interact: {},
            modes: {},
            vendors: {}
        },
        tmp: {}
    };
    var i = this.pJS;
    a && Object.deepExtend(i, a), i.tmp.obj = {
        size_value: i.particles.size.value,
        size_anim_speed: i.particles.size.anim.speed,
        move_speed: i.particles.move.speed,
        line_linked_distance: i.particles.line_linked.distance,
        line_linked_width: i.particles.line_linked.width,
        mode_grab_distance: i.interactivity.modes.grab.distance,
        mode_bubble_distance: i.interactivity.modes.bubble.distance,
        mode_bubble_size: i.interactivity.modes.bubble.size,
        mode_repulse_distance: i.interactivity.modes.repulse.distance
    }, i.fn.retinaInit = function() {
        i.retina_detect && window.devicePixelRatio > 1 ? (i.canvas.pxratio = window.devicePixelRatio, i.tmp.retina = !0) : (i.canvas.pxratio = 1, i.tmp.retina = !1), i.canvas.w = i.canvas.el.offsetWidth * i.canvas.pxratio, i.canvas.h = i.canvas.el.offsetHeight * i.canvas.pxratio, i.particles.size.value = i.tmp.obj.size_value * i.canvas.pxratio, i.particles.size.anim.speed = i.tmp.obj.size_anim_speed * i.canvas.pxratio, i.particles.move.speed = i.tmp.obj.move_speed * i.canvas.pxratio, i.particles.line_linked.distance = i.tmp.obj.line_linked_distance * i.canvas.pxratio, i.interactivity.modes.grab.distance = i.tmp.obj.mode_grab_distance * i.canvas.pxratio, i.interactivity.modes.bubble.distance = i.tmp.obj.mode_bubble_distance * i.canvas.pxratio, i.particles.line_linked.width = i.tmp.obj.line_linked_width * i.canvas.pxratio, i.interactivity.modes.bubble.size = i.tmp.obj.mode_bubble_size * i.canvas.pxratio, i.interactivity.modes.repulse.distance = i.tmp.obj.mode_repulse_distance * i.canvas.pxratio
    }, i.fn.canvasInit = function() {
        i.canvas.ctx = i.canvas.el.getContext("2d")
    }, i.fn.canvasSize = function() {
        i.canvas.el.width = i.canvas.w, i.canvas.el.height = i.canvas.h, i && i.interactivity.events.resize && window.addEventListener("resize", function() {
            i.canvas.w = i.canvas.el.offsetWidth, i.canvas.h = i.canvas.el.offsetHeight, i.tmp.retina && (i.canvas.w *= i.canvas.pxratio, i.canvas.h *= i.canvas.pxratio), i.canvas.el.width = i.canvas.w, i.canvas.el.height = i.canvas.h, i.particles.move.enable || (i.fn.particlesEmpty(), i.fn.particlesCreate(), i.fn.particlesDraw(), i.fn.vendors.densityAutoParticles()), i.fn.vendors.densityAutoParticles()
        })
    }, i.fn.canvasPaint = function() {
        i.canvas.ctx.fillRect(0, 0, i.canvas.w, i.canvas.h)
    }, i.fn.canvasClear = function() {
        i.canvas.ctx.clearRect(0, 0, i.canvas.w, i.canvas.h)
    }, i.fn.particle = function(e, a, t) {
        if (this.radius = (i.particles.size.random ? Math.random() : 1) * i.particles.size.value, i.particles.size.anim.enable && (this.size_status = !1, this.vs = i.particles.size.anim.speed / 100, i.particles.size.anim.sync || (this.vs = this.vs * Math.random())), this.x = t ? t.x : Math.random() * i.canvas.w, this.y = t ? t.y : Math.random() * i.canvas.h, this.x > i.canvas.w - 2 * this.radius ? this.x = this.x - this.radius : this.x < 2 * this.radius && (this.x = this.x + this.radius), this.y > i.canvas.h - 2 * this.radius ? this.y = this.y - this.radius : this.y < 2 * this.radius && (this.y = this.y + this.radius), i.particles.move.bounce && i.fn.vendors.checkOverlap(this, t), this.color = {}, "object" == typeof e.value)
            if (e.value instanceof Array) {
                var s = e.value[Math.floor(Math.random() * i.particles.color.value.length)];
                this.color.rgb = hexToRgb(s)
            } else void 0 != e.value.r && void 0 != e.value.g && void 0 != e.value.b && (this.color.rgb = {
                r: e.value.r,
                g: e.value.g,
                b: e.value.b
            }), void 0 != e.value.h && void 0 != e.value.s && void 0 != e.value.l && (this.color.hsl = {
                h: e.value.h,
                s: e.value.s,
                l: e.value.l
            });
        else "random" == e.value ? this.color.rgb = {
            r: Math.floor(256 * Math.random()) + 0,
            g: Math.floor(256 * Math.random()) + 0,
            b: Math.floor(256 * Math.random()) + 0
        } : "string" == typeof e.value && (this.color = e, this.color.rgb = hexToRgb(this.color.value));
        this.opacity = (i.particles.opacity.random ? Math.random() : 1) * i.particles.opacity.value, i.particles.opacity.anim.enable && (this.opacity_status = !1, this.vo = i.particles.opacity.anim.speed / 100, i.particles.opacity.anim.sync || (this.vo = this.vo * Math.random()));
        var n = {};
        switch (i.particles.move.direction) {
            case "top":
                n = {
                    x: 0,
                    y: -1
                };
                break;
            case "top-right":
                n = {

                    x: .5,
                    y: -.5
                };
                break;
            case "right":
                n = {
                    x: 1,
                    y: -0
                };
                break;
            case "bottom-right":
                n = {
                    x: .5,
                    y: .5
                };
                break;
            case "bottom":
                n = {
                    x: 0,
                    y: 1
                };
                break;
            case "bottom-left":
                n = {
                    x: -.5,
                    y: 1
                };
                break;
            case "left":
                n = {
                    x: -1,
                    y: 0
                };
                break;
            case "top-left":
                n = {
                    x: -.5,
                    y: -.5
                };
                break;
            default:
                n = {
                    x: 0,
                    y: 0
                }
        }
        i.particles.move.straight ? (this.vx = n.x, this.vy = n.y, i.particles.move.random && (this.vx = this.vx * Math.random(), this.vy = this.vy * Math.random())) : (this.vx = n.x + Math.random() - .5, this.vy = n.y + Math.random() - .5), this.vx_i = this.vx, this.vy_i = this.vy;
        var r = i.particles.shape.type;
        if ("object" == typeof r) {
            if (r instanceof Array) {
                var c = r[Math.floor(Math.random() * r.length)];
                this.shape = c
            }
        } else this.shape = r;
        if ("image" == this.shape) {
            var o = i.particles.shape;
            this.img = {
                src: o.image.src,
                ratio: o.image.width / o.image.height
            }, this.img.ratio || (this.img.ratio = 1), "svg" == i.tmp.img_type && void 0 != i.tmp.source_svg && (i.fn.vendors.createSvgImg(this), i.tmp.pushing && (this.img.loaded = !1))
        }
    }, i.fn.particle.prototype.draw = function() {
        function e() {
            i.canvas.ctx.drawImage(r, a.x - t, a.y - t, 2 * t, 2 * t / a.img.ratio)
        }
        var a = this;
        if (void 0 != a.radius_bubble) var t = a.radius_bubble;
        else var t = a.radius;
        if (void 0 != a.opacity_bubble) var s = a.opacity_bubble;
        else var s = a.opacity;
        if (a.color.rgb) var n = "rgba(" + a.color.rgb.r + "," + a.color.rgb.g + "," + a.color.rgb.b + "," + s + ")";
        else var n = "hsla(" + a.color.hsl.h + "," + a.color.hsl.s + "%," + a.color.hsl.l + "%," + s + ")";
        switch (i.canvas.ctx.fillStyle = n, i.canvas.ctx.beginPath(), a.shape) {
            case "circle":
                i.canvas.ctx.arc(a.x, a.y, t, 0, 2 * Math.PI, !1);
                break;
            case "edge":
                i.canvas.ctx.rect(a.x - t, a.y - t, 2 * t, 2 * t);
                break;
            case "triangle":
                i.fn.vendors.drawShape(i.canvas.ctx, a.x - t, a.y + t / 1.66, 2 * t, 3, 2);
                break;
            case "polygon":
                i.fn.vendors.drawShape(i.canvas.ctx, a.x - t / (i.particles.shape.polygon.nb_sides / 3.5), a.y - t / .76, 2.66 * t / (i.particles.shape.polygon.nb_sides / 3), i.particles.shape.polygon.nb_sides, 1);
                break;
            case "star":
                i.fn.vendors.drawShape(i.canvas.ctx, a.x - 2 * t / (i.particles.shape.polygon.nb_sides / 4), a.y - t / 1.52, 2 * t * 2.66 / (i.particles.shape.polygon.nb_sides / 3), i.particles.shape.polygon.nb_sides, 2);
                break;
            case "image":
                if ("svg" == i.tmp.img_type) var r = a.img.obj;
                else var r = i.tmp.img_obj;
                r && e()
        }
        i.canvas.ctx.closePath(), i.particles.shape.stroke.width > 0 && (i.canvas.ctx.strokeStyle = i.particles.shape.stroke.color, i.canvas.ctx.lineWidth = i.particles.shape.stroke.width, i.canvas.ctx.stroke()), i.canvas.ctx.fill()
    }, i.fn.particlesCreate = function() {
        for (var e = 0; e < i.particles.number.value; e++) i.particles.array.push(new i.fn.particle(i.particles.color, i.particles.opacity.value))
    }, i.fn.particlesUpdate = function() {
        for (var e = 0; e < i.particles.array.length; e++) {
            var a = i.particles.array[e];
            if (i.particles.move.enable) {
                var t = i.particles.move.speed / 2;
                a.x += a.vx * t, a.y += a.vy * t
            }
            if (i.particles.opacity.anim.enable && (1 == a.opacity_status ? (a.opacity >= i.particles.opacity.value && (a.opacity_status = !1), a.opacity += a.vo) : (a.opacity <= i.particles.opacity.anim.opacity_min && (a.opacity_status = !0), a.opacity -= a.vo), a.opacity < 0 && (a.opacity = 0)), i.particles.size.anim.enable && (1 == a.size_status ? (a.radius >= i.particles.size.value && (a.size_status = !1), a.radius += a.vs) : (a.radius <= i.particles.size.anim.size_min && (a.size_status = !0), a.radius -= a.vs), a.radius < 0 && (a.radius = 0)), "bounce" == i.particles.move.out_mode) var s = {
                x_left: a.radius,
                x_right: i.canvas.w,
                y_top: a.radius,
                y_bottom: i.canvas.h
            };
            else var s = {
                x_left: -a.radius,
                x_right: i.canvas.w + a.radius,
                y_top: -a.radius,
                y_bottom: i.canvas.h + a.radius
            };
            switch (a.x - a.radius > i.canvas.w ? (a.x = s.x_left, a.y = Math.random() * i.canvas.h) : a.x + a.radius < 0 && (a.x = s.x_right, a.y = Math.random() * i.canvas.h), a.y - a.radius > i.canvas.h ? (a.y = s.y_top, a.x = Math.random() * i.canvas.w) : a.y + a.radius < 0 && (a.y = s.y_bottom, a.x = Math.random() * i.canvas.w), i.particles.move.out_mode) {
                case "bounce":
                    a.x + a.radius > i.canvas.w ? a.vx = -a.vx : a.x - a.radius < 0 && (a.vx = -a.vx), a.y + a.radius > i.canvas.h ? a.vy = -a.vy : a.y - a.radius < 0 && (a.vy = -a.vy)
            }
            if (isInArray("grab", i.interactivity.events.onhover.mode) && i.fn.modes.grabParticle(a), (isInArray("bubble", i.interactivity.events.onhover.mode) || isInArray("bubble", i.interactivity.events.onclick.mode)) && i.fn.modes.bubbleParticle(a), (isInArray("repulse", i.interactivity.events.onhover.mode) || isInArray("repulse", i.interactivity.events.onclick.mode)) && i.fn.modes.repulseParticle(a), i.particles.line_linked.enable || i.particles.move.attract.enable)
                for (var n = e + 1; n < i.particles.array.length; n++) {
                    var r = i.particles.array[n];
                    i.particles.line_linked.enable && i.fn.interact.linkParticles(a, r), i.particles.move.attract.enable && i.fn.interact.attractParticles(a, r), i.particles.move.bounce && i.fn.interact.bounceParticles(a, r)
                }
        }
    }, i.fn.particlesDraw = function() {
        i.canvas.ctx.clearRect(0, 0, i.canvas.w, i.canvas.h), i.fn.particlesUpdate();
        for (var e = 0; e < i.particles.array.length; e++) {
            var a = i.particles.array[e];
            a.draw()
        }
    }, i.fn.particlesEmpty = function() {
        i.particles.array = []
    }, i.fn.particlesRefresh = function() {
        cancelRequestAnimFrame(i.fn.checkAnimFrame), cancelRequestAnimFrame(i.fn.drawAnimFrame), i.tmp.source_svg = void 0, i.tmp.img_obj = void 0, i.tmp.count_svg = 0, i.fn.particlesEmpty(), i.fn.canvasClear(), i.fn.vendors.start()
    }, i.fn.interact.linkParticles = function(e, a) {
        var t = e.x - a.x,
            s = e.y - a.y,
            n = Math.sqrt(t * t + s * s);
        if (n <= i.particles.line_linked.distance) {
            var r = i.particles.line_linked.opacity - n / (1 / i.particles.line_linked.opacity) / i.particles.line_linked.distance;
            if (r > 0) {
                var c = i.particles.line_linked.color_rgb_line;
                i.canvas.ctx.strokeStyle = "rgba(" + c.r + "," + c.g + "," + c.b + "," + r + ")", i.canvas.ctx.lineWidth = i.particles.line_linked.width, i.canvas.ctx.beginPath(), i.canvas.ctx.moveTo(e.x, e.y), i.canvas.ctx.lineTo(a.x, a.y), i.canvas.ctx.stroke(), i.canvas.ctx.closePath()
            }
        }
    }, i.fn.interact.attractParticles = function(e, a) {
        var t = e.x - a.x,
            s = e.y - a.y,
            n = Math.sqrt(t * t + s * s);
        if (n <= i.particles.line_linked.distance) {
            var r = t / (1e3 * i.particles.move.attract.rotateX),
                c = s / (1e3 * i.particles.move.attract.rotateY);
            e.vx -= r, e.vy -= c, a.vx += r, a.vy += c
        }
    }, i.fn.interact.bounceParticles = function(e, a) {
        var t = e.x - a.x,
            i = e.y - a.y,
            s = Math.sqrt(t * t + i * i),
            n = e.radius + a.radius;
        s <= n && (e.vx = -e.vx, e.vy = -e.vy, a.vx = -a.vx, a.vy = -a.vy)
    }, i.fn.modes.pushParticles = function(e, a) {
        i.tmp.pushing = !0;
        for (var t = 0; t < e; t++) i.particles.array.push(new i.fn.particle(i.particles.color, i.particles.opacity.value, {
            x: a ? a.pos_x : Math.random() * i.canvas.w,
            y: a ? a.pos_y : Math.random() * i.canvas.h
        })), t == e - 1 && (i.particles.move.enable || i.fn.particlesDraw(), i.tmp.pushing = !1)
    }, i.fn.modes.removeParticles = function(e) {
        i.particles.array.splice(0, e), i.particles.move.enable || i.fn.particlesDraw()
    }, i.fn.modes.bubbleParticle = function(e) {
        function a() {
            e.opacity_bubble = e.opacity, e.radius_bubble = e.radius
        }

        function t(a, t, s, n, c) {
            if (a != t)
                if (i.tmp.bubble_duration_end) {
                    if (void 0 != s) {
                        var o = n - p * (n - a) / i.interactivity.modes.bubble.duration,
                            l = a - o;
                        d = a + l, "size" == c && (e.radius_bubble = d), "opacity" == c && (e.opacity_bubble = d)
                    }
                } else if (r <= i.interactivity.modes.bubble.distance) {
                if (void 0 != s) var v = s;
                else var v = n;
                if (v != a) {
                    var d = n - p * (n - a) / i.interactivity.modes.bubble.duration;
                    "size" == c && (e.radius_bubble = d), "opacity" == c && (e.opacity_bubble = d)
                }
            } else "size" == c && (e.radius_bubble = void 0), "opacity" == c && (e.opacity_bubble = void 0)
        }
        if (i.interactivity.events.onhover.enable && isInArray("bubble", i.interactivity.events.onhover.mode)) {
            var s = e.x - i.interactivity.mouse.pos_x,
                n = e.y - i.interactivity.mouse.pos_y,
                r = Math.sqrt(s * s + n * n),
                c = 1 - r / i.interactivity.modes.bubble.distance;
            if (r <= i.interactivity.modes.bubble.distance) {
                if (c >= 0 && "mousemove" == i.interactivity.status) {
                    if (i.interactivity.modes.bubble.size != i.particles.size.value)
                        if (i.interactivity.modes.bubble.size > i.particles.size.value) {
                            var o = e.radius + i.interactivity.modes.bubble.size * c;
                            o >= 0 && (e.radius_bubble = o)
                        } else {
                            var l = e.radius - i.interactivity.modes.bubble.size,
                                o = e.radius - l * c;
                            o > 0 ? e.radius_bubble = o : e.radius_bubble = 0
                        }
                    if (i.interactivity.modes.bubble.opacity != i.particles.opacity.value)
                        if (i.interactivity.modes.bubble.opacity > i.particles.opacity.value) {
                            var v = i.interactivity.modes.bubble.opacity * c;
                            v > e.opacity && v <= i.interactivity.modes.bubble.opacity && (e.opacity_bubble = v)
                        } else {
                            var v = e.opacity - (i.particles.opacity.value - i.interactivity.modes.bubble.opacity) * c;
                            v < e.opacity && v >= i.interactivity.modes.bubble.opacity && (e.opacity_bubble = v)
                        }
                }
            } else a();
            "mouseleave" == i.interactivity.status && a()
        } else if (i.interactivity.events.onclick.enable && isInArray("bubble", i.interactivity.events.onclick.mode)) {
            if (i.tmp.bubble_clicking) {
                var s = e.x - i.interactivity.mouse.click_pos_x,
                    n = e.y - i.interactivity.mouse.click_pos_y,
                    r = Math.sqrt(s * s + n * n),
                    p = ((new Date).getTime() - i.interactivity.mouse.click_time) / 1e3;
                p > i.interactivity.modes.bubble.duration && (i.tmp.bubble_duration_end = !0), p > 2 * i.interactivity.modes.bubble.duration && (i.tmp.bubble_clicking = !1, i.tmp.bubble_duration_end = !1)
            }
            i.tmp.bubble_clicking && (t(i.interactivity.modes.bubble.size, i.particles.size.value, e.radius_bubble, e.radius, "size"), t(i.interactivity.modes.bubble.opacity, i.particles.opacity.value, e.opacity_bubble, e.opacity, "opacity"))
        }
    }, i.fn.modes.repulseParticle = function(e) {
        function a() {
            var a = Math.atan2(d, p);
            if (e.vx = u * Math.cos(a), e.vy = u * Math.sin(a), "bounce" == i.particles.move.out_mode) {
                var t = {
                    x: e.x + e.vx,
                    y: e.y + e.vy
                };
                t.x + e.radius > i.canvas.w ? e.vx = -e.vx : t.x - e.radius < 0 && (e.vx = -e.vx), t.y + e.radius > i.canvas.h ? e.vy = -e.vy : t.y - e.radius < 0 && (e.vy = -e.vy)
            }
        }
        if (i.interactivity.events.onhover.enable && isInArray("repulse", i.interactivity.events.onhover.mode) && "mousemove" == i.interactivity.status) {
            var t = e.x - i.interactivity.mouse.pos_x,
                s = e.y - i.interactivity.mouse.pos_y,
                n = Math.sqrt(t * t + s * s),
                r = {
                    x: t / n,
                    y: s / n
                },
                c = i.interactivity.modes.repulse.distance,
                o = 100,
                l = clamp(1 / c * (-1 * Math.pow(n / c, 2) + 1) * c * o, 0, 50),
                v = {
                    x: e.x + r.x * l,
                    y: e.y + r.y * l
                };
            "bounce" == i.particles.move.out_mode ? (v.x - e.radius > 0 && v.x + e.radius < i.canvas.w && (e.x = v.x), v.y - e.radius > 0 && v.y + e.radius < i.canvas.h && (e.y = v.y)) : (e.x = v.x, e.y = v.y)
        } else if (i.interactivity.events.onclick.enable && isInArray("repulse", i.interactivity.events.onclick.mode))
            if (i.tmp.repulse_finish || (i.tmp.repulse_count++, i.tmp.repulse_count == i.particles.array.length && (i.tmp.repulse_finish = !0)), i.tmp.repulse_clicking) {
                var c = Math.pow(i.interactivity.modes.repulse.distance / 6, 3),
                    p = i.interactivity.mouse.click_pos_x - e.x,
                    d = i.interactivity.mouse.click_pos_y - e.y,
                    m = p * p + d * d,
                    u = -c / m * 1;
                m <= c && a()
            } else 0 == i.tmp.repulse_clicking && (e.vx = e.vx_i, e.vy = e.vy_i)
    }, i.fn.modes.grabParticle = function(e) {
        if (i.interactivity.events.onhover.enable && "mousemove" == i.interactivity.status) {
            var a = e.x - i.interactivity.mouse.pos_x,
                t = e.y - i.interactivity.mouse.pos_y,
                s = Math.sqrt(a * a + t * t);
            if (s <= i.interactivity.modes.grab.distance) {
                var n = i.interactivity.modes.grab.line_linked.opacity - s / (1 / i.interactivity.modes.grab.line_linked.opacity) / i.interactivity.modes.grab.distance;
                if (n > 0) {
                    var r = i.particles.line_linked.color_rgb_line;
                    i.canvas.ctx.strokeStyle = "rgba(" + r.r + "," + r.g + "," + r.b + "," + n + ")", i.canvas.ctx.lineWidth = i.particles.line_linked.width, i.canvas.ctx.beginPath(), i.canvas.ctx.moveTo(e.x, e.y), i.canvas.ctx.lineTo(i.interactivity.mouse.pos_x, i.interactivity.mouse.pos_y), i.canvas.ctx.stroke(), i.canvas.ctx.closePath()
                }
            }
        }
    }, i.fn.vendors.eventsListeners = function() {
        "window" == i.interactivity.detect_on ? i.interactivity.el = window : i.interactivity.el = i.canvas.el, (i.interactivity.events.onhover.enable || i.interactivity.events.onclick.enable) && (i.interactivity.el.addEventListener("mousemove", function(e) {
            if (i.interactivity.el == window) var a = e.clientX,
                t = e.clientY;
            else var a = e.offsetX || e.clientX,
                t = e.offsetY || e.clientY;
            i.interactivity.mouse.pos_x = a, i.interactivity.mouse.pos_y = t, i.tmp.retina && (i.interactivity.mouse.pos_x *= i.canvas.pxratio, i.interactivity.mouse.pos_y *= i.canvas.pxratio), i.interactivity.status = "mousemove"
        }), i.interactivity.el.addEventListener("mouseleave", function(e) {
            i.interactivity.mouse.pos_x = null, i.interactivity.mouse.pos_y = null, i.interactivity.status = "mouseleave"
        })), i.interactivity.events.onclick.enable && i.interactivity.el.addEventListener("click", function() {
            if (i.interactivity.mouse.click_pos_x = i.interactivity.mouse.pos_x, i.interactivity.mouse.click_pos_y = i.interactivity.mouse.pos_y, i.interactivity.mouse.click_time = (new Date).getTime(), i.interactivity.events.onclick.enable) switch (i.interactivity.events.onclick.mode) {
                case "push":
                    i.particles.move.enable ? i.fn.modes.pushParticles(i.interactivity.modes.push.particles_nb, i.interactivity.mouse) : 1 == i.interactivity.modes.push.particles_nb ? i.fn.modes.pushParticles(i.interactivity.modes.push.particles_nb, i.interactivity.mouse) : i.interactivity.modes.push.particles_nb > 1 && i.fn.modes.pushParticles(i.interactivity.modes.push.particles_nb);
                    break;
                case "remove":
                    i.fn.modes.removeParticles(i.interactivity.modes.remove.particles_nb);
                    break;
                case "bubble":
                    i.tmp.bubble_clicking = !0;
                    break;
                case "repulse":
                    i.tmp.repulse_clicking = !0, i.tmp.repulse_count = 0, i.tmp.repulse_finish = !1, setTimeout(function() {
                        i.tmp.repulse_clicking = !1
                    }, 1e3 * i.interactivity.modes.repulse.duration)
            }
        })
    }, i.fn.vendors.densityAutoParticles = function() {
        if (i.particles.number.density.enable) {
            var e = i.canvas.el.width * i.canvas.el.height / 1e3;
            i.tmp.retina && (e /= 2 * i.canvas.pxratio);
            var a = e * i.particles.number.value / i.particles.number.density.value_area,
                t = i.particles.array.length - a;
            t < 0 ? i.fn.modes.pushParticles(Math.abs(t)) : i.fn.modes.removeParticles(t)
        }
    }, i.fn.vendors.checkOverlap = function(e, a) {
        for (var t = 0; t < i.particles.array.length; t++) {
            var s = i.particles.array[t],
                n = e.x - s.x,
                r = e.y - s.y,
                c = Math.sqrt(n * n + r * r);
            c <= e.radius + s.radius && (e.x = a ? a.x : Math.random() * i.canvas.w, e.y = a ? a.y : Math.random() * i.canvas.h, i.fn.vendors.checkOverlap(e))
        }
    }, i.fn.vendors.createSvgImg = function(e) {
        var a = i.tmp.source_svg,
            t = /#([0-9A-F]{3,6})/gi,
            s = a.replace(t, function(a, t, i, s) {
                if (e.color.rgb) var n = "rgba(" + e.color.rgb.r + "," + e.color.rgb.g + "," + e.color.rgb.b + "," + e.opacity + ")";
                else var n = "hsla(" + e.color.hsl.h + "," + e.color.hsl.s + "%," + e.color.hsl.l + "%," + e.opacity + ")";
                return n
            }),
            n = new Blob([s], {
                type: "image/svg+xml;charset=utf-8"
            }),
            r = window.URL || window.webkitURL || window,
            c = r.createObjectURL(n),
            o = new Image;
        o.addEventListener("load", function() {
            e.img.obj = o, e.img.loaded = !0, r.revokeObjectURL(c), i.tmp.count_svg++
        }), o.src = c
    }, i.fn.vendors.destroypJS = function() {
        cancelAnimationFrame(i.fn.drawAnimFrame), t.remove(), pJSDom = null
    }, i.fn.vendors.drawShape = function(e, a, t, i, s, n) {
        var r = s * n,
            c = s / n,
            o = 180 * (c - 2) / c,
            l = Math.PI - Math.PI * o / 180;
        e.save(), e.beginPath(), e.translate(a, t), e.moveTo(0, 0);
        for (var v = 0; v < r; v++) e.lineTo(i, 0), e.translate(i, 0), e.rotate(l);
        e.fill(), e.restore()
    }, i.fn.vendors.exportImg = function() {
        window.open(i.canvas.el.toDataURL("image/png"), "_blank")
    }, i.fn.vendors.loadImg = function(e) {
        if (i.tmp.img_error = void 0, "" != i.particles.shape.image.src)
            if ("svg" == e) {
                var a = new XMLHttpRequest;
                a.open("GET", i.particles.shape.image.src), a.onreadystatechange = function(e) {
                    4 == a.readyState && (200 == a.status ? (i.tmp.source_svg = e.currentTarget.response, i.fn.vendors.checkBeforeDraw()) : (console.log("Error pJS - Image not found"), i.tmp.img_error = !0))
                }, a.send()
            } else {
                var t = new Image;
                t.addEventListener("load", function() {
                    i.tmp.img_obj = t, i.fn.vendors.checkBeforeDraw()
                }), t.src = i.particles.shape.image.src
            }
        else console.log("Error pJS - No image.src"), i.tmp.img_error = !0
    }, i.fn.vendors.draw = function() {
        "image" == i.particles.shape.type ? "svg" == i.tmp.img_type ? i.tmp.count_svg >= i.particles.number.value ? (i.fn.particlesDraw(), i.particles.move.enable ? i.fn.drawAnimFrame = requestAnimFrame(i.fn.vendors.draw) : cancelRequestAnimFrame(i.fn.drawAnimFrame)) : i.tmp.img_error || (i.fn.drawAnimFrame = requestAnimFrame(i.fn.vendors.draw)) : void 0 != i.tmp.img_obj ? (i.fn.particlesDraw(), i.particles.move.enable ? i.fn.drawAnimFrame = requestAnimFrame(i.fn.vendors.draw) : cancelRequestAnimFrame(i.fn.drawAnimFrame)) : i.tmp.img_error || (i.fn.drawAnimFrame = requestAnimFrame(i.fn.vendors.draw)) : (i.fn.particlesDraw(), i.particles.move.enable ? i.fn.drawAnimFrame = requestAnimFrame(i.fn.vendors.draw) : cancelRequestAnimFrame(i.fn.drawAnimFrame))
    }, i.fn.vendors.checkBeforeDraw = function() {
        "image" == i.particles.shape.type ? "svg" == i.tmp.img_type && void 0 == i.tmp.source_svg ? i.tmp.checkAnimFrame = requestAnimFrame(check) : (cancelRequestAnimFrame(i.tmp.checkAnimFrame), i.tmp.img_error || (i.fn.vendors.init(), i.fn.vendors.draw())) : (i.fn.vendors.init(), i.fn.vendors.draw())
    }, i.fn.vendors.init = function() {
        i.fn.retinaInit(), i.fn.canvasInit(), i.fn.canvasSize(), i.fn.canvasPaint(), i.fn.particlesCreate(), i.fn.vendors.densityAutoParticles(), i.particles.line_linked.color_rgb_line = hexToRgb(i.particles.line_linked.color)
    }, i.fn.vendors.start = function() {
        isInArray("image", i.particles.shape.type) ? (i.tmp.img_type = i.particles.shape.image.src.substr(i.particles.shape.image.src.length - 3), i.fn.vendors.loadImg(i.tmp.img_type)) : i.fn.vendors.checkBeforeDraw()
    }, i.fn.vendors.eventsListeners(), i.fn.vendors.start()
};
Object.deepExtend = function(e, a) {
    for (var t in a) a[t] && a[t].constructor && a[t].constructor === Object ? (e[t] = e[t] || {}, arguments.callee(e[t], a[t])) : e[t] = a[t];
    return e
}, window.requestAnimFrame = function() {
    return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || function(e) {
        window.setTimeout(e, 1e3 / 60)
    }
}(), window.cancelRequestAnimFrame = function() {
    return window.cancelAnimationFrame || window.webkitCancelRequestAnimationFrame || window.mozCancelRequestAnimationFrame || window.oCancelRequestAnimationFrame || window.msCancelRequestAnimationFrame || clearTimeout
}(), window.pJSDom = [], window.particlesJS = function(e, a) {
    "string" != typeof e && (a = e, e = "particles-js"), e || (e = "particles-js");
    var t = document.getElementById(e),
        i = "particles-js-canvas-el",
        s = t.getElementsByClassName(i);
    if (s.length)
        for (; s.length > 0;) t.removeChild(s[0]);
    var n = document.createElement("canvas");
    n.className = i, n.style.width = "100%", n.style.height = "100%";
    var r = document.getElementsByClassName(e)[0].appendChild(n);
    null != r && pJSDom.push(new pJS(e, a))
}, window.particlesJS.load = function(e, a, t) {
    var i = new XMLHttpRequest;
    i.open("GET", a), i.onreadystatechange = function(a) {
        if (4 == i.readyState)
            if (200 == i.status) {
                var s = JSON.parse(a.currentTarget.response);
                window.particlesJS(e, s), t && t()
            } else console.log("Error pJS - XMLHttpRequest status: " + i.status), console.log("Error pJS - File config not found")
    }, i.send()
};
















/* font load*/



$(function() {
    var e = new FontFaceObserver("DINCondensed-Bold", {
            weight: 700
        }),
        o = new FontFaceObserver("GothamPro-Medium", {
            weight: 500
        }),
        a = new FontFaceObserver("GothamPro-Black", {
            weight: 900
        }),
        t = new FontFaceObserver("GothamPro-Regular", {
            weight: 400
        }),
        n = new FontFaceObserver("GothamPro-Light", {
            weight: 300
        }),
        d = $(document.documentElement);
    Promise.all([e.load(), o.load(), a.load(), t.load(), n.load()]).then(function() {
        d.removeClass("fonts-failed").addClass("fonts-loaded")
    })["catch"](function() {
        d.removeClass("fonts-loaded").addClass("fonts-failed")
    })
});




/* font load end*/




$(function() {
    function e(e, a) {
        $.getScript("./main.js", function() {
            console.log("Test script is loaded!")
        });
        var s = $(this);
        switch (s.find(".content").append('<div id="particles-js" class="particles-js"></div>'), n(s), i(s), t(s), e) {
            case "Banner":
                d.addClass("sidenav-light"), p.addClass("show-stick"), s.find(f).addClass("banner-title-appear"), v.click(function() {
                    $.fn.fullpage.moveTo("Strategy", 2)
                }), o(1, 1, .2, _).then(function() {
                    o(1, 1, 0, s.find(h))
                }), r("#fff");
                break;
            case "Strategy":
                d.addClass("sidenav-dark"), o().then(function() {
                    s.find(k).addClass("text-slide"), s.addClass("bg-title-appear")
                }).then(function() {
                    o(1, 1, .5, [g, s.find(h)])
                }), r("#000");
                break;
            case "Strategy1":
                d.addClass("sidenav-dark"), o().then(function() {
                    s.find(k).addClass("text-slide"), s.addClass("bg-title-appear")
                }).then(function() {
                    o(1, 1, .5, [g, s.find(h)])
                }), r("#000");
                break;
            case "Process":
                d.addClass("sidenav-dark"), o().then(function() {
                    s.find(k).addClass("text-slide"), s.addClass("bg-title-appear")
                }).then(function() {
                    o(1, 1, .5, [m, s.find(h), y])
                }), r("#000");
                break;
            case "Portfolio":
			d.addClass("sidenav-dark"), o().then(function() {
                    s.find(k).addClass("text-slide"), s.addClass("bg-title-appear")
                }).then(function() {
                    o(1, 1, .5, [m, s.find(h), y])
                }),
                r("#fff");
                break;


            case "Contact":
                d.addClass("sidenav-dark"), o().then(function() {
                    s.find(k).addClass("text-slide"), s.addClass("bg-title-appear")
                }).then(function() {
                    o(1, 1, .5, [m, s.find(h), y])
                }),
                    r("#fff");
                break;

                

       
        }
        particlesJS("particles-js", C)
    }

    function a(e, a, s) {
        var n = $(this),
            t = n[0].dataset.anchor;
        if (d.hasClass("sidenav-opened")) return !1;
        switch (t) {
            case "Banner":
                d.removeClass("sidenav-light"), n.find(f).removeClass("banner-title-appear"), o(1, 0, .2, _), o(1, 0, 0, n.find(h));
                break;
            case "Strategy":
                d.removeClass("sidenav-dark"), n.find(k).removeClass("text-slide"), n.removeClass("bg-title-appear"), o(.5, 0, 0, [n.find(g), n.find(h)]);
                break;
            case "Strategy1":
                d.removeClass("sidenav-dark"), n.find(k).removeClass("text-slide"), n.removeClass("bg-title-appear"), o(.5, 0, 0, [n.find(g), n.find(h)]);
                break;
            case "Process":
                d.removeClass("sidenav-dark"), n.find(k).removeClass("text-slide"), n.removeClass("bg-title-appear"), o(.5, 0, .5, [m, n.find(h), y])
				       break;
            case "Portfolio":
                d.removeClass("sidenav-dark"), n.find(k).removeClass("text-slide"), n.removeClass("bg-title-appear"), o(.5, 0, .5, [m, n.find(h), y])
            case "Contact":
                d.removeClass("sidenav-dark"), n.find(k).removeClass("text-slide"), n.removeClass("bg-title-appear"), o(.5, 0, .5, [m, n.find(h), y])
        }
        setTimeout(function() {
            n.find(c).removeClass("appeared"), n.find(".particles-js").remove(), p.before().removeClass("show-stick"), TweenMax.staggerTo(n.find(".label_appear"), .1, {
                opacity: 0
            }, .2), TweenMax.staggerTo(n.find(".socials__label"), .1, {
                opacity: 0
            }, .2)
        }, 700)
    }

    function s() {
        var a = $(this),
            s = a[0].ownerDocument.body.className,
            n = s.substr(11);
        switch (n) {
            case "Banner":
                e("Banner", 1);
                break;
            case "Strategy":
                e("Strategy", 2);
                break;
            case "Strategy1":
                e("Strategy1", 5);
                break;
            case "Process":
                e("Process", 3);
                break;
            case "Portfolio":
                e("Portfolio", 4) 

        }
    }

    function n(e) {
        e.find(c).addClass("appeared")
    }

    function t(e) {
        TweenMax.staggerTo(e.find(".socials__label"), .5, {
            opacity: 1,
            delay: 1
        }, .1)
    }

    function i(e) {
        TweenMax.staggerTo(e.find(".label_appear"), .5, {
            opacity: 1
        }, .1)
    }

    function r(e) {
        "string" == typeof e ? (C.particles.color.value = e, C.particles.line_linked.color = e) : (C.particles.color.value = "#000", C.particles.line_linked.color = "#000")
    }

    function o(e, a, s) {
        var n = $.Deferred(),
            t = [].slice.apply(arguments);
        return t.splice(0, 3), 0 == t.length && n.resolve(), TweenMax.staggerTo(t[0], e, {
            opacity: a
        }, s, function() {
            n.resolve()
        }), n.promise()
    }
    var l = ["Banner", "Strategy", "Process", "Portfolio",  "Strategy1"],
        c = $(".nav"),
        d = $(".sidenav"),
        p = $(".banner__bottom"),
        f = $(".banner__title"),
        b = $(".banner__secret"),
        v = $(".banner__btn"),
        u = $(".banner__privacy"),
        _ = [b, v, u],
        g = $(".strategy__card"),
        m = $(".process__card"),
        y = $(".process__card-label"),
        h = $(".call"),
        k = $(".title_appear"),
        C = {
            "particles": {
    "number": {
      "value": 80,
      "density": {
        "enable": true,
        "value_area": 1000
      }
    },
    "color": {
      "value": "#fff"
    },
    "shape": {
      "type": "circle",
      "stroke": {
        "width": 0,
        "color": "#fff"
      },
      "polygon": {
        "nb_sides": 9
      },
    },
    "opacity": {
      "value": 0.8,
      "random": false,
      "anim": {
        "enable": false,
        "speed": 1,
        "opacity_min": 0.5,
        "sync": false
      }
    },
    "size": {
      "value": 3,
      "random": true,
      "anim": {
        "enable": false,
        "speed": 40,
        "size_min": 0.1,
        "sync": false
      }
    },
    "line_linked": {
      "enable": true,
      "distance": 200,
      "color": "#fff",
      "opacity": 0.8,
      "width": 1
    },
    "move": {
      "enable": true,
      "speed": 9,
      "direction": "none",
      "random": false,
      "straight": false,
      "out_mode": "out",
      "bounce": false,
      "attract": {
        "enable": false,
        "rotateX": 600,
        "rotateY": 1200
      }
    }
  },
  "interactivity": {
    "detect_on": "canvas",
    "events": {
      "onhover": {
        "enable": true,
        "mode": "grab"
      },
      "onclick": {
        "enable": true,
        "mode": "push"
      },
      "resize": true
    },
    "modes": {
      "grab": {
        "distance": 140,
        "line_linked": {
          "opacity": 1
        }
      },
      "bubble": {
        "distance": 400,
        "size": 40,
        "duration": 2,
        "opacity": 1,
        "speed": 3
      },
      "repulse": {
        "distance": 200,
        "duration": 0.4
      },
      "push": {
        "particles_nb": 4
      },
      "remove": {
        "particles_nb": 2
      }
                }
            },
            retina_detect: !0
        };
    $("#fullpage").fullpage({
        lockAnchors: !0,
        anchors: l,
        afterLoad: e,
        onLeave: a,
        afterResize: s
    })
});
$(function() {
    function e() {
        TweenMax.to(s, .5, {
            top: "100vh"
        }), TweenMax.fromTo(n, .5, {
            height: 0,
            padding: 0
        }, {
            height: "100vh",
            padding: "5.6vh 3.3vw 4.7vh 14.3vw"
        })
    }

    function a() {
        TweenMax.fromTo(n, .5, {
            height: "100vh",
            padding: "5.6vh 3.3vw 4.7vh 14.3vw"
        }, {
            height: 0,
            padding: 0
        }), TweenMax.to(s, .5, {
            top: "7.3vh"
        })
    }

    function t() {
        TweenMax.staggerTo(v, .1, {
            display: "flex"
        }), TweenMax.staggerFromTo([d, l, c], 1, {
            opacity: 0
        }, {
            opacity: 1
        }, .3)
    }

    function o(e, t) {
        var o = "#fff";
        n.hasClass("sidenav-dark") && (o = "#000"), f.to([d, l, c], .5, {
            opacity: 0
        }).to(v, .1, {
            display: "none"
        }).call(a).fromTo(s, .4, {
            top: "100vh"
        }, {
            top: "7.2vh"
        }).fromTo(s, .3, {
            width: "100vw",
            right: "-96.3vw"
        }, {
            width: "14px",
            right: "3.3vw",
            onComplete: function() {
                s.css({
                    opacity: 0
                }), n.removeClass("sidenav-opened").addClass("sidenav-closed"), e && t && e(t)
            }
        })
    }
    var i = $(".menu"),
        n = $(".sidenav"),
        s = $(".sidenav__anim-line"),
        v = $(".sidenav__nav"),
        l = $(".sidenav__link"),
        d = $(".sidenav__close"),
        r = $(".socials__text_sidenav"),
        c = $(".socials__label_sidenav"),
        h = $("body"),
        f = new TimelineMax;
    i.click(function(a) {
        a.preventDefault(), n.removeClass("sidenav-closed").addClass("sidenav-opened");
        var o = "#fff";
        n.hasClass("sidenav-dark") && (o = "#000"), f.fromTo(s, .5, {
            width: "14px",
            right: "2vw",
            opacity: 0,
            backgroundColor: o
        }, {
            width: "100vw",
            right: "-96.3vw",
            opacity: 1,
            backgroundColor: o
        }).call(e).call(t)
    }), h.click(function(e) {
        e.preventDefault();
        var a = ["Banner", "Strategy", "Process", "Portfolio", "Contact", "Services", "About", "Strategy1"];
        if ($(e.target).closest(n).length && !$(e.target).closest(i).length)
            if ($(e.target).is(l))
                for (var t = 0; t < a.length; t++) e.target.attributes["data-slide"].value == a[t] && o($.fn.fullpage.moveTo, a[t]);
            else $(e.target).is(r) || o()
    })
});
$(function() {
    var l = ($(".socials__link"), $(".socials__label"), $(".tel"));
    l.click(function() {
        $.fn.fullpage.moveTo("Contact", 4)
    })
});
