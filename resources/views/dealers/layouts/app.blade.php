<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }}</title>
    <link rel="shortcut icon" href="{{ asset('admins/images/favicon.png') }}"/>
    <link href="https://fonts.googleapis.com/css?family=Khand:300,400,500,600,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('dealer/css/app.css') }}">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.4.1/cropper.css">

    @yield('css')

    <script type="text/javascript">
        window.base_url = "{{ url('/') }}";
        window.user = @json(auth()->guard('dealer')->user());
        window.guard = 'dealer';
    </script>
</head>
<body class="vertical-layout vertical-menu 2-columns fixed-navbar  menu-expanded pace-done @yield('body-class')">

<div id="app">
    <app></app>
</div>

@yield('js')
<script src="{{ asset('dealer/js/app.js') }}"></script>
</body>
</html>
