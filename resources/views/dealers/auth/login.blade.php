<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?=((isset($title))?$title:'Auto Bazaar - Login');?></title>
    <link href="https://fonts.googleapis.com/css?family=Exo+2:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700|Orbitron:400,500,700,900|Poppins:100i,200,200i,300,300i,400,400i,500,500i,600,600i,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="icon" type="image/gif" sizes="32x32" href="{{ asset('images/favicon.png') }}">
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
    <link href="{{ asset('fonts/fontawesome/css/all.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/mega-menu.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/flexslider.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}" type="text/css"/>
</head>

<body>

<section class="login-main" id="registerApp">
    <img src="{{ asset('images/login-car.png') }}" class="log-car" alt="">
    <div class="container">

        <div class="login-inner">
            <div class="row d-flex">
                <div class="col-lg-6 col-12 ">
                    <div class="left">
                        <img src="{{ asset('images/login.png') }}" class="img-fluid" alt="">
                    </div>
                </div>
                <div class="col-lg-6 col-12 align-self-center">
                    <div class="right">
                        <h1>Dealer Login</h1>
                        <h4>Log in to your Account </h4>

                        <form action="{{ route('dealer.login') }}" method="POST" id="loginForm">
                            @csrf
                            <div class="row">
                                <div class="col-12 form-group">
                                    <input type="email" class="form-control" id="loginEmail" name="email" placeholder="Enter Email Address">
                                    <i class="fa fa-envelope"></i>
                                    @error('email')
                                        <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>

                                <div class="col-12 form-group">
                                    <input name="password" type="password" id="loginPassword" class="form-control" placeholder="Password">
                                    <i class="fa fa-lock"></i>
                                    @error('password')
                                    <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="">
                                    <label class="login-check">Remember Me
                                        <input type="checkbox" checked="checked" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="">
                                    <a href="#" class="forgot" data-toggle="modal" data-target="#exampleModalCenter"> Forgot Password?</a>
                                </div>
                            </div>
                            <button type="submit" class="red">
                                login <i class="fa fa-angle-right"></i>
                            </button>
                        </form>
                        <!--login modal start here-->
                        <forgot-password></forgot-password>
                        <!-- Button trigger modal -->
                        <!-- Modal -->
                        <div class="modal fade" id="vr-modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="forget-pass">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                                        <div class="modal-body">
                                            <h1>Password Recovery</h1>
                                            <form action="">
                                                <div class="row">
                                                    <div class="col-12 form-group">
                                                        <input type="number" placeholder="Enter verification code" class="form-control">
                                                        <i class="fas fa-pencil-alt"></i>
                                                    </div>
                                                    <div class="col-12">
                                                        <button type="button" onclick="$('#vr-modal').modal('toggle')" data-toggle="modal" data-target="#rp-modal" class="form-control red" id="count-2"> Continue <i class="fa fa-angle-right"></i></button>
                                                    </div>
                                                    <a href="#" data-dismiss="modal" aria-label="Close"><i class="fa fa-arrow-left"></i> back to login</a> </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Modal -->
                        <div class="modal fade" id="rp-modal" tabindex="-1" role="dialog" aria-labelledby="cont" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="forget-pass">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                                        <div class="modal-body">
                                            <h1>Password Recovery</h1>
                                            <form action="">
                                                <div class="row">
                                                    <div class="col-12 form-group">
                                                        <input type="password" placeholder="Enter New Password" class="form-control">
                                                        <i class="fas fa-lock"></i>
                                                    </div>
                                                    <div class="col-12 form-group">
                                                        <input type="password" placeholder="Retype Password" class="form-control">
                                                        <i class="fas fa-lock"></i>
                                                    </div>
                                                    <div class="col-12">
                                                        <button data-dismiss="modal" aria-label="Close" type="button" class="form-control red"> Continue <i class="fa fa-angle-right"></i></button>
                                                    </div>
                                                    <a href="#" data-dismiss="modal" aria-label="Close"><i class="fa fa-arrow-left"></i> back to login</a>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--forgot step 2 end here-->
                        <p class="text-center">- Not a member? Sign up now -</p>
                        <a href="#" class="register pur" data-toggle="modal" data-target="#rg-modal">register <i class="fa fa-angle-right"></i> </a>
                    </div>

                    <!-- Modal -->
                    <div class="modal fade" id="rg-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="forget-pass">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                                    <div class="modal-body">
                                        <registration-body></registration-body>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--register modal end here-->

                </div>
            </div>
        </div>
    </div>
</section>

<script src="{{ asset('js/frontend.js') }}"></script>
</body>
</html>
