<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">

    <meta name="author" content="PIXINVENT">
    <title>{{ config('app.name') }} - Password Recovery</title>
    <link rel="shortcut icon" href="{{ asset('admins/images/favicon.png') }}"/>
    <link href="https://fonts.googleapis.com/css?family=Khand:300,400,500,600,700&display=swap" rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/css/vendors.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/vendors/css/forms/icheck/icheck.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/vendors/css/forms/icheck/custom.css') }}">
    <!-- END VENDOR CSS-->
    <!-- BEGIN STACK CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/css/app.css') }}">
    <!-- END STACK CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/css/pages/login-register.css') }}">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/assets/css/style.css') }}">
    <!-- END Custom CSS-->
</head>
<body class="vertical-layout vertical-menu 1-column  bg-full-screen-image menu-expanded blank-page blank-page"
      data-open="click" data-menu="vertical-menu" data-col="1-column">


<section class="register forget-pass">
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-xl-3"></div>
            <div class="col-md-8 col-xl-6 col-12">
                <div class="register-main">
                    <form method="POST" action="{{ route('dealers.password.email') }}">
                        @csrf
                        <img src="{{ asset('admins/images/logo.png') }}" class="img-full" alt="logo">
                        <div class="form-main">
                            <div class="row">
                                <div class="col-md-12">
                                    <h1>Password Recovery</h1>
                                    @if (session('status'))
                                        <div class="alert alert-success" role="alert">
                                            {{ session('status') }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <!--fields start here-->
                            <div class="fields">
                                <div class="row">

                                    <div class="col-md-12">
                                        <i class="fa fa-envelope"></i>
                                        <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="email" value="{{ old('email') }}" placeholder="Email Address" required>
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <button type="submit" class="form-control">Send Password Reset Link</button>
                                <div class="new-user">
                                    <p>OR</p>
                                    <a href="{{ route('dealers.login') }}" class="login back"><i class="fa fa-long-arrow-left"></i>back to login</a>
                                </div>
                            </div>
                            <!--fields end here-->

                        </div>
                    </form>
                </div>

            </div>
            <div class="col-md-2 col-xl-3"></div>
        </div>
    </div>
</section>


<!-- ////////////////////////////////////////////////////////////////////////////-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <section class="flexbox-container">
                <div class="col-12 d-flex align-items-center justify-content-center">
                    <div class="col-lg-6 col-xl-4 col-md-10 col-10 box-shadow-2 p-0">
                        <div class="card rad border-grey border-lighten-3 p-5  px-1 py-1 m-0">
                            <div class="card-header border-0">
                                <div class="card-title text-center">
                                    <img src="{{ asset('admins/images/login-logo.png') }}" class="img-fluid" alt="branding logo">
                                </div>

                            </div>
                            <div class="card-content logn-form">

                                <div class="card-body">
                                    <h1>Password Recovery</h1>
                                    @if (session('status'))
                                        <div class="alert alert-success" role="alert">
                                            {{ session('status') }}
                                        </div>
                                    @endif

                                    <form method="POST" action="{{ route('dealers.password.email') }}" class="form-horizontal">
                                        @csrf

                                        <fieldset class="form-group position-relative has-icon-left">
                                            <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="email" value="{{ old('email') }}" placeholder="Email" required>
                                            <div class="form-control-position">
                                                <i class="fa fa-envelope"></i>
                                            </div>
                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </fieldset>
                                        <button type="submit" class="btn btn-outline-primary btn-block">{{ __('Send Password Reset Link') }}</button>
                                        <a href="{{ route('dealers.login') }}" class="back-loginn">Back To Login</a>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<!-- ////////////////////////////////////////////////////////////////////////////-->
<!-- BEGIN VENDOR JS-->
<script src="{{ asset('admins/app-assets/vendors/js/vendors.min.js') }}" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="{{ asset('admins/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('admins/app-assets/vendors/js/forms/icheck/icheck.min.js') }}" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN STACK JS-->
<script src="{{ asset('admins/app-assets/js/core/app-menu.js') }}" type="text/javascript"></script>
<script src="{{ asset('admins/app-assets/js/core/app.js') }}" type="text/javascript"></script>
<script src="{{ asset('admins/app-assets/js/scripts/customizer.js') }}" type="text/javascript"></script>
<!-- END STACK JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="{{ asset('admins/app-assets/js/scripts/forms/form-login-register.js') }}" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->
</body>
</html>
