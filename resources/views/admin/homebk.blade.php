<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }}</title>
    <link rel="shortcut icon" href="{{asset('images/favicon.ico')}}" />
    <link href="https://fonts.googleapis.com/css?family=Khand:300,400,500,600,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin-assets/css/app.css') }}">
    {{--    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">--}}
    {{--    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">--}}
    @yield('css')

    <script type="text/javascript">
        window.base_url = "{{ url('/') }}";
        window.user = @json(auth()->guard('admin')->user());
    </script>
</head>
<body class="@yield('body-class')" >

<div id="app">

    @if(Auth::check())

        @include('partials.header')

        @include('partials.nav')

    @endif

    @yield('content')

</div>

<script src="{{ mix('admin-assets/js/app.js')}}"></script>
</body>
</html>