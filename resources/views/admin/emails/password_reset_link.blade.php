@extends('emails.master')

@section('content')

    <p>You are receiving this email because we received a password reset request for your account.</p>

    <p style="text-align: center;">
        <a style="background: darkslateblue; color: #ffffff; padding: 10px 20px;" href="{{ route('admin.password.reset', [$token]) }}">Link is to reset</a>
    </p>

    <p>This password reset link will expire in 60 minutes.</p>

    <p>If you did not request a password reset, no further action is required.</p>

@endsection