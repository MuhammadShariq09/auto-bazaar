<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">

    <meta name="author" content="PIXINVENT">
    <title>{{ config('app.name') }} - Login</title>
    <link rel="shortcut icon" href="{{ asset('admins/images/favicon.png') }}"/>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/css/vendors.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/vendors/css/forms/icheck/icheck.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/vendors/css/forms/icheck/custom.css') }}">
    <!-- END VENDOR CSS-->
    <!-- BEGIN STACK CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/css/app.css') }}">
    <!-- END STACK CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/css/pages/login-register.css') }}">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/assets/css/style.css') }}">

    <!-- END Custom CSS-->
</head>

<body class="vertical-layout vertical-menu 2-columns menu-expanded" data-open="click" data-menu="vertical-menu" data-col="2-columns">

    <section class="register loginn">
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-xl-3"></div>
                <div class="col-md-8 col-xl-6 col-12">
                    <div class="register-main">
                        <img src="{{ asset('admins/images/logo.png') }}" class="img-full" alt="logo">
                        <div class="form-main">
                            <h1>login to your account</h1>
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <!--fields start here-->
                            <form method="post" class="" action="{{ route('admin.login') }}" novalidate>
                                @csrf
                                <div class="fields">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <i class="fa fa-user"></i>
                                            <input type="text" class="form-control" name="email" spellcheck="true" placeholder="Email" value="{{ old('email') }}">

                                            @error('email')
                                            <span class="invalid-feedback" role="alert" style="display: block;">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror

                                        </div>
                                        <div class="col-md-12">
                                            <i class="fa fa-key"></i>
                                            <input type="password" name="password" spellcheck="true" class="form-control" placeholder="Password">
                                            @error('password')
                                            <span class="invalid-feedback" role="alert" style="display: block;">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-6">
                                            <label class="check">Remember me
                                                <input type="checkbox" name="remember" >
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                        <div class="col-md-6 col-6"><a href="{{ route('admin.password.request') }}">Forgot Password?</a></div>
                                    </div>
                                    <button type="submit" class="form-control">login</button>
                                   
                                    <a href="#" class="back-to-web"><i class="fa fa-angle-left"></i> back to website</a>
                                    
                                    <div class="new-user">
{{--                                        <p>new user</p>--}}
{{--                                        <button class="reg form-control">Register Account</button></div>--}}
                                    <!--fields end here-->
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-xl-3"></div>
            </div>
        </div>
</section>

<!-- BEGIN VENDOR JS-->
<script src="{{ asset('admins/app-assets/vendors/js/vendors.min.js') }}" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="{{ asset('admins/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('admins/app-assets/vendors/js/forms/icheck/icheck.min.js') }}" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN STACK JS-->
<script src="{{ asset('admins/app-assets/js/core/app-menu.js') }}" type="text/javascript"></script>
<script src="{{ asset('admins/app-assets/js/core/app.js') }}" type="text/javascript"></script>
<script src="{{ asset('admins/app-assets/js/scripts/customizer.js') }}" type="text/javascript"></script>
<!-- END STACK JS-->
<!-- BEGIN PAGE LEVEL JS-->
{{--<script src="{{ asset('admins/app-assets/js/scripts/forms/form-login-register.js') }}" type="text/javascript"></script>--}}
<!-- END PAGE LEVEL JS-->
</body>
</html>
