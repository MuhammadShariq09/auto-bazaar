<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?=((isset($title))?$title:'Auto Bazaar - Login');?></title>
    <link href="https://fonts.googleapis.com/css?family=Exo+2:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700|Orbitron:400,500,700,900|Poppins:100i,200,200i,300,300i,400,400i,500,500i,600,600i,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="icon" type="image/gif" sizes="32x32" href="{{ asset('images/favicon.png') }}">
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
    <link href="{{ asset('fonts/fontawesome/css/all.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/mega-menu.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/flexslider.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}" type="text/css"/>
    <script type="text/javascript">
        window.base_url = "{{ url('/') }}";
    </script>
</head>

<body>
<header class="header_area inner-header-main main-header">
            <div class="container">
                <div class="main_header_area animated">
                    <nav id="navigation1" class="navigation">
                        <div class="nav-header">
                            <a class="nav-brand" :to="{ name: 'home'}">
                                <img src="{{asset('images/logo.png')}}" alt="Site Logo">
                            </a>
                            <div class="nav-toggle"></div>
                        </div>
                        <div class="nav-menus-wrapper">
                            <ul class="nav-menu align-to-right">
                                <li>
                                    <a href="{{url('/')}}">Home</a>
                                </li>
                                <li>
                                    <a href="{{url('/about')}}">About us</a>
                                </li>
                             
                                <li>
                                     <a href="{{url('/')}}#advertisers">advertisers</a>
                                  
                                </li>
                               
                                 <li>
                                    <a href="{{url('/prepare-car-for-sale')}}">Prepare Car For Sale</a>
                                </li>
                                <li>
                                    <a href="{{url('/contact')}}">Contact</a>
                                </li>
</ul>
                               
                        </div>
                    </nav>
                </div>
            </div>
        </header>

    <section class="login-main" id="registerApp">
        <img src="{{ asset('images/login-car.png') }}" class="log-car" alt="">
        <div class="container">
            <div class="login-inner">
                <div class="row d-flex">
                    <div class="col-lg-6 col-12 ">
                        <div class="left">
                            <img src="{{ asset('images/login.png') }}" class="img-fluid" alt="">
                        </div>
                    </div>
                    <div class="col-lg-6 col-12 align-self-center">
                      
                        <div class="right">
                            <h1>Seller/Dealer Login</h1>
                            <h4>Log in to your Account </h4>
                           <br/>
                            <form action="{{ route('login') }}" method="POST" id="loginForm">
                                @csrf
                                @if (session('error'))
                                <div class="alert alert-danger">{{ session('error') }}</div>
                                 @endif
                                <div class="row">
                                    <div class="col-12 form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                        <input type="email" class="form-control" id="loginEmail" name="email" placeholder="Enter Email Address">
                                        <i class="fa fa-envelope"></i>
                                        @if ($errors->has('email'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="col-12 form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                        <input name="password" type="password" id="loginPassword" class="form-control" placeholder="Password">
                                        <i class="fa fa-lock"></i>
                                        @if ($errors->has('password'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="d-flex justify-content-between">
                                    <div class="">
                                        <label class="login-check">Remember Me
                                            <input type="checkbox" checked="checked" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                    <div class="">
                                        <a href="#" class="forgot" data-toggle="modal" data-target="#exampleModalCenter"> Forgot Password?</a>
                                    </div>
                                </div>
                                <button type="submit" class="red">
                                    login <i class="fa fa-angle-right"></i>
                                </button>
                            </form>

                            <!--login modal start here-->

                            <forgot-password></forgot-password>

                            <!-- Button trigger modal -->

                            <!-- Modal -->
                            <div class="modal fade" id="vr-modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="forget-pass">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                                            <div class="modal-body">
                                                <h1>Password Recovery</h1>
                                                <form action="">
                                                    <div class="row">
                                                        <div class="col-12 form-group">
                                                            <input type="number" placeholder="Enter verification code" class="form-control">
                                                            <i class="fas fa-pencil-alt"></i>
                                                        </div>
                                                        <div class="col-12">
                                                            <button type="button" onclick="$('#vr-modal').modal('toggle')" data-toggle="modal" data-target="#rp-modal" class="form-control red" id="count-2"> Continue <i class="fa fa-angle-right"></i></button>
                                                        </div>
                                                        <a href="#" data-dismiss="modal" aria-label="Close"><i class="fa fa-arrow-left"></i> back to login</a> </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Modal -->
                            <div class="modal fade" id="rp-modal" tabindex="-1" role="dialog" aria-labelledby="cont" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="forget-pass">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                                            <div class="modal-body">
                                                <h1>Password Recovery</h1>
                                                <form action="">
                                                    <div class="row">
                                                        <div class="col-12 form-group">
                                                            <input type="password" placeholder="Enter New Password" class="form-control">
                                                            <i class="fas fa-lock"></i>
                                                        </div>
                                                        <div class="col-12 form-group">
                                                            <input type="password" placeholder="Retype Password" class="form-control">
                                                            <i class="fas fa-lock"></i>
                                                        </div>
                                                        <div class="col-12">
                                                            <button data-dismiss="modal" aria-label="Close" type="button" class="form-control red"> Continue <i class="fa fa-angle-right"></i></button>
                                                        </div>
                                                        <a href="#" data-dismiss="modal" aria-label="Close"><i class="fa fa-arrow-left"></i> back to login</a> </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--forgot step 2 end here-->

                            <p class="text-center">- Not a member? Sign up now -</p>

                            <a href="#" class="register pur" data-toggle="modal" data-target="#rg-modal">register <i class="fa fa-angle-right"></i> </a>

                        </div>

                        <registration-body></registration-body>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <footer style="background: url(asset('/images/footer-banner.png')) no-repeat;background-size: cover;">
        <div class="container">
            <div class="footer-top">
                <div class="row">
                    <div class="col-md-6 col-12">
                        <a :to="{ name: 'home'}">
                            <img src="{{asset('/images/footer-logo.png')}}" class="img-fluid w-100 wow fadeIn"  data-wow-duration="1s" alt="footer logo"></a>
                    </div>
                    <div class="col-md-6 col-12">
                      
                    </div>
                </div>
            </div>
            <div class="footer-bottom">

                <div class="row">
                    <div class="col-lg-4 col-12">
                        <p class="wow fadeInUp"  data-wow-duration="1s">AutoBazaarUSA website is a simple to use online solution for buying and selling new and used cars. Our site is designed to make finding a vehicle easier than ever before and give you more control of the buying process. The AutoBazaarUSA website is a vehicle listing and information service that brings together buyers and sellers. AutoBazaarUSA website offer fantastic opportunity for automotive related advertisers.</p>
                    </div>
                    <div class="col-lg-4 col-sm-6 col-12 offset-lg-1">
                        <div class="row">
                            <div class="col-lg-6 col-12">
                                <h2 class="wow fadeInUp" data-wow-duration="1s">USEFUL LINKS</h2>
                                <ul>
                                    <li class="wow fadeInUp"  data-wow-duration="1.3s"><a href="{{url('/')}}">Home</a></li>
                                  
                                    <li class="wow fadeInUp"  data-wow-duration="2.1s"><a href="{{url('/contact')}}">Contact Us</a></li>
                                </ul>
                            </div>
                            <div class="col-lg-6 col-12">

                            </div>
                        </div>

                    </div>
                    <div class="col-lg-3 col-sm-6 col-12">
                        <div class="footer-last">
                            <h2 class="wow fadeInUp" data-wow-duration="1.5s">You’ve questions</h2>
                            <ul class="footer-3"><li class="wow fadeInUp"  data-wow-duration="1.3s"><p>Get a Quote</p><a href="tel:800-417-2048"><br>800-417-2048</a></li></ul>
                            <ul><li  class="wow fadeInUp" data-wow-duration="1.5"><p>Send Email</p><a href="mailto:autobazaarusa@yahoo.com"><br>info@autobazaarusa.com</a></li></ul>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-12 copy-right text-center">
                    <p class="wow fadeInUp"  data-wow-duration="1.5s">© Copyright 2020 Auto bazaar </p>
                </div>
            </div>
        </div>
    </footer>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAHPUufTlBkF5NfBT3uhS9K4BbW2N-mkb4&libraries=places" async></script>

    <script src="{{ asset('js/frontend.js') }}"></script>
    @if(request('register', '0') === '1')
        <script>
            $('#rg-modal').modal('show');
        </script>
    @endif
</body>
</html>


{{--
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
--}}
