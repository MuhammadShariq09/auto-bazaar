<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Models\Package::class, function (Faker $faker) {
    return [
        'title' => $faker->name,
        'description' => $faker->paragraph,
        'type' => 'ad',
        'status' => 1,
        'created_by' => 1,
        'months' => rand(1, 12),
        'price' => $faker->randomNumber(3),
        'no_images' => rand(1, 10)
    ];
});
