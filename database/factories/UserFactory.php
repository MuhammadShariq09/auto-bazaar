<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\User;
use App\Models\Dealer;
use App\Models\Admin;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'first_name' => $faker->name,
        'last_name' => $faker->name,
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => bcrypt("Admin123"),
        'status' => rand(0, 1),
        'contact' => $faker->phoneNumber,
        'address' => $faker->address,
        'zipcode' => $faker->postcode,
        'state' => $faker->streetAddress,
        'city' => $faker->city,
        'country' => $faker->country,
        'remember_token' => Str::random(10),
        'accepted_privacy' => 1,
        'image' => 'images/profile/' . $faker->image( storage_path('app/public/images/profile/'),150, 150, 'sports', false)
    ];
});

$factory->define(Dealer::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'about' => $faker->paragraph,
        'email' => $faker->unique()->safeEmail,
        'contact' => $faker->phoneNumber,
        'address' => $faker->address,
        'zipcode' => $faker->postcode,
        'state' => $faker->streetAddress,
        'city' => $faker->city,
        'country' => $faker->country,
        'password' => bcrypt("Admin123"),
        'status' => rand(0, 1),
        'remember_token' => Str::random(10),
        'accepted_privacy' => 1,
        'image' => 'images/profile/' . $faker->image( storage_path('app/public/images/profile/'),150, 150, 'sports', false)
    ];
});

$factory->define(Admin::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => bcrypt("Admin123"),
        'remember_token' => Str::random(10),
        'image' => 'images/profile/' . $faker->image( storage_path('app/public/images/profile/'),150, 150, 'sports', false)
    ];
});

$factory->define(\App\Models\AdModel::class, function (Faker $faker) {
    $year = rand(2000, 2019);
    return [
        'name' => $year,
        'year' => $year,
        'status' => 1,
    ];
});

$factory->define(\App\Models\Make::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->paragraph,
        'status' => 1,
        'image' => null
    ];
});

$factory->define(\App\Models\Feedback::class, function (Faker $faker) {
    return [
        'user_id' => rand(1, 25),
        'email' => $faker->freeEmail,
        'subject' => $faker->sentence,
        'message' => $faker->paragraph
    ];
});
