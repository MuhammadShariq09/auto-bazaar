<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->unsignedBigInteger('model_id');
            $table->unsignedBigInteger('make_id');
            $table->morphs('adable');
            $table->text('description')->nullable();
            $table->decimal('price')->default(0);

            $table->string('condition')->default("used");
            $table->integer('mileage')->default(0);
            $table->string('fuel_type')->default("fuel");
            $table->string('transmission')->default("manual");

            $table->boolean('featured')->default(false);
            $table->boolean('soled')->default(false);
            $table->boolean('status')->default(false);

            $table->string('vin')->nullable();
            $table->string('year')->nullable();
            $table->boolean('published')->default(false);
            $table->unsignedBigInteger('package_id')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ads');
    }
}
