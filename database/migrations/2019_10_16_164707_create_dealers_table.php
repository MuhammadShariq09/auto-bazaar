<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDealersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dealers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->text('about')->nullable();
            $table->string("password");
            $table->string('email');
            $table->date('featured_end_at')->nullable();
            $table->unsignedBigInteger('package_id')->nullable();
            $table->string('contact');
            $table->string('image')->nullable();
            $table->string('address')->nullable();
            $table->string('zipcode')->nullable();
            $table->string('state')->nullable();
            $table->string('city')->nullable();
            $table->string('country')->nullable();
            $table->string('remember_token')->default("")->nullable();
            $table->boolean('status')->default(true);
            $table->boolean('accepted_privacy')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dealers');
    }
}
