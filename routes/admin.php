<?php
use \Illuminate\Support\Facades\Route;
use \Illuminate\Support\Facades\Auth;

//Route::get('/', function(){
//    return redirect(url('admin/dashboard'));
//});

Route::get('logout', 'Auth\LoginController@logout');

Auth::routes();

Route::middleware('auth:admin')->get('/{admin?}', 'HomeController@index')->where('admin', '.*')->name('home');
