<?php

use \Illuminate\Support\Facades\Route;

Route::middleware('auth:api_dealer')->group(function(){
    Route::get('/profile', 'UsersController@profile');
});
