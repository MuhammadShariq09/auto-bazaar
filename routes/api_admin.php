<?php

use \Illuminate\Support\Facades\Route;

Route::middleware('auth:api_admin')->group(function(){

    Route::post('/password-change', 'ProfileController@changePassword');

    Route::get('/dashboard', 'HomeController@dashboard');

    Route::get('/notifications', 'NotificationsController@index');
    Route::put('/notifications/{id?}', 'NotificationsController@markRead');

    Route::prefix('users')->group(function() {
        Route::get('/', 'UsersController@index');
        Route::post('/store', 'UsersController@store');
        Route::get('/{user}', 'UsersController@show');
        Route::put('/{user}', 'UsersController@update');
        Route::delete('/{user}', 'UsersController@destroy');
    });

    Route::prefix('dealers')->group(function(){

        Route::get('/', 'DealersController@index');
        Route::get('/{dealer}', 'DealersController@show');
        Route::put('/{dealer}', 'DealersController@update');
        Route::delete('/{dealer}', 'DealersController@destroy');

    });

    Route::prefix('packages')->group(function() {
        Route::get('/', 'PackagesController@index');
        Route::put('/{package}', 'PackagesController@update');
    });

    Route::prefix('ads')->group(function() {
        Route::get('/', 'AdsController@index');
        Route::get('/{ad}', 'AdsController@show');
        Route::patch('/{ad}', 'AdsController@statusPatch');
        Route::delete('/{ad}', 'AdsController@destroy');
    });

    Route::prefix('feedback')->group(function() {
        Route::get('/', 'FeedbackController@index');
        Route::get('/{feedback}', 'FeedbackController@show');
        Route::delete('/{feedback}', 'FeedbackController@destroy');
    });

    Route::prefix('banners')->group(function() {
        Route::get('/', 'BannersController@index');
        Route::post('/store', 'BannersController@store');
        Route::get('/{banners}', 'BannersController@show');
        Route::put('/{banners}', 'BannersController@update');
        Route::delete('/{banners}', 'BannersController@destroy');
    });

    Route::prefix('pages')->group(function() {
        Route::get('/', 'PagesController@index');
        Route::get('/{pages}', 'PagesController@show');
        Route::put('/{pages}', 'PagesController@update');
    });

    Route::prefix('transactions')->group(function() {
        Route::get('/', 'TransactionsController@index');
    });
});
