<?php

use Illuminate\Http\Request;
use \Illuminate\Support\Facades\Route;


Route::middleware('auth:api')->group(function () {
    Route::put('/user', 'UsersController@user');
    Route::put('/profile', 'UsersController@profile');
    Route::get('/user-notifications', 'NotificationsController@index');
    Route::put('/user-notifications/{id?}', 'NotificationsController@markRead');
    Route::get('/user/package', 'UsersController@package');
    Route::put('/subscribe', 'UsersController@subscribe');
    Route::post('/ads', 'AdsController@store');
    Route::put('/ads/{ad}/sold', 'AdsController@sold');
    Route::get('/my-listing', 'AdsController@myListing');
    Route::delete('/my-listing/{ad}', 'AdsController@destroy');
    Route::get('/wishlist', 'AdsController@wishlist');
    Route::post('/admin-contact', 'ContactsController@admin');
    Route::post('/ads/{ad}/pay', 'AdsController@pay');
    Route::post('/dealers/{dealer}/review', 'DealersController@review');
    Route::post('/password-change', 'ProfileController@changePassword');
    Route::put('wishlist/add', 'WishListsController@add');
    Route::get('dealers/{dealer}', 'DealersController@show');
    Route::put('wishlist/remove', 'WishListsController@remove');
    Route::post('threads/', 'ThreadsController@store');
    Route::get('dealers-threads/', 'ThreadsController@index');
});
Route::get('/packages', 'UsersController@packages');
Route::post('/contact', 'ContactsController@contact');

Route::middleware(['auth:api', 'dealer'])->group(function () {
    Route::get('dealer-notifications', 'NotificationsController@index');
    Route::put('dealer-notifications/{id?}', 'NotificationsController@markRead');
//    Route::get('dealers/{dealer}', 'DealersController@show')->middleware('dealer');
    Route::post('dealers/{dealer}', 'DealersController@update');
    Route::get('users-threads/', 'ThreadsController@index');
});

Route::get('zipcodes', 'HomeController@zipcodes');
Route::get('ads', 'AdsController@index');
Route::get('ads/{ad}', 'AdsController@show');

Route::get('makes', 'MakesController@index');
Route::get('modals', 'ModalsController@index');
Route::post('register', 'Auth\RegisterController@register');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get('featured-cars', 'HomeController@featuredCars');
Route::get('car-stats', 'HomeController@carsStats');
Route::get('banners', 'HomeController@banners');
Route::get('dealers', 'DealersController@index');

//pages data
Route::get('pages/{id}', 'HomeController@page');
