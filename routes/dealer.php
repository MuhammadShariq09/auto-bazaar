<?php

use Illuminate\Support\Facades\Route;
use \Illuminate\Support\Facades\Auth;


Auth::routes();

Route::get('logout', function () {
    Auth::guard('dealer')->logout();
    return redirect(url('dealers/login'));
});

//Route::get('/home', 'HomeController@index')->name('home');
Route::get('/{dealers?}', 'DealersController@index')->where('dealers', '(!admin).*')->name('home');
