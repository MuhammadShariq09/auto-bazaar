<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
     return redirect(url('home'));
});

Auth::routes();

Route::get('logout',function(){
    Auth::logout();
    return redirect(url('login'));
});

//Route::get('/home', 'HomeController@index')->name('home');
Route::get('/{home?}', 'HomeController@index')->where('home', '.*')->name('home');
