<?php

namespace App\Models;

use App\Core\Traits\ImageableFill;
use App\Core\Traits\Mediable;
use App\Core\Traits\Payloggable;
use App\Core\Traits\Wishable;
use Codebyray\ReviewRateable\Contracts\ReviewRateable;
use Codebyray\ReviewRateable\Models\Rating;
use Codebyray\ReviewRateable\Traits\ReviewRateable as ReviewRateableTrait;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasOneThrough;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Cashier\Billable;
use Laravel\Cashier\Cashier;
use Laravel\Cashier\Exceptions\InvalidStripeCustomer;
use Laravel\Cashier\PaymentMethod;
use Laravel\Passport\HasApiTokens;


class User extends Authenticatable implements ReviewRateable
{
    use Notifiable, ImageableFill, Wishable, HasApiTokens, Payloggable, Billable, Mediable, ReviewRateableTrait;

    protected $fillable = ['first_name', 'last_name', 'name', 'email', 'company', 'about', 'is_dealer',
        'password', 'image', 'contact', 'address', 'zipcode', 'state', 'city', 'package_id', 'country', 'status', 'accepted_privacy'];

    protected $hidden = ['password', 'remember_token'];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function setPackageIdAttribute($value){ $this->package_id = $value?? 0; }

  //  public function getNameAttribute($value){ return $this->first_name . ' ' . $this->last_name; }

    public function scopeFilter($query, $filters)
    {
        return $filters->apply($query);
    }

    public function ads(): MorphMany{
        return $this->morphMany(Ad::class, 'adable');
    }

    public function adPackage(): BelongsTo {
        return $this->belongsTo(Package::class, 'package_id')->where('type', 'ad');
    }

    public function profilePackage(): BelongsTo {
        return $this->belongsTo(Package::class,'package_id')->where('type', 'profile');
    }

    public function feedback(): HasMany {
        return $this->hasMany(Feedback::class);
    }

    public function addCards($card_details, $stripe_pm_id = null)
    {

        foreach ($card_details as $key => $value)
            $card_details[$key] = encrypt($value);

        $card_details = array_merge($card_details, ['stripe_pm_id' => $stripe_pm_id, 'default' => 1]);

        $card = new CreditCard($card_details);

        $this->cards()->whereDefault(true)->update(['default' => false]);

        $this->cards()->save($card);
    }

    public function cards()
    {
        return $this->hasMany(CreditCard::class, 'user_id');
    }


    /**
     * Add a payment method to the customer.
     *
     * @param \Stripe\PaymentMethod|string $paymentMethod
     * @return \Laravel\Cashier\PaymentMethod
     * @throws InvalidStripeCustomer
     * @throws \Exception
     */
    public function addPaymentMethod($card_details)
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));
        $paymentMethod = \Stripe\PaymentMethod::create(['type' => 'card', 'card' => $card_details]);

        $this->assertCustomerExists();

        $this->addCards($card_details, $paymentMethod->id);

        $stripePaymentMethod = $this->resolveStripePaymentMethod($paymentMethod);

        if ($stripePaymentMethod->customer !== $this->stripe_id) {
            $stripePaymentMethod = $stripePaymentMethod->attach(
                ['customer' => $this->stripe_id], Cashier::stripeOptions()
            );
        }

        return new PaymentMethod($this, $stripePaymentMethod);
    }

//    public function ratings()
//    {
//        return $this->morphMany(Rating::class, 'author');
//    }

    public function isDealer(){
        return !! $this->is_dealer;
    }

    public function threads() : HasMany {
        return $this->hasMany(Thread::class, 'user_id')
            ->with('sender:id,name,image,first_name,last_name')
            ->with('recipient:id,name,image,first_name,last_name');
    }


}
