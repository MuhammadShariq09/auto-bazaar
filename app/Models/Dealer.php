<?php

namespace App\Models;

use App\Core\Traits\ImageableFill;
use App\Core\Traits\Mediable;
use Codebyray\ReviewRateable\Models\Rating;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Notifications\Notifiable;
use Codebyray\ReviewRateable\Traits\ReviewRateable as ReviewRateableTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class Dealer extends Authenticatable
{
    use Notifiable, ImageableFill, ReviewRateableTrait, Mediable, HasApiTokens, Mediable;

    protected $fillable = ['name', "password", 'about', 'email', 'image', 'contact', 'image', 'address', 'zipcode', 'state', 'city', 'country', 'remember_token', 'status', 'accepted_privacy', 'price'];

    public function scopeFilter($query, $filters)
    {
        return $filters->apply($query);
    }

    public function getRatingsAvgAttribute($value) {
        return (float)$value;
    }

    public function ads(): MorphMany{
        return $this->morphMany(Ad::class, 'adable');
    }

    /**
     *
     * @param $sort
     * @return mixed
     */
    public function getRatings($sort = 'desc')
    {
        return ($this->ratings()->orderBy('created_at', $sort)->with('author')->get());
        // return (new Rating())->getAllRatings($this->id, $sort);
    }

    /**
     *
     * @param $sort
     * @return mixed
     */
    public function avgRatings()
    {
        return $this->morphMany(Rating::class, 'reviewrateable')->selectRaw('avg(reviews.rating) as ratings_avg, reviews.reviewrateable_type, reviews.reviewrateable_id');
//        return (new Rating())->getAllRatings($this->id, $sort);
    }
}
