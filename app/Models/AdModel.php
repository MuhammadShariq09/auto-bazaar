<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdModel extends Model
{
    protected $table = 'models';
    protected $guarded = [];
}
