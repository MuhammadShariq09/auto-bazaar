<?php

namespace App\Models;

use App\Core\Traits\ImageableFill;
use App\Core\Traits\Mediable;
use App\Core\Traits\Payloggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ad extends Model
{
    use ImageableFill, SoftDeletes, Mediable, Payloggable;

    protected $fillable = ['name', 'model_id', 'make_id', 'description', 'package_id', 'price', 'condition', 'mileage', 'fuel_type', 'transmission', 'featured', 'soled', 'status', 'vin', 'year', 'zipcode'];

    protected $casts = [
        'featured' => 'boolean',
        'published' => 'boolean',
        'soled' => 'boolean',
    ];

    public function adable(): MorphTo {
        return $this->morphTo();
    }

    public function make(): BelongsTo {
        return $this->belongsTo(Make::class);
    }

    public function model(): BelongsTo {
        return $this->belongsTo(AdModel::class, 'model_id');
    }

    public function package(): BelongsTo
    {
        return $this->belongsTo(Package::class);
    }

    public function scopeFilter($query, $filters)
    {
        return $filters->apply($query);
    }
}
