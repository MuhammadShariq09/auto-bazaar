<?php

namespace App\Models;

use App\Core\Traits\Payloggable;
use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    use Payloggable;

    protected $fillable = ['title', 'description', 'type', 'status', 'created_by', 'months', 'price', 'no_images'];
}
