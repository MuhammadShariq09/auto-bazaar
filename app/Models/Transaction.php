<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Transaction extends Model
{
    protected $fillable = ['description', 'transactor', 'amount', 'transaction_id'];

    public function source() : MorphTo{
        return $this->morphTo('transitionable');
    }

    public function payer() : BelongsTo{
        return $this->belongsTo(User::class, 'transactor');
    }
}
