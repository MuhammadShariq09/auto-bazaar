<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Thread extends Model
{
    protected $fillable = [ 'user_id', 'dealer_id' ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    public function sender()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function recipient()
    {
        return $this->belongsTo(User::class, 'dealer_id');
    }
}
