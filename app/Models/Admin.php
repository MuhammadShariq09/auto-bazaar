<?php

namespace App\Models;

use App\Core\Traits\Loggable;
use App\Notifications\SendAdminPasswordResetNotification;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class Admin extends Authenticatable
{
    use Notifiable, HasApiTokens, Loggable;

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new SendAdminPasswordResetNotification($token));
    }
}
