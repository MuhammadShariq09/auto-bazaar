<?php

namespace App\Models;

use App\Core\Traits\ImageableFill;
use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    use ImageableFill;

    protected $guarded = [];
}
