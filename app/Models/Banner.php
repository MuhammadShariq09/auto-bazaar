<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Core\Traits\ImageableFill;

class Banner extends Model
{
    use ImageableFill;
    protected $fillable = ['title', 'image', 'status'];
}
