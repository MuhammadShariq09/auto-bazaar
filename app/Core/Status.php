<?php

namespace App\Core;

class Status
{
    const PENDING = 0;
    const APPROVED = 1;
    const ACTIVE = 2;
    const REJECTED = 3;
    const PENDING_PAYMENT = 4;
    const SOLD = 5;
}
