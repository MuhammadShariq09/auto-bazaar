<?php

namespace App\Core\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiBaseController extends Controller
{
    protected function responseWithError($message, $errors = [], $code = 422)
    {
        return response(['message' => $message, 'errors' => $errors], $code);
    }

    protected function response($data, $code = 200)
    {
        return response()->json($data, $code);
    }
}
