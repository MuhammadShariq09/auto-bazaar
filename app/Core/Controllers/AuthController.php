<?php

namespace App\Core\Controllers;

use App\Models\Administrator\Admin;
use App\Notifications\UserRegistered;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Validator;

class AuthController extends ApiBaseController
{
    public function register (Request $request)
    {
        /*$request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        $request['password']= Hash::make($request['password']);

        $user = User::create($request->toArray());

        $user->addState();

        $token = $user->createToken('Laravel Password Grant Client')->accessToken;

        $response = ['token' => $token];

        event(new \App\Events\UserRegistered($user));

        return response()->json($response, 200);*/
    }

    public function login (Request $request)
    {
        $request->validate([
            'email' => 'required|email|max:255|exists:users',
            'password' => 'required',
        ],[
            'email.exists' => "Email is not registered on the platform"
        ]);

        $user = User::query()->where('email', $request->email)
            ->where('status', 1)->first();

        if(!$user)
            return $this->responseWithError('Invalid Credentials', [ 'password' => ['Your account is blocked. Please contact admin'] ]);

        if (Hash::check($request->password, $user->password)) {
            $user->device_id = $request->device_id;
            $user->device_type = $request->device_type;
            $user->save();
            $token = $user->createToken('Laravel Password Grant Client')->accessToken;
            $response = ['token' => $token];
            return response($response);
        }

        return $this->responseWithError('Invalid Credentials', [ 'password' => ['Invalid password'] ]);
    }

    public function logout (Request $request)
    {
        $request->user()->update(['device_id' => null, 'device_type' => null]);
        $token = $request->user()->token();
        $token->revoke();
        return response(['message' => 'You have been successfully logged out!'], 200);
    }

}
