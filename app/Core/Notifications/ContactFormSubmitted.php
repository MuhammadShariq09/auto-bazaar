<?php

namespace App\Core\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ContactFormSubmitted extends Notification implements ShouldQueue
{
    use Queueable;

    public $feedback;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($feedback)
    {
        $this->feedback = $feedback;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->view('emails.contact_form_submitted', ['user' => $notifiable, 'admin' => true])
            ->subject("New Query Submitted");
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'notification' => "New feedback submitted",
            'user' => $this->feedback
        ];
    }

    public function toDatabase($notifiable)
    {
        return [
            'feedback_id' => $this->feedback->id,
            'feedback' => $this->feedback->message,
            'message' => "New Feedback is sent",
        ];
    }
}
