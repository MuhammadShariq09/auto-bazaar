<?php

namespace App\Core\Listeners;

use App\Core\Notifications\ContactFormSubmitted;
use App\Models\Administrator\Admin;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;

class ContactSubmittedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        Notification::send(Admin::all(), new ContactFormSubmitted($event->feedback));
//        auth()->user()->notify(new NewContactFormSubmitted());
    }
}
