<?php

namespace App\Core\Traits;


use App\Core\Models\WishList;
use App\Models\Ad;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

trait Wishable{

    public function wish_lists() : BelongsToMany
    {
        return $this->belongsToMany(Ad::class, WishList::class, 'user_id','product_id');
    }

    public function add_to_wish_list($ad)
    {
        $id = is_int($ad)? $ad: $ad->id;
        $this->wish_lists()->syncWithoutDetaching($id);
    }

    public function remove_from_wish_list($ad)
    {
        $id = is_int($ad)? $ad: $ad->id;
        $this->wish_lists()->detach($id);
    }
}
