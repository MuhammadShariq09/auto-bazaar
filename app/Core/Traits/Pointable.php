<?php

namespace App\Core\Traits;

use App\Models\Transaction;
use App\PointLog;

trait Pointable
{

    public function points()
    {
        return $this->morphMany(PointLog::class, 'pointable');
    }

    public function savePointLog($points, $message, $userId = null, $module = "ftfp")
    {
        $log = new PointLog([
            'description' => $message,
            'points' => $points,
            'player_id' => $userId ?? auth()->id(),
            'module' => $module,
        ]);
        return $this->points()->save($log);
    }

}
