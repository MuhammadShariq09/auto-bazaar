<?php

namespace App\Core\Traits;

use App\Models\Media;

trait Mediable{

    public function medias()
    {
        return $this->morphMany(Media::class, 'mediable');
    }

    public function saveMedia($filename, $type = 'image')
    {
        return $this->medias()->save(new Media([ 'type' => $type, 'file_name' => $filename ]));
    }

}
