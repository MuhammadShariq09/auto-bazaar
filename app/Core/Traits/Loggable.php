<?php

namespace App\Core\Traits;


use App\Core\Models\ActivityLog;

trait Loggable{

    public function logs()
    {
        return $this->morphMany(ActivityLog::class, 'loggable');
    }

    public function saveLog($message)
    {
        $log = new ActivityLog([
            'subject' => $message,
            'url' => request()->fullUrl(),
            'method' => request()->method(),
            'ip' => request()->ip(),
            'agent' => request()->userAgent()
        ]);
        return $this->logs()->save($log);
    }

}
