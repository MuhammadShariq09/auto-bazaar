<?php

namespace App\Core\Filters;

class UserFilters extends Filters {

    protected $filters = ['name', 'status', 'dealer'];

    public function name($name)
    {
//        $this->builder->where('id', '!=', auth('api')->id());

        if(!trim($name)) return;

        $this->builder->where(function($q) use($name){
            $names = explode(' ', $name);
            collect($names)->each(function($word) use($q){
                $q->orWhere("last_name", 'LIKE', "%{$word}%");
                $q->orWhere("first_name", 'LIKE', "%{$word}%");
            });
        });

    }

    public function status($status)
    {
        $this->builder->where("status", $status);
    }

    public function dealer($isDealer)
    {
        $this->builder->where("is_dealer", $isDealer);
    }

}
