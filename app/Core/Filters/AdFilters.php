<?php

namespace App\Core\Filters;

use Illuminate\Http\Request;
use App\Core\Status;

class AdFilters extends Filters {

    protected $miles;

    protected $filters = ['name', 'status', 'model', 'make', 'year_between', 'soled', 'inactive', 'year_from', 'year_to', 'miles', 'zipcode'];

    function __construct(Request $request)
    {
        parent::__construct($request);
        $this->miles = $request->miles;
    }
    
    public function name($name)
    {
        $this->builder->where('id', '!=', auth()->id());
        $this->builder->where("name", 'LIKE', "%{$name}%");
    }

    public function zipcode($zipcode)
    {
       if(!$zipcode) return;
        $region = \DB::table('zipcodes')->where('zipcode', $zipcode)->select('latitude', 'longitude')->first();

        $zipcods = \App\Models\Zipcode::selectRaw("zipcode, 
        ( 3959 * acos( cos( radians($region->latitude) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians($region->longitude) ) + sin( radians($region->latitude) ) * sin( radians( latitude ) ) ) ) AS distance ")
            ->having('distance', '<=', $this->miles);

    \DB::listen(function($q){
        // dump($q->sql, $q->bindings);
    });

        $this->builder->whereIn('zipcode', $zipcods->pluck('zipcode'));
    }

    public function status($status)
    {
        $this->builder->whereIn("status", explode(',', $status));
    }

    public function model($model_id)
    {
        if(!$model_id) return;
        $this->builder->where("model_id", $model_id);
    }

    public function soled($bool)
    {
        $this->builder->where("soled", $bool);
    }

    public function inactive($bool)
    {
        $this->builder->where("soled", $bool)->orWhere('status', Status::PENDING_PAYMENT);
    }

    public function make($make_id)
    {
        if(!$make_id) return;
        $this->builder->where("make_id", $make_id);
    }

    public function year($year)
    {
        if(!$year) return;

        $this->builder->whereHas("model", function($q) use($year){
            $q
//                ->where('model_id', $this->request->model)
                ->whereIn('year', [$year]);
        });
    }

    public function year_between($year)
    {
        list($from, $to) = explode(',', $year);
        if(!$year || !$to) return;

        $this->builder->whereBetween('year', [$from, $to]);
    }

    public function year_to($year)
    {
        if(!$year) return;
        $this->builder->where('year', '<=', $year);
    }

}
