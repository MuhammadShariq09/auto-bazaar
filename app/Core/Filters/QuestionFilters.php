<?php

namespace App\Core\Filters;


use App\User;

class QuestionFilters extends Filters {

    protected $filters = ['mode', 'topics', 'selections'];

    public function mode($mode)
    {
        if($mode == 'all')
            return;
        $this->builder->whereIn('type', $mode);
    }

    public function topics($topicIds)
    {
        if($topicIds == 'all')
            return;
        $this->builder->whereIn('topic_id', $topicIds);
    }

    public function selections($selection)
    {
        switch ($selection)
        {
            case 'answered':
                $this->filterAnswered();
                break;
            case 'unanswered':
                $this->filterUnanswered();
                break;
            default:
                return;
        }

    }

    private function filterUnanswered()
    {
        if(! $ids = $this->getAnsweredQuestionIds())
            return;

        $this->builder->whereNotIn('id', $ids);
    }

    private function filterAnswered()
    {
        if(! $ids = $this->getAnsweredQuestionIds())
            return;

        $this->builder->whereIn('id', $ids);
    }

    private function getAnsweredQuestionIds()
    {
        /** @var User $user */
        $user = $this->request->user();
        if(!$user)
            return false;

        $tests = $user->tests()->with(['questions'])->get();

        $questionIds = collect([]);

        $tests->each(function($test) use($questionIds){
            $questionIds->push($test->questions->implode('id', ', '));
        });

        return explode(', ', $questionIds->join(', '));
    }
}
