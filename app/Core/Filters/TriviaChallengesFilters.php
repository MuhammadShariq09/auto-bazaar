<?php

namespace App\Core\Filters;

use App\Models\ChallengeRequest;

class TriviaChallengesFilters extends Filters {

    protected $filters = ['title', 'paid'];

    public function title($title)
    {
        $this->builder->where('title', "LIKE", "%{$title}%");
    }

    public function paid($isPaid)
    {
        if($isPaid)
            $this->builder->where('type', 'PAID');
        else
            $this->builder->where('type', 'FREE');
    }
}
