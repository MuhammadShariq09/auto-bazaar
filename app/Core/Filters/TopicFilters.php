<?php

namespace App\Core\Filters;

class TopicFilters extends Filters {

    protected $filters = ['course'];

    public function course($course_id)
    {
        $this->builder->where('course_id', $course_id);
    }
}
