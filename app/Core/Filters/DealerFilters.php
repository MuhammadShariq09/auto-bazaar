<?php

namespace App\Core\Filters;

class DealerFilters extends Filters {

    protected $filters = ['name', 'status'];

    public function name($name)
    {
        $this->builder->where('id', '!=', auth()->id());
        $this->builder->where("name", 'LIKE', "%{$name}%");
    }

    public function status($status)
    {
        $this->builder->where("status", $status);
    }

}
