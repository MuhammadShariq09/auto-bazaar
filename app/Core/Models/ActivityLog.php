<?php

namespace App\Core\Models;

use Illuminate\Database\Eloquent\Model;

class ActivityLog extends Model
{
    protected $fillable = ['loggable_id', 'loggable_type', 'subject', 'url', 'method', 'ip', 'agent'];

    public function loggable()
    {
        return $this->morphTo('loggable');
    }
}
