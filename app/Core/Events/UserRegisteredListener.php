<?php


namespace App\Core\Events;

use App\Models\Administrator\Admin;
use App\Notifications\NewRegistration;
use App\Notifications\UserRegistered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;

class UserRegisteredListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        Notification::send(Admin::all(), new UserRegistered($event->user));
        $event->user->notify(new NewRegistration());
    }
}
