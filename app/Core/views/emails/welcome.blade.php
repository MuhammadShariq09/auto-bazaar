@extends('emails.core-master')

@section('title', "Welcome!")

@section('content')

        <tr style="border-collapse:collapse;">
            <td class="es-m-txt-l" bgcolor="#ffffff" align="left"
                style="Margin:0;padding-top:20px;padding-bottom:20px;padding-left:30px;padding-right:30px;">
                <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:18px;font-family:lato, 'helvetica neue', helvetica, arial, sans-serif;line-height:27px;color:#666666;">
                    We're excited to have you get started. Just press the button below in order to login to your account.
                </p>
            </td>
        </tr>
        <tr style="border-collapse:collapse;">
            <td align="center" style="Margin:0;padding-left:10px;padding-right:10px;padding-top:35px;padding-bottom:35px;">
                <span class="es-button-border" style="border-style:solid;border-color:{{ config('app.secondary_color') }};background: {{ config('app.secondary_color') }};border-width:1px;display:inline-block;border-radius:2px;width:auto;">
                    <a href="{{ config('app.frontend_url') }}" class="es-button" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:helvetica, 'helvetica neue', arial, verdana, sans-serif;font-size:20px;color:#FFFFFF;border-style:solid;border-color:{{ config('app.secondary_color') }};border-width:15px 30px;display:inline-block;background:{{ config('app.secondary_color') }};border-radius:2px;font-weight:normal;font-style:normal;line-height:24px;width:auto;text-align:center;"> Login</a>
                </span>
            </td>
        </tr>
        <tr style="border-collapse:collapse;">
            <td class="es-m-txt-l" align="left"
                style="padding:0;Margin:0;padding-top:20px;padding-left:30px;padding-right:30px;">
                <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:18px;font-family:lato, 'helvetica neue', helvetica, arial, sans-serif;line-height:27px;color:#666666;">
                    If that doesn't work, copy and paste the following
                    link in your browser:</p></td>
        </tr>
        <tr style="border-collapse:collapse;">
            <td class="es-m-txt-l" align="left" style="padding:0;Margin:0;padding-top:20px;padding-left:30px;padding-right:30px;">
                <a target="_blank" href="{{ config('app.frontend_url') }}" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:lato, 'helvetica neue', helvetica, arial, sans-serif;font-size:18px;text-decoration:underline;color:{{ config('app.secondary_color') }};">
                    {{ config('app.frontend_url') }}
                </a>
            </td>
        </tr>

@endsection
