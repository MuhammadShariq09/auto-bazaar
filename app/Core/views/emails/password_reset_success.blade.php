@extends('emails.core-master')

@section('title', "Password Reset")

@section('content')

    <tr style="border-collapse:collapse;">
        <td class="es-m-txt-l" bgcolor="#ffffff" align="left"
            style="Margin:0;padding-top:20px;padding-bottom:20px;padding-left:30px;padding-right:30px;">
            <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:18px;font-family:lato, 'helvetica neue', helvetica, arial, sans-serif;line-height:27px;color:#666666;">
                Your password is changed successfully please use new password to log in to you account.
            </p>
        </td>
    </tr>

    <tr style="border-collapse:collapse;">
        <td align="center" style="Margin:0;padding-left:10px;padding-right:10px;padding-top:35px;padding-bottom:35px;">
                <span class="es-button-border" style="border-style:solid;border-color:{{ config('app.secondary_color') }};background: {{ config('app.secondary_color') }};border-width:1px;display:inline-block;border-radius:2px;width:auto;">
                    <a href="{{ config('app.frontend_url') }}" class="es-button" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:helvetica, 'helvetica neue', arial, verdana, sans-serif;font-size:20px;color:#FFFFFF;border-style:solid;border-color:{{ config('app.secondary_color') }};border-width:15px 30px;display:inline-block;background:{{ config('app.secondary_color') }};border-radius:2px;font-weight:normal;font-style:normal;line-height:24px;width:auto;text-align:center;"> Login</a>
                </span>
        </td>
    </tr>

    <!-- Action -->
    <table class="body-action" align="center" width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="center">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td align="center">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <p></p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

@endsection
