@extends('emails.core-master')

@section('title', isset($admin)? "New Query Submitted": "Thank you")

@section('content')

    @if(isset($admin))


        <tr style="border-collapse:collapse;">
            <td class="es-m-txt-l" bgcolor="#ffffff" align="left" style="Margin:0;padding-top:20px;padding-bottom:20px;padding-left:30px;padding-right:30px;">

                <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:28px;font-family:lato, 'helvetica neue', helvetica, arial, sans-serif;line-height:27px;color:#666666;">
                    New Query form is submitted
                </p>

                <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:18px;font-family:lato, 'helvetica neue', helvetica, arial, sans-serif;line-height:27px;color:#666666;">
                    To view the details of user <a href="{{ route('admin.login') }}">please log</a> in to your admin panel.
                </p>
            </td>
        </tr>

    @else

        <tr style="border-collapse:collapse;">
            <td class="es-m-txt-l" bgcolor="#ffffff" align="left"
                style="Margin:0;padding-top:20px;padding-bottom:20px;padding-left:30px;padding-right:30px;">
                <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:28px;font-family:lato, 'helvetica neue', helvetica, arial, sans-serif;line-height:27px;color:#666666;">
                    Hi, {{ $user->name }}
                </p>

                <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:18px;font-family:lato, 'helvetica neue', helvetica, arial, sans-serif;line-height:27px;color:#666666;">
                    Thank you for submitting query to us, we will get back to you soon.
                </p>
            </td>
        </tr>
    @endif

@endsection
