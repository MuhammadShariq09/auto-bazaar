@extends('emails.core-master')

@section('title', $user->status? "Account Resumed": "Account Suspended!")

@section('content')


    <tr style="border-collapse:collapse;">
        <td class="es-m-txt-l" bgcolor="#ffffff" align="left"
            style="Margin:0;padding-top:20px;padding-bottom:20px;padding-left:30px;padding-right:30px;">
            <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:18px;font-family:lato, 'helvetica neue', helvetica, arial, sans-serif;line-height:27px;color:#666666;">
                @if($user->status)
                    <h5>Your Account has been reactivated</h5>
                @else
                    <h5>Your Account has been Suspended, please contact to admin.</h5>
                @endif
            </p>
        </td>
    </tr>

@endsection
