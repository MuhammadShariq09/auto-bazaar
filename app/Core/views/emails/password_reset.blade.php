@extends('emails.core-master')

@section('title', "Password Reset Request")

@section('content')


    <tr style="border-collapse:collapse;">
        <td class="es-m-txt-l" bgcolor="#ffffff" align="left"
            style="Margin:0;padding-top:20px;padding-bottom:20px;padding-left:30px;padding-right:30px;">
            <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:18px;font-family:lato, 'helvetica neue', helvetica, arial, sans-serif;line-height:27px;color:#666666;">
                You just requested to reset your password, please use the Reset Token mentioned below;
            </p>
        </td>
    </tr>

    <tr style="border-collapse:collapse;">
        <td align="center" style="Margin:0;padding-left:10px;padding-right:10px;padding-top:35px;padding-bottom:35px;">
            <span class="es-button-border" style="border-style:solid;border-color:{{ config('app.secondary_color') }};background: {{ config('app.secondary_color') }};border-width:1px;display:inline-block;border-radius:2px;width:auto;">
               <a style="background: darkslateblue; color: #ffffff; padding: 10px 20px;" href="{{ route('admin.password.reset', [$token]) }}">Link is to reset</a>
            </span>
        </td>
    </tr>

    <tr style="border-collapse:collapse;">
        <td class="es-m-txt-l" bgcolor="#ffffff" align="left"
            style="Margin:0;padding-top:20px;padding-bottom:20px;padding-left:30px;padding-right:30px;">
            <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:18px;font-family:lato, 'helvetica neue', helvetica, arial, sans-serif;line-height:27px;color:#666666;">
                If you do not make this request please ignore this email.
            </p>
        </td>
    </tr>

@endsection
