<?php


namespace App\Core\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ActiveUser
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @param string $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = "web")
    {
        if(auth($guard)->user()->status)
            return $next($request);

        return response(['message' => "Your account has been suspended.", "errors" => ["message" => ["Your account has been suspended. please contact to admin."]]], Response::HTTP_FORBIDDEN);
    }
}
