<?php

namespace App\Notifications;

use App\Models\Ad;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class AdPosted extends Notification
{
    use Queueable;
    /**
     * @var Ad
     */
    public $ad;

    /**
     * Create a new notification instance.
     *
     * @param Ad $ad
     */
    public function __construct(Ad $ad)
    {
        $this->ad = $ad;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->greeting('Sorry')
            ->line('Your Ad has been rejected please click below link to view details.')
            ->action('View Ad', url('/car-details/'.$this->ad->id))
            ->line('Thank you for using ' . config('app.name'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'title' => 'Ad Posted',
            'message' => 'New Ad has been posted by ' . $notifiable->name,
            'ad_id' => $this->ad->id,
            'link' => '/ads/' . $this->ad->id,
        ];
    }
}
