<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Contact extends Mailable
{
    use Queueable, SerializesModels;

    public $payload;

    public function __construct($payload,$type)
    {
        $this->payload = $payload;
        $this->type = $type;
        $this->subject = $type;
    }

   
    public function build()
    {
        return $this->view('email.contact_us')
                ->subject("AutoBazaar - Contact Us")
                ->with(['payload' => $this->payload, 'type'=>$this->type]);
    }
}
