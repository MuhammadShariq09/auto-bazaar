<?php

namespace App\Http\Controllers;

use App\Models\Make;
use Illuminate\Http\Request;

class MakesController extends Controller
{
    public function index()
    {
        return Make::all();
    }
}
