<?php

namespace App\Http\Controllers;

use App\Models\AdModel;
use Illuminate\Http\Request;

class ModalsController extends Controller
{

    public function index()
    {
        $q = AdModel::query();

        if(request('make_id'))
            $q->where('make_id', request('make_id'));

        return $q->get();
    }
}
