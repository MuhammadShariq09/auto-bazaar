<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class WishListsController extends Controller
{
    function construct()
    {
        return $this->middleware('api');
    }

    public function index()
    {
        return auth()->user()->wish_lists;
    }

    public function add(Request $request)
    {
        $request->validate(['ad_id' => 'required|exists:ads,id']);

        /** @var User $user */
        $user = auth('api')->user();

        $user->add_to_wish_list($request->ad_id);

        return response(['message' => "Added from "]);
    }

    public function remove(Request $request)
    {
        $request->validate(['ad_id' => 'required|exists:ads,id']);

        /** @var User $user */
        $user = auth('api')->user();

        $user->remove_from_wish_list($request->ad_id);

        return response(['message' => "Removed from wishlist"]);
    }
}
