<?php

namespace App\Http\Controllers;

use App\Core\Filters\AdFilters;
use App\Core\Models\WishList;
use App\Core\Status;
use App\Models\Ad;
use App\Models\Admin;
use App\Models\AdModel;
use App\Models\Make;
use App\Models\Package;
use App\Models\User;
use App\Notifications\AdPosted;
use App\Notifications\NewPayment;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;

class AdsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param AdFilters $filters
     * @return Response
     */
    public function index(AdFilters $filters)
    {
        $featuredCars = \App\Http\Resources\Ad::collection(Ad::with('make', 'medias', 'model')
            ->filter($filters)->where('featured', '1')
            ->whereStatus(Status::ACTIVE)
            ->limit(25)->get());
        /** @var Paginator $cars */
        $cars = (Ad::with('make', 'medias', 'model')->filter($filters)
            ->whereStatus(Status::ACTIVE)
            ->where('featured', '0')->paginate(15));
        $adsCount = Ad::filter($filters)->where('featured', '0')
            ->whereStatus(Status::ACTIVE)
            ->count();

        return response([
            'featuredCars' => $featuredCars,
            'cars' => $cars->toJson(),
            'adsCount' => $adsCount,
            'wishlist_ids' => WishList::query()->where('user_id', auth('api')->id())->pluck('product_id'),
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param AdFilters $filters
     * @return Response
     */
    public function myListing(AdFilters $filters)
    {
        $cars = (auth()->user()->ads()->filter($filters)->with('make', 'medias', 'model')->paginate(10));
        $adsCount = auth()->user()->ads()->count();
        $activeAdsCount = auth()->user()->ads()->where('status', '2')->count();
        $inactiveAdsCount = auth()->user()->ads()
            ->where(function($q){
                $q->orWhere('soled', '1')->orWhereIn('status', [Status::PENDING_PAYMENT, Status::REJECTED]);
            })->count();
        $pendingAdsCount = auth()->user()->ads()->where('status', '0')->count();

        return response([
            'cars' => $cars->toJson(),
            'activeAdsCount' => $activeAdsCount,
            'inactiveAdsCount' => $inactiveAdsCount,
            'adsCount' => $adsCount,
            'pendingAdsCount' => $pendingAdsCount,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        DB::transaction(function() use($request){
            /** @var User $user */
            $user = $request->user();

            $make = Make::find($request->make_id);
            $model = AdModel::find($request->model_id);
            $package = Package::find($request->package_id);

            $request->merge(['name' => $request->year . ' ' . $make->name .  ' ' . $model->name]);
            $ad = new Ad();
            $ad->fill($request->only($ad->getFillable()));
            $ad->package()->associate($package);
            $user->ads()->save($ad);

            foreach ($request->file('files') as $file)
                $ad->saveMedia(str_replace('public/', '', $file->store('public/images/ads')));

            Notification::send(Admin::all(), new AdPosted($ad));
        });

        return response(['message' => 'Ad created successfully'], Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  Ad $ad
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function show(Ad $ad)
    {
        $ad->load('make','model', 'medias', 'adable', 'package');
        $ad = new \App\Http\Resources\Ad($ad);
        return response([
            'car' => $ad->resource,
            'wishlist_ids' => WishList::query()->where('user_id', auth('api')->id())->pluck('product_id')
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Ad $ad
     * @return \Illuminate\Http\JsonResponse
     */
    public function sold(Request $request, Ad $ad)
    {
        $ad->soled = 1;
        $ad->status = Status::SOLD;
        $ad->save();
        return \response()->json(['message' => 'Your ad has been marked as soled!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Ad $ad
     * @return Response
     * @throws \Exception
     */
    public function destroy(Ad $ad)
    {
        try {
            $ad->delete();
        }catch (\Exception $exception) {
            return \response(['message' => $exception->getMessage()]);
        }
        return \response(['message' => 'Ad has been deleted successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return Collection
     */
    public function wishlist()
    {
        /** @var User $user */
        $user = auth()->user();

        return $user->wish_lists()->with('make', 'model', 'medias')->get();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function pay(Request $request, Ad $ad)
    {
        /*if($ad->status === Status::ACTIVE)
            return response([], Response::HTTP_UNPROCESSABLE_ENTITY);*/

        $request->validate([
            "number" => 'required|digits:16',
            "exp_month" => 'required|date_format:m',
            "exp_year" => 'required|date_format:Y',
            "cvc" => 'required',
            "medias" => "array",
            "deletedMedia" => "array"
        ]);

        DB::transaction(function() use($ad, $request){
            /** @var User $user */
            $user = auth()->user();

            $package = Package::find(request('package_id', $ad->package_id));

            $request->validate(["medias" => "array|max:" . $package->no_images]);

            $user->createOrGetStripeCustomer();

            $user->addPaymentMethod($request->only('number', 'exp_month', 'exp_year', 'cvc'));
            $charge = $user->charge($package->price * 100, $user->cards()->where('default', true)->first()->stripe_pm_id);
            $ad->savePayLog($package->price, "Featured", $charge->id);

            $ad->status = Status::ACTIVE;
            $ad->featured = 1;
            $ad->package_id = $package->id;
            $ad->featured_end_at = now()->addDays($package->months)->toDateString();

            foreach($request->medias ?? [] as $file) {
                try {
                    $filename = upload_base_64_image($file['file_name'], 'ads');
                    $ad->saveMedia($filename);
                }catch (\Exception $exception){}
            }

             Notification::send(Admin::all(), new NewPayment($ad));

            $ad->save();
        });

        return response(['message' => 'Ad has been activated successfully']);
    }
}
