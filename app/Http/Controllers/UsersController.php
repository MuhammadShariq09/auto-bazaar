<?php

namespace App\Http\Controllers;

use App\Core\Helpers\Soachat;
use App\Models\Package;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UsersController extends Controller
{
    public function user(Request $request)
    {
        return $request->user();
    }

    public function profile(Request $request)
    {
        $this->setPassword($request);
       
        /** @var User $user */
        $user = $request->user();
        
       // Soachat::addUser($user->id, $user->name, $user->image);

       User::find($user->id)->update($request->only($user->getFillable()));
      
        return response(['message' => 'Profile updated successfully']);
    }

    public function package(Request $request)
    {
        /** @var User $user */
        $user = $request->user();
        return ($user->adPackage);
    }

    public function packages(Request $request)
    {
        $query = Package::query();
        if($request->type)
            $query->where('type', $request->type);

        return $query->get();
    }

    public function subscribe(Request $request)
    {
       // dd($request->all());
       $user = auth()->user();
        $user->addPaymentMethod($request->only('number', 'exp_month', 'exp_year', 'cvc'));
        $charge = $user->charge($request->price * 100, $user->cards()->where('default', true)->first()->stripe_pm_id);

        
        $user->savePayLog($request->price, "Featured Profile", $charge->id);

        $request->user()->update(['package_id' => $request->package_id]);
        return response(['message' => 'Thank you for featuring your profile']);
    }

    public function setPassword(Request $request)
    {
        if(!$request->password)
        {
            $request->request->remove('password');
            return;
        }
        $request->merge(['password' => '']);
    }
}
