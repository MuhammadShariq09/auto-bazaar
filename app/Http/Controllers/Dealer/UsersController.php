<?php

namespace App\Http\Controllers\Dealer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function user(Request $request)
    {
        return auth('api_dealer')->user();
    }
}
