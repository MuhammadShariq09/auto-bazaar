<?php

namespace App\Http\Controllers\Dealer;

use App\Core\Controllers\ApiBaseController;
use Illuminate\Http\Request;

class DealersController extends ApiBaseController
{
    public function index()
    {
        return view('dealers.index');
    }
}
