<?php

namespace App\Http\Controllers\Admin;

use App\Core\Controllers\ApiBaseController;
use App\Core\Filters\AdFilters;
use App\Core\Status;
use App\Http\Controllers\Controller;
use App\Models\Ad;
use App\Models\Admin;
use App\Notifications\AdApproved;
use App\Notifications\AdRejected;
use Illuminate\Http\Request;

class AdsController extends ApiBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(AdFilters $filters)
    {
        $ads = Ad::with('medias', 'make', 'model', 'adable')->filter($filters)->get();
        return $this->response(\App\Http\Resources\Ad::collection($ads));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  Ad  $ad
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Ad $ad)
    {
        $ad->load('medias');
        return $this->response(new \App\Http\Resources\Ad($ad));
    }

    /**
     * Display the specified resource.
     *
     * @param  Ad  $ad
     * @return \Illuminate\Http\JsonResponse
     */
    public function statusPatch(Request $request, Ad $ad)
    {
        //dd($request->all());
        $message = \DB::transaction(function() use($request, $ad){

            $ad->status = $request->status;
            $ad->rejection_reason = $request->reason;

            $ad->save();

            $isApproved = in_array($request->status, [Status::APPROVED, Status::PENDING_PAYMENT, Status::ACTIVE]);

            $ad->adable->notify($isApproved? new AdApproved($ad): new AdRejected($ad));

            $message = "Ad #$ad->id has been " . ($isApproved? 'approved': 'rejected');

            /** @var Admin $admin*/
            $admin = auth()->user();
            $admin->saveLog($message);

            return $message;
        });

        return $this->response(['message' => $message]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ad $ad)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ad $ad)
    {
        $message = "You have deleted ad #$ad->id";
        $ad->delete();

        /** @var Admin $admin*/
        $admin = auth()->user();
        $admin->saveLog($message);
        return $this->response(['message' => $message]);
    }
}
