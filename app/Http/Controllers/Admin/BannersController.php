<?php

namespace App\Http\Controllers\Admin;
use App\Core\Controllers\ApiBaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Core\Filters\AdFilters;
use App\Models\Banner;

class BannersController extends Controller
{
    public function index(AdFilters $filters)
    {
        $banners = Banner::orderBy('id', 'DESC')->get();
        return response()->json($banners);
    }

    public function store(Request $request){

        $request->validate([
            'title' => ['required', 'string', 'max:255'],
            'image' => ['required', 'string']
        ]);

      //  dd($request->all());
        $banner = new Banner();
        $data = $request->only($banner->getFillable());
      //  $data['image'] = $this->uploadImage($request);
        $banner->fill($data);
        $banner->save();

        /** @var Admin $admin*/
        $admin = auth()->user();

        $admin->saveLog(sprintf("You have added a new banner %s", $request->title));

        return response()->json(['message' => sprintf("New Banner has been added successfully")]);

    }

    public function show($id)
    {
        $banner = Banner::find($id);
        return response()->json($banner);
    }
    
        public function update(Request $request, Banner $banner)
        {
            $request->validate([
                'title' => "required"
            ]);
    
    
            $banner->fill($request->only($banner->getFillable()));
         //     dd($banner);
            $banner->save();
    
            /** @var Admin $admin*/
            $admin = auth()->user();
    
            $admin->saveLog(sprintf("You have updated banner title as %s", $banner->title));
    
            return response()->json(['message' => sprintf("You have updated banner title as %s", $banner->title)]);
        }
    
        public function destroy(Banner $banner)
        {
            $message = "You have deleted banner #$banner->id";
            $banner->delete();
    
            /** @var Admin $admin*/
            $admin = auth()->user();
            $admin->saveLog($message);
            return response()->json(['message' => $message]);
        }
     
}
