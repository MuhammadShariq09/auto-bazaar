<?php

namespace App\Http\Controllers\Admin;
use App\Core\Controllers\ApiBaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Page;

class PagesController extends Controller
{
    public function index()
    {
        $pages = Page::orderBy('id', 'DESC')->get();
        return response()->json($pages);
    }

    public function show($id)
    {
        $page = Page::find($id);
        return response()->json($page);
    }
    
        public function update(Request $request)
        {
            $request->validate([
                'title' => "required",
                'description' => "required"
            ]);
    
            $page = Page::find($request->id);
            $page->fill($request->only($page->getFillable()));
         //     dd($page);
            $page->save();
    
            /** @var Admin $admin*/
            $admin = auth()->user();
    
            $admin->saveLog(sprintf("You have updated content"));
    
            return response()->json(['message' => sprintf("You have updated content.")]);
        }
    
      
     
}
