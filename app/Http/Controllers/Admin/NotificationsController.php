<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Ad;
use App\Models\Admin;
use App\Notifications\AdPosted;
use Illuminate\Http\Request;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Support\Facades\Notification;

class NotificationsController extends Controller
{
    public function index()
    {
        return auth()->user()->unreadNotifications()->get();
    }

    public function markRead($id=null)
    {
        if($id)
            DatabaseNotification::find($id)->markAsRead();
        else
            auth()->user()->notifications->markAsRead();

        return ['message' => 'Notifications marked as read'];
    }
}
