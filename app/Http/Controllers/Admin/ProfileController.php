<?php

namespace App\Http\Controllers\Admin;

use App\Core\Controllers\ApiBaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ProfileController extends ApiBaseController
{
    public function changePassword(Request $request){

        $user = auth()->user();

        if(!Hash::check($request->current_password, $user->password))
        {
            return $this->responseWithError("Invalidate password", ['password' => "Invalid Password"]);
        }

        $user->password = bcrypt($request->password);

        $user->save();

        return $this->response(['message' => 'Password has been changed successfully!']);
    }
}
