<?php

namespace App\Http\Controllers\Admin;

use App\Core\Controllers\ApiBaseController;
use App\Models\Package;
use Illuminate\Http\Request;

class PackagesController extends ApiBaseController
{
    public function index()
    {
        return Package::all();
    }

    public function update(Request $request, Package $package)
    {
        $request->validate([
            'months' => "required",
            'price' => 'required',
        ]);
        $package->fill($request->only($package->getFillable()));
        $package->save();
        return $this->response(['message' => 'Packages has been updated successfully!']);
    }
}
