<?php

namespace App\Http\Controllers\Admin;

use App\Core\Controllers\ApiBaseController;
use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Feedback;
use Illuminate\Http\Request;

class FeedbackController extends ApiBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $feedback = Feedback::with('owner')->get();
        return $this->response(\App\Http\Resources\Feedback::collection($feedback));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  Feedback $feedback
     * @return \Illuminate\Http\Response
     */
    public function show(Feedback $feedback)
    {
        $feedback->load('owner');
        return $this->response(new \App\Http\Resources\Feedback($feedback));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Feedback $feedback
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Feedback $feedback)
    {
        $message = "You have deleted feedback #$feedback->id";
        $feedback->delete();

        /** @var Admin $admin*/
        $admin = auth()->user();
        $admin->saveLog($message);
        return $this->response(['message' => $message]);
    }
}
