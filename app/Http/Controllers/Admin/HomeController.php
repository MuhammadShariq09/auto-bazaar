<?php

namespace App\Http\Controllers\Admin;

use App\Core\Status;
use App\Http\Resources\User;
use App\Models\Ad;
use App\Models\Dealer;
use App\Models\Transaction;
use App\Models\Vehicle;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    function __construct()
    {
        $this->middleware('auth:admin')->only('index');
        $this->middleware('auth:api_admin')->only('dashboard');
    }

    public function index()
    {
        /** @var Dealer $dealer */
        /*$dealer = Dealer::first();

        $ad = new Ad([
            'name' => 'testing',
            'model_id' => 1,
            'make_id' => 1,
        ]);

        $ad = $dealer->ads()->save($ad);

        $dealer->loadCount('ads');
        return $ad;*/

        return view('admin.home');
    }

    public function dashboard()
    {
        return response([
            'recent_users' => User::collection(\App\Models\User::query()->latest()->limit(15)->get()),
            'active_ads' => Ad::query()->where('status', Status::ACTIVE)->where('soled', 0)->count(),
            'active_featured_ads' => Ad::query()->where('status', Status::ACTIVE)->where('package_id', '>=',  0)->count(),
            'users_count' => \App\Models\User::where('is_dealer', 0)->count(),
            'dealers_count' => \App\Models\User::where('is_dealer', 1)->count(),
            'earning' => '$' . thousandsCurrencyFormat(Transaction::sum('amount')),
            'states' => (new TransactionsController())->getStateData(),
        ]);
    }
}
