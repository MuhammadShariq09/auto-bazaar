<?php

namespace App\Http\Controllers\Admin;

use App\Core\Controllers\ApiBaseController;
use App\Core\Filters\UserFilters;
use App\Core\Status;
use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UsersController extends ApiBaseController
{
    public function index(Request $request, UserFilters $filters){
        $user = User::with('profilePackage:id,title')->filter($filters);
        if($request->dealer){
            $user->withCount('ads');
            /*$user->withCount(['ads as featured_ads_count' => function($q){
                $q->where('package_id', '>', 0);
            }]);*/
        }
        return $user->get();
        return $this->response(\App\Http\Resources\User::collection($users));
    }

    public function show(Request $request, User $user)
    {
        $user->loadCount(['ads' => function($q) { $q->whereStatus(Status::ACTIVE); }]);
        return $this->response(new \App\Http\Resources\User($user));
    }

    public function update(Request $request, User $user)
    {
        $request->validate([
            'first_name' => "required",
            'contact' => 'required',
        ]);

        if($request->password)
            $request->merge(['password' => bcrypt($request->password)]);
        
        $user->fill($request->only($user->getFillable()));

        $user->save();

        /** @var Admin $admin*/
        $admin = auth()->user();

        $admin->saveLog(sprintf("You have updated profile of %s", $user->first_name));

        return $this->response(['message' => sprintf("You have updated profile of %s", $user->first_name)]);
    }

    public function store(Request $request){

        $request->validate([
            'first_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);


        $user = new User();
        $data = $request->only($user->getFillable());
        $data['password'] = bcrypt($data['password']);
        //$data['image'] = $this->uploadImage($request);
        $user->fill($data);
        $user->save();

        /** @var Admin $admin*/
        $admin = auth()->user();

        $admin->saveLog(sprintf("You have added a new user %s", $request->name));

        return $this->response(['message' => sprintf("New User has been added successfully")]);

    }

    public function destroy(User $user)
    {
        $user->status = !$user->status;
         $user->save();

        /** @var Admin $admin*/
        $admin = auth()->user();
        $admin->saveLog(sprintf("You have %s %s", $user->status? "blocked": "reactivated",$user->name));
        return $this->response(['message' => "User has been " . (!$user->status? "blocked.": "reactivated.")]);
    }
}
