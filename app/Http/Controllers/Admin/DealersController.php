<?php

namespace App\Http\Controllers\Admin;

use App\Core\Controllers\ApiBaseController;
use App\Core\Filters\DealerFilters;
use App\Core\Filters\UserFilters;
use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Dealer;
use App\Models\User;
use App\Models\Vehicle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class DealersController extends ApiBaseController
{
    public function index(Request $request, DealerFilters $filters)
    {
        $dealers = Dealer::query()->withCount('ads', 'package')->filter($filters)->get();
        return $this->response(\App\Http\Resources\Dealer::collection($dealers));
    }

    public function show($id)
    {
        $dealer = User::find($id);
        $dealer->load(['ads' => function($ad){
            $ad->with('make', 'model');
        }, 'ratings.author:id,image,first_name,last_name,email']);
        return $this->response(new \App\Http\Resources\Dealer($dealer));
    }

    public function destroy(User $dealer)
    {
        $dealer->status = !$dealer->status;
        $dealer->save();

        /** @var Admin $admin*/
        $admin = auth()->user();
        $admin->saveLog(sprintf("You have %s %s", $dealer->status? "blocked": "reactivated",$dealer->name));
        return $this->response(['message' => "Dealer has been " . (!$dealer->status? "blocked.": "reactivated.")]);
    }
}
