<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\Transaction as TransactionResource;
use App\Models\Transaction;
use Illuminate\Http\Request;

class TransactionsController extends Controller
{
    public function index()
    {

        return [
            'transactions' => TransactionResource::collection(Transaction::with('source')->get()),
            'states' => $this->getStateData()
        ];
    }


    public function getStateData($year = false)
    {
        if(!$year) $year = date('Y');

        $transactions = \DB::select('select year(created_at) as year, month(created_at) as month, sum(amount) as amount from transactions where  year(created_at) = ' . $year . ' group by year(created_at), month(created_at)');

        $transactions = collect($transactions);

        $data = collect([]);

        $months = ['', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

        for ($i = 1; $i <= 12; $i++){
            $std = new \stdClass();
            $std->name = $months[$i];
            $std->value = $transactions->where('month', $i)->sum('amount');
            $data->push($std);
        }

        return $data;
    }
}
