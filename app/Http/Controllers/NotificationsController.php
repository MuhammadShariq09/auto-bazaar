<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Notifications\DatabaseNotification;

class NotificationsController extends Controller
{
    public function __construct()
    {
        $this->middleware('api');
    }

    public function index()
    {
        return auth()->user()->unreadNotifications()->get();
    }

    public function markRead($id=null)
    {
        if($id)
            DatabaseNotification::find($id)->markAsRead();
        else
            auth()->user()->notifications->markAsRead();

        return ['message' => 'Notifications marked as read'];
    }
}
