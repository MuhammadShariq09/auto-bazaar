<?php

namespace App\Http\Controllers;

use App\Models\Feedback;
use App\Models\User;
use Illuminate\Http\Request;
use App\Mail\Contact;

class ContactsController extends Controller
{
    public function admin(Request $request)
    {
        /** @var User $user */
        $user = $request->user();
        $user->feedback()->save(new Feedback($request->all()));
        return response(['message' => 'Contact form has been sent. we will reach you soon.']);
    }

    public function contact(Request $request)
    {
        \Mail::to('autobazaarusa@yahoo.com')->send(new Contact($request->all(), 'admin'));
        \Mail::to($request->email)->send(new Contact($request->all(), 'user'));
        return response(['message' => 'Contact form has been sent. we will reach you soon.']);
    }
}
