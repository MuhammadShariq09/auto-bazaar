<?php

namespace App\Http\Controllers;

use App\Core\Filters\AdFilters;
use App\Core\Models\WishList;
use App\Core\Status;
use App\Http\Resources\Ad;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use App\Models\Banner;
use App\Models\Page;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function featuredCars()
    {
        return response([
            'newCars' => \App\Models\Ad::with('medias', 'model', 'make')
                ->whereStatus(Status::ACTIVE)
                ->whereCondition('new')->latest()->limit('3')
                ->get(),
            'usedCars' => \App\Models\Ad::with('medias', 'model', 'make')
                ->whereStatus(Status::ACTIVE)
                ->whereCondition('used')->latest()->limit('3')
                ->get(),
        ]);
    }

    public function carsStats()
    {
        return response([
            'newCars' => \App\Models\Ad::whereStatus(Status::ACTIVE)->whereCondition('new')->count(),
            'usedCars' => \App\Models\Ad::whereStatus(Status::ACTIVE)->whereCondition('used')->count(),
            'clients' => \App\Models\User::whereStatus(1)->count(),
        ]);
    }

    public function banners()
    {
        $banners = Banner::orderBy('id', 'DESC')->get();
        return response()->json(\App\Http\Resources\Banner::collection($banners));
    }

    public function zipcodes(Request $request)
    {
        $zipcodes = \DB::table('zipcodes')->where('zipcode', 'LIKE', "$request->q%")->limit(100)->get();
        return response()->json($zipcodes);
    }

    public function page($id)
    {
        $page = Page::find($id);
        return response()->json($page);
    }

}
