<?php

namespace App\Http\Controllers;

use App\Core\Filters\DealerFilters;
use App\Core\Filters\UserFilters;
use App\Core\Helpers\Soachat;
use App\Core\Status;
use App\Http\Resources\DealersResource;
use App\Models\Media;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;

class DealersController extends Controller
{
    public function index(UserFilters $filters)
    {
        $featured = User::query()->with('medias')->whereNotNull('package_id')->withCount(['ratings as ratings_avg' => function($query) {
            $query->select(DB::raw('avg(reviews.rating)'));
        }])->inRandomOrder()->limit(4)->filter($filters)->get();

        /** @var Paginator $dealers */
        $dealers = User::query()->with('medias')->where('is_dealer', 1)->whereNull('package_id')->withCount(['ads', 'ratings', 'ratings as ratings_avg' => function($query) {
            $query->select(DB::raw('avg(reviews.rating)'));
        }])->filter($filters)->paginate(15);

        return array(
            'featuredDealers' => $featured,
            'dealers' => json_decode($dealers->toJson()),
        );
    }

    public function show(User $dealer)
    {
        $dealer->loadCount(['ratings', 'ratings as ratings_avg' => function($query) {
            $query->select(DB::raw('avg(reviews.rating)'));
        }]);

        $dealer->ads = $dealer->ads()->with('medias', 'make', 'model')->paginate(25);

        $dealer->featuredAds = $dealer->ads()->with('medias', 'make', 'model')->whereNotNull('package_id')->where('status', Status::ACTIVE)->limit(3)->get();

        $dealer->ratings = $dealer->ratings()->with(['author' => function($q){
            $q->select('id', 'name', 'image', 'first_name', 'last_name');
        }])->paginate(50);

        /** @var User $user */
        $user = auth('api')->user();

        $dealer->current_review = $user->ratings()->where('reviewrateable_type', 'dealers')
            ->where('reviewrateable_id', $dealer->id)
            ->first();
        $dealer->current_review = null; $dealer->load('medias');

        return $dealer;
    }

    public function update(Request $request, User $dealer)
    {
        DB::transaction(function() use($request, $dealer) {
            if($request->file('files')) {
                foreach ($request->file('files') as $file)
                    $dealer->saveMedia(str_replace('public/', '', $file->store('public/images/dealers')));
            }

            $dealer->fill($request->only($dealer->getFillable()));

            $dealer->save();

            if($request->deletedMedia)
                Media::whereIn('id', explode(',', $request->deletedMedia))->delete();
        });

        Soachat::addUser($dealer->id, $dealer->name, $dealer->image);

        return response()->json(['message' => 'profile updated successfully']);
    }

    public function review(Request $request, User $dealer)
    {
        /** @var User $user */
        $user = auth()->user();

        $rating = $dealer->rating($this->prepareData($request), $user);

        return response([
            'id' => $rating->id,
            'body' => $rating->body,
            'rating' => $rating->rating,
            'created_at' => $rating->created_at,
            'author' => $user,
        ], Response::HTTP_CREATED);
    }

    private function prepareData(Request $request)
    {
        return [
            'title' => '',
            'body' => $request->review,
            'customer_service_rating' => $request->rating,
            'quality_rating' => $request->rating,
            'friendly_rating' => $request->rating,
            'pricing_rating' => $request->rating,
            'rating' => $request->rating,
            'recommend' => 'Yes',
            'approved' => true, // This is optional and defaults to false
        ];
    }
}
