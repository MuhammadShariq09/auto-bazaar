<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'contact' => $this->contact,
            'ads_count' => $this->ads_count ?? 0,
            'created_at' => $this->created_at,
            'image' => $this->image,
            "package" => array_key_exists('profilePackage', $this->getRelations())? $this->profilePackage: null,
            "featured_ads_count" => $this->featured_ads_count,
            'created_date' => $this->created_at->format(config('app.date_format')),
            'time_ago' => convert_time_ago($this->created_at),
        ];
    }
}
