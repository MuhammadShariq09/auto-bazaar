<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class DealersResource extends ResourceCollection
{
    public function toArray($request)
    {
        return [
            'data' => Dealer::collection($this->collection),
        ];
    }
}
