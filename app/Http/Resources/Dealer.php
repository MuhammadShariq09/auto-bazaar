<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Dealer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "reviews" => $this->reviews,
            "about" => $this->about,
            "name" => $this->name,
            "email" => $this->email,
            "contact" => $this->contact,
            "image" => $this->image,
            "ads_count" => $this->ads_count,
            "ads" => array_key_exists('ads', $this->getRelations())? $this->ads: [],
            "ratings" => array_key_exists('ratings', $this->getRelations())? $this->ratings: [],
            "package" => array_key_exists('package', $this->getRelations())? $this->package: null,
            "featured" => "no",
            "address" => $this->address,
            "state" => $this->state,
            "zipcode" => $this->zipcode,
            "city" => $this->city,
            "country" => $this->country,
            "created_at" => $this->created_at,
            //"created_date" => $this->created_at->format(config('app.date_format')) ?? "",
            "created_date" =>  "",
        ];
    }

    public function additional(array $data)
    {
        dd($data);
    }
}
