<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Ad extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'adable' => $this->adable,
            'adable_id' => $this->adable_id,
            'adable_type' => $this->adable_type,
            'model' => $this->model,
            'make' => $this->make,
            'description' => $this->description,
            'price' => $this->price,
            'featured' => $this->featured,
            'soled' => $this->soled,
            'mileage' => $this->mileage,
            'fuel_type' => $this->fuel_type,
            'condition' => $this->condition,
            'transmission' => $this->transmission,
            'rejection_reason' => $this->rejection_reason,
            'status' => $this->status,
            'medias' => $this->medias,
            'published' => $this->published,
            'package_id' => $this->package_id,
            'created_date' => $this->created_at->format(config('app.date_format')),
            'created_at' => $this->created_at->format(config('app.datetime_format')),
        ];
    }
}
