<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Transaction extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
//        dd($this->transitionable);
        return [
            "id" => $this->id,
            "user" => $this->transactor,
            "payer" => $this->payer->name,
            "user_type" => str_replace('App\\Models\\', '', get_class($this->payer)),
            "amount" => $this->amount,
            "payment_type" => $this->description,
            "created_date" => $this->created_at->format(config('app.date_format')),
        ];
    }
}
