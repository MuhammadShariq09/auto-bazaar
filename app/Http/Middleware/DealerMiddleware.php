<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

class DealerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!auth('api')->user()->isDealer())
            return response()->json(['message' => 'Unauthenticated!'], Response::HTTP_UNAUTHORIZED);

        return $next($request);
    }
}
