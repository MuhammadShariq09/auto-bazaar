(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["ads-edit"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/admin/js/components/ads/Edit.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/admin/js/components/ads/Edit.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _dealers_MainSlider__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../dealers/MainSlider */ "./resources/admin/js/components/dealers/MainSlider.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    MainSlider: _dealers_MainSlider__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  data: function data() {
    return {
      base_url: window.base_url,
      blocked: false,
      ad: undefined
    };
  },
  mounted: function mounted() {
    this.loadAd();
  },
  computed: {
    status: function status() {
      switch (this.ad.status) {
        case 1:
          return "Active";
          break;

        case 2:
          return "Rejected";
          break;

        default:
          return "Pending";
      }
    }
  },
  methods: {
    loadAd: function loadAd() {
      var _this = this;

      axios.get("/ads/".concat(this.$route.params.id)).then(function (data) {
        _this.ad = data.data;
      });
    },
    approve: function approve() {
      var _this2 = this;

      var _approve = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;

      this.$dialog.confirm("Are you sure you want to ".concat(_approve === 1 ? 'approve' : 'reject', " this Ad?")).then(function (dialog) {
        axios.patch("/ads/".concat(_this2.ad.id), {
          status: _approve
        }).then(function (d) {
          _this2.$toastr.success(d.data.message, 'Success', {}); // dialog.close();

        })["catch"](function (d) {});
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/admin/js/components/dealers/MainSlider.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/admin/js/components/dealers/MainSlider.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_awesome_swiper__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-awesome-swiper */ "./node_modules/vue-awesome-swiper/dist/vue-awesome-swiper.js");
/* harmony import */ var vue_awesome_swiper__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue_awesome_swiper__WEBPACK_IMPORTED_MODULE_0__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['images'],
  components: {
    swiper: vue_awesome_swiper__WEBPACK_IMPORTED_MODULE_0__["swiper"],
    swiperSlide: vue_awesome_swiper__WEBPACK_IMPORTED_MODULE_0__["swiperSlide"]
  },
  data: function data() {
    return {
      base_url: window.base_url,
      swiperOptionTop: {
        spaceBetween: 10,
        loop: true,
        loopedSlides: 5,
        //looped slides should be the same
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev'
        }
      },
      swiperOptionThumbs: {
        spaceBetween: 10,
        slidesPerView: 4,
        touchRatio: 0.2,
        loop: true,
        loopedSlides: 5,
        //looped slides should be the same
        slideToClickedSlide: true
      }
    };
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/admin/js/components/ads/Edit.vue?vue&type=template&id=228a6898&":
/*!*****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/admin/js/components/ads/Edit.vue?vue&type=template&id=228a6898& ***!
  \*****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "app-content content view pending-ad" }, [
    _c("div", { staticClass: "content-wrapper" }, [
      _c("div", { staticClass: "content-body" }, [
        _vm.ad
          ? _c("section", { attrs: { id: "configuration" } }, [
              _c("div", { staticClass: "row" }, [
                _c("div", { staticClass: "col-12" }, [
                  _c("div", { staticClass: "card rounded pad-20" }, [
                    _c("div", { staticClass: "card-content collapse show" }, [
                      _c(
                        "div",
                        {
                          staticClass:
                            "card-body table-responsive card-dashboard"
                        },
                        [
                          _vm._m(0),
                          _vm._v(" "),
                          _c("div", { staticClass: "row" }, [
                            _c(
                              "div",
                              { staticClass: "col-xl-8 col-12" },
                              [_c("main-slider")],
                              1
                            ),
                            _vm._v(" "),
                            _c("div", { staticClass: "col-xl-4 col-12 p-0" }, [
                              _c("div", { staticClass: "row" }, [
                                _c(
                                  "div",
                                  {
                                    staticClass:
                                      "col-xl-12 col-lg-6 col-12 d-flex "
                                  },
                                  [
                                    _c(
                                      "div",
                                      { staticClass: "box box-1 w-100" },
                                      [
                                        _c(
                                          "div",
                                          { staticClass: "btn-group " },
                                          [
                                            _vm._m(1),
                                            _vm._v(" "),
                                            _c(
                                              "div",
                                              {
                                                staticClass: "dropdown-menu",
                                                staticStyle: {
                                                  position: "absolute",
                                                  transform:
                                                    "translate3d(4px, 23px, 0px)",
                                                  top: "0px",
                                                  left: "0px",
                                                  "will-change": "transform"
                                                },
                                                attrs: {
                                                  "x-placement": "bottom-start"
                                                }
                                              },
                                              [
                                                _c(
                                                  "router-link",
                                                  {
                                                    staticClass:
                                                      "dropdown-item",
                                                    attrs: {
                                                      to: {
                                                        name: "ads.edit",
                                                        params: {
                                                          id: _vm.ad.id
                                                        }
                                                      }
                                                    }
                                                  },
                                                  [
                                                    _c("i", {
                                                      staticClass: "fa fa-edit"
                                                    }),
                                                    _vm._v(
                                                      "edit\n                                                                "
                                                    )
                                                  ]
                                                )
                                              ],
                                              1
                                            )
                                          ]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          {
                                            staticClass:
                                              "d-flex justify-content-between act align-items-center"
                                          },
                                          [
                                            _c("div", {}, [
                                              _c("h2", [
                                                _vm._v(
                                                  "$" + _vm._s(_vm.ad.price)
                                                )
                                              ])
                                            ]),
                                            _vm._v(" "),
                                            _c("div", {}, [
                                              _c("p", [
                                                _vm._v(_vm._s(_vm.status))
                                              ])
                                            ])
                                          ]
                                        ),
                                        _vm._v(" "),
                                        _c("h3", [
                                          _vm._v(
                                            _vm._s(_vm.ad.model.year) +
                                              " " +
                                              _vm._s(_vm.ad.make.name)
                                          )
                                        ]),
                                        _vm._v(" "),
                                        _c("p", [_vm._v("Used/New")]),
                                        _vm._v(" "),
                                        _c("p", [
                                          _vm._v(
                                            "Listing id: " + _vm._s(_vm.ad.id)
                                          )
                                        ])
                                      ]
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  {
                                    staticClass:
                                      "col-xl-12 col-lg-6 col-12 d-flex "
                                  },
                                  [
                                    _c(
                                      "div",
                                      { staticClass: "box seller-info w-100" },
                                      [
                                        _c("h3", [_vm._v("Contact Seller")]),
                                        _vm._v(" "),
                                        _c("div", { staticClass: "media" }, [
                                          _c("img", {
                                            staticClass: "img-fluid",
                                            attrs: {
                                              src: _vm.ad.adable.image,
                                              alt: _vm.ad.adable.name
                                            }
                                          }),
                                          _vm._v(" "),
                                          _c(
                                            "div",
                                            { staticClass: "media-body" },
                                            [
                                              _c("h4", [
                                                _vm._v(
                                                  _vm._s(_vm.ad.adable.name)
                                                )
                                              ]),
                                              _vm._v(" "),
                                              _c("p", [
                                                _vm._v(
                                                  "Call: " +
                                                    _vm._s(
                                                      _vm.ad.adable.contact
                                                    )
                                                )
                                              ]),
                                              _vm._v(" "),
                                              _c("p", [
                                                _vm._v(
                                                  "Email: " +
                                                    _vm._s(_vm.ad.adable.email)
                                                )
                                              ]),
                                              _vm._v(" "),
                                              _c(
                                                "router-link",
                                                {
                                                  attrs: {
                                                    to: {
                                                      name:
                                                        _vm.ad.adable_type +
                                                        ".show",
                                                      params: {
                                                        id: _vm.ad.adable.id
                                                      }
                                                    }
                                                  }
                                                },
                                                [_vm._v("edit profile")]
                                              )
                                            ],
                                            1
                                          )
                                        ])
                                      ]
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  { staticClass: "col-xl-12 col-sm-6 col-12" },
                                  [
                                    _c("div", { staticClass: "status" }, [
                                      _vm.ad.status === 0
                                        ? _c(
                                            "button",
                                            {
                                              staticClass: "form-control",
                                              attrs: { type: "button" },
                                              on: {
                                                click: function($event) {
                                                  return _vm.approve(1)
                                                }
                                              }
                                            },
                                            [_vm._v("approved")]
                                          )
                                        : _vm._e(),
                                      _vm._v(" "),
                                      _vm.ad.status === 0
                                        ? _c(
                                            "button",
                                            {
                                              staticClass: "form-control",
                                              attrs: { type: "button" },
                                              on: {
                                                click: function($event) {
                                                  return _vm.approve(2)
                                                }
                                              }
                                            },
                                            [_vm._v("reject")]
                                          )
                                        : _vm._e()
                                    ])
                                  ]
                                ),
                                _vm._v(" "),
                                _vm._m(2)
                              ])
                            ])
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "detail" }, [
                            _c("div", { staticClass: "row" }, [
                              _c("div", { staticClass: "col-12 col-xl-8 " }, [
                                _c("h4", [_vm._v("Details")]),
                                _vm._v(" "),
                                _c("div", { staticClass: "row" }, [
                                  _c(
                                    "div",
                                    { staticClass: "col-sm-6 col-12" },
                                    [
                                      _c("div", { staticClass: "row" }, [
                                        _vm._m(3),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          { staticClass: "col-md-6 col-12" },
                                          [
                                            _c("p", [
                                              _vm._v(_vm._s(_vm.ad.make.name))
                                            ])
                                          ]
                                        )
                                      ]),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "row" }, [
                                        _vm._m(4),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          { staticClass: "col-md-6 col-12" },
                                          [
                                            _c("p", [
                                              _vm._v(_vm._s(_vm.ad.model.year))
                                            ])
                                          ]
                                        )
                                      ]),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "row" }, [
                                        _vm._m(5),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          { staticClass: "col-md-6 col-12" },
                                          [
                                            _c("p", [
                                              _vm._v(_vm._s(_vm.ad.fuel_type))
                                            ])
                                          ]
                                        )
                                      ])
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "div",
                                    { staticClass: "col-sm-6 col-12" },
                                    [
                                      _c("div", { staticClass: "row" }, [
                                        _vm._m(6),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          { staticClass: "col-md-6 col-12" },
                                          [
                                            _c("p", [
                                              _vm._v(_vm._s(_vm.ad.model.name))
                                            ])
                                          ]
                                        )
                                      ]),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "row" }, [
                                        _vm._m(7),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          { staticClass: "col-md-6 col-12" },
                                          [
                                            _c("p", [
                                              _vm._v(_vm._s(_vm.ad.mileage))
                                            ])
                                          ]
                                        )
                                      ]),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "row" }, [
                                        _vm._m(8),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          { staticClass: "col-md-6 col-12" },
                                          [
                                            _c("p", [
                                              _vm._v(
                                                _vm._s(_vm.ad.transmission)
                                              )
                                            ])
                                          ]
                                        )
                                      ])
                                    ]
                                  )
                                ])
                              ])
                            ])
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "row" }, [
                            _c(
                              "div",
                              { staticClass: "col-xl-5 col-md-6 col-12" },
                              [
                                _c("div", { staticClass: "des" }, [
                                  _c("h4", [_vm._v("Description ")]),
                                  _vm._v(" "),
                                  _c("p", [_vm._v(_vm._s(_vm.ad.description))])
                                ])
                              ]
                            )
                          ])
                        ]
                      )
                    ])
                  ])
                ])
              ])
            ])
          : _vm._e()
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-md-6 col-12" }, [
        _c("h1", { staticClass: "pull-left" }, [_vm._v("Ad Approval")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "button",
      {
        staticClass: "btn  btn-drop-table btn-sm",
        attrs: {
          type: "button",
          "data-toggle": "dropdown",
          "aria-haspopup": "true",
          "aria-expanded": "false"
        }
      },
      [_c("i", { staticClass: "fa fa-ellipsis-v" })]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-xl-12 col-sm-6 col-12" }, [
      _c("div", { staticClass: "reject-reason" }, [
        _c("h3", [_vm._v("Rejection Reasons:")]),
        _vm._v(" "),
        _c("p", [_vm._v("No rejection reason found. ")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-6 col-12" }, [
      _c("label", { attrs: { for: "" } }, [_vm._v("Make")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-6 col-12" }, [
      _c("label", { attrs: { for: "" } }, [_vm._v("Year")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-6 col-12" }, [
      _c("label", { attrs: { for: "" } }, [_vm._v("Fuel")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-6 col-12" }, [
      _c("label", { attrs: { for: "" } }, [_vm._v("model")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-6 col-12" }, [
      _c("label", { attrs: { for: "" } }, [_vm._v("Mileage")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-6 col-12" }, [
      _c("label", { attrs: { for: "" } }, [_vm._v("Transmission")])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/admin/js/components/dealers/MainSlider.vue?vue&type=template&id=b47ebecc&":
/*!***************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/admin/js/components/dealers/MainSlider.vue?vue&type=template&id=b47ebecc& ***!
  \***************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "main-slider" },
    [
      _c(
        "swiper",
        {
          ref: "swiperTop",
          staticClass: "slider-top",
          attrs: { options: _vm.swiperOptionTop }
        },
        _vm._l(_vm.images, function(img, index) {
          return _c("swiper-slide", { key: img.id, class: "slide-" + index }, [
            _c("img", {
              staticClass: "img-fluid w-100",
              attrs: { src: "" + img.file_name, alt: "" }
            })
          ])
        }),
        1
      ),
      _vm._v(" "),
      _c(
        "swiper",
        {
          ref: "swiperThumbs",
          staticClass: "gallery-thumbs",
          attrs: { options: _vm.swiperOptionThumbs }
        },
        _vm._l(_vm.images, function(img, index) {
          return _c("swiper-slide", { key: img.id, class: "slide-" + index }, [
            _c("img", {
              staticClass: "img-fluid w-100",
              attrs: { src: "" + img.file_name, alt: "" }
            })
          ])
        }),
        1
      ),
      _vm._v(" "),
      _c("div", {
        staticClass: "swiper-button-next swiper-button-white",
        attrs: { slot: "button-next" },
        slot: "button-next"
      }),
      _vm._v(" "),
      _c("div", {
        staticClass: "swiper-button-prev swiper-button-white",
        attrs: { slot: "button-prev" },
        slot: "button-prev"
      })
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/admin/js/components/ads/Edit.vue":
/*!****************************************************!*\
  !*** ./resources/admin/js/components/ads/Edit.vue ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Edit_vue_vue_type_template_id_228a6898___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Edit.vue?vue&type=template&id=228a6898& */ "./resources/admin/js/components/ads/Edit.vue?vue&type=template&id=228a6898&");
/* harmony import */ var _Edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Edit.vue?vue&type=script&lang=js& */ "./resources/admin/js/components/ads/Edit.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Edit_vue_vue_type_template_id_228a6898___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Edit_vue_vue_type_template_id_228a6898___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/admin/js/components/ads/Edit.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/admin/js/components/ads/Edit.vue?vue&type=script&lang=js&":
/*!*****************************************************************************!*\
  !*** ./resources/admin/js/components/ads/Edit.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Edit.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/admin/js/components/ads/Edit.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/admin/js/components/ads/Edit.vue?vue&type=template&id=228a6898&":
/*!***********************************************************************************!*\
  !*** ./resources/admin/js/components/ads/Edit.vue?vue&type=template&id=228a6898& ***!
  \***********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Edit_vue_vue_type_template_id_228a6898___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Edit.vue?vue&type=template&id=228a6898& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/admin/js/components/ads/Edit.vue?vue&type=template&id=228a6898&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Edit_vue_vue_type_template_id_228a6898___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Edit_vue_vue_type_template_id_228a6898___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/admin/js/components/dealers/MainSlider.vue":
/*!**************************************************************!*\
  !*** ./resources/admin/js/components/dealers/MainSlider.vue ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _MainSlider_vue_vue_type_template_id_b47ebecc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./MainSlider.vue?vue&type=template&id=b47ebecc& */ "./resources/admin/js/components/dealers/MainSlider.vue?vue&type=template&id=b47ebecc&");
/* harmony import */ var _MainSlider_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./MainSlider.vue?vue&type=script&lang=js& */ "./resources/admin/js/components/dealers/MainSlider.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _MainSlider_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _MainSlider_vue_vue_type_template_id_b47ebecc___WEBPACK_IMPORTED_MODULE_0__["render"],
  _MainSlider_vue_vue_type_template_id_b47ebecc___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/admin/js/components/dealers/MainSlider.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/admin/js/components/dealers/MainSlider.vue?vue&type=script&lang=js&":
/*!***************************************************************************************!*\
  !*** ./resources/admin/js/components/dealers/MainSlider.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_MainSlider_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./MainSlider.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/admin/js/components/dealers/MainSlider.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_MainSlider_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/admin/js/components/dealers/MainSlider.vue?vue&type=template&id=b47ebecc&":
/*!*********************************************************************************************!*\
  !*** ./resources/admin/js/components/dealers/MainSlider.vue?vue&type=template&id=b47ebecc& ***!
  \*********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MainSlider_vue_vue_type_template_id_b47ebecc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./MainSlider.vue?vue&type=template&id=b47ebecc& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/admin/js/components/dealers/MainSlider.vue?vue&type=template&id=b47ebecc&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MainSlider_vue_vue_type_template_id_b47ebecc___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MainSlider_vue_vue_type_template_id_b47ebecc___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);