(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["dashboard"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/admin/js/components/DashboardComponent.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/admin/js/components/DashboardComponent.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _BarChart__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./BarChart */ "./resources/admin/js/components/BarChart.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    BarChart: _BarChart__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  data: function data() {
    return {
      name: "",
      base_url: window.base_url,
      datacollection: null,
      selectedYear: new Date().getFullYear(),
      isLoading: true,
      data: {
        available_years: []
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        legend: {
          display: false,
          labels: {
            fontColor: 'rgb(16, 22, 99)'
          }
        }
      }
    };
  },
  mounted: function mounted() {
    var _this = this;

    axios.get('dashboard').then(function (data) {
      _this.data = data.data;
      _this.data.available_years = [];

      _this.fillData(_this.data.states);
    });
  },
  methods: {
    fillData: function fillData(datax) {
      var data = datax;
      var labels = data.map(function (month) {
        return month.name;
      });
      var values = data.map(function (month) {
        return month.value;
      });
      this.datacollection = {
        labels: labels,
        datasets: [{
          label: 'Sellers Registration',
          backgroundColor: '#101663',
          data: values
        }]
      };
    }
  },
  computed: {
    allYears: function allYears() {
      var year = new Date().getFullYear();
      var years = [];

      for (var i = year - 9; i <= year; i++) {
        years.push(i);
      }

      return years;
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/admin/js/components/DashboardComponent.vue?vue&type=style&index=0&lang=css&":
/*!******************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--8-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--8-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/admin/js/components/DashboardComponent.vue?vue&type=style&index=0&lang=css& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.small {\n    width: 100%;\n    height: 400px;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/admin/js/components/DashboardComponent.vue?vue&type=style&index=0&lang=css&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--8-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--8-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/admin/js/components/DashboardComponent.vue?vue&type=style&index=0&lang=css& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--8-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--8-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./DashboardComponent.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/admin/js/components/DashboardComponent.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/admin/js/components/DashboardComponent.vue?vue&type=template&id=78e0f698&":
/*!***************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/admin/js/components/DashboardComponent.vue?vue&type=template&id=78e0f698& ***!
  \***************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "app-content content view home" }, [
    _c("div", { staticClass: "content-wrapper" }, [
      _c("div", { staticClass: "content-body" }, [
        _vm.data
          ? _c(
              "section",
              {
                staticClass: "search view-cause",
                attrs: { id: "configuration" }
              },
              [
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-12" }, [
                    _c("div", { staticClass: "card rounded pad-20" }, [
                      _c("div", { staticClass: "card-content collapse show" }, [
                        _c(
                          "div",
                          {
                            staticClass:
                              "card-body table-responsive card-dashboard"
                          },
                          [
                            _vm._m(0),
                            _vm._v(" "),
                            _c("div", { staticClass: "row m-0" }, [
                              _c("div", { staticClass: "col-xl-8 col-12" }, [
                                _c("div", { staticClass: "row" }, [
                                  _c(
                                    "div",
                                    {
                                      staticClass:
                                        "col-xl-4 col-md-6 col-12 p-0 d-flex w-100 "
                                    },
                                    [
                                      _c(
                                        "div",
                                        {
                                          staticClass:
                                            "d-flex justify-content-between align-items-center box"
                                        },
                                        [
                                          _c("div", { staticClass: "left" }, [
                                            _c("h3", [_vm._v("Sellers")]),
                                            _vm._v(" "),
                                            _c("h2", [
                                              _vm._v(
                                                _vm._s(_vm.data.users_count)
                                              )
                                            ])
                                          ]),
                                          _vm._v(" "),
                                          _c("div", { staticClass: "right" }, [
                                            _c("img", {
                                              staticClass: "img-fluid",
                                              attrs: {
                                                src:
                                                  _vm.base_url +
                                                  "/admins/images/home-1.png",
                                                alt: ""
                                              }
                                            })
                                          ])
                                        ]
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "div",
                                    {
                                      staticClass:
                                        "col-xl-4 col-md-6 col-12 d-flex w-100  p-0"
                                    },
                                    [
                                      _c(
                                        "div",
                                        {
                                          staticClass:
                                            "d-flex justify-content-between align-items-center box"
                                        },
                                        [
                                          _c("div", { staticClass: "left" }, [
                                            _c("h3", [_vm._v("Dealer")]),
                                            _vm._v(" "),
                                            _c("h2", [
                                              _vm._v(
                                                _vm._s(_vm.data.dealers_count)
                                              )
                                            ])
                                          ]),
                                          _vm._v(" "),
                                          _c("div", { staticClass: "right" }, [
                                            _c("img", {
                                              staticClass: "img-fluid",
                                              attrs: {
                                                src:
                                                  _vm.base_url +
                                                  "/admins/images/home-2.png",
                                                alt: ""
                                              }
                                            })
                                          ])
                                        ]
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "div",
                                    {
                                      staticClass:
                                        "col-xl-4 col-md-6 col-12 d-flex w-100 p-0  "
                                    },
                                    [
                                      _c(
                                        "div",
                                        {
                                          staticClass:
                                            "d-flex justify-content-between align-items-center box"
                                        },
                                        [
                                          _c("div", { staticClass: "left" }, [
                                            _c("h3", [_vm._v("Earnings")]),
                                            _vm._v(" "),
                                            _c("h2", [
                                              _vm._v(_vm._s(_vm.data.earning))
                                            ])
                                          ]),
                                          _vm._v(" "),
                                          _c("div", { staticClass: "right" }, [
                                            _c("img", {
                                              staticClass: "img-fluid",
                                              attrs: {
                                                src:
                                                  _vm.base_url +
                                                  "/admins/images/home-3.png",
                                                alt: ""
                                              }
                                            })
                                          ])
                                        ]
                                      )
                                    ]
                                  )
                                ]),
                                _vm._v(" "),
                                _c("div", { staticClass: "card mt-md-3" }, [
                                  _c(
                                    "div",
                                    {
                                      staticClass:
                                        "row d-flex align-items-center"
                                    },
                                    [
                                      _c("div", { staticClass: "col-md-6" }, [
                                        _c(
                                          "select",
                                          {
                                            directives: [
                                              {
                                                name: "model",
                                                rawName: "v-model",
                                                value: _vm.selectedYear,
                                                expression: "selectedYear"
                                              }
                                            ],
                                            staticClass: "form-control",
                                            on: {
                                              change: function($event) {
                                                var $$selectedVal = Array.prototype.filter
                                                  .call(
                                                    $event.target.options,
                                                    function(o) {
                                                      return o.selected
                                                    }
                                                  )
                                                  .map(function(o) {
                                                    var val =
                                                      "_value" in o
                                                        ? o._value
                                                        : o.value
                                                    return val
                                                  })
                                                _vm.selectedYear = $event.target
                                                  .multiple
                                                  ? $$selectedVal
                                                  : $$selectedVal[0]
                                              }
                                            }
                                          },
                                          _vm._l(_vm.allYears, function(year) {
                                            return _c(
                                              "option",
                                              { domProps: { value: year } },
                                              [_vm._v(_vm._s(year))]
                                            )
                                          }),
                                          0
                                        )
                                      ])
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "div",
                                    {
                                      staticClass:
                                        "row d-flex align-items-center"
                                    },
                                    [
                                      _c("div", { staticClass: "col-md-12" }, [
                                        _c("h3", [_vm._v("Earnings")]),
                                        _vm._v(" "),
                                        _vm.datacollection
                                          ? _c(
                                              "div",
                                              { staticClass: "small" },
                                              [
                                                _c("bar-chart", {
                                                  attrs: {
                                                    "chart-data":
                                                      _vm.datacollection,
                                                    options: _vm.options
                                                  }
                                                })
                                              ],
                                              1
                                            )
                                          : _vm._e()
                                      ])
                                    ]
                                  )
                                ]),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  { staticClass: "home-bottom-boxes" },
                                  [
                                    _c("div", { staticClass: "row " }, [
                                      _c(
                                        "div",
                                        { staticClass: "col-md-6 col-12" },
                                        [
                                          _c(
                                            "div",
                                            {
                                              staticClass:
                                                "box d-flex justify-content-between"
                                            },
                                            [
                                              _c("div", {}, [
                                                _c("h4", [
                                                  _vm._v("Active Ads ")
                                                ]),
                                                _vm._v(" "),
                                                _c("h3", [
                                                  _vm._v(
                                                    _vm._s(_vm.data.active_ads)
                                                  )
                                                ])
                                              ]),
                                              _vm._v(" "),
                                              _c("div", {}, [
                                                _c("img", {
                                                  staticClass: "img-fluid",
                                                  attrs: {
                                                    src:
                                                      _vm.base_url +
                                                      "/admins/images/home-i.png",
                                                    alt: ""
                                                  }
                                                })
                                              ])
                                            ]
                                          )
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "div",
                                        { staticClass: "col-md-6 col-12" },
                                        [
                                          _c(
                                            "div",
                                            {
                                              staticClass:
                                                "box box-2 d-flex justify-content-between"
                                            },
                                            [
                                              _c("div", {}, [
                                                _c("h4", [
                                                  _vm._v("Featured Ads ")
                                                ]),
                                                _vm._v(" "),
                                                _c("h3", [
                                                  _vm._v(
                                                    _vm._s(
                                                      _vm.data
                                                        .active_featured_ads
                                                    )
                                                  )
                                                ])
                                              ]),
                                              _vm._v(" "),
                                              _c("div", {}, [
                                                _c("img", {
                                                  staticClass: "img-fluid",
                                                  attrs: {
                                                    src:
                                                      _vm.base_url +
                                                      "/admins/images/home-i.png",
                                                    alt: ""
                                                  }
                                                })
                                              ])
                                            ]
                                          )
                                        ]
                                      )
                                    ])
                                  ]
                                )
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "col-xl-4 col-12" }, [
                                _c("div", { staticClass: "activity-main" }, [
                                  _c("h3", [_vm._v("Activity ")]),
                                  _vm._v(" "),
                                  _c(
                                    "div",
                                    { staticClass: "activity" },
                                    _vm._l(_vm.data.recent_users, function(
                                      user
                                    ) {
                                      return _c("div", { staticClass: "box" }, [
                                        _c("div", { staticClass: "media" }, [
                                          _c("img", {
                                            staticClass: "img-fluid mr-1",
                                            attrs: {
                                              src: user.image,
                                              alt: user.name
                                            }
                                          }),
                                          _vm._v(" "),
                                          _c(
                                            "div",
                                            { staticClass: "media-body" },
                                            [
                                              _c("h3", [
                                                _vm._v(_vm._s(user.name))
                                              ]),
                                              _vm._v(" "),
                                              _c("p", [
                                                _vm._v(_vm._s(user.time_ago))
                                              ])
                                            ]
                                          )
                                        ])
                                      ])
                                    }),
                                    0
                                  )
                                ])
                              ])
                            ])
                          ]
                        )
                      ])
                    ])
                  ])
                ])
              ]
            )
          : _vm._e()
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-md-6 col-12" }, [
        _c("h1", { staticClass: "pull-left" }, [_vm._v("Dashboard")])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/admin/js/components/BarChart.js":
/*!***************************************************!*\
  !*** ./resources/admin/js/components/BarChart.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_chartjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-chartjs */ "./node_modules/vue-chartjs/es/index.js");

var reactiveProp = vue_chartjs__WEBPACK_IMPORTED_MODULE_0__["mixins"].reactiveProp;
/* harmony default export */ __webpack_exports__["default"] = ({
  "extends": vue_chartjs__WEBPACK_IMPORTED_MODULE_0__["Bar"],
  mixins: [reactiveProp],
  props: ['options'],
  mounted: function mounted() {
    // this.chartData is created in the mixin.
    // If you want to pass options please create a local options object
    this.renderChart(this.chartData, this.options);
  }
});

/***/ }),

/***/ "./resources/admin/js/components/DashboardComponent.vue":
/*!**************************************************************!*\
  !*** ./resources/admin/js/components/DashboardComponent.vue ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _DashboardComponent_vue_vue_type_template_id_78e0f698___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./DashboardComponent.vue?vue&type=template&id=78e0f698& */ "./resources/admin/js/components/DashboardComponent.vue?vue&type=template&id=78e0f698&");
/* harmony import */ var _DashboardComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./DashboardComponent.vue?vue&type=script&lang=js& */ "./resources/admin/js/components/DashboardComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _DashboardComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./DashboardComponent.vue?vue&type=style&index=0&lang=css& */ "./resources/admin/js/components/DashboardComponent.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _DashboardComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _DashboardComponent_vue_vue_type_template_id_78e0f698___WEBPACK_IMPORTED_MODULE_0__["render"],
  _DashboardComponent_vue_vue_type_template_id_78e0f698___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/admin/js/components/DashboardComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/admin/js/components/DashboardComponent.vue?vue&type=script&lang=js&":
/*!***************************************************************************************!*\
  !*** ./resources/admin/js/components/DashboardComponent.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_DashboardComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./DashboardComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/admin/js/components/DashboardComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_DashboardComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/admin/js/components/DashboardComponent.vue?vue&type=style&index=0&lang=css&":
/*!***********************************************************************************************!*\
  !*** ./resources/admin/js/components/DashboardComponent.vue?vue&type=style&index=0&lang=css& ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_8_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_vue_loader_lib_index_js_vue_loader_options_DashboardComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--8-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--8-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./DashboardComponent.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/admin/js/components/DashboardComponent.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_8_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_vue_loader_lib_index_js_vue_loader_options_DashboardComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_8_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_vue_loader_lib_index_js_vue_loader_options_DashboardComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_8_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_vue_loader_lib_index_js_vue_loader_options_DashboardComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_8_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_vue_loader_lib_index_js_vue_loader_options_DashboardComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_8_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_vue_loader_lib_index_js_vue_loader_options_DashboardComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/admin/js/components/DashboardComponent.vue?vue&type=template&id=78e0f698&":
/*!*********************************************************************************************!*\
  !*** ./resources/admin/js/components/DashboardComponent.vue?vue&type=template&id=78e0f698& ***!
  \*********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DashboardComponent_vue_vue_type_template_id_78e0f698___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./DashboardComponent.vue?vue&type=template&id=78e0f698& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/admin/js/components/DashboardComponent.vue?vue&type=template&id=78e0f698&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DashboardComponent_vue_vue_type_template_id_78e0f698___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DashboardComponent_vue_vue_type_template_id_78e0f698___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);