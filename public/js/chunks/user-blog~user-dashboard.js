(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["user-blog~user-dashboard"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/user/js/views/BlogComponent.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/user/js/views/BlogComponent.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {};
  },
  mounted: function mounted() {
    new WOW().init();
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/user/js/views/BlogComponent.vue?vue&type=template&id=038f41f0&":
/*!****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/user/js/views/BlogComponent.vue?vue&type=template&id=038f41f0& ***!
  \****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("section", { staticClass: "news" }, [
    _c("div", { staticClass: "container-fluid" }, [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-12 text-center" }, [
          _c("img", {
            directives: [{ name: "wow", rawName: "v-wow" }],
            staticClass: "img-fluid wow fadeInRight",
            attrs: {
              src: "images/car.png",
              "data-wow-delay": ".8s",
              "data-wow-duration": "1.5s",
              alt: "white car"
            }
          }),
          _vm._v(" "),
          _c(
            "h2",
            {
              directives: [{ name: "wow", rawName: "v-wow" }],
              staticClass: "wow fadeInUp",
              attrs: { "data-wow-delay": ".5s", "data-wow-duration": "1s" }
            },
            [_vm._v("Latest Auto bazaar News")]
          )
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "container" }, [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-lg-4 col-md-6 col-12 d-flex" }, [
          _c(
            "div",
            {
              directives: [{ name: "wow", rawName: "v-wow" }],
              staticClass: "box w-100 wow fadeIn",
              attrs: { "data-wow-delay": ".5s", "data-wow-duration": "1.2s" }
            },
            [
              _vm._m(0),
              _vm._v(" "),
              _c("img", {
                staticClass: "img-fluid",
                attrs: { src: _vm.$baseUrl + "/images/news-1.png" }
              }),
              _vm._v(" "),
              _c(
                "h3",
                {
                  directives: [{ name: "wow", rawName: "v-wow" }],
                  staticClass: "wow fadeInUp",
                  attrs: {
                    "data-wow-delay": ".9s",
                    "data-wow-duration": "1.5s"
                  }
                },
                [
                  _vm._v(
                    "MONOTONECTALLY BUILD DISTINCTIVE CONVERGENCE AND AN ATTEMPT"
                  )
                ]
              ),
              _vm._v(" "),
              _c(
                "p",
                {
                  directives: [{ name: "wow", rawName: "v-wow" }],
                  staticClass: "wow fadeInUp",
                  attrs: {
                    "data-wow-delay": "1.1s",
                    "data-wow-duration": "1.7s"
                  }
                },
                [
                  _vm._v(
                    "Sed non mauris vitae erat consequat vitae auctor eu in elit. Class aptent taciti non sociosqu ad..."
                  )
                ]
              ),
              _vm._v(" "),
              _c(
                "a",
                {
                  directives: [{ name: "wow", rawName: "v-wow" }],
                  staticClass: "wow fadeInUp",
                  attrs: {
                    href: "#",
                    "data-wow-delay": "1.6s",
                    "data-wow-duration": "2.3s"
                  }
                },
                [_vm._v("READ MORE")]
              )
            ]
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-lg-4 col-md-6 col-12 d-flex" }, [
          _c(
            "div",
            {
              directives: [{ name: "wow", rawName: "v-wow" }],
              staticClass: "box w-100 wow fadeIn",
              attrs: { "data-wow-delay": ".9s", "data-wow-duration": "1.4s" }
            },
            [
              _vm._m(1),
              _vm._v(" "),
              _c("img", {
                staticClass: "img-fluid",
                attrs: { src: _vm.$baseUrl + "/images/news-2.png" }
              }),
              _vm._v(" "),
              _c(
                "h3",
                {
                  directives: [{ name: "wow", rawName: "v-wow" }],
                  staticClass: "wow fadeInUp",
                  attrs: {
                    "data-wow-delay": ".9s",
                    "data-wow-duration": "1.5s"
                  }
                },
                [
                  _vm._v(
                    "MONOTONECTALLY BUILD DISTINCTIVE CONVERGENCE AND AN ATTEMPT"
                  )
                ]
              ),
              _vm._v(" "),
              _c(
                "p",
                {
                  directives: [{ name: "wow", rawName: "v-wow" }],
                  staticClass: "wow fadeInUp",
                  attrs: {
                    "data-wow-delay": "1.1s",
                    "data-wow-duration": "1.7s"
                  }
                },
                [
                  _vm._v(
                    "Sed non mauris vitae erat consequat vitae auctor eu in elit. Class aptent taciti non sociosqu ad..."
                  )
                ]
              ),
              _vm._v(" "),
              _c(
                "a",
                {
                  directives: [{ name: "wow", rawName: "v-wow" }],
                  staticClass: "wow fadeInUp",
                  attrs: {
                    href: "#",
                    "data-wow-delay": "1.6s",
                    "data-wow-duration": "2.3s"
                  }
                },
                [_vm._v("READ MORE")]
              )
            ]
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-lg-4 col-md-6 col-12 d-flex" }, [
          _c(
            "div",
            {
              directives: [{ name: "wow", rawName: "v-wow" }],
              staticClass: "box w-100 wow fadeIn",
              attrs: { "data-wow-delay": "1.2s", "data-wow-duration": "1.7s" }
            },
            [
              _vm._m(2),
              _vm._v(" "),
              _c("img", {
                staticClass: "img-fluid",
                attrs: { src: _vm.$baseUrl + "/images/news-3.png" }
              }),
              _vm._v(" "),
              _c(
                "h3",
                {
                  directives: [{ name: "wow", rawName: "v-wow" }],
                  staticClass: "wow fadeInUp",
                  attrs: {
                    "data-wow-delay": ".9s",
                    "data-wow-duration": "1.5s"
                  }
                },
                [
                  _vm._v(
                    "MONOTONECTALLY BUILD DISTINCTIVE CONVERGENCE AND AN ATTEMPT"
                  )
                ]
              ),
              _vm._v(" "),
              _c(
                "p",
                {
                  directives: [{ name: "wow", rawName: "v-wow" }],
                  staticClass: "wow fadeInUp",
                  attrs: {
                    "data-wow-delay": "1.1s",
                    "data-wow-duration": "1.7s"
                  }
                },
                [
                  _vm._v(
                    "Sed non mauris vitae erat consequat vitae auctor eu in elit. Class aptent taciti non sociosqu ad..."
                  )
                ]
              ),
              _vm._v(" "),
              _c(
                "a",
                {
                  directives: [{ name: "wow", rawName: "v-wow" }],
                  staticClass: "wow fadeInUp",
                  attrs: {
                    href: "#",
                    "data-wow-delay": "1.6s",
                    "data-wow-duration": "2.3s"
                  }
                },
                [_vm._v("READ MORE")]
              )
            ]
          )
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "badge" }, [_c("p", [_vm._v("26 april")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "badge" }, [_c("p", [_vm._v("26 april")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "badge" }, [_c("p", [_vm._v("26 april")])])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/user/js/views/BlogComponent.vue":
/*!***************************************************!*\
  !*** ./resources/user/js/views/BlogComponent.vue ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _BlogComponent_vue_vue_type_template_id_038f41f0___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./BlogComponent.vue?vue&type=template&id=038f41f0& */ "./resources/user/js/views/BlogComponent.vue?vue&type=template&id=038f41f0&");
/* harmony import */ var _BlogComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./BlogComponent.vue?vue&type=script&lang=js& */ "./resources/user/js/views/BlogComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _BlogComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _BlogComponent_vue_vue_type_template_id_038f41f0___WEBPACK_IMPORTED_MODULE_0__["render"],
  _BlogComponent_vue_vue_type_template_id_038f41f0___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/user/js/views/BlogComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/user/js/views/BlogComponent.vue?vue&type=script&lang=js&":
/*!****************************************************************************!*\
  !*** ./resources/user/js/views/BlogComponent.vue?vue&type=script&lang=js& ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_BlogComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./BlogComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/user/js/views/BlogComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_BlogComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/user/js/views/BlogComponent.vue?vue&type=template&id=038f41f0&":
/*!**********************************************************************************!*\
  !*** ./resources/user/js/views/BlogComponent.vue?vue&type=template&id=038f41f0& ***!
  \**********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BlogComponent_vue_vue_type_template_id_038f41f0___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./BlogComponent.vue?vue&type=template&id=038f41f0& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/user/js/views/BlogComponent.vue?vue&type=template&id=038f41f0&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BlogComponent_vue_vue_type_template_id_038f41f0___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BlogComponent_vue_vue_type_template_id_038f41f0___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);