(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["terms-and-conditions"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/user/js/views/TermsAndConditionsComponent.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/user/js/views/TermsAndConditionsComponent.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {};
  },
  mounted: function mounted() {},
  methods: {}
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/user/js/views/TermsAndConditionsComponent.vue?vue&type=template&id=aa27022c&":
/*!******************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/user/js/views/TermsAndConditionsComponent.vue?vue&type=template&id=aa27022c& ***!
  \******************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("section", { staticClass: "terms pad-100" }, [
      _c("div", { staticClass: "container" }, [
        _c("h1", [_vm._v("AutoBazaarUSA")]),
        _vm._v(" "),
        _c("h2", [_vm._v("Buying and Listing Vehicles")]),
        _vm._v(" "),
        _c("p", [
          _vm._v(
            "The AutoBazaarUSA Website is a vehicle listing and information service that brings together buyers and sellers. AutoBazaarUSA is not a party to any transaction between vehicle buyers and sellers that originates on or through the AutoBazaarUSA Website. Information about a listed vehicle is supplied by the sellers, not by AutoBazaarUSA the price of any sale remains subject to direct negotiation between the buyer and the seller."
          )
        ]),
        _vm._v(" "),
        _c("p", [
          _vm._v(
            "AutoBazaarUSA doesn't own, buy or sell vehicles listed on our website."
          )
        ]),
        _vm._v(" "),
        _c("p", [
          _vm._v(
            "AutoBazaarUSA is not a car dealer. Any listing information about a particular car comes directly from the seller that listed the car on are website, not us."
          )
        ]),
        _vm._v(" "),
        _c("p", [
          _vm._v(
            "We cannot verify the information that sellers supply or guarantee the vehicles they offer for sale.  When using the AutoBazaarUSA Website to find a buyer for your vehicle or a vehicle you like to purchase, we recommend using the same common sense as you would use in selling a vehicle through the newspaper advertising."
          )
        ]),
        _vm._v(" "),
        _c("h2", [_vm._v("Collection of personal information")]),
        _vm._v(" "),
        _c("p", [
          _vm._v(
            "We receive and store any information you knowingly provide to us when you create an account, publish content, make a purchase, fill any online forms on the Website. When required this information may include your email address, name, phone number, address, credit card information, or other Personal Information. You can choose not to provide us with certain information, but then you may not be able to take advantage of some of the Website's features. Users who are uncertain about what information is mandatory are welcome to contact us."
          )
        ]),
        _vm._v(" "),
        _c("h2", [_vm._v("Buying Vehicle")]),
        _vm._v(" "),
        _c("p", [
          _vm._v(
            "The prices listed by sellers on the AutoBazaarUSA Website exclude sales tax, license fees, emission testing, and dealers’ fees, any or all of which may be added to the listed price at the time of sale. Before purchasing a vehicle advertised on AutoBazaarUSA Website, you should verify with the seller all his information, also confirm the price. AutoBazaarUSA does not guarantee the conditions of any such vehicles."
          )
        ]),
        _vm._v(" "),
        _c("p", [
          _vm._v(
            'Our listings by dealers my include vehicles that have been "certified" as meeting certain standards established by the dealers in connection with their used preowned vehicle programs. The certification of any particular car is made by the dealer or manufactures. AutoBazaarUSA is not responsible for the information contained in a “certified” vehicle listing. Only the dealer that listing this vehicle for sale is responsible for that vehicle. '
          )
        ]),
        _vm._v(" "),
        _c("h2", [_vm._v("Listing Vehicle")]),
        _vm._v(" "),
        _c("p", [
          _vm._v(
            "The AutoBazaarUSA Website offers a deferent variety of packages."
          )
        ]),
        _vm._v(" "),
        _c("p", [
          _vm._v(
            "In order for you to list a vehicle for sale through our service for private sellers, you must agree to our Terms of Sale. The Terms of Sale require, among other things, that you be prepared to sell your vehicle at the price that you have listed this vehicle. You must have possession of the vehicle and have title to this vehicle. To list a vehicle for sale on the AutoBazaarUSA Website, sellers also are required to provide contact information. The information must identify the seller and the way to contact the seller. The buyers will directly communicate with the seller. \nYou may not use the AutoBazaarUSA Website to promote any other website, product, or service without our prior permission.\n"
          )
        ]),
        _vm._v(" "),
        _c("p", [
          _vm._v(
            "By using the AutoBazaarUSA Website to sell your vehicle as a private seller, you must be at least 18 years of age, and are not a motor vehicle dealer, or their representative.\nListings are not to be used to advertise more than one vehicle, regardless of a listing's duration. For this reason, you cannot edit your vehicle's year, make, model and VIN once you have purchased a listing. Listing fees are generally not refundable, so if you enter incorrect information in the year, make, model, or VIN , you will have to delete the listing and purchase a new one. By using the AutoBazaarUSA Website to sell your vehicle, you agree to pay the price of the package you select regardless of whether your vehicle sells or not, as we not guaranty the sale of your vehicle.\n"
          )
        ]),
        _vm._v(" "),
        _c("p", [
          _vm._v(
            "Responsibility for the listing information contained in each listing lies with each seller. You alone are responsible for the material you post in listing information and photos of your vehicle, and for the content of all your messages or emails transmitted by you through the AutoBazaarUSA Website."
          )
        ]),
        _vm._v(" "),
        _c("p", [
          _vm._v(
            "In efforts to combat Internet fraud, some listings may be screened before being posted. This may delay the publication of your listing. Though we cannot monitor every transaction that originates through a listing on the AutoBazaarUSA Website. When publishing anything to the AutoBazaarUSA Website  you agree that you will not post any copyrighted materials, materials that are knowingly false or defamatory, inaccurate, abusive, vulgar, hateful, racist, bigoted, sexist, threatening, inflammatory, obscene, profane, sexually-oriented, invasive of privacy, or will be in violation of any applicable law. This may result in the removal of any content you have posted, revocation of the account you have on the AutoBazaarUSA Website and ban you from creating new accounts."
          )
        ]),
        _vm._v(" "),
        _c("p", [
          _vm._v(
            "AutoBazaarUSA takes no responsibility and assumes no liability for any content posted to the AutoBazaarUSA Website by you or by any third parties."
          )
        ]),
        _vm._v(" "),
        _c("p", [
          _vm._v(
            "YOU AND AUTOBAZAARUSA AGREE THAT ALL CLAIMS BETWEEN US WILL BE RESOLVED BY ARBITRATION.\nUNDER NO CIRCUMSTANCES WILL AUTOBAZAARUSA OR ITS OWNER, AFFILIATES, AGENTS, IF ANY, BE LIABLE TO YOU OR ANYONE ELSE FOR ANY DAMAGES ARISING OUT OF YOUR USE OF THE AUTOBAZAARUSA WEBSITE. YOU AGREE THAT THE LIABILITY OF AUTOBAZAARUSA AND ITS OWNER, AFFILIATES, AGENTS, IF ANY, ARISING OUT OF ANY KIND OF LEGAL CLAIM ARISING OUT OF OR OTHERWISE RELATING TO THE AUTOBAZAARUSA WEBSITE WILL NOT EXCEED THE AMOUNT YOU PAID TO AUTOBAZARUSA. YOU AGREE TO INDEMNIFY AUTOBAZAARUSA AND ITS OWNERS, SHAREHOLDERS, SUBSIDIARIES, AFFILIATES, OFFICERS, EMPLOYEES, AND HOLD THEM HARMLESS FROM ANY AND ALL CLAIMS AND EXPENSES, INCLUDING ATTORNEY'S FEES, ARISING FROM OR RELATED IN ANY WAY TO YOUR USE OF THE AUTOBAZAARUSA WEBSITE OR ANY BREACH OF THIS AGREEMENT.\n"
          )
        ]),
        _vm._v(" "),
        _c("h2", [_vm._v("Member Registration")]),
        _vm._v(" "),
        _c("p", [
          _vm._v(
            "To obtain access to any services from the AutoBazaarUSA Website and like to manage your private seller listing on the AutoBazaarUSA Website, you will be required to register on the AutoBazaarUSA Website. You agree that the information you provide to register will be accurate and fully complete and that you will not register under the name of another person then yourself. You will be responsible for preserving the confidentiality of your password and for all actions of persons accessing the AutoBazaarUSA Website through any username and password that is assigned to you."
          )
        ]),
        _vm._v(" "),
        _c("p", [_vm._v("This document was last updated on March 25, 2020")])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/user/js/views/TermsAndConditionsComponent.vue":
/*!*****************************************************************!*\
  !*** ./resources/user/js/views/TermsAndConditionsComponent.vue ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _TermsAndConditionsComponent_vue_vue_type_template_id_aa27022c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./TermsAndConditionsComponent.vue?vue&type=template&id=aa27022c& */ "./resources/user/js/views/TermsAndConditionsComponent.vue?vue&type=template&id=aa27022c&");
/* harmony import */ var _TermsAndConditionsComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./TermsAndConditionsComponent.vue?vue&type=script&lang=js& */ "./resources/user/js/views/TermsAndConditionsComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _TermsAndConditionsComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _TermsAndConditionsComponent_vue_vue_type_template_id_aa27022c___WEBPACK_IMPORTED_MODULE_0__["render"],
  _TermsAndConditionsComponent_vue_vue_type_template_id_aa27022c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/user/js/views/TermsAndConditionsComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/user/js/views/TermsAndConditionsComponent.vue?vue&type=script&lang=js&":
/*!******************************************************************************************!*\
  !*** ./resources/user/js/views/TermsAndConditionsComponent.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_TermsAndConditionsComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./TermsAndConditionsComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/user/js/views/TermsAndConditionsComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_TermsAndConditionsComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/user/js/views/TermsAndConditionsComponent.vue?vue&type=template&id=aa27022c&":
/*!************************************************************************************************!*\
  !*** ./resources/user/js/views/TermsAndConditionsComponent.vue?vue&type=template&id=aa27022c& ***!
  \************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TermsAndConditionsComponent_vue_vue_type_template_id_aa27022c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./TermsAndConditionsComponent.vue?vue&type=template&id=aa27022c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/user/js/views/TermsAndConditionsComponent.vue?vue&type=template&id=aa27022c&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TermsAndConditionsComponent_vue_vue_type_template_id_aa27022c___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TermsAndConditionsComponent_vue_vue_type_template_id_aa27022c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);