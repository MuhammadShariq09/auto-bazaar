(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["dealer-edit-profile"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/user/js/views/DealerEditProfileComponent.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/user/js/views/DealerEditProfileComponent.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      dealer: window.user
    };
  },
  mounted: function mounted() {
    this.getDealer();
  },
  methods: {
    getDealer: function getDealer() {
      var _this = this;

      axios.get("/dealers/".concat(this.dealer.id)).then(function (_ref) {
        var data = _ref.data;
        _this.dealer = data;
        _this.dealer.deletedMedia = [];
        _this.dealer.filesBinary = _this.dealer.medias.map(function (file) {
          return file.file_name;
        });
      });
    },
    fileChanged: function fileChanged(e) {
      var _this2 = this;

      Array.from(e.target.files).forEach(function (file) {
        // console.log(URL.createObjectURL(file));
        var fileReader = new FileReader();
        var that = _this2;

        _this2.dealer.filesBinary.push(file); // when image is loaded, set the src of the image where you want to display it


        fileReader.onload = function (e) {
          that.dealer.medias.push({
            file_name: this.result,
            id: undefined
          });
        };

        fileReader.readAsDataURL(file);
      });
    },
    deleteFile: function deleteFile(index) {
      var _this3 = this;

      this.$dialog.confirm('are you sure you like to remove this file.').then(function (result) {
        if (!result) return;
        if (_this3.dealer.medias[index].id) _this3.dealer.deletedMedia.push(_this3.dealer.medias[index].id);

        _this3.dealer.medias.splice(index, 1);

        _this3.dealer.filesBinary.splice(index, 1);
      });
    },
    save: function save() {
      var _this4 = this;

      this.$validator.validateAll().then(function (result) {
        if (!result) return;
        var data = new FormData();

        for (var key in _this4.dealer) {
          if (key === 'filesBinary') _this4.dealer.filesBinary.forEach(function (file) {
            if (file instanceof File) data.append('files[]', file);
          });else data.append(key, _this4.dealer[key]);
        }

        axios.post("/dealers/".concat(_this4.dealer.id), data).then(function (data) {
          _this4.$toastr.success(data.data.message, 'Success'); // window.history.back();

        });
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/user/js/views/DealerEditProfileComponent.vue?vue&type=style&index=0&id=72678812&scoped=true&lang=css&":
/*!********************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--8-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--8-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/user/js/views/DealerEditProfileComponent.vue?vue&type=style&index=0&id=72678812&scoped=true&lang=css& ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.d-edit ul li[data-v-72678812] {\n    position: relative;\n}\n.d-edit ul li i.fa-times[data-v-72678812] {\n    position: absolute;\n    right: 5px;\n    top: 5px;\n    background: #00000073;\n    color: white;\n    width: 20px;\n    height: 20px;\n    line-height: 20px;\n    text-align: center;\n    border-radius: 10px;\n    cursor: pointer;\n    opacity: 0;\n    transition: opacity 0.2s ease-in-out;\n}\n.d-edit ul li:hover i.fa-times[data-v-72678812] {\n    opacity: 1;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/user/js/views/DealerEditProfileComponent.vue?vue&type=style&index=0&id=72678812&scoped=true&lang=css&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--8-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--8-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/user/js/views/DealerEditProfileComponent.vue?vue&type=style&index=0&id=72678812&scoped=true&lang=css& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--8-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--8-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./DealerEditProfileComponent.vue?vue&type=style&index=0&id=72678812&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/user/js/views/DealerEditProfileComponent.vue?vue&type=style&index=0&id=72678812&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/user/js/views/DealerEditProfileComponent.vue?vue&type=template&id=72678812&scoped=true&":
/*!*****************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/user/js/views/DealerEditProfileComponent.vue?vue&type=template&id=72678812&scoped=true& ***!
  \*****************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.dealer
    ? _c("section", { staticClass: "dealer-profile my-60 d-edit" }, [
        _c("div", { staticClass: "container" }, [
          _vm._m(0),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _c(
              "div",
              {
                staticClass: "col-12 d-xl-flex d-block justify-content-between"
              },
              [
                _c(
                  "router-link",
                  {
                    staticClass: "red",
                    attrs: {
                      to: { name: "user.profile", query: { featured: "yes" } }
                    }
                  },
                  [_vm._v("feature profile")]
                )
              ],
              1
            )
          ]),
          _vm._v(" "),
          _c(
            "form",
            {
              staticClass: "row",
              on: {
                submit: function($event) {
                  $event.preventDefault()
                  return _vm.save($event)
                }
              }
            },
            [
              _c("div", { staticClass: "col-lg-6 offset-lg-3 col-12" }, [
                _c("div", { staticClass: "dealer-box-main" }, [
                  _c("div", { staticClass: "top" }, [
                    _c("img", {
                      staticClass: "img-fluid",
                      attrs: { src: _vm.dealer.image, alt: "" }
                    }),
                    _vm._v(" "),
                    _c("h3", [_vm._v(_vm._s(_vm.dealer.name))])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "middle" }, [
                    _c("h3", [_vm._v("About ")]),
                    _vm._v(" "),
                    _c("textarea", {
                      directives: [
                        {
                          name: "validate",
                          rawName: "v-validate",
                          value: "required",
                          expression: "'required'"
                        },
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.dealer.about,
                          expression: "dealer.about"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: {
                        name: "about",
                        id: "",
                        placeholder: "",
                        rows: ""
                      },
                      domProps: { value: _vm.dealer.about },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.dealer, "about", $event.target.value)
                        }
                      }
                    }),
                    _vm._v(" "),
                    _vm.errors.has("about")
                      ? _c("p", { staticClass: "text-danger" }, [
                          _vm._v(_vm._s(_vm.errors.first("about")))
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _c("h3", [_vm._v("Upload Pictures")]),
                    _vm._v(" "),
                    _c(
                      "ul",
                      [
                        _vm._l(_vm.dealer.medias, function(file, index) {
                          return _vm.dealer.medias
                            ? _c("li", [
                                _c("i", {
                                  staticClass: "fa fa-times",
                                  on: {
                                    click: function($event) {
                                      return _vm.deleteFile(index)
                                    }
                                  }
                                }),
                                _vm._v(" "),
                                _c("img", {
                                  staticClass: "img-fluid",
                                  attrs: { src: file.file_name, alt: "" }
                                })
                              ])
                            : _vm._e()
                        }),
                        _vm._v(" "),
                        _c("li", [
                          _c("div", { staticClass: "upload-img" }, [
                            _vm._m(1),
                            _vm._v(" "),
                            _c("input", {
                              attrs: {
                                type: "file",
                                id: "upload",
                                name: "file",
                                multiple: ""
                              },
                              on: { change: _vm.fileChanged }
                            })
                          ])
                        ])
                      ],
                      2
                    )
                  ]),
                  _vm._v(" "),
                  _vm._m(2)
                ])
              ])
            ]
          )
        ])
      ])
    : _vm._e()
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-12" }, [
        _c("h2", [_vm._v("dealer overview")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "button",
      {
        staticClass: "upload-cover",
        attrs: {
          name: "file",
          type: "button",
          onclick: "document.getElementById('upload').click()"
        }
      },
      [_c("i", { staticClass: "fa fa-plus-circle" })]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "bottom text-center" }, [
      _c("button", { staticClass: "red", attrs: { type: "submit" } }, [
        _vm._v("save")
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/user/js/views/DealerEditProfileComponent.vue":
/*!****************************************************************!*\
  !*** ./resources/user/js/views/DealerEditProfileComponent.vue ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _DealerEditProfileComponent_vue_vue_type_template_id_72678812_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./DealerEditProfileComponent.vue?vue&type=template&id=72678812&scoped=true& */ "./resources/user/js/views/DealerEditProfileComponent.vue?vue&type=template&id=72678812&scoped=true&");
/* harmony import */ var _DealerEditProfileComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./DealerEditProfileComponent.vue?vue&type=script&lang=js& */ "./resources/user/js/views/DealerEditProfileComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _DealerEditProfileComponent_vue_vue_type_style_index_0_id_72678812_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./DealerEditProfileComponent.vue?vue&type=style&index=0&id=72678812&scoped=true&lang=css& */ "./resources/user/js/views/DealerEditProfileComponent.vue?vue&type=style&index=0&id=72678812&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _DealerEditProfileComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _DealerEditProfileComponent_vue_vue_type_template_id_72678812_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _DealerEditProfileComponent_vue_vue_type_template_id_72678812_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "72678812",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/user/js/views/DealerEditProfileComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/user/js/views/DealerEditProfileComponent.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************!*\
  !*** ./resources/user/js/views/DealerEditProfileComponent.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_DealerEditProfileComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./DealerEditProfileComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/user/js/views/DealerEditProfileComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_DealerEditProfileComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/user/js/views/DealerEditProfileComponent.vue?vue&type=style&index=0&id=72678812&scoped=true&lang=css&":
/*!*************************************************************************************************************************!*\
  !*** ./resources/user/js/views/DealerEditProfileComponent.vue?vue&type=style&index=0&id=72678812&scoped=true&lang=css& ***!
  \*************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_8_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_vue_loader_lib_index_js_vue_loader_options_DealerEditProfileComponent_vue_vue_type_style_index_0_id_72678812_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--8-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--8-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./DealerEditProfileComponent.vue?vue&type=style&index=0&id=72678812&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/user/js/views/DealerEditProfileComponent.vue?vue&type=style&index=0&id=72678812&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_8_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_vue_loader_lib_index_js_vue_loader_options_DealerEditProfileComponent_vue_vue_type_style_index_0_id_72678812_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_8_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_vue_loader_lib_index_js_vue_loader_options_DealerEditProfileComponent_vue_vue_type_style_index_0_id_72678812_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_8_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_vue_loader_lib_index_js_vue_loader_options_DealerEditProfileComponent_vue_vue_type_style_index_0_id_72678812_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_8_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_vue_loader_lib_index_js_vue_loader_options_DealerEditProfileComponent_vue_vue_type_style_index_0_id_72678812_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_8_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_vue_loader_lib_index_js_vue_loader_options_DealerEditProfileComponent_vue_vue_type_style_index_0_id_72678812_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/user/js/views/DealerEditProfileComponent.vue?vue&type=template&id=72678812&scoped=true&":
/*!***********************************************************************************************************!*\
  !*** ./resources/user/js/views/DealerEditProfileComponent.vue?vue&type=template&id=72678812&scoped=true& ***!
  \***********************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DealerEditProfileComponent_vue_vue_type_template_id_72678812_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./DealerEditProfileComponent.vue?vue&type=template&id=72678812&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/user/js/views/DealerEditProfileComponent.vue?vue&type=template&id=72678812&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DealerEditProfileComponent_vue_vue_type_template_id_72678812_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DealerEditProfileComponent_vue_vue_type_template_id_72678812_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);