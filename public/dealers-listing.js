(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["dealers-listing"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/admin/js/components/dealers/Index.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/admin/js/components/dealers/Index.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      base_url: window.base_url,
      blocked: false,
      dealers: [],
      table: undefined
    };
  },
  mounted: function mounted() {
    this.loadDealers();
  },
  methods: {
    loadDealers: function loadDealers() {
      var _this = this;

      if (this.table !== undefined) this.table.destroy();
      axios.get("/users?dealer=1&status=".concat(this.blocked ? 0 : 1)).then(function (data) {
        _this.dealers = data.data;
        setTimeout(function () {
          _this.table = $('.dealer-table').DataTable();
        }, 100);
      });
    },
    blockDealer: function blockDealer(id) {
      var _this2 = this;

      this.$dialog.confirm("Are you sure you want to ".concat(this.blocked ? 'Unblock' : 'block', " this dealer?")).then(function (dialog) {
        axios["delete"]("/dealers/".concat(id)).then(function (d) {
          _this2.loadDealers();

          _this2.$toastr.success(d.data.message, 'Success', {});

          dialog.close();
        })["catch"](function (d) {});
      });
    }
  },
  watch: {
    'blocked': function blocked() {
      this.loadDealers();
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/admin/js/components/dealers/Index.vue?vue&type=template&id=4140de62&":
/*!**********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/admin/js/components/dealers/Index.vue?vue&type=template&id=4140de62& ***!
  \**********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "app-content content view orders user-listing" },
    [
      _c("div", { staticClass: "content-wrapper" }, [
        _c("div", { staticClass: "content-body" }, [
          _c(
            "section",
            {
              staticClass: "search view-cause",
              attrs: { id: "configuration" }
            },
            [
              _c("div", { staticClass: "row" }, [
                _c("div", { staticClass: "col-12" }, [
                  _c("div", { staticClass: "card rounded pad-20" }, [
                    _c("div", { staticClass: "card-content collapse show" }, [
                      _c(
                        "div",
                        {
                          staticClass:
                            "card-body table-responsive card-dashboard"
                        },
                        [
                          _vm._m(0),
                          _vm._v(" "),
                          _c(
                            "ul",
                            {
                              staticClass:
                                "nav nav-tabs nav-underline no-hover-bg"
                            },
                            [
                              _c(
                                "li",
                                { staticClass: "nav-item" },
                                [
                                  _c(
                                    "router-link",
                                    {
                                      staticClass: "nav-link",
                                      attrs: { to: { name: "users.index" } }
                                    },
                                    [_vm._v("User")]
                                  )
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "li",
                                { staticClass: "nav-item" },
                                [
                                  _c(
                                    "router-link",
                                    {
                                      staticClass: "nav-link active",
                                      attrs: { to: { name: "dealers.index" } }
                                    },
                                    [_vm._v("Dealer")]
                                  )
                                ],
                                1
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c("div", { staticClass: "tab-content" }, [
                            _c(
                              "div",
                              {
                                staticClass: "tab-pane active",
                                attrs: {
                                  role: "tabpanel",
                                  id: "tab31",
                                  "aria-expanded": "true",
                                  "aria-labelledby": "base-tab31"
                                }
                              },
                              [
                                _c("div", { staticClass: "row" }, [
                                  _c(
                                    "div",
                                    {
                                      staticClass:
                                        "col-12 text-md-right text-center"
                                    },
                                    [
                                      _c(
                                        "a",
                                        {
                                          staticClass: "blocked",
                                          attrs: { href: "#" },
                                          on: {
                                            click: function($event) {
                                              _vm.blocked = !_vm.blocked
                                            }
                                          }
                                        },
                                        [
                                          _c("i", { staticClass: "fa fa-ban" }),
                                          _vm._v(
                                            " " +
                                              _vm._s(
                                                !_vm.blocked
                                                  ? "Blocked dealer"
                                                  : "Active dealer"
                                              )
                                          )
                                        ]
                                      )
                                    ]
                                  )
                                ]),
                                _vm._v(" "),
                                _c("div", { staticClass: "clearfix" }),
                                _vm._v(" "),
                                _c("div", { staticClass: "maain-tabble" }, [
                                  _c(
                                    "table",
                                    {
                                      staticClass:
                                        "table table-striped table-bordered dealer-table"
                                    },
                                    [
                                      _vm._m(1),
                                      _vm._v(" "),
                                      _c(
                                        "tbody",
                                        _vm._l(_vm.dealers, function(
                                          dealer,
                                          index
                                        ) {
                                          return _c("tr", [
                                            _c("td", [
                                              _vm._v(_vm._s(index + 1))
                                            ]),
                                            _vm._v(" "),
                                            _c("td", [
                                              _vm._v(
                                                "\n                                                                " +
                                                  _vm._s(dealer.name) +
                                                  "\n                                                            "
                                              )
                                            ]),
                                            _vm._v(" "),
                                            _c("td", [
                                              _vm._v(_vm._s(dealer.email))
                                            ]),
                                            _vm._v(" "),
                                            _c("td", [
                                              _vm._v(_vm._s(dealer.contact))
                                            ]),
                                            _vm._v(" "),
                                            _c("td", [
                                              _vm._v(_vm._s(dealer.ads_count))
                                            ]),
                                            _vm._v(" "),
                                            _c("td", [
                                              _vm._v(
                                                _vm._s(
                                                  dealer.profile_package
                                                    ? dealer.profile_package
                                                        .title
                                                    : ""
                                                )
                                              )
                                            ]),
                                            _vm._v(" "),
                                            _c("td", [
                                              _vm._v(
                                                _vm._s(
                                                  dealer.profile_package
                                                    ? "yes"
                                                    : ""
                                                )
                                              )
                                            ]),
                                            _vm._v(" "),
                                            _c("td", [
                                              _vm._v(
                                                _vm._s(
                                                  _vm._f("date")(
                                                    dealer.created_at
                                                  )
                                                )
                                              )
                                            ]),
                                            _vm._v(" "),
                                            _c("td", [
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "btn-group mr-1 mb-1"
                                                },
                                                [
                                                  _vm._m(2, true),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    {
                                                      staticClass:
                                                        "dropdown-menu",
                                                      staticStyle: {
                                                        position: "absolute",
                                                        transform:
                                                          "translate3d(0px, 21px, 0px)",
                                                        top: "0px",
                                                        left: "0px",
                                                        "will-change":
                                                          "transform"
                                                      },
                                                      attrs: {
                                                        "x-placement":
                                                          "bottom-start"
                                                      }
                                                    },
                                                    [
                                                      _c(
                                                        "router-link",
                                                        {
                                                          staticClass:
                                                            "dropdown-item",
                                                          attrs: {
                                                            to: {
                                                              name:
                                                                "dealers.show",
                                                              params: {
                                                                id: dealer.id
                                                              }
                                                            }
                                                          }
                                                        },
                                                        [
                                                          _c("i", {
                                                            staticClass:
                                                              "fa fa-eye"
                                                          }),
                                                          _vm._v("View ")
                                                        ]
                                                      ),
                                                      _vm._v(" "),
                                                      _c(
                                                        "a",
                                                        {
                                                          staticClass:
                                                            "dropdown-item",
                                                          on: {
                                                            click: function(
                                                              $event
                                                            ) {
                                                              return _vm.blockDealer(
                                                                dealer.id
                                                              )
                                                            }
                                                          }
                                                        },
                                                        [
                                                          _c("i", {
                                                            staticClass:
                                                              "fa fa-ban"
                                                          }),
                                                          _vm._v(
                                                            _vm._s(
                                                              _vm.blocked
                                                                ? "UnBlock"
                                                                : "Block"
                                                            ) + " "
                                                          )
                                                        ]
                                                      )
                                                    ],
                                                    1
                                                  )
                                                ]
                                              )
                                            ])
                                          ])
                                        }),
                                        0
                                      )
                                    ]
                                  )
                                ])
                              ]
                            )
                          ])
                        ]
                      )
                    ])
                  ])
                ])
              ])
            ]
          )
        ])
      ])
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-md-6 col-12" }, [
        _c("h1", { staticClass: "pull-left" }, [_vm._v("Dealers Listing ")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("S.No")]),
        _vm._v(" "),
        _c("th", [_vm._v("Name")]),
        _vm._v(" "),
        _c("th", [_vm._v("email")]),
        _vm._v(" "),
        _c("th", [_vm._v("contact")]),
        _vm._v(" "),
        _c("th", [_vm._v("vehicle List")]),
        _vm._v(" "),
        _c("th", [_vm._v("package")]),
        _vm._v(" "),
        _c("th", [_vm._v("featured")]),
        _vm._v(" "),
        _c("th", [_vm._v("Registered On")]),
        _vm._v(" "),
        _c("th", [_vm._v("action")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "button",
      {
        staticClass: "btn  btn-drop-table btn-sm",
        attrs: {
          type: "button",
          "data-toggle": "dropdown",
          "aria-haspopup": "true",
          "aria-expanded": "false"
        }
      },
      [_c("i", { staticClass: "fa fa-ellipsis-v" })]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/admin/js/components/dealers/Index.vue":
/*!*********************************************************!*\
  !*** ./resources/admin/js/components/dealers/Index.vue ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Index_vue_vue_type_template_id_4140de62___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Index.vue?vue&type=template&id=4140de62& */ "./resources/admin/js/components/dealers/Index.vue?vue&type=template&id=4140de62&");
/* harmony import */ var _Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Index.vue?vue&type=script&lang=js& */ "./resources/admin/js/components/dealers/Index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Index_vue_vue_type_template_id_4140de62___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Index_vue_vue_type_template_id_4140de62___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/admin/js/components/dealers/Index.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/admin/js/components/dealers/Index.vue?vue&type=script&lang=js&":
/*!**********************************************************************************!*\
  !*** ./resources/admin/js/components/dealers/Index.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Index.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/admin/js/components/dealers/Index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/admin/js/components/dealers/Index.vue?vue&type=template&id=4140de62&":
/*!****************************************************************************************!*\
  !*** ./resources/admin/js/components/dealers/Index.vue?vue&type=template&id=4140de62& ***!
  \****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_template_id_4140de62___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Index.vue?vue&type=template&id=4140de62& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/admin/js/components/dealers/Index.vue?vue&type=template&id=4140de62&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_template_id_4140de62___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_template_id_4140de62___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);