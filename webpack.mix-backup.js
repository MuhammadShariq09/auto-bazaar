const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


// mix.webpackConfig({
//     output: {
//         // chunkFilename: process.env.NODE_ENV === 'production' ? `${process.env.MIX_PRODUCTION_VUE_ROUTER_BASE_URL}/js/admin-chunks/[name].js`: `js/admin-chunks/[name].js`,
//         chunkFilename: `js/chunks/[name].js`,
//     }
// });

// if(process.env.NODE_ENV === "userProduction" || process.env.NODE_ENV === "adminProduction")
// {
//     mix.webpackConfig({
//         output: {
//             publicPath: `/${process.env.MIX_PRODUCTION_VUE_ROUTER_BASE_URL}/`,
//             chunkFilename: `js/chunks/[name].js`,
//         }
//     });
// }


mix
    .sass('resources/admin/sass/app.scss', 'public/admins/css/app.css')
    .sass('resources/dealer/sass/app.scss', 'public/dealer/css')
    .sass('resources/user/sass/app.scss', 'public/user/css')
    .options({
        processCssUrls: false
    });

if(process.env.NODE_ENV === "userDevelopment" || process.env.NODE_ENV === "userProduction")
{
    mix
        // .js('resources/admin/js/app.js', 'public/admins/js/app.js')
        .js('resources/user/js/app.js', 'public/js/frontend.js')
}

if(process.env.NODE_ENV === "adminDevelopment" || process.env.NODE_ENV === "adminProduction")
{
    mix
        .js('resources/admin/js/app.js', 'public/admins/js/app.js')
        // .js('resources/user/js/app.js', 'public/js/frontend.js')
}



/*mix
    .sass('resources/user/sass/app.scss', 'public/css/frontend.css')
    .options({
        processCssUrls: false
    });*/

// mix.js('resources/admin/js/app.js', 'public/admins/js')
//     .sass([
//         'resources/admin/sass/app.scss',
//         'resources/dealer/sass/app.scss'
//     ], [
//         'public/admins/css',
//         //'public/dealer/css'
//     ]).options({
//     processCssUrls: false
// });

// mix.js('resources/dealer/js/app.js', 'public/dealer/js')
//     .sass('resources/dealer/sass/app.scss', 'public/dealer/css');

// mix.js('resources/user/js/app.js', 'public/users/js')
//     .sass('resources/user/sass/app.scss', 'public/users/css').options({
//     processCssUrls: false
// });
